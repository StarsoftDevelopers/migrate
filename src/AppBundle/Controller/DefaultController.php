<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="index")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        if ($username = $request->get('hotdigital_old_usermane')) {
            $this->get('session')->set('hotdigital_old_usermane_db', $username);
            $this->get('migrate_data')->changeDb(trim($username));
        }

        return $this->render('default/index.html.twig', ['username' => trim($username)]);
    }

    /**
     * @Route("/csv/product", name="csv_product")
     * @return Response
     */
    public function csvProductAction()
    {
        if ($username = $this->get('session')->get('hotdigital_old_usermane_db')) {
            $this->get('migrate_data')->changeDb(trim($username));

            return $this->get('migrate_data')->getProductsCsv($username);
        }

        return $this->render('default/index.html.twig', ['error' => 'You must insert username']);
    }

    /**
     * @Route("/csv/categories", name="csv_categories")
     * @return Response
     */
    public function csvCategoriesAction()
    {
        if ($username = $this->get('session')->get('hotdigital_old_usermane_db')) {
            $this->get('migrate_data')->changeDb(trim($username));

            return $this->get('migrate_data')->getCategoriesCsv($username);
        }

        return $this->render('default/index.html.twig', ['error' => 'You must insert username']);
    }

    /**
     * @Route("csv/manufacturer", name="csv_manufacturers")
     * @return Response
     */
    public function csvManufacturersAction()
    {
        if ($username = $this->get('session')->get('hotdigital_old_usermane_db')) {
            $this->get('migrate_data')->changeDb(trim($username));

            return $this->get('migrate_data')->getManufacturersCsv($username);
        }

        return $this->render('default/index.html.twig', ['error' => 'You must insert username']);
    }

    /**
     * @Route("/csv/combinations", name="csv_combinations")
     * @return Response
     */
    public function csvCombinationsAction()
    {
        if ($username = $this->get('session')->get('hotdigital_old_usermane_db')) {
            $this->get('migrate_data')->changeDb(trim($username));

            return $this->get('migrate_data')->getCombinationCsv($username);
        }

        return $this->render('default/index.html.twig', ['error' => 'You must insert username']);
    }

    /**
     * @Route("/csv/customers", name="csv_customers")
     * @return Response
     */
    public function csvCustomersAction()
    {
        if ($username = $this->get('session')->get('hotdigital_old_usermane_db')) {
            $this->get('migrate_data')->changeDb(trim($username));

            return $this->get('migrate_data')->getCustomersCsv($username);
        }

        return $this->render('default/index.html.twig', ['error' => 'You must insert username']);
    }

  /**
     * @Route("/csv/address", name="csv_address")
     * @return Response
     */
    public function csvAddressAction()
    {
        if ($username = $this->get('session')->get('hotdigital_old_usermane_db')) {
            $this->get('migrate_data')->changeDb(trim($username));

            return $this->get('migrate_data')->getAddressCsv($username);
        }

        return $this->render('default/index.html.twig', ['error' => 'You must insert username']);
    }

    /**
     * @Route("/sync", name="symc")
     * @return Response
     */
    public function synchronizeProductsData()
    {
        if ($username = $this->get('session')->get('hotdigital_old_usermane_db')) {
            $this->get('migrate_data')->changeDb(trim($username));

            return $this->get('migrate_data')->synchronizeProductsData($username);
        }

        return $this->render('default/index.html.twig', ['error' => 'You must insert username']);
    }

    /**
     * @Route("/list/product", name="csv")
     * @return Response
     */
    public function productListAction()
    {
        return $this->get('migrate_data')->getProductsList();

    }
}
