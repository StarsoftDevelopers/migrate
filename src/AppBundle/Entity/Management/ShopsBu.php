<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShopsBu
 *
 * @ORM\Table(name="shops_bu")
 * @ORM\Entity
 */
class ShopsBu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $shopId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="shop_name", type="string", length=64, nullable=false)
     */
    private $shopName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_server", type="string", length=64, nullable=false)
     */
    private $dbServer = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_user", type="string", length=64, nullable=false)
     */
    private $dbUser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_passwd", type="string", length=64, nullable=false)
     */
    private $dbPasswd = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_name", type="string", length=64, nullable=false)
     */
    private $dbName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="confixx_login", type="string", length=8, nullable=false)
     */
    private $confixxLogin = '';

    /**
     * @var string
     *
     * @ORM\Column(name="confixx_pw", type="string", length=16, nullable=false)
     */
    private $confixxPw = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="reseller_id", type="integer", nullable=false)
     */
    private $resellerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="partner_id1", type="integer", nullable=false)
     */
    private $partnerId1 = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="partner_id2", type="integer", nullable=false)
     */
    private $partnerId2 = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ssl", type="boolean", nullable=false)
     */
    private $ssl = '0';



    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ShopsBu
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set shopName
     *
     * @param string $shopName
     *
     * @return ShopsBu
     */
    public function setShopName($shopName)
    {
        $this->shopName = $shopName;

        return $this;
    }

    /**
     * Get shopName
     *
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * Set dbServer
     *
     * @param string $dbServer
     *
     * @return ShopsBu
     */
    public function setDbServer($dbServer)
    {
        $this->dbServer = $dbServer;

        return $this;
    }

    /**
     * Get dbServer
     *
     * @return string
     */
    public function getDbServer()
    {
        return $this->dbServer;
    }

    /**
     * Set dbUser
     *
     * @param string $dbUser
     *
     * @return ShopsBu
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;

        return $this;
    }

    /**
     * Get dbUser
     *
     * @return string
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * Set dbPasswd
     *
     * @param string $dbPasswd
     *
     * @return ShopsBu
     */
    public function setDbPasswd($dbPasswd)
    {
        $this->dbPasswd = $dbPasswd;

        return $this;
    }

    /**
     * Get dbPasswd
     *
     * @return string
     */
    public function getDbPasswd()
    {
        return $this->dbPasswd;
    }

    /**
     * Set dbName
     *
     * @param string $dbName
     *
     * @return ShopsBu
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;

        return $this;
    }

    /**
     * Get dbName
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * Set confixxLogin
     *
     * @param string $confixxLogin
     *
     * @return ShopsBu
     */
    public function setConfixxLogin($confixxLogin)
    {
        $this->confixxLogin = $confixxLogin;

        return $this;
    }

    /**
     * Get confixxLogin
     *
     * @return string
     */
    public function getConfixxLogin()
    {
        return $this->confixxLogin;
    }

    /**
     * Set confixxPw
     *
     * @param string $confixxPw
     *
     * @return ShopsBu
     */
    public function setConfixxPw($confixxPw)
    {
        $this->confixxPw = $confixxPw;

        return $this;
    }

    /**
     * Get confixxPw
     *
     * @return string
     */
    public function getConfixxPw()
    {
        return $this->confixxPw;
    }

    /**
     * Set resellerId
     *
     * @param integer $resellerId
     *
     * @return ShopsBu
     */
    public function setResellerId($resellerId)
    {
        $this->resellerId = $resellerId;

        return $this;
    }

    /**
     * Get resellerId
     *
     * @return integer
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * Set partnerId1
     *
     * @param integer $partnerId1
     *
     * @return ShopsBu
     */
    public function setPartnerId1($partnerId1)
    {
        $this->partnerId1 = $partnerId1;

        return $this;
    }

    /**
     * Get partnerId1
     *
     * @return integer
     */
    public function getPartnerId1()
    {
        return $this->partnerId1;
    }

    /**
     * Set partnerId2
     *
     * @param integer $partnerId2
     *
     * @return ShopsBu
     */
    public function setPartnerId2($partnerId2)
    {
        $this->partnerId2 = $partnerId2;

        return $this;
    }

    /**
     * Get partnerId2
     *
     * @return integer
     */
    public function getPartnerId2()
    {
        return $this->partnerId2;
    }

    /**
     * Set ssl
     *
     * @param boolean $ssl
     *
     * @return ShopsBu
     */
    public function setSsl($ssl)
    {
        $this->ssl = $ssl;

        return $this;
    }

    /**
     * Get ssl
     *
     * @return boolean
     */
    public function getSsl()
    {
        return $this->ssl;
    }
}
