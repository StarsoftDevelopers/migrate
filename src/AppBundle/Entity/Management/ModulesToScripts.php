<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModulesToScripts
 *
 * @ORM\Table(name="modules_to_scripts")
 * @ORM\Entity
 */
class ModulesToScripts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ModuleID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $moduleid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ScriptName", type="string", length=64, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $scriptname = '';



    /**
     * Set moduleid
     *
     * @param integer $moduleid
     *
     * @return ModulesToScripts
     */
    public function setModuleid($moduleid)
    {
        $this->moduleid = $moduleid;

        return $this;
    }

    /**
     * Get moduleid
     *
     * @return integer
     */
    public function getModuleid()
    {
        return $this->moduleid;
    }

    /**
     * Set scriptname
     *
     * @param string $scriptname
     *
     * @return ModulesToScripts
     */
    public function setScriptname($scriptname)
    {
        $this->scriptname = $scriptname;

        return $this;
    }

    /**
     * Get scriptname
     *
     * @return string
     */
    public function getScriptname()
    {
        return $this->scriptname;
    }
}
