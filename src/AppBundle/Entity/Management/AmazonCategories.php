<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmazonCategories
 *
 * @ORM\Table(name="amazon_categories", indexes={@ORM\Index(name="Name", columns={"Name"})})
 * @ORM\Entity
 */
class AmazonCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="Parent", type="bigint", nullable=true)
     */
    private $parent;

    /**
     * @var integer
     *
     * @ORM\Column(name="KatID", type="integer", nullable=true)
     */
    private $katid;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AmazonCategories
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     *
     * @return AmazonCategories
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set katid
     *
     * @param integer $katid
     *
     * @return AmazonCategories
     */
    public function setKatid($katid)
    {
        $this->katid = $katid;

        return $this;
    }

    /**
     * Get katid
     *
     * @return integer
     */
    public function getKatid()
    {
        return $this->katid;
    }
}
