<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModulesToKeys
 *
 * @ORM\Table(name="modules_to_keys")
 * @ORM\Entity
 */
class ModulesToKeys
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ModuleID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $moduleid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="KeyName", type="string", length=64, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $keyname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="KeyValue", type="text", length=65535, nullable=false)
     */
    private $keyvalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="KeyGroupId", type="integer", nullable=false)
     */
    private $keygroupid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="KeySetFunction", type="string", length=255, nullable=false)
     */
    private $keysetfunction = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="KeyVisible", type="boolean", nullable=false)
     */
    private $keyvisible = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="KeySortOrder", type="integer", nullable=false)
     */
    private $keysortorder = '0';



    /**
     * Set moduleid
     *
     * @param integer $moduleid
     *
     * @return ModulesToKeys
     */
    public function setModuleid($moduleid)
    {
        $this->moduleid = $moduleid;

        return $this;
    }

    /**
     * Get moduleid
     *
     * @return integer
     */
    public function getModuleid()
    {
        return $this->moduleid;
    }

    /**
     * Set keyname
     *
     * @param string $keyname
     *
     * @return ModulesToKeys
     */
    public function setKeyname($keyname)
    {
        $this->keyname = $keyname;

        return $this;
    }

    /**
     * Get keyname
     *
     * @return string
     */
    public function getKeyname()
    {
        return $this->keyname;
    }

    /**
     * Set keyvalue
     *
     * @param string $keyvalue
     *
     * @return ModulesToKeys
     */
    public function setKeyvalue($keyvalue)
    {
        $this->keyvalue = $keyvalue;

        return $this;
    }

    /**
     * Get keyvalue
     *
     * @return string
     */
    public function getKeyvalue()
    {
        return $this->keyvalue;
    }

    /**
     * Set keygroupid
     *
     * @param integer $keygroupid
     *
     * @return ModulesToKeys
     */
    public function setKeygroupid($keygroupid)
    {
        $this->keygroupid = $keygroupid;

        return $this;
    }

    /**
     * Get keygroupid
     *
     * @return integer
     */
    public function getKeygroupid()
    {
        return $this->keygroupid;
    }

    /**
     * Set keysetfunction
     *
     * @param string $keysetfunction
     *
     * @return ModulesToKeys
     */
    public function setKeysetfunction($keysetfunction)
    {
        $this->keysetfunction = $keysetfunction;

        return $this;
    }

    /**
     * Get keysetfunction
     *
     * @return string
     */
    public function getKeysetfunction()
    {
        return $this->keysetfunction;
    }

    /**
     * Set keyvisible
     *
     * @param boolean $keyvisible
     *
     * @return ModulesToKeys
     */
    public function setKeyvisible($keyvisible)
    {
        $this->keyvisible = $keyvisible;

        return $this;
    }

    /**
     * Get keyvisible
     *
     * @return boolean
     */
    public function getKeyvisible()
    {
        return $this->keyvisible;
    }

    /**
     * Set keysortorder
     *
     * @param integer $keysortorder
     *
     * @return ModulesToKeys
     */
    public function setKeysortorder($keysortorder)
    {
        $this->keysortorder = $keysortorder;

        return $this;
    }

    /**
     * Get keysortorder
     *
     * @return integer
     */
    public function getKeysortorder()
    {
        return $this->keysortorder;
    }
}
