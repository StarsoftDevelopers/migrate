<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * FlashbannerTemplates
 *
 * @ORM\Table(name="flashbanner_templates")
 * @ORM\Entity
 */
class FlashbannerTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="template_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $templateId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="src_file", type="string", length=250, nullable=false)
     */
    private $srcFile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="example", type="string", length=250, nullable=false)
     */
    private $example = '';

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=10, nullable=false)
     */
    private $size = '';

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=false)
     */
    private $params;



    /**
     * Get templateId
     *
     * @return integer
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FlashbannerTemplates
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return FlashbannerTemplates
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set srcFile
     *
     * @param string $srcFile
     *
     * @return FlashbannerTemplates
     */
    public function setSrcFile($srcFile)
    {
        $this->srcFile = $srcFile;

        return $this;
    }

    /**
     * Get srcFile
     *
     * @return string
     */
    public function getSrcFile()
    {
        return $this->srcFile;
    }

    /**
     * Set example
     *
     * @param string $example
     *
     * @return FlashbannerTemplates
     */
    public function setExample($example)
    {
        $this->example = $example;

        return $this;
    }

    /**
     * Get example
     *
     * @return string
     */
    public function getExample()
    {
        return $this->example;
    }

    /**
     * Set size
     *
     * @param string $size
     *
     * @return FlashbannerTemplates
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set params
     *
     * @param string $params
     *
     * @return FlashbannerTemplates
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }
}
