<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Templates
 *
 * @ORM\Table(name="templates")
 * @ORM\Entity
 */
class Templates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="template_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $templateId;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=64, nullable=false)
     */
    private $templateName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="template_path", type="string", length=32, nullable=false)
     */
    private $templatePath = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="logo_width", type="integer", nullable=false)
     */
    private $logoWidth = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="logo_height", type="integer", nullable=false)
     */
    private $logoHeight = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="partner_id", type="integer", nullable=true)
     */
    private $partnerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="reseller_id", type="integer", nullable=true)
     */
    private $resellerId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="header_bgcolor", type="boolean", nullable=false)
     */
    private $headerBgcolor = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="logo_position", type="boolean", nullable=false)
     */
    private $logoPosition = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_server", type="string", length=64, nullable=false)
     */
    private $ftpServer = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_path", type="string", length=64, nullable=false)
     */
    private $ftpPath = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_user", type="string", length=50, nullable=false)
     */
    private $ftpUser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_pass", type="string", length=100, nullable=false)
     */
    private $ftpPass = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="edit_mode", type="boolean", nullable=false)
     */
    private $editMode = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ftp_mode", type="boolean", nullable=false)
     */
    private $ftpMode = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="bgcolor", type="string", length=32, nullable=false)
     */
    private $bgcolor = '';

    /**
     * @var string
     *
     * @ORM\Column(name="logo_position_value", type="string", length=32, nullable=false)
     */
    private $logoPositionValue = 'left';

    /**
     * @var integer
     *
     * @ORM\Column(name="base_template_id", type="integer", nullable=true)
     */
    private $baseTemplateId;

    /**
     * @var string
     *
     * @ORM\Column(name="design_logo", type="string", length=255, nullable=true)
     */
    private $designLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="design_invoice_logo", type="string", length=255, nullable=true)
     */
    private $designInvoiceLogo;



    /**
     * Get templateId
     *
     * @return integer
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * Set templateName
     *
     * @param string $templateName
     *
     * @return Templates
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName
     *
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Set templatePath
     *
     * @param string $templatePath
     *
     * @return Templates
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;

        return $this;
    }

    /**
     * Get templatePath
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * Set logoWidth
     *
     * @param integer $logoWidth
     *
     * @return Templates
     */
    public function setLogoWidth($logoWidth)
    {
        $this->logoWidth = $logoWidth;

        return $this;
    }

    /**
     * Get logoWidth
     *
     * @return integer
     */
    public function getLogoWidth()
    {
        return $this->logoWidth;
    }

    /**
     * Set logoHeight
     *
     * @param integer $logoHeight
     *
     * @return Templates
     */
    public function setLogoHeight($logoHeight)
    {
        $this->logoHeight = $logoHeight;

        return $this;
    }

    /**
     * Get logoHeight
     *
     * @return integer
     */
    public function getLogoHeight()
    {
        return $this->logoHeight;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Templates
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Templates
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set partnerId
     *
     * @param integer $partnerId
     *
     * @return Templates
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;

        return $this;
    }

    /**
     * Get partnerId
     *
     * @return integer
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * Set resellerId
     *
     * @param integer $resellerId
     *
     * @return Templates
     */
    public function setResellerId($resellerId)
    {
        $this->resellerId = $resellerId;

        return $this;
    }

    /**
     * Get resellerId
     *
     * @return integer
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Templates
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set headerBgcolor
     *
     * @param boolean $headerBgcolor
     *
     * @return Templates
     */
    public function setHeaderBgcolor($headerBgcolor)
    {
        $this->headerBgcolor = $headerBgcolor;

        return $this;
    }

    /**
     * Get headerBgcolor
     *
     * @return boolean
     */
    public function getHeaderBgcolor()
    {
        return $this->headerBgcolor;
    }

    /**
     * Set logoPosition
     *
     * @param boolean $logoPosition
     *
     * @return Templates
     */
    public function setLogoPosition($logoPosition)
    {
        $this->logoPosition = $logoPosition;

        return $this;
    }

    /**
     * Get logoPosition
     *
     * @return boolean
     */
    public function getLogoPosition()
    {
        return $this->logoPosition;
    }

    /**
     * Set ftpServer
     *
     * @param string $ftpServer
     *
     * @return Templates
     */
    public function setFtpServer($ftpServer)
    {
        $this->ftpServer = $ftpServer;

        return $this;
    }

    /**
     * Get ftpServer
     *
     * @return string
     */
    public function getFtpServer()
    {
        return $this->ftpServer;
    }

    /**
     * Set ftpPath
     *
     * @param string $ftpPath
     *
     * @return Templates
     */
    public function setFtpPath($ftpPath)
    {
        $this->ftpPath = $ftpPath;

        return $this;
    }

    /**
     * Get ftpPath
     *
     * @return string
     */
    public function getFtpPath()
    {
        return $this->ftpPath;
    }

    /**
     * Set ftpUser
     *
     * @param string $ftpUser
     *
     * @return Templates
     */
    public function setFtpUser($ftpUser)
    {
        $this->ftpUser = $ftpUser;

        return $this;
    }

    /**
     * Get ftpUser
     *
     * @return string
     */
    public function getFtpUser()
    {
        return $this->ftpUser;
    }

    /**
     * Set ftpPass
     *
     * @param string $ftpPass
     *
     * @return Templates
     */
    public function setFtpPass($ftpPass)
    {
        $this->ftpPass = $ftpPass;

        return $this;
    }

    /**
     * Get ftpPass
     *
     * @return string
     */
    public function getFtpPass()
    {
        return $this->ftpPass;
    }

    /**
     * Set editMode
     *
     * @param boolean $editMode
     *
     * @return Templates
     */
    public function setEditMode($editMode)
    {
        $this->editMode = $editMode;

        return $this;
    }

    /**
     * Get editMode
     *
     * @return boolean
     */
    public function getEditMode()
    {
        return $this->editMode;
    }

    /**
     * Set ftpMode
     *
     * @param boolean $ftpMode
     *
     * @return Templates
     */
    public function setFtpMode($ftpMode)
    {
        $this->ftpMode = $ftpMode;

        return $this;
    }

    /**
     * Get ftpMode
     *
     * @return boolean
     */
    public function getFtpMode()
    {
        return $this->ftpMode;
    }

    /**
     * Set bgcolor
     *
     * @param string $bgcolor
     *
     * @return Templates
     */
    public function setBgcolor($bgcolor)
    {
        $this->bgcolor = $bgcolor;

        return $this;
    }

    /**
     * Get bgcolor
     *
     * @return string
     */
    public function getBgcolor()
    {
        return $this->bgcolor;
    }

    /**
     * Set logoPositionValue
     *
     * @param string $logoPositionValue
     *
     * @return Templates
     */
    public function setLogoPositionValue($logoPositionValue)
    {
        $this->logoPositionValue = $logoPositionValue;

        return $this;
    }

    /**
     * Get logoPositionValue
     *
     * @return string
     */
    public function getLogoPositionValue()
    {
        return $this->logoPositionValue;
    }

    /**
     * Set baseTemplateId
     *
     * @param integer $baseTemplateId
     *
     * @return Templates
     */
    public function setBaseTemplateId($baseTemplateId)
    {
        $this->baseTemplateId = $baseTemplateId;

        return $this;
    }

    /**
     * Get baseTemplateId
     *
     * @return integer
     */
    public function getBaseTemplateId()
    {
        return $this->baseTemplateId;
    }

    /**
     * Set designLogo
     *
     * @param string $designLogo
     *
     * @return Templates
     */
    public function setDesignLogo($designLogo)
    {
        $this->designLogo = $designLogo;

        return $this;
    }

    /**
     * Get designLogo
     *
     * @return string
     */
    public function getDesignLogo()
    {
        return $this->designLogo;
    }

    /**
     * Set designInvoiceLogo
     *
     * @param string $designInvoiceLogo
     *
     * @return Templates
     */
    public function setDesignInvoiceLogo($designInvoiceLogo)
    {
        $this->designInvoiceLogo = $designInvoiceLogo;

        return $this;
    }

    /**
     * Get designInvoiceLogo
     *
     * @return string
     */
    public function getDesignInvoiceLogo()
    {
        return $this->designInvoiceLogo;
    }
}
