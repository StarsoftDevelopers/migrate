<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServerTasks
 *
 * @ORM\Table(name="server_tasks")
 * @ORM\Entity
 */
class ServerTasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ShopID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $shopid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="Hash", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $hash = '';

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=32, nullable=false)
     */
    private $name = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="State", type="boolean", nullable=false)
     */
    private $state = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="RequestState", type="boolean", nullable=false)
     */
    private $requeststate = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="Message", type="string", length=255, nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="Progress", type="decimal", precision=11, scale=1, nullable=false)
     */
    private $progress = '0.0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Starttime", type="datetime", nullable=false)
     */
    private $starttime = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LastProgress", type="datetime", nullable=false)
     */
    private $lastprogress = '0000-00-00 00:00:00';



    /**
     * Set shopid
     *
     * @param integer $shopid
     *
     * @return ServerTasks
     */
    public function setShopid($shopid)
    {
        $this->shopid = $shopid;

        return $this;
    }

    /**
     * Get shopid
     *
     * @return integer
     */
    public function getShopid()
    {
        return $this->shopid;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return ServerTasks
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ServerTasks
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set state
     *
     * @param boolean $state
     *
     * @return ServerTasks
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return boolean
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set requeststate
     *
     * @param boolean $requeststate
     *
     * @return ServerTasks
     */
    public function setRequeststate($requeststate)
    {
        $this->requeststate = $requeststate;

        return $this;
    }

    /**
     * Get requeststate
     *
     * @return boolean
     */
    public function getRequeststate()
    {
        return $this->requeststate;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return ServerTasks
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set progress
     *
     * @param string $progress
     *
     * @return ServerTasks
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * Get progress
     *
     * @return string
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Set starttime
     *
     * @param \DateTime $starttime
     *
     * @return ServerTasks
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return \DateTime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set lastprogress
     *
     * @param \DateTime $lastprogress
     *
     * @return ServerTasks
     */
    public function setLastprogress($lastprogress)
    {
        $this->lastprogress = $lastprogress;

        return $this;
    }

    /**
     * Get lastprogress
     *
     * @return \DateTime
     */
    public function getLastprogress()
    {
        return $this->lastprogress;
    }
}
