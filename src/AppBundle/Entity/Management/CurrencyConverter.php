<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrencyConverter
 *
 * @ORM\Table(name="currency_converter", uniqueConstraints={@ORM\UniqueConstraint(name="countries_id", columns={"countries_id"})})
 * @ORM\Entity
 */
class CurrencyConverter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="countries_id", type="integer", nullable=false)
     */
    private $countriesId;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_code", type="string", length=3, nullable=false)
     */
    private $currencyCode;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_name", type="string", length=64, nullable=false)
     */
    private $currencyName;

    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float", precision=13, scale=8, nullable=false)
     */
    private $rate;

    /**
     * @var string
     *
     * @ORM\Column(name="zeit", type="string", length=32, nullable=false)
     */
    private $zeit;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countriesId
     *
     * @param integer $countriesId
     *
     * @return CurrencyConverter
     */
    public function setCountriesId($countriesId)
    {
        $this->countriesId = $countriesId;

        return $this;
    }

    /**
     * Get countriesId
     *
     * @return integer
     */
    public function getCountriesId()
    {
        return $this->countriesId;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return CurrencyConverter
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set currencyName
     *
     * @param string $currencyName
     *
     * @return CurrencyConverter
     */
    public function setCurrencyName($currencyName)
    {
        $this->currencyName = $currencyName;

        return $this;
    }

    /**
     * Get currencyName
     *
     * @return string
     */
    public function getCurrencyName()
    {
        return $this->currencyName;
    }

    /**
     * Set rate
     *
     * @param float $rate
     *
     * @return CurrencyConverter
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set zeit
     *
     * @param string $zeit
     *
     * @return CurrencyConverter
     */
    public function setZeit($zeit)
    {
        $this->zeit = $zeit;

        return $this;
    }

    /**
     * Get zeit
     *
     * @return string
     */
    public function getZeit()
    {
        return $this->zeit;
    }
}
