<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountriesIsocodes
 *
 * @ORM\Table(name="countries_isocodes")
 * @ORM\Entity
 */
class CountriesIsocodes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="countries_iso_code_2", type="string", length=2, nullable=false)
     */
    private $countriesIsoCode2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="countries_iso_code_3", type="string", length=3, nullable=false)
     */
    private $countriesIsoCode3 = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="address_format_id", type="integer", nullable=false)
     */
    private $addressFormatId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="continent_id", type="integer", nullable=false)
     */
    private $continentId = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countriesIsoCode2
     *
     * @param string $countriesIsoCode2
     *
     * @return CountriesIsocodes
     */
    public function setCountriesIsoCode2($countriesIsoCode2)
    {
        $this->countriesIsoCode2 = $countriesIsoCode2;

        return $this;
    }

    /**
     * Get countriesIsoCode2
     *
     * @return string
     */
    public function getCountriesIsoCode2()
    {
        return $this->countriesIsoCode2;
    }

    /**
     * Set countriesIsoCode3
     *
     * @param string $countriesIsoCode3
     *
     * @return CountriesIsocodes
     */
    public function setCountriesIsoCode3($countriesIsoCode3)
    {
        $this->countriesIsoCode3 = $countriesIsoCode3;

        return $this;
    }

    /**
     * Get countriesIsoCode3
     *
     * @return string
     */
    public function getCountriesIsoCode3()
    {
        return $this->countriesIsoCode3;
    }

    /**
     * Set addressFormatId
     *
     * @param integer $addressFormatId
     *
     * @return CountriesIsocodes
     */
    public function setAddressFormatId($addressFormatId)
    {
        $this->addressFormatId = $addressFormatId;

        return $this;
    }

    /**
     * Get addressFormatId
     *
     * @return integer
     */
    public function getAddressFormatId()
    {
        return $this->addressFormatId;
    }

    /**
     * Set continentId
     *
     * @param integer $continentId
     *
     * @return CountriesIsocodes
     */
    public function setContinentId($continentId)
    {
        $this->continentId = $continentId;

        return $this;
    }

    /**
     * Get continentId
     *
     * @return integer
     */
    public function getContinentId()
    {
        return $this->continentId;
    }
}
