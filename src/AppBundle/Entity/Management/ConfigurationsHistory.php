<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigurationsHistory
 *
 * @ORM\Table(name="configurations_history")
 * @ORM\Entity
 */
class ConfigurationsHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="configuration_key", type="text", length=65535, nullable=false)
     */
    private $configurationKey;

    /**
     * @var string
     *
     * @ORM\Column(name="configuration_values", type="text", nullable=false)
     */
    private $configurationValues;

    /**
     * @var string
     *
     * @ORM\Column(name="date_deleted", type="string", length=30, nullable=true)
     */
    private $dateDeleted;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set configurationKey
     *
     * @param string $configurationKey
     *
     * @return ConfigurationsHistory
     */
    public function setConfigurationKey($configurationKey)
    {
        $this->configurationKey = $configurationKey;

        return $this;
    }

    /**
     * Get configurationKey
     *
     * @return string
     */
    public function getConfigurationKey()
    {
        return $this->configurationKey;
    }

    /**
     * Set configurationValues
     *
     * @param string $configurationValues
     *
     * @return ConfigurationsHistory
     */
    public function setConfigurationValues($configurationValues)
    {
        $this->configurationValues = $configurationValues;

        return $this;
    }

    /**
     * Get configurationValues
     *
     * @return string
     */
    public function getConfigurationValues()
    {
        return $this->configurationValues;
    }

    /**
     * Set dateDeleted
     *
     * @param string $dateDeleted
     *
     * @return ConfigurationsHistory
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->dateDeleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted
     *
     * @return string
     */
    public function getDateDeleted()
    {
        return $this->dateDeleted;
    }
}
