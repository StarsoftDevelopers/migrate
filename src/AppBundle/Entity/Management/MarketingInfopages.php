<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarketingInfopages
 *
 * @ORM\Table(name="marketing_infopages")
 * @ORM\Entity
 */
class MarketingInfopages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="htmlcontent", type="text", length=65535, nullable=false)
     */
    private $htmlcontent;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set htmlcontent
     *
     * @param string $htmlcontent
     *
     * @return MarketingInfopages
     */
    public function setHtmlcontent($htmlcontent)
    {
        $this->htmlcontent = $htmlcontent;

        return $this;
    }

    /**
     * Get htmlcontent
     *
     * @return string
     */
    public function getHtmlcontent()
    {
        return $this->htmlcontent;
    }
}
