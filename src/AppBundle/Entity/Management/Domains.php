<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Domains
 *
 * @ORM\Table(name="domains", uniqueConstraints={@ORM\UniqueConstraint(name="domain", columns={"domain"})}, indexes={@ORM\Index(name="facebook_page_id", columns={"facebook_page_id"})})
 * @ORM\Entity
 */
class Domains
{
    /**
     * @var integer
     *
     * @ORM\Column(name="domain_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $domainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=254, nullable=false)
     */
    private $domain = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="intern", type="boolean", nullable=false)
     */
    private $intern = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="standard", type="boolean", nullable=false)
     */
    private $standard = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_page_id", type="string", length=20, nullable=true)
     */
    private $facebookPageId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="secure", type="boolean", nullable=false)
     */
    private $secure = '0';



    /**
     * Get domainId
     *
     * @return integer
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * Set shopId
     *
     * @param integer $shopId
     *
     * @return Domains
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return Domains
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set intern
     *
     * @param boolean $intern
     *
     * @return Domains
     */
    public function setIntern($intern)
    {
        $this->intern = $intern;

        return $this;
    }

    /**
     * Get intern
     *
     * @return boolean
     */
    public function getIntern()
    {
        return $this->intern;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Domains
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set standard
     *
     * @param boolean $standard
     *
     * @return Domains
     */
    public function setStandard($standard)
    {
        $this->standard = $standard;

        return $this;
    }

    /**
     * Get standard
     *
     * @return boolean
     */
    public function getStandard()
    {
        return $this->standard;
    }

    /**
     * Set facebookPageId
     *
     * @param string $facebookPageId
     *
     * @return Domains
     */
    public function setFacebookPageId($facebookPageId)
    {
        $this->facebookPageId = $facebookPageId;

        return $this;
    }

    /**
     * Get facebookPageId
     *
     * @return string
     */
    public function getFacebookPageId()
    {
        return $this->facebookPageId;
    }

    /**
     * Set secure
     *
     * @param boolean $secure
     *
     * @return Domains
     */
    public function setSecure($secure)
    {
        $this->secure = $secure;

        return $this;
    }

    /**
     * Get secure
     *
     * @return boolean
     */
    public function getSecure()
    {
        return $this->secure;
    }
}
