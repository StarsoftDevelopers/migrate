<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * TemplatesVariants
 *
 * @ORM\Table(name="templates_variants")
 * @ORM\Entity
 */
class TemplatesVariants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="variant_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $variantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="template_id", type="integer", nullable=false)
     */
    private $templateId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="variant_name", type="string", length=64, nullable=false)
     */
    private $variantName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="variant_filename", type="string", length=64, nullable=false)
     */
    private $variantFilename = '';



    /**
     * Get variantId
     *
     * @return integer
     */
    public function getVariantId()
    {
        return $this->variantId;
    }

    /**
     * Set templateId
     *
     * @param integer $templateId
     *
     * @return TemplatesVariants
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;

        return $this;
    }

    /**
     * Get templateId
     *
     * @return integer
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * Set variantName
     *
     * @param string $variantName
     *
     * @return TemplatesVariants
     */
    public function setVariantName($variantName)
    {
        $this->variantName = $variantName;

        return $this;
    }

    /**
     * Get variantName
     *
     * @return string
     */
    public function getVariantName()
    {
        return $this->variantName;
    }

    /**
     * Set variantFilename
     *
     * @param string $variantFilename
     *
     * @return TemplatesVariants
     */
    public function setVariantFilename($variantFilename)
    {
        $this->variantFilename = $variantFilename;

        return $this;
    }

    /**
     * Get variantFilename
     *
     * @return string
     */
    public function getVariantFilename()
    {
        return $this->variantFilename;
    }
}
