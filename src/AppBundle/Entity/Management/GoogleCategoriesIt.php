<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * GoogleCategoriesIt
 *
 * @ORM\Table(name="google_categories_it")
 * @ORM\Entity
 */
class GoogleCategoriesIt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Full", type="string", length=255, nullable=false)
     */
    private $full;

    /**
     * @var integer
     *
     * @ORM\Column(name="Parent", type="smallint", nullable=true)
     */
    private $parent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Ebene", type="boolean", nullable=false)
     */
    private $ebene;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GoogleCategoriesIt
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set full
     *
     * @param string $full
     *
     * @return GoogleCategoriesIt
     */
    public function setFull($full)
    {
        $this->full = $full;

        return $this;
    }

    /**
     * Get full
     *
     * @return string
     */
    public function getFull()
    {
        return $this->full;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     *
     * @return GoogleCategoriesIt
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set ebene
     *
     * @param boolean $ebene
     *
     * @return GoogleCategoriesIt
     */
    public function setEbene($ebene)
    {
        $this->ebene = $ebene;

        return $this;
    }

    /**
     * Get ebene
     *
     * @return boolean
     */
    public function getEbene()
    {
        return $this->ebene;
    }
}
