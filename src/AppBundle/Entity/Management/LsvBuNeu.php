<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * LsvBuNeu
 *
 * @ORM\Table(name="lsv_bu_neu")
 * @ORM\Entity
 */
class LsvBuNeu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="values_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $valuesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languagesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="variable", type="text", length=65535, nullable=true)
     */
    private $variable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changed", type="boolean", nullable=false)
     */
    private $changed = '0';



    /**
     * Set valuesId
     *
     * @param integer $valuesId
     *
     * @return LsvBuNeu
     */
    public function setValuesId($valuesId)
    {
        $this->valuesId = $valuesId;

        return $this;
    }

    /**
     * Get valuesId
     *
     * @return integer
     */
    public function getValuesId()
    {
        return $this->valuesId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return LsvBuNeu
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set variable
     *
     * @param string $variable
     *
     * @return LsvBuNeu
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * Get variable
     *
     * @return string
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * Set changed
     *
     * @param boolean $changed
     *
     * @return LsvBuNeu
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * Get changed
     *
     * @return boolean
     */
    public function getChanged()
    {
        return $this->changed;
    }
}
