<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * YategoCategories
 *
 * @ORM\Table(name="yatego_categories", indexes={@ORM\Index(name="Name", columns={"Name"})})
 * @ORM\Entity
 */
class YategoCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="Parent", type="bigint", nullable=true)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="KatID", type="string", length=8, nullable=true)
     */
    private $katid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Ebene", type="boolean", nullable=false)
     */
    private $ebene = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return YategoCategories
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     *
     * @return YategoCategories
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set katid
     *
     * @param string $katid
     *
     * @return YategoCategories
     */
    public function setKatid($katid)
    {
        $this->katid = $katid;

        return $this;
    }

    /**
     * Get katid
     *
     * @return string
     */
    public function getKatid()
    {
        return $this->katid;
    }

    /**
     * Set ebene
     *
     * @param boolean $ebene
     *
     * @return YategoCategories
     */
    public function setEbene($ebene)
    {
        $this->ebene = $ebene;

        return $this;
    }

    /**
     * Get ebene
     *
     * @return boolean
     */
    public function getEbene()
    {
        return $this->ebene;
    }
}
