<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * CsvTemplatesFiles
 *
 * @ORM\Table(name="csv_templates_files")
 * @ORM\Entity
 */
class CsvTemplatesFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="csv_files_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $csvFilesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="csv_id", type="integer", nullable=false)
     */
    private $csvId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="csv_filename", type="text", length=65535, nullable=false)
     */
    private $csvFilename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="csv_files_added", type="datetime", nullable=false)
     */
    private $csvFilesAdded = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=10, nullable=false)
     */
    private $userId = '';



    /**
     * Get csvFilesId
     *
     * @return integer
     */
    public function getCsvFilesId()
    {
        return $this->csvFilesId;
    }

    /**
     * Set csvId
     *
     * @param integer $csvId
     *
     * @return CsvTemplatesFiles
     */
    public function setCsvId($csvId)
    {
        $this->csvId = $csvId;

        return $this;
    }

    /**
     * Get csvId
     *
     * @return integer
     */
    public function getCsvId()
    {
        return $this->csvId;
    }

    /**
     * Set csvFilename
     *
     * @param string $csvFilename
     *
     * @return CsvTemplatesFiles
     */
    public function setCsvFilename($csvFilename)
    {
        $this->csvFilename = $csvFilename;

        return $this;
    }

    /**
     * Get csvFilename
     *
     * @return string
     */
    public function getCsvFilename()
    {
        return $this->csvFilename;
    }

    /**
     * Set csvFilesAdded
     *
     * @param \DateTime $csvFilesAdded
     *
     * @return CsvTemplatesFiles
     */
    public function setCsvFilesAdded($csvFilesAdded)
    {
        $this->csvFilesAdded = $csvFilesAdded;

        return $this;
    }

    /**
     * Get csvFilesAdded
     *
     * @return \DateTime
     */
    public function getCsvFilesAdded()
    {
        return $this->csvFilesAdded;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return CsvTemplatesFiles
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
