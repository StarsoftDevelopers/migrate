<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarketingProviders
 *
 * @ORM\Table(name="marketing_providers", uniqueConstraints={@ORM\UniqueConstraint(name="affiliate_id", columns={"affiliate_id"})})
 * @ORM\Entity
 */
class MarketingProviders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="provider_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $providerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="provider_type", type="integer", nullable=false)
     */
    private $providerType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="provider_name", type="string", length=64, nullable=false)
     */
    private $providerName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="provider_parent", type="string", length=64, nullable=true)
     */
    private $providerParent;

    /**
     * @var float
     *
     * @ORM\Column(name="provider_klickpreis", type="float", precision=10, scale=0, nullable=false)
     */
    private $providerKlickpreis = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="provider_prozentual", type="boolean", nullable=false)
     */
    private $providerProzentual = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="provider_provision", type="float", precision=10, scale=0, nullable=false)
     */
    private $providerProvision = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="csv_format_id", type="integer", nullable=false)
     */
    private $csvFormatId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     */
    private $affiliateId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="provider_active", type="boolean", nullable=false)
     */
    private $providerActive = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="csv_format_active", type="boolean", nullable=false)
     */
    private $csvFormatActive = '0';



    /**
     * Get providerId
     *
     * @return integer
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set providerType
     *
     * @param integer $providerType
     *
     * @return MarketingProviders
     */
    public function setProviderType($providerType)
    {
        $this->providerType = $providerType;

        return $this;
    }

    /**
     * Get providerType
     *
     * @return integer
     */
    public function getProviderType()
    {
        return $this->providerType;
    }

    /**
     * Set providerName
     *
     * @param string $providerName
     *
     * @return MarketingProviders
     */
    public function setProviderName($providerName)
    {
        $this->providerName = $providerName;

        return $this;
    }

    /**
     * Get providerName
     *
     * @return string
     */
    public function getProviderName()
    {
        return $this->providerName;
    }

    /**
     * Set providerParent
     *
     * @param string $providerParent
     *
     * @return MarketingProviders
     */
    public function setProviderParent($providerParent)
    {
        $this->providerParent = $providerParent;

        return $this;
    }

    /**
     * Get providerParent
     *
     * @return string
     */
    public function getProviderParent()
    {
        return $this->providerParent;
    }

    /**
     * Set providerKlickpreis
     *
     * @param float $providerKlickpreis
     *
     * @return MarketingProviders
     */
    public function setProviderKlickpreis($providerKlickpreis)
    {
        $this->providerKlickpreis = $providerKlickpreis;

        return $this;
    }

    /**
     * Get providerKlickpreis
     *
     * @return float
     */
    public function getProviderKlickpreis()
    {
        return $this->providerKlickpreis;
    }

    /**
     * Set providerProzentual
     *
     * @param boolean $providerProzentual
     *
     * @return MarketingProviders
     */
    public function setProviderProzentual($providerProzentual)
    {
        $this->providerProzentual = $providerProzentual;

        return $this;
    }

    /**
     * Get providerProzentual
     *
     * @return boolean
     */
    public function getProviderProzentual()
    {
        return $this->providerProzentual;
    }

    /**
     * Set providerProvision
     *
     * @param float $providerProvision
     *
     * @return MarketingProviders
     */
    public function setProviderProvision($providerProvision)
    {
        $this->providerProvision = $providerProvision;

        return $this;
    }

    /**
     * Get providerProvision
     *
     * @return float
     */
    public function getProviderProvision()
    {
        return $this->providerProvision;
    }

    /**
     * Set csvFormatId
     *
     * @param integer $csvFormatId
     *
     * @return MarketingProviders
     */
    public function setCsvFormatId($csvFormatId)
    {
        $this->csvFormatId = $csvFormatId;

        return $this;
    }

    /**
     * Get csvFormatId
     *
     * @return integer
     */
    public function getCsvFormatId()
    {
        return $this->csvFormatId;
    }

    /**
     * Set affiliateId
     *
     * @param integer $affiliateId
     *
     * @return MarketingProviders
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;

        return $this;
    }

    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set providerActive
     *
     * @param boolean $providerActive
     *
     * @return MarketingProviders
     */
    public function setProviderActive($providerActive)
    {
        $this->providerActive = $providerActive;

        return $this;
    }

    /**
     * Get providerActive
     *
     * @return boolean
     */
    public function getProviderActive()
    {
        return $this->providerActive;
    }

    /**
     * Set csvFormatActive
     *
     * @param boolean $csvFormatActive
     *
     * @return MarketingProviders
     */
    public function setCsvFormatActive($csvFormatActive)
    {
        $this->csvFormatActive = $csvFormatActive;

        return $this;
    }

    /**
     * Get csvFormatActive
     *
     * @return boolean
     */
    public function getCsvFormatActive()
    {
        return $this->csvFormatActive;
    }
}
