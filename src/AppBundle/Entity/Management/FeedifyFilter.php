<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeedifyFilter
 *
 * @ORM\Table(name="feedify_filter")
 * @ORM\Entity
 */
class FeedifyFilter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime", nullable=false)
     */
    private $datecreated;

    /**
     * @var string
     *
     * @ORM\Column(name="clientid", type="string", length=255, nullable=false)
     */
    private $clientid;

    /**
     * @var string
     *
     * @ORM\Column(name="userid", type="string", length=255, nullable=false)
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="userDbName", type="string", length=255, nullable=false)
     */
    private $userdbname;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var array
     *
     * @ORM\Column(name="data", type="json_array", nullable=false)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datecreated
     *
     * @param \DateTime $datecreated
     *
     * @return FeedifyFilter
     */
    public function setDatecreated($datecreated)
    {
        $this->datecreated = $datecreated;

        return $this;
    }

    /**
     * Get datecreated
     *
     * @return \DateTime
     */
    public function getDatecreated()
    {
        return $this->datecreated;
    }

    /**
     * Set clientid
     *
     * @param string $clientid
     *
     * @return FeedifyFilter
     */
    public function setClientid($clientid)
    {
        $this->clientid = $clientid;

        return $this;
    }

    /**
     * Get clientid
     *
     * @return string
     */
    public function getClientid()
    {
        return $this->clientid;
    }

    /**
     * Set userid
     *
     * @param string $userid
     *
     * @return FeedifyFilter
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return string
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set userdbname
     *
     * @param string $userdbname
     *
     * @return FeedifyFilter
     */
    public function setUserdbname($userdbname)
    {
        $this->userdbname = $userdbname;

        return $this;
    }

    /**
     * Get userdbname
     *
     * @return string
     */
    public function getUserdbname()
    {
        return $this->userdbname;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return FeedifyFilter
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return FeedifyFilter
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FeedifyFilter
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
