<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modules
 *
 * @ORM\Table(name="modules")
 * @ORM\Entity
 */
class Modules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="module_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $moduleId;

    /**
     * @var string
     *
     * @ORM\Column(name="module_short_name", type="string", length=16, nullable=false)
     */
    private $moduleShortName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="module_name", type="string", length=64, nullable=false)
     */
    private $moduleName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible = '0';



    /**
     * Get moduleId
     *
     * @return integer
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * Set moduleShortName
     *
     * @param string $moduleShortName
     *
     * @return Modules
     */
    public function setModuleShortName($moduleShortName)
    {
        $this->moduleShortName = $moduleShortName;

        return $this;
    }

    /**
     * Get moduleShortName
     *
     * @return string
     */
    public function getModuleShortName()
    {
        return $this->moduleShortName;
    }

    /**
     * Set moduleName
     *
     * @param string $moduleName
     *
     * @return Modules
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get moduleName
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Modules
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
