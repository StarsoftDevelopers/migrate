<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * FacebookActivationCodes
 *
 * @ORM\Table(name="facebook_activation_codes", uniqueConstraints={@ORM\UniqueConstraint(name="key", columns={"activation_code"})})
 * @ORM\Entity
 */
class FacebookActivationCodes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="activation_code", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $activationCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId;

    /**
     * @var integer
     *
     * @ORM\Column(name="installed", type="integer", nullable=false)
     */
    private $installed = '0';



    /**
     * Get activationCode
     *
     * @return integer
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * Set shopId
     *
     * @param integer $shopId
     *
     * @return FacebookActivationCodes
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set installed
     *
     * @param integer $installed
     *
     * @return FacebookActivationCodes
     */
    public function setInstalled($installed)
    {
        $this->installed = $installed;

        return $this;
    }

    /**
     * Get installed
     *
     * @return integer
     */
    public function getInstalled()
    {
        return $this->installed;
    }
}
