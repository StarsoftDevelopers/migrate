<?php

namespace AppBundle\Entity\Management\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * AffiliateAffiliate
 *
 * @ORM\Table(name="affiliate_affiliate", indexes={@ORM\Index(name="affiliate_lastname", columns={"affiliate_lastname"}), @ORM\Index(name="fxm_status", columns={"fxm_status"})})
 * @ORM\Entity
 */
class AffiliateAffiliate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $affiliateId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fxm_status", type="boolean", nullable=false)
     */
    private $fxmStatus = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_gender", type="string", length=1, nullable=false)
     */
    private $affiliateGender = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_firstname", type="string", length=32, nullable=false)
     */
    private $affiliateFirstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_lastname", type="string", length=32, nullable=false)
     */
    private $affiliateLastname = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affiliate_dob", type="datetime", nullable=false)
     */
    private $affiliateDob = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_email_address", type="string", length=96, nullable=false)
     */
    private $affiliateEmailAddress = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_telephone", type="string", length=32, nullable=false)
     */
    private $affiliateTelephone = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_fax", type="string", length=32, nullable=false)
     */
    private $affiliateFax = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_password", type="string", length=40, nullable=false)
     */
    private $affiliatePassword = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_homepage", type="string", length=96, nullable=false)
     */
    private $affiliateHomepage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_street_address", type="string", length=64, nullable=false)
     */
    private $affiliateStreetAddress = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_suburb", type="string", length=64, nullable=false)
     */
    private $affiliateSuburb = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_city", type="string", length=32, nullable=false)
     */
    private $affiliateCity = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_postcode", type="string", length=10, nullable=false)
     */
    private $affiliatePostcode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_state", type="string", length=32, nullable=false)
     */
    private $affiliateState = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_country_id", type="integer", nullable=false)
     */
    private $affiliateCountryId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_zone_id", type="integer", nullable=false)
     */
    private $affiliateZoneId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="affiliate_agb", type="boolean", nullable=false)
     */
    private $affiliateAgb = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_company", type="string", length=60, nullable=false)
     */
    private $affiliateCompany = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_company_taxid", type="string", length=64, nullable=false)
     */
    private $affiliateCompanyTaxid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_commission_percent", type="decimal", precision=4, scale=2, nullable=false)
     */
    private $affiliateCommissionPercent = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment_check", type="string", length=100, nullable=false)
     */
    private $affiliatePaymentCheck = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment_paypal", type="string", length=64, nullable=false)
     */
    private $affiliatePaymentPaypal = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment_bank_name", type="string", length=64, nullable=false)
     */
    private $affiliatePaymentBankName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment_bank_branch_number", type="string", length=64, nullable=false)
     */
    private $affiliatePaymentBankBranchNumber = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment_bank_swift_code", type="string", length=64, nullable=false)
     */
    private $affiliatePaymentBankSwiftCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment_bank_account_name", type="string", length=64, nullable=false)
     */
    private $affiliatePaymentBankAccountName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment_bank_account_number", type="string", length=64, nullable=false)
     */
    private $affiliatePaymentBankAccountNumber = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affiliate_date_of_last_logon", type="datetime", nullable=false)
     */
    private $affiliateDateOfLastLogon = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_number_of_logons", type="integer", nullable=false)
     */
    private $affiliateNumberOfLogons = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affiliate_date_account_created", type="datetime", nullable=false)
     */
    private $affiliateDateAccountCreated = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affiliate_date_account_last_modified", type="datetime", nullable=false)
     */
    private $affiliateDateAccountLastModified = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_lft", type="integer", nullable=false)
     */
    private $affiliateLft = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_rgt", type="integer", nullable=false)
     */
    private $affiliateRgt = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_root", type="integer", nullable=false)
     */
    private $affiliateRoot = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_newsletter", type="string", length=1, nullable=false)
     */
    private $affiliateNewsletter = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="login_url", type="string", length=150, nullable=false)
     */
    private $loginUrl = '';

    /**
     * @var string
     *
     * @ORM\Column(name="login_userfieldname", type="string", length=50, nullable=false)
     */
    private $loginUserfieldname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="login_pwfieldname", type="string", length=50, nullable=false)
     */
    private $loginPwfieldname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="login_submitbuttonname", type="string", length=100, nullable=false)
     */
    private $loginSubmitbuttonname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="login_submitbuttonvalue", type="string", length=100, nullable=false)
     */
    private $loginSubmitbuttonvalue = '';



    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set fxmStatus
     *
     * @param boolean $fxmStatus
     *
     * @return AffiliateAffiliate
     */
    public function setFxmStatus($fxmStatus)
    {
        $this->fxmStatus = $fxmStatus;

        return $this;
    }

    /**
     * Get fxmStatus
     *
     * @return boolean
     */
    public function getFxmStatus()
    {
        return $this->fxmStatus;
    }

    /**
     * Set affiliateGender
     *
     * @param string $affiliateGender
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateGender($affiliateGender)
    {
        $this->affiliateGender = $affiliateGender;

        return $this;
    }

    /**
     * Get affiliateGender
     *
     * @return string
     */
    public function getAffiliateGender()
    {
        return $this->affiliateGender;
    }

    /**
     * Set affiliateFirstname
     *
     * @param string $affiliateFirstname
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateFirstname($affiliateFirstname)
    {
        $this->affiliateFirstname = $affiliateFirstname;

        return $this;
    }

    /**
     * Get affiliateFirstname
     *
     * @return string
     */
    public function getAffiliateFirstname()
    {
        return $this->affiliateFirstname;
    }

    /**
     * Set affiliateLastname
     *
     * @param string $affiliateLastname
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateLastname($affiliateLastname)
    {
        $this->affiliateLastname = $affiliateLastname;

        return $this;
    }

    /**
     * Get affiliateLastname
     *
     * @return string
     */
    public function getAffiliateLastname()
    {
        return $this->affiliateLastname;
    }

    /**
     * Set affiliateDob
     *
     * @param \DateTime $affiliateDob
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateDob($affiliateDob)
    {
        $this->affiliateDob = $affiliateDob;

        return $this;
    }

    /**
     * Get affiliateDob
     *
     * @return \DateTime
     */
    public function getAffiliateDob()
    {
        return $this->affiliateDob;
    }

    /**
     * Set affiliateEmailAddress
     *
     * @param string $affiliateEmailAddress
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateEmailAddress($affiliateEmailAddress)
    {
        $this->affiliateEmailAddress = $affiliateEmailAddress;

        return $this;
    }

    /**
     * Get affiliateEmailAddress
     *
     * @return string
     */
    public function getAffiliateEmailAddress()
    {
        return $this->affiliateEmailAddress;
    }

    /**
     * Set affiliateTelephone
     *
     * @param string $affiliateTelephone
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateTelephone($affiliateTelephone)
    {
        $this->affiliateTelephone = $affiliateTelephone;

        return $this;
    }

    /**
     * Get affiliateTelephone
     *
     * @return string
     */
    public function getAffiliateTelephone()
    {
        return $this->affiliateTelephone;
    }

    /**
     * Set affiliateFax
     *
     * @param string $affiliateFax
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateFax($affiliateFax)
    {
        $this->affiliateFax = $affiliateFax;

        return $this;
    }

    /**
     * Get affiliateFax
     *
     * @return string
     */
    public function getAffiliateFax()
    {
        return $this->affiliateFax;
    }

    /**
     * Set affiliatePassword
     *
     * @param string $affiliatePassword
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePassword($affiliatePassword)
    {
        $this->affiliatePassword = $affiliatePassword;

        return $this;
    }

    /**
     * Get affiliatePassword
     *
     * @return string
     */
    public function getAffiliatePassword()
    {
        return $this->affiliatePassword;
    }

    /**
     * Set affiliateHomepage
     *
     * @param string $affiliateHomepage
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateHomepage($affiliateHomepage)
    {
        $this->affiliateHomepage = $affiliateHomepage;

        return $this;
    }

    /**
     * Get affiliateHomepage
     *
     * @return string
     */
    public function getAffiliateHomepage()
    {
        return $this->affiliateHomepage;
    }

    /**
     * Set affiliateStreetAddress
     *
     * @param string $affiliateStreetAddress
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateStreetAddress($affiliateStreetAddress)
    {
        $this->affiliateStreetAddress = $affiliateStreetAddress;

        return $this;
    }

    /**
     * Get affiliateStreetAddress
     *
     * @return string
     */
    public function getAffiliateStreetAddress()
    {
        return $this->affiliateStreetAddress;
    }

    /**
     * Set affiliateSuburb
     *
     * @param string $affiliateSuburb
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateSuburb($affiliateSuburb)
    {
        $this->affiliateSuburb = $affiliateSuburb;

        return $this;
    }

    /**
     * Get affiliateSuburb
     *
     * @return string
     */
    public function getAffiliateSuburb()
    {
        return $this->affiliateSuburb;
    }

    /**
     * Set affiliateCity
     *
     * @param string $affiliateCity
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateCity($affiliateCity)
    {
        $this->affiliateCity = $affiliateCity;

        return $this;
    }

    /**
     * Get affiliateCity
     *
     * @return string
     */
    public function getAffiliateCity()
    {
        return $this->affiliateCity;
    }

    /**
     * Set affiliatePostcode
     *
     * @param string $affiliatePostcode
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePostcode($affiliatePostcode)
    {
        $this->affiliatePostcode = $affiliatePostcode;

        return $this;
    }

    /**
     * Get affiliatePostcode
     *
     * @return string
     */
    public function getAffiliatePostcode()
    {
        return $this->affiliatePostcode;
    }

    /**
     * Set affiliateState
     *
     * @param string $affiliateState
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateState($affiliateState)
    {
        $this->affiliateState = $affiliateState;

        return $this;
    }

    /**
     * Get affiliateState
     *
     * @return string
     */
    public function getAffiliateState()
    {
        return $this->affiliateState;
    }

    /**
     * Set affiliateCountryId
     *
     * @param integer $affiliateCountryId
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateCountryId($affiliateCountryId)
    {
        $this->affiliateCountryId = $affiliateCountryId;

        return $this;
    }

    /**
     * Get affiliateCountryId
     *
     * @return integer
     */
    public function getAffiliateCountryId()
    {
        return $this->affiliateCountryId;
    }

    /**
     * Set affiliateZoneId
     *
     * @param integer $affiliateZoneId
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateZoneId($affiliateZoneId)
    {
        $this->affiliateZoneId = $affiliateZoneId;

        return $this;
    }

    /**
     * Get affiliateZoneId
     *
     * @return integer
     */
    public function getAffiliateZoneId()
    {
        return $this->affiliateZoneId;
    }

    /**
     * Set affiliateAgb
     *
     * @param boolean $affiliateAgb
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateAgb($affiliateAgb)
    {
        $this->affiliateAgb = $affiliateAgb;

        return $this;
    }

    /**
     * Get affiliateAgb
     *
     * @return boolean
     */
    public function getAffiliateAgb()
    {
        return $this->affiliateAgb;
    }

    /**
     * Set affiliateCompany
     *
     * @param string $affiliateCompany
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateCompany($affiliateCompany)
    {
        $this->affiliateCompany = $affiliateCompany;

        return $this;
    }

    /**
     * Get affiliateCompany
     *
     * @return string
     */
    public function getAffiliateCompany()
    {
        return $this->affiliateCompany;
    }

    /**
     * Set affiliateCompanyTaxid
     *
     * @param string $affiliateCompanyTaxid
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateCompanyTaxid($affiliateCompanyTaxid)
    {
        $this->affiliateCompanyTaxid = $affiliateCompanyTaxid;

        return $this;
    }

    /**
     * Get affiliateCompanyTaxid
     *
     * @return string
     */
    public function getAffiliateCompanyTaxid()
    {
        return $this->affiliateCompanyTaxid;
    }

    /**
     * Set affiliateCommissionPercent
     *
     * @param string $affiliateCommissionPercent
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateCommissionPercent($affiliateCommissionPercent)
    {
        $this->affiliateCommissionPercent = $affiliateCommissionPercent;

        return $this;
    }

    /**
     * Get affiliateCommissionPercent
     *
     * @return string
     */
    public function getAffiliateCommissionPercent()
    {
        return $this->affiliateCommissionPercent;
    }

    /**
     * Set affiliatePaymentCheck
     *
     * @param string $affiliatePaymentCheck
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePaymentCheck($affiliatePaymentCheck)
    {
        $this->affiliatePaymentCheck = $affiliatePaymentCheck;

        return $this;
    }

    /**
     * Get affiliatePaymentCheck
     *
     * @return string
     */
    public function getAffiliatePaymentCheck()
    {
        return $this->affiliatePaymentCheck;
    }

    /**
     * Set affiliatePaymentPaypal
     *
     * @param string $affiliatePaymentPaypal
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePaymentPaypal($affiliatePaymentPaypal)
    {
        $this->affiliatePaymentPaypal = $affiliatePaymentPaypal;

        return $this;
    }

    /**
     * Get affiliatePaymentPaypal
     *
     * @return string
     */
    public function getAffiliatePaymentPaypal()
    {
        return $this->affiliatePaymentPaypal;
    }

    /**
     * Set affiliatePaymentBankName
     *
     * @param string $affiliatePaymentBankName
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePaymentBankName($affiliatePaymentBankName)
    {
        $this->affiliatePaymentBankName = $affiliatePaymentBankName;

        return $this;
    }

    /**
     * Get affiliatePaymentBankName
     *
     * @return string
     */
    public function getAffiliatePaymentBankName()
    {
        return $this->affiliatePaymentBankName;
    }

    /**
     * Set affiliatePaymentBankBranchNumber
     *
     * @param string $affiliatePaymentBankBranchNumber
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePaymentBankBranchNumber($affiliatePaymentBankBranchNumber)
    {
        $this->affiliatePaymentBankBranchNumber = $affiliatePaymentBankBranchNumber;

        return $this;
    }

    /**
     * Get affiliatePaymentBankBranchNumber
     *
     * @return string
     */
    public function getAffiliatePaymentBankBranchNumber()
    {
        return $this->affiliatePaymentBankBranchNumber;
    }

    /**
     * Set affiliatePaymentBankSwiftCode
     *
     * @param string $affiliatePaymentBankSwiftCode
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePaymentBankSwiftCode($affiliatePaymentBankSwiftCode)
    {
        $this->affiliatePaymentBankSwiftCode = $affiliatePaymentBankSwiftCode;

        return $this;
    }

    /**
     * Get affiliatePaymentBankSwiftCode
     *
     * @return string
     */
    public function getAffiliatePaymentBankSwiftCode()
    {
        return $this->affiliatePaymentBankSwiftCode;
    }

    /**
     * Set affiliatePaymentBankAccountName
     *
     * @param string $affiliatePaymentBankAccountName
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePaymentBankAccountName($affiliatePaymentBankAccountName)
    {
        $this->affiliatePaymentBankAccountName = $affiliatePaymentBankAccountName;

        return $this;
    }

    /**
     * Get affiliatePaymentBankAccountName
     *
     * @return string
     */
    public function getAffiliatePaymentBankAccountName()
    {
        return $this->affiliatePaymentBankAccountName;
    }

    /**
     * Set affiliatePaymentBankAccountNumber
     *
     * @param string $affiliatePaymentBankAccountNumber
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliatePaymentBankAccountNumber($affiliatePaymentBankAccountNumber)
    {
        $this->affiliatePaymentBankAccountNumber = $affiliatePaymentBankAccountNumber;

        return $this;
    }

    /**
     * Get affiliatePaymentBankAccountNumber
     *
     * @return string
     */
    public function getAffiliatePaymentBankAccountNumber()
    {
        return $this->affiliatePaymentBankAccountNumber;
    }

    /**
     * Set affiliateDateOfLastLogon
     *
     * @param \DateTime $affiliateDateOfLastLogon
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateDateOfLastLogon($affiliateDateOfLastLogon)
    {
        $this->affiliateDateOfLastLogon = $affiliateDateOfLastLogon;

        return $this;
    }

    /**
     * Get affiliateDateOfLastLogon
     *
     * @return \DateTime
     */
    public function getAffiliateDateOfLastLogon()
    {
        return $this->affiliateDateOfLastLogon;
    }

    /**
     * Set affiliateNumberOfLogons
     *
     * @param integer $affiliateNumberOfLogons
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateNumberOfLogons($affiliateNumberOfLogons)
    {
        $this->affiliateNumberOfLogons = $affiliateNumberOfLogons;

        return $this;
    }

    /**
     * Get affiliateNumberOfLogons
     *
     * @return integer
     */
    public function getAffiliateNumberOfLogons()
    {
        return $this->affiliateNumberOfLogons;
    }

    /**
     * Set affiliateDateAccountCreated
     *
     * @param \DateTime $affiliateDateAccountCreated
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateDateAccountCreated($affiliateDateAccountCreated)
    {
        $this->affiliateDateAccountCreated = $affiliateDateAccountCreated;

        return $this;
    }

    /**
     * Get affiliateDateAccountCreated
     *
     * @return \DateTime
     */
    public function getAffiliateDateAccountCreated()
    {
        return $this->affiliateDateAccountCreated;
    }

    /**
     * Set affiliateDateAccountLastModified
     *
     * @param \DateTime $affiliateDateAccountLastModified
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateDateAccountLastModified($affiliateDateAccountLastModified)
    {
        $this->affiliateDateAccountLastModified = $affiliateDateAccountLastModified;

        return $this;
    }

    /**
     * Get affiliateDateAccountLastModified
     *
     * @return \DateTime
     */
    public function getAffiliateDateAccountLastModified()
    {
        return $this->affiliateDateAccountLastModified;
    }

    /**
     * Set affiliateLft
     *
     * @param integer $affiliateLft
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateLft($affiliateLft)
    {
        $this->affiliateLft = $affiliateLft;

        return $this;
    }

    /**
     * Get affiliateLft
     *
     * @return integer
     */
    public function getAffiliateLft()
    {
        return $this->affiliateLft;
    }

    /**
     * Set affiliateRgt
     *
     * @param integer $affiliateRgt
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateRgt($affiliateRgt)
    {
        $this->affiliateRgt = $affiliateRgt;

        return $this;
    }

    /**
     * Get affiliateRgt
     *
     * @return integer
     */
    public function getAffiliateRgt()
    {
        return $this->affiliateRgt;
    }

    /**
     * Set affiliateRoot
     *
     * @param integer $affiliateRoot
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateRoot($affiliateRoot)
    {
        $this->affiliateRoot = $affiliateRoot;

        return $this;
    }

    /**
     * Get affiliateRoot
     *
     * @return integer
     */
    public function getAffiliateRoot()
    {
        return $this->affiliateRoot;
    }

    /**
     * Set affiliateNewsletter
     *
     * @param string $affiliateNewsletter
     *
     * @return AffiliateAffiliate
     */
    public function setAffiliateNewsletter($affiliateNewsletter)
    {
        $this->affiliateNewsletter = $affiliateNewsletter;

        return $this;
    }

    /**
     * Get affiliateNewsletter
     *
     * @return string
     */
    public function getAffiliateNewsletter()
    {
        return $this->affiliateNewsletter;
    }

    /**
     * Set loginUrl
     *
     * @param string $loginUrl
     *
     * @return AffiliateAffiliate
     */
    public function setLoginUrl($loginUrl)
    {
        $this->loginUrl = $loginUrl;

        return $this;
    }

    /**
     * Get loginUrl
     *
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * Set loginUserfieldname
     *
     * @param string $loginUserfieldname
     *
     * @return AffiliateAffiliate
     */
    public function setLoginUserfieldname($loginUserfieldname)
    {
        $this->loginUserfieldname = $loginUserfieldname;

        return $this;
    }

    /**
     * Get loginUserfieldname
     *
     * @return string
     */
    public function getLoginUserfieldname()
    {
        return $this->loginUserfieldname;
    }

    /**
     * Set loginPwfieldname
     *
     * @param string $loginPwfieldname
     *
     * @return AffiliateAffiliate
     */
    public function setLoginPwfieldname($loginPwfieldname)
    {
        $this->loginPwfieldname = $loginPwfieldname;

        return $this;
    }

    /**
     * Get loginPwfieldname
     *
     * @return string
     */
    public function getLoginPwfieldname()
    {
        return $this->loginPwfieldname;
    }

    /**
     * Set loginSubmitbuttonname
     *
     * @param string $loginSubmitbuttonname
     *
     * @return AffiliateAffiliate
     */
    public function setLoginSubmitbuttonname($loginSubmitbuttonname)
    {
        $this->loginSubmitbuttonname = $loginSubmitbuttonname;

        return $this;
    }

    /**
     * Get loginSubmitbuttonname
     *
     * @return string
     */
    public function getLoginSubmitbuttonname()
    {
        return $this->loginSubmitbuttonname;
    }

    /**
     * Set loginSubmitbuttonvalue
     *
     * @param string $loginSubmitbuttonvalue
     *
     * @return AffiliateAffiliate
     */
    public function setLoginSubmitbuttonvalue($loginSubmitbuttonvalue)
    {
        $this->loginSubmitbuttonvalue = $loginSubmitbuttonvalue;

        return $this;
    }

    /**
     * Get loginSubmitbuttonvalue
     *
     * @return string
     */
    public function getLoginSubmitbuttonvalue()
    {
        return $this->loginSubmitbuttonvalue;
    }
}
