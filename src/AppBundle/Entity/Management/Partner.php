<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partner
 *
 * @ORM\Table(name="partner")
 * @ORM\Entity
 */
class Partner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="partner_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $partnerId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=128, nullable=false)
     */
    private $link = '';

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=128, nullable=false)
     */
    private $logo = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="pss_user_id", type="integer", nullable=false)
     */
    private $pssUserId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=128, nullable=false)
     */
    private $text;



    /**
     * Get partnerId
     *
     * @return integer
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Partner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Partner
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Partner
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set pssUserId
     *
     * @param integer $pssUserId
     *
     * @return Partner
     */
    public function setPssUserId($pssUserId)
    {
        $this->pssUserId = $pssUserId;

        return $this;
    }

    /**
     * Get pssUserId
     *
     * @return integer
     */
    public function getPssUserId()
    {
        return $this->pssUserId;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Partner
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
