<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModificationHistory
 *
 * @ORM\Table(name="modification_history")
 * @ORM\Entity
 */
class ModificationHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mod_hist_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $modHistId;

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId;

    /**
     * @var integer
     *
     * @ORM\Column(name="item", type="integer", nullable=false)
     */
    private $item;

    /**
     * @var string
     *
     * @ORM\Column(name="value_old", type="text", length=65535, nullable=false)
     */
    private $valueOld;

    /**
     * @var string
     *
     * @ORM\Column(name="value_new", type="text", length=65535, nullable=false)
     */
    private $valueNew;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=24, nullable=false)
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="datetime", nullable=false)
     */
    private $datum = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="table", type="string", length=128, nullable=false)
     */
    private $table;



    /**
     * Get modHistId
     *
     * @return integer
     */
    public function getModHistId()
    {
        return $this->modHistId;
    }

    /**
     * Set shopId
     *
     * @param integer $shopId
     *
     * @return ModificationHistory
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set item
     *
     * @param integer $item
     *
     * @return ModificationHistory
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return integer
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set valueOld
     *
     * @param string $valueOld
     *
     * @return ModificationHistory
     */
    public function setValueOld($valueOld)
    {
        $this->valueOld = $valueOld;

        return $this;
    }

    /**
     * Get valueOld
     *
     * @return string
     */
    public function getValueOld()
    {
        return $this->valueOld;
    }

    /**
     * Set valueNew
     *
     * @param string $valueNew
     *
     * @return ModificationHistory
     */
    public function setValueNew($valueNew)
    {
        $this->valueNew = $valueNew;

        return $this;
    }

    /**
     * Get valueNew
     *
     * @return string
     */
    public function getValueNew()
    {
        return $this->valueNew;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return ModificationHistory
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return ModificationHistory
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return ModificationHistory
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Set table
     *
     * @param string $table
     *
     * @return ModificationHistory
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Get table
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }
}
