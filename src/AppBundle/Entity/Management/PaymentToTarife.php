<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentToTarife
 *
 * @ORM\Table(name="payment_to_tarife", uniqueConstraints={@ORM\UniqueConstraint(name="tarif_id", columns={"tarif_id"})})
 * @ORM\Entity
 */
class PaymentToTarife
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="payment", type="text", length=65535, nullable=false)
     */
    private $payment;

    /**
     * @var integer
     *
     * @ORM\Column(name="tarif_id", type="integer", nullable=false)
     */
    private $tarifId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return PaymentToTarife
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set tarifId
     *
     * @param integer $tarifId
     *
     * @return PaymentToTarife
     */
    public function setTarifId($tarifId)
    {
        $this->tarifId = $tarifId;

        return $this;
    }

    /**
     * Get tarifId
     *
     * @return integer
     */
    public function getTarifId()
    {
        return $this->tarifId;
    }
}
