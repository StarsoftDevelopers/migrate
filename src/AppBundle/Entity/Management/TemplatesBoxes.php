<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * TemplatesBoxes
 *
 * @ORM\Table(name="templates_boxes")
 * @ORM\Entity
 */
class TemplatesBoxes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="template_to_box", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $templateToBox;

    /**
     * @var integer
     *
     * @ORM\Column(name="template_id", type="integer", nullable=false)
     */
    private $templateId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="box", type="string", length=32, nullable=false)
     */
    private $box = '';

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", nullable=false)
     */
    private $position = 'fixed';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="display", type="boolean", nullable=false)
     */
    private $display = '1';



    /**
     * Get templateToBox
     *
     * @return integer
     */
    public function getTemplateToBox()
    {
        return $this->templateToBox;
    }

    /**
     * Set templateId
     *
     * @param integer $templateId
     *
     * @return TemplatesBoxes
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;

        return $this;
    }

    /**
     * Get templateId
     *
     * @return integer
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * Set box
     *
     * @param string $box
     *
     * @return TemplatesBoxes
     */
    public function setBox($box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return string
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return TemplatesBoxes
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return TemplatesBoxes
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set display
     *
     * @param boolean $display
     *
     * @return TemplatesBoxes
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display
     *
     * @return boolean
     */
    public function getDisplay()
    {
        return $this->display;
    }
}
