<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reseller
 *
 * @ORM\Table(name="reseller")
 * @ORM\Entity
 */
class Reseller
{
    /**
     * @var integer
     *
     * @ORM\Column(name="reseller_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $resellerId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="admin_domain", type="string", length=255, nullable=false)
     */
    private $adminDomain = '';

    /**
     * @var string
     *
     * @ORM\Column(name="help_domain", type="string", length=255, nullable=false)
     */
    private $helpDomain = '';

    /**
     * @var string
     *
     * @ORM\Column(name="login_url", type="string", length=255, nullable=false)
     */
    private $loginUrl = '';

    /**
     * @var string
     *
     * @ORM\Column(name="support_email", type="text", length=65535, nullable=false)
     */
    private $supportEmail;



    /**
     * Get resellerId
     *
     * @return integer
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Reseller
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set adminDomain
     *
     * @param string $adminDomain
     *
     * @return Reseller
     */
    public function setAdminDomain($adminDomain)
    {
        $this->adminDomain = $adminDomain;

        return $this;
    }

    /**
     * Get adminDomain
     *
     * @return string
     */
    public function getAdminDomain()
    {
        return $this->adminDomain;
    }

    /**
     * Set helpDomain
     *
     * @param string $helpDomain
     *
     * @return Reseller
     */
    public function setHelpDomain($helpDomain)
    {
        $this->helpDomain = $helpDomain;

        return $this;
    }

    /**
     * Get helpDomain
     *
     * @return string
     */
    public function getHelpDomain()
    {
        return $this->helpDomain;
    }

    /**
     * Set loginUrl
     *
     * @param string $loginUrl
     *
     * @return Reseller
     */
    public function setLoginUrl($loginUrl)
    {
        $this->loginUrl = $loginUrl;

        return $this;
    }

    /**
     * Get loginUrl
     *
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * Set supportEmail
     *
     * @param string $supportEmail
     *
     * @return Reseller
     */
    public function setSupportEmail($supportEmail)
    {
        $this->supportEmail = $supportEmail;

        return $this;
    }

    /**
     * Get supportEmail
     *
     * @return string
     */
    public function getSupportEmail()
    {
        return $this->supportEmail;
    }
}
