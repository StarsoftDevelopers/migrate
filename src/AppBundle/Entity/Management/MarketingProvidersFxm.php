<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarketingProvidersFxm
 *
 * @ORM\Table(name="marketing_providers_fxm")
 * @ORM\Entity
 */
class MarketingProvidersFxm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="provider_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $providerId;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip", type="text", length=65535, nullable=false)
     */
    private $tooltip;

    /**
     * @var integer
     *
     * @ORM\Column(name="startguthaben", type="integer", nullable=false)
     */
    private $startguthaben;



    /**
     * Get providerId
     *
     * @return integer
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return MarketingProvidersFxm
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set tooltip
     *
     * @param string $tooltip
     *
     * @return MarketingProvidersFxm
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    /**
     * Get tooltip
     *
     * @return string
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * Set startguthaben
     *
     * @param integer $startguthaben
     *
     * @return MarketingProvidersFxm
     */
    public function setStartguthaben($startguthaben)
    {
        $this->startguthaben = $startguthaben;

        return $this;
    }

    /**
     * Get startguthaben
     *
     * @return integer
     */
    public function getStartguthaben()
    {
        return $this->startguthaben;
    }
}
