<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * HelpFiles
 *
 * @ORM\Table(name="help_files")
 * @ORM\Entity
 */
class HelpFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="files_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $filesId;

    /**
     * @var string
     *
     * @ORM\Column(name="files_name", type="string", length=64, nullable=false)
     */
    private $filesName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="config_id", type="boolean", nullable=false)
     */
    private $configId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="help_id", type="smallint", nullable=false)
     */
    private $helpId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="video_languages", type="string", length=64, nullable=false)
     */
    private $videoLanguages = '';

    /**
     * @var string
     *
     * @ORM\Column(name="help_anchor", type="string", length=32, nullable=false)
     */
    private $helpAnchor = '';

    /**
     * @var string
     *
     * @ORM\Column(name="help_action", type="string", length=32, nullable=false)
     */
    private $helpAction = '';



    /**
     * Get filesId
     *
     * @return integer
     */
    public function getFilesId()
    {
        return $this->filesId;
    }

    /**
     * Set filesName
     *
     * @param string $filesName
     *
     * @return HelpFiles
     */
    public function setFilesName($filesName)
    {
        $this->filesName = $filesName;

        return $this;
    }

    /**
     * Get filesName
     *
     * @return string
     */
    public function getFilesName()
    {
        return $this->filesName;
    }

    /**
     * Set configId
     *
     * @param boolean $configId
     *
     * @return HelpFiles
     */
    public function setConfigId($configId)
    {
        $this->configId = $configId;

        return $this;
    }

    /**
     * Get configId
     *
     * @return boolean
     */
    public function getConfigId()
    {
        return $this->configId;
    }

    /**
     * Set helpId
     *
     * @param integer $helpId
     *
     * @return HelpFiles
     */
    public function setHelpId($helpId)
    {
        $this->helpId = $helpId;

        return $this;
    }

    /**
     * Get helpId
     *
     * @return integer
     */
    public function getHelpId()
    {
        return $this->helpId;
    }

    /**
     * Set videoLanguages
     *
     * @param string $videoLanguages
     *
     * @return HelpFiles
     */
    public function setVideoLanguages($videoLanguages)
    {
        $this->videoLanguages = $videoLanguages;

        return $this;
    }

    /**
     * Get videoLanguages
     *
     * @return string
     */
    public function getVideoLanguages()
    {
        return $this->videoLanguages;
    }

    /**
     * Set helpAnchor
     *
     * @param string $helpAnchor
     *
     * @return HelpFiles
     */
    public function setHelpAnchor($helpAnchor)
    {
        $this->helpAnchor = $helpAnchor;

        return $this;
    }

    /**
     * Get helpAnchor
     *
     * @return string
     */
    public function getHelpAnchor()
    {
        return $this->helpAnchor;
    }

    /**
     * Set helpAction
     *
     * @param string $helpAction
     *
     * @return HelpFiles
     */
    public function setHelpAction($helpAction)
    {
        $this->helpAction = $helpAction;

        return $this;
    }

    /**
     * Get helpAction
     *
     * @return string
     */
    public function getHelpAction()
    {
        return $this->helpAction;
    }
}
