<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResellerHistory
 *
 * @ORM\Table(name="reseller_history")
 * @ORM\Entity
 */
class ResellerHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ActionID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $actionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="Type", type="integer", nullable=false)
     */
    private $type = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="Module", type="string", length=64, nullable=false)
     */
    private $module = '';

    /**
     * @var string
     *
     * @ORM\Column(name="Function", type="string", length=64, nullable=false)
     */
    private $function = '';

    /**
     * @var string
     *
     * @ORM\Column(name="Message", type="text", length=65535, nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="Data", type="text", length=65535, nullable=true)
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="RemoteIP", type="string", length=16, nullable=false)
     */
    private $remoteip = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ResellerID", type="integer", nullable=false)
     */
    private $resellerid = '0';



    /**
     * Get actionid
     *
     * @return integer
     */
    public function getActionid()
    {
        return $this->actionid;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return ResellerHistory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set module
     *
     * @param string $module
     *
     * @return ResellerHistory
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set function
     *
     * @param string $function
     *
     * @return ResellerHistory
     */
    public function setFunction($function)
    {
        $this->function = $function;

        return $this;
    }

    /**
     * Get function
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return ResellerHistory
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return ResellerHistory
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ResellerHistory
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set remoteip
     *
     * @param string $remoteip
     *
     * @return ResellerHistory
     */
    public function setRemoteip($remoteip)
    {
        $this->remoteip = $remoteip;

        return $this;
    }

    /**
     * Get remoteip
     *
     * @return string
     */
    public function getRemoteip()
    {
        return $this->remoteip;
    }

    /**
     * Set resellerid
     *
     * @param integer $resellerid
     *
     * @return ResellerHistory
     */
    public function setResellerid($resellerid)
    {
        $this->resellerid = $resellerid;

        return $this;
    }

    /**
     * Get resellerid
     *
     * @return integer
     */
    public function getResellerid()
    {
        return $this->resellerid;
    }
}
