<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * LanguagesScriptsConstants
 *
 * @ORM\Table(name="languages_scripts_constants")
 * @ORM\Entity
 */
class LanguagesScriptsConstants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="values_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $valuesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="file_id", type="integer", nullable=false)
     */
    private $fileId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="values_name", type="text", length=65535, nullable=false)
     */
    private $valuesName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible = '1';



    /**
     * Get valuesId
     *
     * @return integer
     */
    public function getValuesId()
    {
        return $this->valuesId;
    }

    /**
     * Set fileId
     *
     * @param integer $fileId
     *
     * @return LanguagesScriptsConstants
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;

        return $this;
    }

    /**
     * Get fileId
     *
     * @return integer
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Set valuesName
     *
     * @param string $valuesName
     *
     * @return LanguagesScriptsConstants
     */
    public function setValuesName($valuesName)
    {
        $this->valuesName = $valuesName;

        return $this;
    }

    /**
     * Get valuesName
     *
     * @return string
     */
    public function getValuesName()
    {
        return $this->valuesName;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return LanguagesScriptsConstants
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
