<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserSave
 *
 * @ORM\Table(name="user_save", uniqueConstraints={@ORM\UniqueConstraint(name="user_name", columns={"user_name"})})
 * @ORM\Entity
 */
class UserSave
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=32, nullable=false)
     */
    private $userName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="user_password", type="string", length=32, nullable=false)
     */
    private $userPassword = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="user_level", type="integer", nullable=false)
     */
    private $userLevel = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="user_info", type="string", length=150, nullable=false)
     */
    private $userInfo = '';



    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return UserSave
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set userPassword
     *
     * @param string $userPassword
     *
     * @return UserSave
     */
    public function setUserPassword($userPassword)
    {
        $this->userPassword = $userPassword;

        return $this;
    }

    /**
     * Get userPassword
     *
     * @return string
     */
    public function getUserPassword()
    {
        return $this->userPassword;
    }

    /**
     * Set userLevel
     *
     * @param integer $userLevel
     *
     * @return UserSave
     */
    public function setUserLevel($userLevel)
    {
        $this->userLevel = $userLevel;

        return $this;
    }

    /**
     * Get userLevel
     *
     * @return integer
     */
    public function getUserLevel()
    {
        return $this->userLevel;
    }

    /**
     * Set userInfo
     *
     * @param string $userInfo
     *
     * @return UserSave
     */
    public function setUserInfo($userInfo)
    {
        $this->userInfo = $userInfo;

        return $this;
    }

    /**
     * Get userInfo
     *
     * @return string
     */
    public function getUserInfo()
    {
        return $this->userInfo;
    }
}
