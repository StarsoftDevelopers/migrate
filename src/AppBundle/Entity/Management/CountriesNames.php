<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountriesNames
 *
 * @ORM\Table(name="countries_names", indexes={@ORM\Index(name="IDX_COUNTRIES_NAME", columns={"language_id", "countries_name"})})
 * @ORM\Entity
 */
class CountriesNames
{
    /**
     * @var integer
     *
     * @ORM\Column(name="countries_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $countriesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="countries_name", type="string", length=64, nullable=false)
     */
    private $countriesName = 'enter name';



    /**
     * Set countriesId
     *
     * @param integer $countriesId
     *
     * @return CountriesNames
     */
    public function setCountriesId($countriesId)
    {
        $this->countriesId = $countriesId;

        return $this;
    }

    /**
     * Get countriesId
     *
     * @return integer
     */
    public function getCountriesId()
    {
        return $this->countriesId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return CountriesNames
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set countriesName
     *
     * @param string $countriesName
     *
     * @return CountriesNames
     */
    public function setCountriesName($countriesName)
    {
        $this->countriesName = $countriesName;

        return $this;
    }

    /**
     * Get countriesName
     *
     * @return string
     */
    public function getCountriesName()
    {
        return $this->countriesName;
    }
}
