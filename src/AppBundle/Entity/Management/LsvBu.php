<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * LsvBu
 *
 * @ORM\Table(name="lsv_bu")
 * @ORM\Entity
 */
class LsvBu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="values_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $valuesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languagesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="variable", type="text", length=65535, nullable=true)
     */
    private $variable;



    /**
     * Set valuesId
     *
     * @param integer $valuesId
     *
     * @return LsvBu
     */
    public function setValuesId($valuesId)
    {
        $this->valuesId = $valuesId;

        return $this;
    }

    /**
     * Get valuesId
     *
     * @return integer
     */
    public function getValuesId()
    {
        return $this->valuesId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return LsvBu
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set variable
     *
     * @param string $variable
     *
     * @return LsvBu
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * Get variable
     *
     * @return string
     */
    public function getVariable()
    {
        return $this->variable;
    }
}
