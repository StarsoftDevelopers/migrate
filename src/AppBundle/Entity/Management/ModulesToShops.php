<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModulesToShops
 *
 * @ORM\Table(name="modules_to_shops")
 * @ORM\Entity
 */
class ModulesToShops
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="module_id", type="integer", nullable=false)
     */
    private $moduleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Installed", type="datetime", nullable=false)
     */
    private $installed = '0000-00-00 00:00:00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status = '1';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moduleId
     *
     * @param integer $moduleId
     *
     * @return ModulesToShops
     */
    public function setModuleId($moduleId)
    {
        $this->moduleId = $moduleId;

        return $this;
    }

    /**
     * Get moduleId
     *
     * @return integer
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * Set shopId
     *
     * @param integer $shopId
     *
     * @return ModulesToShops
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set installed
     *
     * @param \DateTime $installed
     *
     * @return ModulesToShops
     */
    public function setInstalled($installed)
    {
        $this->installed = $installed;

        return $this;
    }

    /**
     * Get installed
     *
     * @return \DateTime
     */
    public function getInstalled()
    {
        return $this->installed;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ModulesToShops
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
