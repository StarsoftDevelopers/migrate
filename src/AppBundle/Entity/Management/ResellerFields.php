<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResellerFields
 *
 * @ORM\Table(name="reseller_fields")
 * @ORM\Entity
 */
class ResellerFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="FieldID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $fieldid;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=64, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="Desc", type="text", length=65535, nullable=true)
     */
    private $desc;

    /**
     * @var integer
     *
     * @ORM\Column(name="ResellerID", type="integer", nullable=false)
     */
    private $resellerid = '0';



    /**
     * Get fieldid
     *
     * @return integer
     */
    public function getFieldid()
    {
        return $this->fieldid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ResellerFields
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return ResellerFields
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set resellerid
     *
     * @param integer $resellerid
     *
     * @return ResellerFields
     */
    public function setResellerid($resellerid)
    {
        $this->resellerid = $resellerid;

        return $this;
    }

    /**
     * Get resellerid
     *
     * @return integer
     */
    public function getResellerid()
    {
        return $this->resellerid;
    }
}
