<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountriesNamesToIso
 *
 * @ORM\Table(name="countries_names_to_iso")
 * @ORM\Entity
 */
class CountriesNamesToIso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="iso_id", type="integer", nullable=false)
     */
    private $isoId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="countries_id", type="integer", nullable=false)
     */
    private $countriesId = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isoId
     *
     * @param integer $isoId
     *
     * @return CountriesNamesToIso
     */
    public function setIsoId($isoId)
    {
        $this->isoId = $isoId;

        return $this;
    }

    /**
     * Get isoId
     *
     * @return integer
     */
    public function getIsoId()
    {
        return $this->isoId;
    }

    /**
     * Set countriesId
     *
     * @param integer $countriesId
     *
     * @return CountriesNamesToIso
     */
    public function setCountriesId($countriesId)
    {
        $this->countriesId = $countriesId;

        return $this;
    }

    /**
     * Get countriesId
     *
     * @return integer
     */
    public function getCountriesId()
    {
        return $this->countriesId;
    }
}
