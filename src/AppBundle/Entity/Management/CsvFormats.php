<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * CsvFormats
 *
 * @ORM\Table(name="csv_formats")
 * @ORM\Entity
 */
class CsvFormats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="csv_format_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $csvFormatId;

    /**
     * @var string
     *
     * @ORM\Column(name="formats_name", type="string", length=32, nullable=false)
     */
    private $formatsName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="formats_separator_id", type="boolean", nullable=false)
     */
    private $formatsSeparatorId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="formats_type", type="boolean", nullable=false)
     */
    private $formatsType = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="formats_language", type="boolean", nullable=false)
     */
    private $formatsLanguage = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="columns_fieldseparator", type="string", length=1, nullable=false)
     */
    private $columnsFieldseparator = '';



    /**
     * Get csvFormatId
     *
     * @return integer
     */
    public function getCsvFormatId()
    {
        return $this->csvFormatId;
    }

    /**
     * Set formatsName
     *
     * @param string $formatsName
     *
     * @return CsvFormats
     */
    public function setFormatsName($formatsName)
    {
        $this->formatsName = $formatsName;

        return $this;
    }

    /**
     * Get formatsName
     *
     * @return string
     */
    public function getFormatsName()
    {
        return $this->formatsName;
    }

    /**
     * Set formatsSeparatorId
     *
     * @param boolean $formatsSeparatorId
     *
     * @return CsvFormats
     */
    public function setFormatsSeparatorId($formatsSeparatorId)
    {
        $this->formatsSeparatorId = $formatsSeparatorId;

        return $this;
    }

    /**
     * Get formatsSeparatorId
     *
     * @return boolean
     */
    public function getFormatsSeparatorId()
    {
        return $this->formatsSeparatorId;
    }

    /**
     * Set formatsType
     *
     * @param boolean $formatsType
     *
     * @return CsvFormats
     */
    public function setFormatsType($formatsType)
    {
        $this->formatsType = $formatsType;

        return $this;
    }

    /**
     * Get formatsType
     *
     * @return boolean
     */
    public function getFormatsType()
    {
        return $this->formatsType;
    }

    /**
     * Set formatsLanguage
     *
     * @param boolean $formatsLanguage
     *
     * @return CsvFormats
     */
    public function setFormatsLanguage($formatsLanguage)
    {
        $this->formatsLanguage = $formatsLanguage;

        return $this;
    }

    /**
     * Get formatsLanguage
     *
     * @return boolean
     */
    public function getFormatsLanguage()
    {
        return $this->formatsLanguage;
    }

    /**
     * Set columnsFieldseparator
     *
     * @param string $columnsFieldseparator
     *
     * @return CsvFormats
     */
    public function setColumnsFieldseparator($columnsFieldseparator)
    {
        $this->columnsFieldseparator = $columnsFieldseparator;

        return $this;
    }

    /**
     * Get columnsFieldseparator
     *
     * @return string
     */
    public function getColumnsFieldseparator()
    {
        return $this->columnsFieldseparator;
    }
}
