<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tarife
 *
 * @ORM\Table(name="tarife")
 * @ORM\Entity
 */
class Tarife
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tarif_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tarifId;

    /**
     * @var string
     *
     * @ORM\Column(name="tarifname", type="string", length=32, nullable=false)
     */
    private $tarifname;

    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $cost;



    /**
     * Get tarifId
     *
     * @return integer
     */
    public function getTarifId()
    {
        return $this->tarifId;
    }

    /**
     * Set tarifname
     *
     * @param string $tarifname
     *
     * @return Tarife
     */
    public function setTarifname($tarifname)
    {
        $this->tarifname = $tarifname;

        return $this;
    }

    /**
     * Get tarifname
     *
     * @return string
     */
    public function getTarifname()
    {
        return $this->tarifname;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return Tarife
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }
}
