<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * DubliStatistik
 *
 * @ORM\Table(name="dubli_statistik", uniqueConstraints={@ORM\UniqueConstraint(name="shop_id", columns={"shop_id"})})
 * @ORM\Entity
 */
class DubliStatistik
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_id", type="string", length=10, nullable=false)
     */
    private $shopId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="shopname", type="string", length=60, nullable=false)
     */
    private $shopname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="shoplogo", type="string", length=255, nullable=false)
     */
    private $shoplogo = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sprachen", type="string", length=50, nullable=false)
     */
    private $sprachen = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="produkt_aktiv", type="integer", nullable=false)
     */
    private $produktAktiv = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="versandarten", type="integer", nullable=false)
     */
    private $versandarten = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="bestellungen", type="integer", nullable=false)
     */
    private $bestellungen = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=false)
     */
    private $email = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="laender_id", type="integer", nullable=false)
     */
    private $laenderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="dublicat", type="integer", nullable=false)
     */
    private $dublicat = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="widerruf", type="integer", nullable=false)
     */
    private $widerruf = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shopId
     *
     * @param string $shopId
     *
     * @return DubliStatistik
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * Get shopId
     *
     * @return string
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set shopname
     *
     * @param string $shopname
     *
     * @return DubliStatistik
     */
    public function setShopname($shopname)
    {
        $this->shopname = $shopname;

        return $this;
    }

    /**
     * Get shopname
     *
     * @return string
     */
    public function getShopname()
    {
        return $this->shopname;
    }

    /**
     * Set shoplogo
     *
     * @param string $shoplogo
     *
     * @return DubliStatistik
     */
    public function setShoplogo($shoplogo)
    {
        $this->shoplogo = $shoplogo;

        return $this;
    }

    /**
     * Get shoplogo
     *
     * @return string
     */
    public function getShoplogo()
    {
        return $this->shoplogo;
    }

    /**
     * Set sprachen
     *
     * @param string $sprachen
     *
     * @return DubliStatistik
     */
    public function setSprachen($sprachen)
    {
        $this->sprachen = $sprachen;

        return $this;
    }

    /**
     * Get sprachen
     *
     * @return string
     */
    public function getSprachen()
    {
        return $this->sprachen;
    }

    /**
     * Set produktAktiv
     *
     * @param integer $produktAktiv
     *
     * @return DubliStatistik
     */
    public function setProduktAktiv($produktAktiv)
    {
        $this->produktAktiv = $produktAktiv;

        return $this;
    }

    /**
     * Get produktAktiv
     *
     * @return integer
     */
    public function getProduktAktiv()
    {
        return $this->produktAktiv;
    }

    /**
     * Set versandarten
     *
     * @param integer $versandarten
     *
     * @return DubliStatistik
     */
    public function setVersandarten($versandarten)
    {
        $this->versandarten = $versandarten;

        return $this;
    }

    /**
     * Get versandarten
     *
     * @return integer
     */
    public function getVersandarten()
    {
        return $this->versandarten;
    }

    /**
     * Set bestellungen
     *
     * @param integer $bestellungen
     *
     * @return DubliStatistik
     */
    public function setBestellungen($bestellungen)
    {
        $this->bestellungen = $bestellungen;

        return $this;
    }

    /**
     * Get bestellungen
     *
     * @return integer
     */
    public function getBestellungen()
    {
        return $this->bestellungen;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return DubliStatistik
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set laenderId
     *
     * @param integer $laenderId
     *
     * @return DubliStatistik
     */
    public function setLaenderId($laenderId)
    {
        $this->laenderId = $laenderId;

        return $this;
    }

    /**
     * Get laenderId
     *
     * @return integer
     */
    public function getLaenderId()
    {
        return $this->laenderId;
    }

    /**
     * Set dublicat
     *
     * @param integer $dublicat
     *
     * @return DubliStatistik
     */
    public function setDublicat($dublicat)
    {
        $this->dublicat = $dublicat;

        return $this;
    }

    /**
     * Get dublicat
     *
     * @return integer
     */
    public function getDublicat()
    {
        return $this->dublicat;
    }

    /**
     * Set widerruf
     *
     * @param integer $widerruf
     *
     * @return DubliStatistik
     */
    public function setWiderruf($widerruf)
    {
        $this->widerruf = $widerruf;

        return $this;
    }

    /**
     * Get widerruf
     *
     * @return integer
     */
    public function getWiderruf()
    {
        return $this->widerruf;
    }
}
