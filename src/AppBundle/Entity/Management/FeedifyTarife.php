<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeedifyTarife
 *
 * @ORM\Table(name="feedify_tarife")
 * @ORM\Entity
 */
class FeedifyTarife
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tarif_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tarifId;

    /**
     * @var string
     *
     * @ORM\Column(name="tarifname", type="string", length=32, nullable=false)
     */
    private $tarifname;

    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $cost;

    /**
     * @var integer
     *
     * @ORM\Column(name="klicks_inclusive", type="integer", nullable=false)
     */
    private $klicksInclusive;

    /**
     * @var string
     *
     * @ORM\Column(name="cost_per_klick", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $costPerKlick;



    /**
     * Get tarifId
     *
     * @return integer
     */
    public function getTarifId()
    {
        return $this->tarifId;
    }

    /**
     * Set tarifname
     *
     * @param string $tarifname
     *
     * @return FeedifyTarife
     */
    public function setTarifname($tarifname)
    {
        $this->tarifname = $tarifname;

        return $this;
    }

    /**
     * Get tarifname
     *
     * @return string
     */
    public function getTarifname()
    {
        return $this->tarifname;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return FeedifyTarife
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set klicksInclusive
     *
     * @param integer $klicksInclusive
     *
     * @return FeedifyTarife
     */
    public function setKlicksInclusive($klicksInclusive)
    {
        $this->klicksInclusive = $klicksInclusive;

        return $this;
    }

    /**
     * Get klicksInclusive
     *
     * @return integer
     */
    public function getKlicksInclusive()
    {
        return $this->klicksInclusive;
    }

    /**
     * Set costPerKlick
     *
     * @param string $costPerKlick
     *
     * @return FeedifyTarife
     */
    public function setCostPerKlick($costPerKlick)
    {
        $this->costPerKlick = $costPerKlick;

        return $this;
    }

    /**
     * Get costPerKlick
     *
     * @return string
     */
    public function getCostPerKlick()
    {
        return $this->costPerKlick;
    }
}
