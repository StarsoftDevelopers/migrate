<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * CsvTemplates
 *
 * @ORM\Table(name="csv_templates")
 * @ORM\Entity
 */
class CsvTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="csv_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $csvId;

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     */
    private $languagesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="csv_name", type="text", length=65535, nullable=false)
     */
    private $csvName;

    /**
     * @var string
     *
     * @ORM\Column(name="csv_group", type="string", length=50, nullable=false)
     */
    private $csvGroup = '';

    /**
     * @var string
     *
     * @ORM\Column(name="csv_type", type="string", length=5, nullable=false)
     */
    private $csvType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="csv_delimiter", type="text", length=65535, nullable=false)
     */
    private $csvDelimiter;

    /**
     * @var string
     *
     * @ORM\Column(name="csv_config", type="text", length=65535, nullable=false)
     */
    private $csvConfig;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="csv_added", type="datetime", nullable=false)
     */
    private $csvAdded = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="csv_changed", type="datetime", nullable=false)
     */
    private $csvChanged = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="csv_date_from", type="date", nullable=true)
     */
    private $csvDateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="csv_date_to", type="date", nullable=true)
     */
    private $csvDateTo;



    /**
     * Get csvId
     *
     * @return integer
     */
    public function getCsvId()
    {
        return $this->csvId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return CsvTemplates
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set csvName
     *
     * @param string $csvName
     *
     * @return CsvTemplates
     */
    public function setCsvName($csvName)
    {
        $this->csvName = $csvName;

        return $this;
    }

    /**
     * Get csvName
     *
     * @return string
     */
    public function getCsvName()
    {
        return $this->csvName;
    }

    /**
     * Set csvGroup
     *
     * @param string $csvGroup
     *
     * @return CsvTemplates
     */
    public function setCsvGroup($csvGroup)
    {
        $this->csvGroup = $csvGroup;

        return $this;
    }

    /**
     * Get csvGroup
     *
     * @return string
     */
    public function getCsvGroup()
    {
        return $this->csvGroup;
    }

    /**
     * Set csvType
     *
     * @param string $csvType
     *
     * @return CsvTemplates
     */
    public function setCsvType($csvType)
    {
        $this->csvType = $csvType;

        return $this;
    }

    /**
     * Get csvType
     *
     * @return string
     */
    public function getCsvType()
    {
        return $this->csvType;
    }

    /**
     * Set csvDelimiter
     *
     * @param string $csvDelimiter
     *
     * @return CsvTemplates
     */
    public function setCsvDelimiter($csvDelimiter)
    {
        $this->csvDelimiter = $csvDelimiter;

        return $this;
    }

    /**
     * Get csvDelimiter
     *
     * @return string
     */
    public function getCsvDelimiter()
    {
        return $this->csvDelimiter;
    }

    /**
     * Set csvConfig
     *
     * @param string $csvConfig
     *
     * @return CsvTemplates
     */
    public function setCsvConfig($csvConfig)
    {
        $this->csvConfig = $csvConfig;

        return $this;
    }

    /**
     * Get csvConfig
     *
     * @return string
     */
    public function getCsvConfig()
    {
        return $this->csvConfig;
    }

    /**
     * Set csvAdded
     *
     * @param \DateTime $csvAdded
     *
     * @return CsvTemplates
     */
    public function setCsvAdded($csvAdded)
    {
        $this->csvAdded = $csvAdded;

        return $this;
    }

    /**
     * Get csvAdded
     *
     * @return \DateTime
     */
    public function getCsvAdded()
    {
        return $this->csvAdded;
    }

    /**
     * Set csvChanged
     *
     * @param \DateTime $csvChanged
     *
     * @return CsvTemplates
     */
    public function setCsvChanged($csvChanged)
    {
        $this->csvChanged = $csvChanged;

        return $this;
    }

    /**
     * Get csvChanged
     *
     * @return \DateTime
     */
    public function getCsvChanged()
    {
        return $this->csvChanged;
    }

    /**
     * Set csvDateFrom
     *
     * @param \DateTime $csvDateFrom
     *
     * @return CsvTemplates
     */
    public function setCsvDateFrom($csvDateFrom)
    {
        $this->csvDateFrom = $csvDateFrom;

        return $this;
    }

    /**
     * Get csvDateFrom
     *
     * @return \DateTime
     */
    public function getCsvDateFrom()
    {
        return $this->csvDateFrom;
    }

    /**
     * Set csvDateTo
     *
     * @param \DateTime $csvDateTo
     *
     * @return CsvTemplates
     */
    public function setCsvDateTo($csvDateTo)
    {
        $this->csvDateTo = $csvDateTo;

        return $this;
    }

    /**
     * Get csvDateTo
     *
     * @return \DateTime
     */
    public function getCsvDateTo()
    {
        return $this->csvDateTo;
    }
}
