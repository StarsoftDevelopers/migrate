<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Continent
 *
 * @ORM\Table(name="continent")
 * @ORM\Entity
 */
class Continent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="continent_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $continentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="continent_name", type="string", length=30, nullable=false)
     */
    private $continentName = '';



    /**
     * Set continentId
     *
     * @param integer $continentId
     *
     * @return Continent
     */
    public function setContinentId($continentId)
    {
        $this->continentId = $continentId;

        return $this;
    }

    /**
     * Get continentId
     *
     * @return integer
     */
    public function getContinentId()
    {
        return $this->continentId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return Continent
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set continentName
     *
     * @param string $continentName
     *
     * @return Continent
     */
    public function setContinentName($continentName)
    {
        $this->continentName = $continentName;

        return $this;
    }

    /**
     * Get continentName
     *
     * @return string
     */
    public function getContinentName()
    {
        return $this->continentName;
    }
}
