<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shops
 *
 * @ORM\Table(name="shops")
 * @ORM\Entity
 */
class Shops
{
    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $shopId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="shoptyp", type="boolean", nullable=false)
     */
    private $shoptyp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="tarif", type="integer", nullable=false)
     */
    private $tarif = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="shop_name", type="string", length=64, nullable=false)
     */
    private $shopName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_server", type="string", length=64, nullable=false)
     */
    private $dbServer = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_user", type="string", length=64, nullable=false)
     */
    private $dbUser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_passwd", type="string", length=64, nullable=false)
     */
    private $dbPasswd = '';

    /**
     * @var string
     *
     * @ORM\Column(name="db_name", type="string", length=64, nullable=false)
     */
    private $dbName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="confixx_login", type="string", length=8, nullable=false)
     */
    private $confixxLogin = '';

    /**
     * @var string
     *
     * @ORM\Column(name="confixx_pw", type="string", length=16, nullable=false)
     */
    private $confixxPw = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="reseller_id", type="integer", nullable=true)
     */
    private $resellerId = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="partner_id1", type="integer", nullable=false)
     */
    private $partnerId1 = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="partner_id2", type="integer", nullable=false)
     */
    private $partnerId2 = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ssl", type="boolean", nullable=false)
     */
    private $ssl = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="sslurl", type="string", length=255, nullable=true)
     */
    private $sslurl = 'ssl-security.org';

    /**
     * @var boolean
     *
     * @ORM\Column(name="process_priority", type="boolean", nullable=false)
     */
    private $processPriority = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled_image_hosting", type="boolean", nullable=false)
     */
    private $enabledImageHosting = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="changed", type="boolean", nullable=false)
     */
    private $changed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="PRODUCTSEXPORT_TO_HANDELO", type="boolean", nullable=false)
     */
    private $productsexportToHandelo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled_foxmarkets", type="boolean", nullable=false)
     */
    private $enabledFoxmarkets = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="quickstart_complete", type="string", length=255, nullable=true)
     */
    private $quickstartComplete = '{"status":0}';

    /**
     * @var boolean
     *
     * @ORM\Column(name="feedify", type="boolean", nullable=false)
     */
    private $feedify = '0';



    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Shops
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set shoptyp
     *
     * @param boolean $shoptyp
     *
     * @return Shops
     */
    public function setShoptyp($shoptyp)
    {
        $this->shoptyp = $shoptyp;

        return $this;
    }

    /**
     * Get shoptyp
     *
     * @return boolean
     */
    public function getShoptyp()
    {
        return $this->shoptyp;
    }

    /**
     * Set tarif
     *
     * @param integer $tarif
     *
     * @return Shops
     */
    public function setTarif($tarif)
    {
        $this->tarif = $tarif;

        return $this;
    }

    /**
     * Get tarif
     *
     * @return integer
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * Set shopName
     *
     * @param string $shopName
     *
     * @return Shops
     */
    public function setShopName($shopName)
    {
        $this->shopName = $shopName;

        return $this;
    }

    /**
     * Get shopName
     *
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * Set dbServer
     *
     * @param string $dbServer
     *
     * @return Shops
     */
    public function setDbServer($dbServer)
    {
        $this->dbServer = $dbServer;

        return $this;
    }

    /**
     * Get dbServer
     *
     * @return string
     */
    public function getDbServer()
    {
        return $this->dbServer;
    }

    /**
     * Set dbUser
     *
     * @param string $dbUser
     *
     * @return Shops
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;

        return $this;
    }

    /**
     * Get dbUser
     *
     * @return string
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * Set dbPasswd
     *
     * @param string $dbPasswd
     *
     * @return Shops
     */
    public function setDbPasswd($dbPasswd)
    {
        $this->dbPasswd = $dbPasswd;

        return $this;
    }

    /**
     * Get dbPasswd
     *
     * @return string
     */
    public function getDbPasswd()
    {
        return $this->dbPasswd;
    }

    /**
     * Set dbName
     *
     * @param string $dbName
     *
     * @return Shops
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;

        return $this;
    }

    /**
     * Get dbName
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * Set confixxLogin
     *
     * @param string $confixxLogin
     *
     * @return Shops
     */
    public function setConfixxLogin($confixxLogin)
    {
        $this->confixxLogin = $confixxLogin;

        return $this;
    }

    /**
     * Get confixxLogin
     *
     * @return string
     */
    public function getConfixxLogin()
    {
        return $this->confixxLogin;
    }

    /**
     * Set confixxPw
     *
     * @param string $confixxPw
     *
     * @return Shops
     */
    public function setConfixxPw($confixxPw)
    {
        $this->confixxPw = $confixxPw;

        return $this;
    }

    /**
     * Get confixxPw
     *
     * @return string
     */
    public function getConfixxPw()
    {
        return $this->confixxPw;
    }

    /**
     * Set resellerId
     *
     * @param integer $resellerId
     *
     * @return Shops
     */
    public function setResellerId($resellerId)
    {
        $this->resellerId = $resellerId;

        return $this;
    }

    /**
     * Get resellerId
     *
     * @return integer
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * Set partnerId1
     *
     * @param integer $partnerId1
     *
     * @return Shops
     */
    public function setPartnerId1($partnerId1)
    {
        $this->partnerId1 = $partnerId1;

        return $this;
    }

    /**
     * Get partnerId1
     *
     * @return integer
     */
    public function getPartnerId1()
    {
        return $this->partnerId1;
    }

    /**
     * Set partnerId2
     *
     * @param integer $partnerId2
     *
     * @return Shops
     */
    public function setPartnerId2($partnerId2)
    {
        $this->partnerId2 = $partnerId2;

        return $this;
    }

    /**
     * Get partnerId2
     *
     * @return integer
     */
    public function getPartnerId2()
    {
        return $this->partnerId2;
    }

    /**
     * Set ssl
     *
     * @param boolean $ssl
     *
     * @return Shops
     */
    public function setSsl($ssl)
    {
        $this->ssl = $ssl;

        return $this;
    }

    /**
     * Get ssl
     *
     * @return boolean
     */
    public function getSsl()
    {
        return $this->ssl;
    }

    /**
     * Set sslurl
     *
     * @param string $sslurl
     *
     * @return Shops
     */
    public function setSslurl($sslurl)
    {
        $this->sslurl = $sslurl;

        return $this;
    }

    /**
     * Get sslurl
     *
     * @return string
     */
    public function getSslurl()
    {
        return $this->sslurl;
    }

    /**
     * Set processPriority
     *
     * @param boolean $processPriority
     *
     * @return Shops
     */
    public function setProcessPriority($processPriority)
    {
        $this->processPriority = $processPriority;

        return $this;
    }

    /**
     * Get processPriority
     *
     * @return boolean
     */
    public function getProcessPriority()
    {
        return $this->processPriority;
    }

    /**
     * Set enabledImageHosting
     *
     * @param boolean $enabledImageHosting
     *
     * @return Shops
     */
    public function setEnabledImageHosting($enabledImageHosting)
    {
        $this->enabledImageHosting = $enabledImageHosting;

        return $this;
    }

    /**
     * Get enabledImageHosting
     *
     * @return boolean
     */
    public function getEnabledImageHosting()
    {
        return $this->enabledImageHosting;
    }

    /**
     * Set changed
     *
     * @param boolean $changed
     *
     * @return Shops
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * Get changed
     *
     * @return boolean
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * Set productsexportToHandelo
     *
     * @param boolean $productsexportToHandelo
     *
     * @return Shops
     */
    public function setProductsexportToHandelo($productsexportToHandelo)
    {
        $this->productsexportToHandelo = $productsexportToHandelo;

        return $this;
    }

    /**
     * Get productsexportToHandelo
     *
     * @return boolean
     */
    public function getProductsexportToHandelo()
    {
        return $this->productsexportToHandelo;
    }

    /**
     * Set enabledFoxmarkets
     *
     * @param boolean $enabledFoxmarkets
     *
     * @return Shops
     */
    public function setEnabledFoxmarkets($enabledFoxmarkets)
    {
        $this->enabledFoxmarkets = $enabledFoxmarkets;

        return $this;
    }

    /**
     * Get enabledFoxmarkets
     *
     * @return boolean
     */
    public function getEnabledFoxmarkets()
    {
        return $this->enabledFoxmarkets;
    }

    /**
     * Set quickstartComplete
     *
     * @param string $quickstartComplete
     *
     * @return Shops
     */
    public function setQuickstartComplete($quickstartComplete)
    {
        $this->quickstartComplete = $quickstartComplete;

        return $this;
    }

    /**
     * Get quickstartComplete
     *
     * @return string
     */
    public function getQuickstartComplete()
    {
        return $this->quickstartComplete;
    }

    /**
     * Set feedify
     *
     * @param boolean $feedify
     *
     * @return Shops
     */
    public function setFeedify($feedify)
    {
        $this->feedify = $feedify;

        return $this;
    }

    /**
     * Get feedify
     *
     * @return boolean
     */
    public function getFeedify()
    {
        return $this->feedify;
    }
}
