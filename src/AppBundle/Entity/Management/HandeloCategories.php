<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * HandeloCategories
 *
 * @ORM\Table(name="handelo_categories", indexes={@ORM\Index(name="categories_id", columns={"categories_id", "categories_name"}), @ORM\Index(name="parent_id", columns={"parent_id"})})
 * @ORM\Entity
 */
class HandeloCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="categories_id", type="integer", nullable=false)
     */
    private $categoriesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="categories_image", type="string", length=255, nullable=false)
     */
    private $categoriesImage = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="categories_name", type="string", length=120, nullable=false)
     */
    private $categoriesName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="categories_status", type="boolean", nullable=false)
     */
    private $categoriesStatus = '1';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoriesId
     *
     * @param integer $categoriesId
     *
     * @return HandeloCategories
     */
    public function setCategoriesId($categoriesId)
    {
        $this->categoriesId = $categoriesId;

        return $this;
    }

    /**
     * Get categoriesId
     *
     * @return integer
     */
    public function getCategoriesId()
    {
        return $this->categoriesId;
    }

    /**
     * Set categoriesImage
     *
     * @param string $categoriesImage
     *
     * @return HandeloCategories
     */
    public function setCategoriesImage($categoriesImage)
    {
        $this->categoriesImage = $categoriesImage;

        return $this;
    }

    /**
     * Get categoriesImage
     *
     * @return string
     */
    public function getCategoriesImage()
    {
        return $this->categoriesImage;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return HandeloCategories
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set categoriesName
     *
     * @param string $categoriesName
     *
     * @return HandeloCategories
     */
    public function setCategoriesName($categoriesName)
    {
        $this->categoriesName = $categoriesName;

        return $this;
    }

    /**
     * Get categoriesName
     *
     * @return string
     */
    public function getCategoriesName()
    {
        return $this->categoriesName;
    }

    /**
     * Set categoriesStatus
     *
     * @param boolean $categoriesStatus
     *
     * @return HandeloCategories
     */
    public function setCategoriesStatus($categoriesStatus)
    {
        $this->categoriesStatus = $categoriesStatus;

        return $this;
    }

    /**
     * Get categoriesStatus
     *
     * @return boolean
     */
    public function getCategoriesStatus()
    {
        return $this->categoriesStatus;
    }
}
