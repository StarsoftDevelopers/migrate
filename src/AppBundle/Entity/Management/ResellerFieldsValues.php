<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResellerFieldsValues
 *
 * @ORM\Table(name="reseller_fields_values")
 * @ORM\Entity
 */
class ResellerFieldsValues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="FieldID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $fieldid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ShopID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $shopid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="Value", type="text", length=65535, nullable=true)
     */
    private $value;



    /**
     * Set fieldid
     *
     * @param integer $fieldid
     *
     * @return ResellerFieldsValues
     */
    public function setFieldid($fieldid)
    {
        $this->fieldid = $fieldid;

        return $this;
    }

    /**
     * Get fieldid
     *
     * @return integer
     */
    public function getFieldid()
    {
        return $this->fieldid;
    }

    /**
     * Set shopid
     *
     * @param integer $shopid
     *
     * @return ResellerFieldsValues
     */
    public function setShopid($shopid)
    {
        $this->shopid = $shopid;

        return $this;
    }

    /**
     * Get shopid
     *
     * @return integer
     */
    public function getShopid()
    {
        return $this->shopid;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ResellerFieldsValues
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
