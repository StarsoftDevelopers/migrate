<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShopAnlegeLog
 *
 * @ORM\Table(name="shop_anlege_log", uniqueConstraints={@ORM\UniqueConstraint(name="standardshop_id", columns={"shop_name"})})
 * @ORM\Entity
 */
class ShopAnlegeLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_name", type="string", length=64, nullable=false)
     */
    private $shopName;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_user", type="string", length=5, nullable=false)
     */
    private $shopUser;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_pw", type="string", length=64, nullable=false)
     */
    private $shopPw;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="datetime", nullable=false)
     */
    private $datum = 'CURRENT_TIMESTAMP';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shopName
     *
     * @param string $shopName
     *
     * @return ShopAnlegeLog
     */
    public function setShopName($shopName)
    {
        $this->shopName = $shopName;

        return $this;
    }

    /**
     * Get shopName
     *
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * Set shopUser
     *
     * @param string $shopUser
     *
     * @return ShopAnlegeLog
     */
    public function setShopUser($shopUser)
    {
        $this->shopUser = $shopUser;

        return $this;
    }

    /**
     * Get shopUser
     *
     * @return string
     */
    public function getShopUser()
    {
        return $this->shopUser;
    }

    /**
     * Set shopPw
     *
     * @param string $shopPw
     *
     * @return ShopAnlegeLog
     */
    public function setShopPw($shopPw)
    {
        $this->shopPw = $shopPw;

        return $this;
    }

    /**
     * Get shopPw
     *
     * @return string
     */
    public function getShopPw()
    {
        return $this->shopPw;
    }

    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return ShopAnlegeLog
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }
}
