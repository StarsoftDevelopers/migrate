<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeedifyUsershop
 *
 * @ORM\Table(name="feedify_usershop", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_F4FEC47FF132696E", columns={"userid"})})
 * @ORM\Entity
 */
class FeedifyUsershop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="userid", type="string", length=255, nullable=false)
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255, nullable=false)
     */
    private $role;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=255, nullable=false)
     */
    private $secret;

    /**
     * @var string
     *
     * @ORM\Column(name="bridgeUrl", type="string", length=255, nullable=false)
     */
    private $bridgeurl;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var array
     *
     * @ORM\Column(name="possibleConfig", type="json_array", nullable=false)
     */
    private $possibleconfig;

    /**
     * @var array
     *
     * @ORM\Column(name="actualConfig", type="json_array", nullable=false)
     */
    private $actualconfig;

    /**
     * @var array
     *
     * @ORM\Column(name="possibleFields", type="json_array", nullable=false)
     */
    private $possiblefields;

    /**
     * @var array
     *
     * @ORM\Column(name="actualFields", type="json_array", nullable=false)
     */
    private $actualfields;

    /**
     * @var string
     *
     * @ORM\Column(name="urlParams", type="string", length=255, nullable=false)
     */
    private $urlparams;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param string $userid
     *
     * @return FeedifyUsershop
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return string
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return FeedifyUsershop
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return FeedifyUsershop
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return FeedifyUsershop
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return FeedifyUsershop
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set secret
     *
     * @param string $secret
     *
     * @return FeedifyUsershop
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set bridgeurl
     *
     * @param string $bridgeurl
     *
     * @return FeedifyUsershop
     */
    public function setBridgeurl($bridgeurl)
    {
        $this->bridgeurl = $bridgeurl;

        return $this;
    }

    /**
     * Get bridgeurl
     *
     * @return string
     */
    public function getBridgeurl()
    {
        return $this->bridgeurl;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FeedifyUsershop
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set possibleconfig
     *
     * @param array $possibleconfig
     *
     * @return FeedifyUsershop
     */
    public function setPossibleconfig($possibleconfig)
    {
        $this->possibleconfig = $possibleconfig;

        return $this;
    }

    /**
     * Get possibleconfig
     *
     * @return array
     */
    public function getPossibleconfig()
    {
        return $this->possibleconfig;
    }

    /**
     * Set actualconfig
     *
     * @param array $actualconfig
     *
     * @return FeedifyUsershop
     */
    public function setActualconfig($actualconfig)
    {
        $this->actualconfig = $actualconfig;

        return $this;
    }

    /**
     * Get actualconfig
     *
     * @return array
     */
    public function getActualconfig()
    {
        return $this->actualconfig;
    }

    /**
     * Set possiblefields
     *
     * @param array $possiblefields
     *
     * @return FeedifyUsershop
     */
    public function setPossiblefields($possiblefields)
    {
        $this->possiblefields = $possiblefields;

        return $this;
    }

    /**
     * Get possiblefields
     *
     * @return array
     */
    public function getPossiblefields()
    {
        return $this->possiblefields;
    }

    /**
     * Set actualfields
     *
     * @param array $actualfields
     *
     * @return FeedifyUsershop
     */
    public function setActualfields($actualfields)
    {
        $this->actualfields = $actualfields;

        return $this;
    }

    /**
     * Get actualfields
     *
     * @return array
     */
    public function getActualfields()
    {
        return $this->actualfields;
    }

    /**
     * Set urlparams
     *
     * @param string $urlparams
     *
     * @return FeedifyUsershop
     */
    public function setUrlparams($urlparams)
    {
        $this->urlparams = $urlparams;

        return $this;
    }

    /**
     * Get urlparams
     *
     * @return string
     */
    public function getUrlparams()
    {
        return $this->urlparams;
    }
}
