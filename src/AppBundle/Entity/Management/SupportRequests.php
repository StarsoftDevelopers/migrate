<?php

namespace AppBundle\Entity\Management;

use Doctrine\ORM\Mapping as ORM;

/**
 * SupportRequests
 *
 * @ORM\Table(name="support_requests")
 * @ORM\Entity
 */
class SupportRequests
{
    /**
     * @var integer
     *
     * @ORM\Column(name="request_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $requestId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer", nullable=false)
     */
    private $shopId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="request_message", type="text", length=65535, nullable=false)
     */
    private $requestMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="request_date", type="datetime", nullable=false)
     */
    private $requestDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person", type="string", length=64, nullable=true)
     */
    private $contactPerson;



    /**
     * Get requestId
     *
     * @return integer
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return SupportRequests
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set shopId
     *
     * @param integer $shopId
     *
     * @return SupportRequests
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;

        return $this;
    }

    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set requestMessage
     *
     * @param string $requestMessage
     *
     * @return SupportRequests
     */
    public function setRequestMessage($requestMessage)
    {
        $this->requestMessage = $requestMessage;

        return $this;
    }

    /**
     * Get requestMessage
     *
     * @return string
     */
    public function getRequestMessage()
    {
        return $this->requestMessage;
    }

    /**
     * Set requestDate
     *
     * @param \DateTime $requestDate
     *
     * @return SupportRequests
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    /**
     * Get requestDate
     *
     * @return \DateTime
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     *
     * @return SupportRequests
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }
}
