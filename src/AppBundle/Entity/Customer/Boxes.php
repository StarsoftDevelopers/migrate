<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boxes
 *
 * @ORM\Table(name="boxes")
 * @ORM\Entity
 */
class Boxes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="box_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $boxId;

    /**
     * @var string
     *
     * @ORM\Column(name="box", type="string", length=32, nullable=false)
     */
    private $box = '';

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", nullable=false)
     */
    private $position = 'left';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="display", type="boolean", nullable=false)
     */
    private $display = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="fb_display", type="boolean", nullable=true)
     */
    private $fbDisplay = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="mainpage", type="boolean", nullable=false)
     */
    private $mainpage = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name_intern", type="string", length=255, nullable=true)
     */
    private $nameIntern;



    /**
     * Get boxId
     *
     * @return integer
     */
    public function getBoxId()
    {
        return $this->boxId;
    }

    /**
     * Set box
     *
     * @param string $box
     *
     * @return Boxes
     */
    public function setBox($box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return string
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Boxes
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Boxes
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set display
     *
     * @param boolean $display
     *
     * @return Boxes
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display
     *
     * @return boolean
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * Set fbDisplay
     *
     * @param boolean $fbDisplay
     *
     * @return Boxes
     */
    public function setFbDisplay($fbDisplay)
    {
        $this->fbDisplay = $fbDisplay;

        return $this;
    }

    /**
     * Get fbDisplay
     *
     * @return boolean
     */
    public function getFbDisplay()
    {
        return $this->fbDisplay;
    }

    /**
     * Set mainpage
     *
     * @param boolean $mainpage
     *
     * @return Boxes
     */
    public function setMainpage($mainpage)
    {
        $this->mainpage = $mainpage;

        return $this;
    }

    /**
     * Get mainpage
     *
     * @return boolean
     */
    public function getMainpage()
    {
        return $this->mainpage;
    }

    /**
     * Set nameIntern
     *
     * @param string $nameIntern
     *
     * @return Boxes
     */
    public function setNameIntern($nameIntern)
    {
        $this->nameIntern = $nameIntern;

        return $this;
    }

    /**
     * Get nameIntern
     *
     * @return string
     */
    public function getNameIntern()
    {
        return $this->nameIntern;
    }
}
