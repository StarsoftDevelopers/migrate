<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesToHandelocategories
 *
 * @ORM\Table(name="categories_to_handelocategories")
 * @ORM\Entity
 */
class CategoriesToHandelocategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categories_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $categoriesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="handelo_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $handeloId;



    /**
     * Set categoriesId
     *
     * @param integer $categoriesId
     *
     * @return CategoriesToHandelocategories
     */
    public function setCategoriesId($categoriesId)
    {
        $this->categoriesId = $categoriesId;

        return $this;
    }

    /**
     * Get categoriesId
     *
     * @return integer
     */
    public function getCategoriesId()
    {
        return $this->categoriesId;
    }

    /**
     * Set handeloId
     *
     * @param integer $handeloId
     *
     * @return CategoriesToHandelocategories
     */
    public function setHandeloId($handeloId)
    {
        $this->handeloId = $handeloId;

        return $this;
    }

    /**
     * Get handeloId
     *
     * @return integer
     */
    public function getHandeloId()
    {
        return $this->handeloId;
    }
}
