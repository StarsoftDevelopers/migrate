<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersGroups
 *
 * @ORM\Table(name="customers_groups")
 * @ORM\Entity
 */
class CustomersGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="customers_group_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $customersGroupId;

    /**
     * @var string
     *
     * @ORM\Column(name="customers_group_name", type="string", length=32, nullable=false)
     */
    private $customersGroupName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_group_discount", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $customersGroupDiscount = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="color_bar", type="string", length=8, nullable=false)
     */
    private $colorBar = '#FFFFFF';

    /**
     * @var string
     *
     * @ORM\Column(name="group_payment_unallowed", type="string", length=255, nullable=false)
     */
    private $groupPaymentUnallowed = 'cc';

    /**
     * @var string
     *
     * @ORM\Column(name="group_tax", type="string", length=5, nullable=false)
     */
    private $groupTax = 'false';

    /**
     * @var string
     *
     * @ORM\Column(name="group_coupon", type="string", length=5, nullable=false)
     */
    private $groupCoupon = 'true';

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiplier", type="boolean", nullable=true)
     */
    private $multiplier;

    /**
     * @var string
     *
     * @ORM\Column(name="group_tax_calculation", type="string", length=5, nullable=true)
     */
    private $groupTaxCalculation = 'true';



    /**
     * Get customersGroupId
     *
     * @return integer
     */
    public function getCustomersGroupId()
    {
        return $this->customersGroupId;
    }

    /**
     * Set customersGroupName
     *
     * @param string $customersGroupName
     *
     * @return CustomersGroups
     */
    public function setCustomersGroupName($customersGroupName)
    {
        $this->customersGroupName = $customersGroupName;

        return $this;
    }

    /**
     * Get customersGroupName
     *
     * @return string
     */
    public function getCustomersGroupName()
    {
        return $this->customersGroupName;
    }

    /**
     * Set customersGroupDiscount
     *
     * @param string $customersGroupDiscount
     *
     * @return CustomersGroups
     */
    public function setCustomersGroupDiscount($customersGroupDiscount)
    {
        $this->customersGroupDiscount = $customersGroupDiscount;

        return $this;
    }

    /**
     * Get customersGroupDiscount
     *
     * @return string
     */
    public function getCustomersGroupDiscount()
    {
        return $this->customersGroupDiscount;
    }

    /**
     * Set colorBar
     *
     * @param string $colorBar
     *
     * @return CustomersGroups
     */
    public function setColorBar($colorBar)
    {
        $this->colorBar = $colorBar;

        return $this;
    }

    /**
     * Get colorBar
     *
     * @return string
     */
    public function getColorBar()
    {
        return $this->colorBar;
    }

    /**
     * Set groupPaymentUnallowed
     *
     * @param string $groupPaymentUnallowed
     *
     * @return CustomersGroups
     */
    public function setGroupPaymentUnallowed($groupPaymentUnallowed)
    {
        $this->groupPaymentUnallowed = $groupPaymentUnallowed;

        return $this;
    }

    /**
     * Get groupPaymentUnallowed
     *
     * @return string
     */
    public function getGroupPaymentUnallowed()
    {
        return $this->groupPaymentUnallowed;
    }

    /**
     * Set groupTax
     *
     * @param string $groupTax
     *
     * @return CustomersGroups
     */
    public function setGroupTax($groupTax)
    {
        $this->groupTax = $groupTax;

        return $this;
    }

    /**
     * Get groupTax
     *
     * @return string
     */
    public function getGroupTax()
    {
        return $this->groupTax;
    }

    /**
     * Set groupCoupon
     *
     * @param string $groupCoupon
     *
     * @return CustomersGroups
     */
    public function setGroupCoupon($groupCoupon)
    {
        $this->groupCoupon = $groupCoupon;

        return $this;
    }

    /**
     * Get groupCoupon
     *
     * @return string
     */
    public function getGroupCoupon()
    {
        return $this->groupCoupon;
    }

    /**
     * Set multiplier
     *
     * @param boolean $multiplier
     *
     * @return CustomersGroups
     */
    public function setMultiplier($multiplier)
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    /**
     * Get multiplier
     *
     * @return boolean
     */
    public function getMultiplier()
    {
        return $this->multiplier;
    }

    /**
     * Set groupTaxCalculation
     *
     * @param string $groupTaxCalculation
     *
     * @return CustomersGroups
     */
    public function setGroupTaxCalculation($groupTaxCalculation)
    {
        $this->groupTaxCalculation = $groupTaxCalculation;

        return $this;
    }

    /**
     * Get groupTaxCalculation
     *
     * @return string
     */
    public function getGroupTaxCalculation()
    {
        return $this->groupTaxCalculation;
    }
}
