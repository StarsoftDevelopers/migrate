<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoxesContent
 *
 * @ORM\Table(name="boxes_content")
 * @ORM\Entity
 */
class BoxesContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="boxes_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $boxesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languagesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="boxes_title", type="string", length=255, nullable=true)
     */
    private $boxesTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="boxes_content", type="text", length=65535, nullable=false)
     */
    private $boxesContent;



    /**
     * Set boxesId
     *
     * @param integer $boxesId
     *
     * @return BoxesContent
     */
    public function setBoxesId($boxesId)
    {
        $this->boxesId = $boxesId;

        return $this;
    }

    /**
     * Get boxesId
     *
     * @return integer
     */
    public function getBoxesId()
    {
        return $this->boxesId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return BoxesContent
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set boxesTitle
     *
     * @param string $boxesTitle
     *
     * @return BoxesContent
     */
    public function setBoxesTitle($boxesTitle)
    {
        $this->boxesTitle = $boxesTitle;

        return $this;
    }

    /**
     * Get boxesTitle
     *
     * @return string
     */
    public function getBoxesTitle()
    {
        return $this->boxesTitle;
    }

    /**
     * Set boxesContent
     *
     * @param string $boxesContent
     *
     * @return BoxesContent
     */
    public function setBoxesContent($boxesContent)
    {
        $this->boxesContent = $boxesContent;

        return $this;
    }

    /**
     * Get boxesContent
     *
     * @return string
     */
    public function getBoxesContent()
    {
        return $this->boxesContent;
    }
}
