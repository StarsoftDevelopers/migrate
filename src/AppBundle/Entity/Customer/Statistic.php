<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Statistic
 *
 * @ORM\Table(name="statistic")
 * @ORM\Entity
 */
class Statistic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="stats_name", type="string", length=16, nullable=false)
     */
    private $statsName;

    /**
     * @var string
     *
     * @ORM\Column(name="stats_value", type="string", length=16, nullable=false)
     */
    private $statsValue;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Statistic
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set statsName
     *
     * @param string $statsName
     *
     * @return Statistic
     */
    public function setStatsName($statsName)
    {
        $this->statsName = $statsName;

        return $this;
    }

    /**
     * Get statsName
     *
     * @return string
     */
    public function getStatsName()
    {
        return $this->statsName;
    }

    /**
     * Set statsValue
     *
     * @param string $statsValue
     *
     * @return Statistic
     */
    public function setStatsValue($statsValue)
    {
        $this->statsValue = $statsValue;

        return $this;
    }

    /**
     * Get statsValue
     *
     * @return string
     */
    public function getStatsValue()
    {
        return $this->statsValue;
    }
}
