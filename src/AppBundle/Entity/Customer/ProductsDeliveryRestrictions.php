<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsDeliveryRestrictions
 *
 * @ORM\Table(name="products_delivery_restrictions")
 * @ORM\Entity
 */
class ProductsDeliveryRestrictions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languagesId;

    /**
     * @var string
     *
     * @ORM\Column(name="restriction_title", type="string", length=255, nullable=false)
     */
    private $restrictionTitle = '';



    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ProductsDeliveryRestrictions
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return ProductsDeliveryRestrictions
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set restrictionTitle
     *
     * @param string $restrictionTitle
     *
     * @return ProductsDeliveryRestrictions
     */
    public function setRestrictionTitle($restrictionTitle)
    {
        $this->restrictionTitle = $restrictionTitle;

        return $this;
    }

    /**
     * Get restrictionTitle
     *
     * @return string
     */
    public function getRestrictionTitle()
    {
        return $this->restrictionTitle;
    }
}
