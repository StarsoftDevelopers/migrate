<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigurationEfulfilment
 *
 * @ORM\Table(name="configuration_efulfilment")
 * @ORM\Entity
 */
class ConfigurationEfulfilment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="configuration_type", type="boolean", nullable=false)
     */
    private $configurationType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="configuration_key", type="string", length=32, nullable=false)
     */
    private $configurationKey = '';

    /**
     * @var string
     *
     * @ORM\Column(name="value1", type="string", length=32, nullable=false)
     */
    private $value1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="value2", type="string", length=32, nullable=false)
     */
    private $value2 = '';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set configurationType
     *
     * @param boolean $configurationType
     *
     * @return ConfigurationEfulfilment
     */
    public function setConfigurationType($configurationType)
    {
        $this->configurationType = $configurationType;

        return $this;
    }

    /**
     * Get configurationType
     *
     * @return boolean
     */
    public function getConfigurationType()
    {
        return $this->configurationType;
    }

    /**
     * Set configurationKey
     *
     * @param string $configurationKey
     *
     * @return ConfigurationEfulfilment
     */
    public function setConfigurationKey($configurationKey)
    {
        $this->configurationKey = $configurationKey;

        return $this;
    }

    /**
     * Get configurationKey
     *
     * @return string
     */
    public function getConfigurationKey()
    {
        return $this->configurationKey;
    }

    /**
     * Set value1
     *
     * @param string $value1
     *
     * @return ConfigurationEfulfilment
     */
    public function setValue1($value1)
    {
        $this->value1 = $value1;

        return $this;
    }

    /**
     * Get value1
     *
     * @return string
     */
    public function getValue1()
    {
        return $this->value1;
    }

    /**
     * Set value2
     *
     * @param string $value2
     *
     * @return ConfigurationEfulfilment
     */
    public function setValue2($value2)
    {
        $this->value2 = $value2;

        return $this;
    }

    /**
     * Get value2
     *
     * @return string
     */
    public function getValue2()
    {
        return $this->value2;
    }
}
