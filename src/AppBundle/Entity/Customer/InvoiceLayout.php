<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceLayout
 *
 * @ORM\Table(name="invoice_layout")
 * @ORM\Entity
 */
class InvoiceLayout
{
    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_layout_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $invoiceLayoutId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="lang_key", type="string", length=255, nullable=true)
     */
    private $langKey;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="default_content", type="text", length=65535, nullable=true)
     */
    private $defaultContent;

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", length=65535, nullable=true)
     */
    private $variables;

    /**
     * @var string
     *
     * @ORM\Column(name="default_fields", type="text", length=65535, nullable=true)
     */
    private $defaultFields;

    /**
     * @var string
     *
     * @ORM\Column(name="block_params", type="text", length=65535, nullable=true)
     */
    private $blockParams;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=true)
     */
    private $template;

    /**
     * @var integer
     *
     * @ORM\Column(name="root_id", type="integer", nullable=true)
     */
    private $rootId;

    /**
     * @var integer
     *
     * @ORM\Column(name="lft", type="integer", nullable=true)
     */
    private $lft;

    /**
     * @var integer
     *
     * @ORM\Column(name="rgt", type="integer", nullable=true)
     */
    private $rgt;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="smallint", nullable=true)
     */
    private $level;



    /**
     * Get invoiceLayoutId
     *
     * @return integer
     */
    public function getInvoiceLayoutId()
    {
        return $this->invoiceLayoutId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InvoiceLayout
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set langKey
     *
     * @param string $langKey
     *
     * @return InvoiceLayout
     */
    public function setLangKey($langKey)
    {
        $this->langKey = $langKey;

        return $this;
    }

    /**
     * Get langKey
     *
     * @return string
     */
    public function getLangKey()
    {
        return $this->langKey;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return InvoiceLayout
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set defaultContent
     *
     * @param string $defaultContent
     *
     * @return InvoiceLayout
     */
    public function setDefaultContent($defaultContent)
    {
        $this->defaultContent = $defaultContent;

        return $this;
    }

    /**
     * Get defaultContent
     *
     * @return string
     */
    public function getDefaultContent()
    {
        return $this->defaultContent;
    }

    /**
     * Set variables
     *
     * @param string $variables
     *
     * @return InvoiceLayout
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;

        return $this;
    }

    /**
     * Get variables
     *
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Set defaultFields
     *
     * @param string $defaultFields
     *
     * @return InvoiceLayout
     */
    public function setDefaultFields($defaultFields)
    {
        $this->defaultFields = $defaultFields;

        return $this;
    }

    /**
     * Get defaultFields
     *
     * @return string
     */
    public function getDefaultFields()
    {
        return $this->defaultFields;
    }

    /**
     * Set blockParams
     *
     * @param string $blockParams
     *
     * @return InvoiceLayout
     */
    public function setBlockParams($blockParams)
    {
        $this->blockParams = $blockParams;

        return $this;
    }

    /**
     * Get blockParams
     *
     * @return string
     */
    public function getBlockParams()
    {
        return $this->blockParams;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return InvoiceLayout
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set rootId
     *
     * @param integer $rootId
     *
     * @return InvoiceLayout
     */
    public function setRootId($rootId)
    {
        $this->rootId = $rootId;

        return $this;
    }

    /**
     * Get rootId
     *
     * @return integer
     */
    public function getRootId()
    {
        return $this->rootId;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return InvoiceLayout
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return InvoiceLayout
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return InvoiceLayout
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }
}
