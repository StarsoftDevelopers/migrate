<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarketingClickLog
 *
 * @ORM\Table(name="marketing_click_log")
 * @ORM\Entity
 */
class MarketingClickLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     */
    private $affiliateId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_clickthrough_id", type="integer", nullable=false)
     */
    private $affiliateClickthroughId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="event", type="string", nullable=false)
     */
    private $event = 'inaktiv';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set affiliateId
     *
     * @param integer $affiliateId
     *
     * @return MarketingClickLog
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;

        return $this;
    }

    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set affiliateClickthroughId
     *
     * @param integer $affiliateClickthroughId
     *
     * @return MarketingClickLog
     */
    public function setAffiliateClickthroughId($affiliateClickthroughId)
    {
        $this->affiliateClickthroughId = $affiliateClickthroughId;

        return $this;
    }

    /**
     * Get affiliateClickthroughId
     *
     * @return integer
     */
    public function getAffiliateClickthroughId()
    {
        return $this->affiliateClickthroughId;
    }

    /**
     * Set event
     *
     * @param string $event
     *
     * @return MarketingClickLog
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }
}
