<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsOptions
 *
 * @ORM\Table(name="products_options")
 * @ORM\Entity
 */
class ProductsOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_options_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $productsOptionsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="products_options_name", type="string", length=32, nullable=false)
     */
    private $productsOptionsName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="options_use_merkmal", type="boolean", nullable=false)
     */
    private $optionsUseMerkmal = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="options_use_filter", type="boolean", nullable=false)
     */
    private $optionsUseFilter = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="options_use_specification", type="boolean", nullable=false)
     */
    private $optionsUseSpecification = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="options_filter_view", type="string", length=8, nullable=false)
     */
    private $optionsFilterView;



    /**
     * Set productsOptionsId
     *
     * @param integer $productsOptionsId
     *
     * @return ProductsOptions
     */
    public function setProductsOptionsId($productsOptionsId)
    {
        $this->productsOptionsId = $productsOptionsId;

        return $this;
    }

    /**
     * Get productsOptionsId
     *
     * @return integer
     */
    public function getProductsOptionsId()
    {
        return $this->productsOptionsId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return ProductsOptions
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set productsOptionsName
     *
     * @param string $productsOptionsName
     *
     * @return ProductsOptions
     */
    public function setProductsOptionsName($productsOptionsName)
    {
        $this->productsOptionsName = $productsOptionsName;

        return $this;
    }

    /**
     * Get productsOptionsName
     *
     * @return string
     */
    public function getProductsOptionsName()
    {
        return $this->productsOptionsName;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return ProductsOptions
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set optionsUseMerkmal
     *
     * @param boolean $optionsUseMerkmal
     *
     * @return ProductsOptions
     */
    public function setOptionsUseMerkmal($optionsUseMerkmal)
    {
        $this->optionsUseMerkmal = $optionsUseMerkmal;

        return $this;
    }

    /**
     * Get optionsUseMerkmal
     *
     * @return boolean
     */
    public function getOptionsUseMerkmal()
    {
        return $this->optionsUseMerkmal;
    }

    /**
     * Set optionsUseFilter
     *
     * @param boolean $optionsUseFilter
     *
     * @return ProductsOptions
     */
    public function setOptionsUseFilter($optionsUseFilter)
    {
        $this->optionsUseFilter = $optionsUseFilter;

        return $this;
    }

    /**
     * Get optionsUseFilter
     *
     * @return boolean
     */
    public function getOptionsUseFilter()
    {
        return $this->optionsUseFilter;
    }

    /**
     * Set optionsUseSpecification
     *
     * @param boolean $optionsUseSpecification
     *
     * @return ProductsOptions
     */
    public function setOptionsUseSpecification($optionsUseSpecification)
    {
        $this->optionsUseSpecification = $optionsUseSpecification;

        return $this;
    }

    /**
     * Get optionsUseSpecification
     *
     * @return boolean
     */
    public function getOptionsUseSpecification()
    {
        return $this->optionsUseSpecification;
    }

    /**
     * Set optionsFilterView
     *
     * @param string $optionsFilterView
     *
     * @return ProductsOptions
     */
    public function setOptionsFilterView($optionsFilterView)
    {
        $this->optionsFilterView = $optionsFilterView;

        return $this;
    }

    /**
     * Get optionsFilterView
     *
     * @return string
     */
    public function getOptionsFilterView()
    {
        return $this->optionsFilterView;
    }
}
