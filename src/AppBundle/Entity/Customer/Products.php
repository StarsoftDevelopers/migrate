<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products
 *
 * @ORM\Table(name="products", indexes={@ORM\Index(name="idx_products_date_added", columns={"products_date_added"}), @ORM\Index(name="products_model", columns={"products_model"}), @ORM\Index(name="auktionweb_id", columns={"auktionweb_id"}), @ORM\Index(name="manufacturers_id", columns={"manufacturers_id"}), @ORM\Index(name="products_model_own", columns={"products_model_own"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Customer\Repository\ProductsRepository")
 */
class Products
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiplier", type="boolean", nullable=false)
     */
    private $multiplier = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="bonusarticle", type="boolean", nullable=false)
     */
    private $bonusarticle = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_quantity", type="integer", nullable=false)
     */
    private $productsQuantity = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_quantity_min", type="integer", nullable=true)
     */
    private $productsQuantityMin;

    /**
     * @var string
     *
     * @ORM\Column(name="products_model", type="string", length=32, nullable=true)
     */
    private $productsModel;

    /**
     * @var string
     *
     * @ORM\Column(name="videolink", type="string", length=255, nullable=true)
     */
    private $videolink;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image", type="string", length=255, nullable=true)
     */
    private $productsImage;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_origin", type="string", length=255, nullable=false)
     */
    private $productsImageOrigin;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_med", type="string", length=255, nullable=true)
     */
    private $productsImageMed;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_lrg", type="string", length=255, nullable=true)
     */
    private $productsImageLrg;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_sm_1", type="string", length=255, nullable=true)
     */
    private $productsImageSm1;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_xl_1", type="string", length=255, nullable=true)
     */
    private $productsImageXl1;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_sm_2", type="string", length=255, nullable=true)
     */
    private $productsImageSm2;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_xl_2", type="string", length=255, nullable=true)
     */
    private $productsImageXl2;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_sm_3", type="string", length=255, nullable=true)
     */
    private $productsImageSm3;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_xl_3", type="string", length=255, nullable=true)
     */
    private $productsImageXl3;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_sm_4", type="string", length=255, nullable=true)
     */
    private $productsImageSm4;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_xl_4", type="string", length=255, nullable=true)
     */
    private $productsImageXl4;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_sm_5", type="string", length=255, nullable=true)
     */
    private $productsImageSm5;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_xl_5", type="string", length=255, nullable=true)
     */
    private $productsImageXl5;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_sm_6", type="string", length=255, nullable=true)
     */
    private $productsImageSm6;

    /**
     * @var string
     *
     * @ORM\Column(name="products_image_xl_6", type="string", length=255, nullable=true)
     */
    private $productsImageXl6;

    /**
     * @var string
     *
     * @ORM\Column(name="products_price", type="decimal", precision=15, scale=4, nullable=false)
     */
    private $productsPrice = '0.0000';

    /**
     * @var string
     *
     * @ORM\Column(name="products_ek_price", type="decimal", precision=15, scale=4, nullable=true)
     */
    private $productsEkPrice = '0.0000';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_base_price_id", type="integer", nullable=false)
     */
    private $productsBasePriceId = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="products_price_ratio", type="float", precision=10, scale=0, nullable=false)
     */
    private $productsPriceRatio = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="products_price_display", type="boolean", nullable=true)
     */
    private $productsPriceDisplay = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="products_date_added", type="datetime", nullable=false)
     */
    private $productsDateAdded = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="products_last_modified", type="datetime", nullable=true)
     */
    private $productsLastModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="products_date_available", type="datetime", nullable=true)
     */
    private $productsDateAvailable;

    /**
     * @var string
     *
     * @ORM\Column(name="products_weight", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $productsWeight = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="products_shipping", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $productsShipping;

    /**
     * @var string
     *
     * @ORM\Column(name="products_shipping_group", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $productsShippingGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="products_shipping_pvm", type="string", length=255, nullable=false)
     */
    private $productsShippingPvm = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="products_status", type="boolean", nullable=false)
     */
    private $productsStatus = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_tax_class_id", type="integer", nullable=false)
     */
    private $productsTaxClassId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturers_id", type="integer", nullable=true)
     */
    private $manufacturersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_ordered", type="integer", nullable=false)
     */
    private $productsOrdered = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="products_pmarketing", type="boolean", nullable=false)
     */
    private $productsPmarketing = '-1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="products_eantype", type="boolean", nullable=true)
     */
    private $productsEantype;

    /**
     * @var string
     *
     * @ORM\Column(name="products_ean", type="string", length=32, nullable=true)
     */
    private $productsEan;

    /**
     * @var string
     *
     * @ORM\Column(name="products_model_own", type="string", length=32, nullable=false)
     */
    private $productsModelOwn;

    /**
     * @var string
     *
     * @ORM\Column(name="products_asin", type="string", length=32, nullable=false)
     */
    private $productsAsin;

    /**
     * @var string
     *
     * @ORM\Column(name="products_upc", type="string", length=32, nullable=false)
     */
    private $productsUpc;

    /**
     * @var string
     *
     * @ORM\Column(name="products_isbn", type="string", length=32, nullable=false)
     */
    private $productsIsbn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="products_amazoncond", type="boolean", nullable=true)
     */
    private $productsAmazoncond;

    /**
     * @var string
     *
     * @ORM\Column(name="products_amazoncat", type="string", length=32, nullable=true)
     */
    private $productsAmazoncat;

    /**
     * @var integer
     *
     * @ORM\Column(name="Sort_Order", type="smallint", nullable=true)
     */
    private $sortOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="auktionweb_id", type="string", length=32, nullable=false)
     */
    private $auktionwebId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="afterbuy_id", type="string", length=32, nullable=false)
     */
    private $afterbuyId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ebay_id", type="string", length=32, nullable=false)
     */
    private $ebayId = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_status", type="boolean", nullable=false)
     */
    private $importStatus = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="products_uvp", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $productsUvp;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_availability", type="smallint", nullable=true)
     */
    private $productsAvailability;

    /**
     * @var string
     *
     * @ORM\Column(name="products_delivery_time", type="string", length=8, nullable=false)
     */
    private $productsDeliveryTime = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_user_num1", type="integer", nullable=true)
     */
    private $productsUserNum1;

    /**
     * @var string
     *
     * @ORM\Column(name="products_user_num2", type="decimal", precision=8, scale=2, nullable=true)
     */
    private $productsUserNum2;

    /**
     * @var string
     *
     * @ORM\Column(name="products_user1", type="string", length=128, nullable=true)
     */
    private $productsUser1;

    /**
     * @var string
     *
     * @ORM\Column(name="products_user2", type="string", length=128, nullable=true)
     */
    private $productsUser2;

    /**
     * @var string
     *
     * @ORM\Column(name="products_user3", type="string", length=128, nullable=false)
     */
    private $productsUser3 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="products_user4", type="string", length=128, nullable=false)
     */
    private $productsUser4 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="products_variants", type="string", length=128, nullable=true)
     */
    private $productsVariants;

    /**
     * @var boolean
     *
     * @ORM\Column(name="products_percentage", type="boolean", nullable=false)
     */
    private $productsPercentage = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="products_condition", type="boolean", nullable=true)
     */
    private $productsCondition = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="googlecat", type="smallint", nullable=true)
     */
    private $googlecat;

    /**
     * @var string
     *
     * @ORM\Column(name="products_delivery_restrictions", type="string", length=32, nullable=false)
     */
    private $productsDeliveryRestrictions = '';



    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set multiplier
     *
     * @param boolean $multiplier
     *
     * @return Products
     */
    public function setMultiplier($multiplier)
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    /**
     * Get multiplier
     *
     * @return boolean
     */
    public function getMultiplier()
    {
        return $this->multiplier;
    }

    /**
     * Set bonusarticle
     *
     * @param boolean $bonusarticle
     *
     * @return Products
     */
    public function setBonusarticle($bonusarticle)
    {
        $this->bonusarticle = $bonusarticle;

        return $this;
    }

    /**
     * Get bonusarticle
     *
     * @return boolean
     */
    public function getBonusarticle()
    {
        return $this->bonusarticle;
    }

    /**
     * Set productsQuantity
     *
     * @param integer $productsQuantity
     *
     * @return Products
     */
    public function setProductsQuantity($productsQuantity)
    {
        $this->productsQuantity = $productsQuantity;

        return $this;
    }

    /**
     * Get productsQuantity
     *
     * @return integer
     */
    public function getProductsQuantity()
    {
        return $this->productsQuantity;
    }

    /**
     * Set productsQuantityMin
     *
     * @param integer $productsQuantityMin
     *
     * @return Products
     */
    public function setProductsQuantityMin($productsQuantityMin)
    {
        $this->productsQuantityMin = $productsQuantityMin;

        return $this;
    }

    /**
     * Get productsQuantityMin
     *
     * @return integer
     */
    public function getProductsQuantityMin()
    {
        return $this->productsQuantityMin;
    }

    /**
     * Set productsModel
     *
     * @param string $productsModel
     *
     * @return Products
     */
    public function setProductsModel($productsModel)
    {
        $this->productsModel = $productsModel;

        return $this;
    }

    /**
     * Get productsModel
     *
     * @return string
     */
    public function getProductsModel()
    {
        return $this->productsModel;
    }

    /**
     * Set videolink
     *
     * @param string $videolink
     *
     * @return Products
     */
    public function setVideolink($videolink)
    {
        $this->videolink = $videolink;

        return $this;
    }

    /**
     * Get videolink
     *
     * @return string
     */
    public function getVideolink()
    {
        return $this->videolink;
    }

    /**
     * Set productsImage
     *
     * @param string $productsImage
     *
     * @return Products
     */
    public function setProductsImage($productsImage)
    {
        $this->productsImage = $productsImage;

        return $this;
    }

    /**
     * Get productsImage
     *
     * @return string
     */
    public function getProductsImage()
    {
        return $this->productsImage;
    }

    /**
     * Set productsImageOrigin
     *
     * @param string $productsImageOrigin
     *
     * @return Products
     */
    public function setProductsImageOrigin($productsImageOrigin)
    {
        $this->productsImageOrigin = $productsImageOrigin;

        return $this;
    }

    /**
     * Get productsImageOrigin
     *
     * @return string
     */
    public function getProductsImageOrigin()
    {
        return $this->productsImageOrigin;
    }

    /**
     * Set productsImageMed
     *
     * @param string $productsImageMed
     *
     * @return Products
     */
    public function setProductsImageMed($productsImageMed)
    {
        $this->productsImageMed = $productsImageMed;

        return $this;
    }

    /**
     * Get productsImageMed
     *
     * @return string
     */
    public function getProductsImageMed()
    {
        return $this->productsImageMed;
    }

    /**
     * Set productsImageLrg
     *
     * @param string $productsImageLrg
     *
     * @return Products
     */
    public function setProductsImageLrg($productsImageLrg)
    {
        $this->productsImageLrg = $productsImageLrg;

        return $this;
    }

    /**
     * Get productsImageLrg
     *
     * @return string
     */
    public function getProductsImageLrg()
    {
        return $this->productsImageLrg;
    }

    /**
     * Set productsImageSm1
     *
     * @param string $productsImageSm1
     *
     * @return Products
     */
    public function setProductsImageSm1($productsImageSm1)
    {
        $this->productsImageSm1 = $productsImageSm1;

        return $this;
    }

    /**
     * Get productsImageSm1
     *
     * @return string
     */
    public function getProductsImageSm1()
    {
        return $this->productsImageSm1;
    }

    /**
     * Set productsImageXl1
     *
     * @param string $productsImageXl1
     *
     * @return Products
     */
    public function setProductsImageXl1($productsImageXl1)
    {
        $this->productsImageXl1 = $productsImageXl1;

        return $this;
    }

    /**
     * Get productsImageXl1
     *
     * @return string
     */
    public function getProductsImageXl1()
    {
        return $this->productsImageXl1;
    }

    /**
     * Set productsImageSm2
     *
     * @param string $productsImageSm2
     *
     * @return Products
     */
    public function setProductsImageSm2($productsImageSm2)
    {
        $this->productsImageSm2 = $productsImageSm2;

        return $this;
    }

    /**
     * Get productsImageSm2
     *
     * @return string
     */
    public function getProductsImageSm2()
    {
        return $this->productsImageSm2;
    }

    /**
     * Set productsImageXl2
     *
     * @param string $productsImageXl2
     *
     * @return Products
     */
    public function setProductsImageXl2($productsImageXl2)
    {
        $this->productsImageXl2 = $productsImageXl2;

        return $this;
    }

    /**
     * Get productsImageXl2
     *
     * @return string
     */
    public function getProductsImageXl2()
    {
        return $this->productsImageXl2;
    }

    /**
     * Set productsImageSm3
     *
     * @param string $productsImageSm3
     *
     * @return Products
     */
    public function setProductsImageSm3($productsImageSm3)
    {
        $this->productsImageSm3 = $productsImageSm3;

        return $this;
    }

    /**
     * Get productsImageSm3
     *
     * @return string
     */
    public function getProductsImageSm3()
    {
        return $this->productsImageSm3;
    }

    /**
     * Set productsImageXl3
     *
     * @param string $productsImageXl3
     *
     * @return Products
     */
    public function setProductsImageXl3($productsImageXl3)
    {
        $this->productsImageXl3 = $productsImageXl3;

        return $this;
    }

    /**
     * Get productsImageXl3
     *
     * @return string
     */
    public function getProductsImageXl3()
    {
        return $this->productsImageXl3;
    }

    /**
     * Set productsImageSm4
     *
     * @param string $productsImageSm4
     *
     * @return Products
     */
    public function setProductsImageSm4($productsImageSm4)
    {
        $this->productsImageSm4 = $productsImageSm4;

        return $this;
    }

    /**
     * Get productsImageSm4
     *
     * @return string
     */
    public function getProductsImageSm4()
    {
        return $this->productsImageSm4;
    }

    /**
     * Set productsImageXl4
     *
     * @param string $productsImageXl4
     *
     * @return Products
     */
    public function setProductsImageXl4($productsImageXl4)
    {
        $this->productsImageXl4 = $productsImageXl4;

        return $this;
    }

    /**
     * Get productsImageXl4
     *
     * @return string
     */
    public function getProductsImageXl4()
    {
        return $this->productsImageXl4;
    }

    /**
     * Set productsImageSm5
     *
     * @param string $productsImageSm5
     *
     * @return Products
     */
    public function setProductsImageSm5($productsImageSm5)
    {
        $this->productsImageSm5 = $productsImageSm5;

        return $this;
    }

    /**
     * Get productsImageSm5
     *
     * @return string
     */
    public function getProductsImageSm5()
    {
        return $this->productsImageSm5;
    }

    /**
     * Set productsImageXl5
     *
     * @param string $productsImageXl5
     *
     * @return Products
     */
    public function setProductsImageXl5($productsImageXl5)
    {
        $this->productsImageXl5 = $productsImageXl5;

        return $this;
    }

    /**
     * Get productsImageXl5
     *
     * @return string
     */
    public function getProductsImageXl5()
    {
        return $this->productsImageXl5;
    }

    /**
     * Set productsImageSm6
     *
     * @param string $productsImageSm6
     *
     * @return Products
     */
    public function setProductsImageSm6($productsImageSm6)
    {
        $this->productsImageSm6 = $productsImageSm6;

        return $this;
    }

    /**
     * Get productsImageSm6
     *
     * @return string
     */
    public function getProductsImageSm6()
    {
        return $this->productsImageSm6;
    }

    /**
     * Set productsImageXl6
     *
     * @param string $productsImageXl6
     *
     * @return Products
     */
    public function setProductsImageXl6($productsImageXl6)
    {
        $this->productsImageXl6 = $productsImageXl6;

        return $this;
    }

    /**
     * Get productsImageXl6
     *
     * @return string
     */
    public function getProductsImageXl6()
    {
        return $this->productsImageXl6;
    }

    /**
     * Set productsPrice
     *
     * @param string $productsPrice
     *
     * @return Products
     */
    public function setProductsPrice($productsPrice)
    {
        $this->productsPrice = $productsPrice;

        return $this;
    }

    /**
     * Get productsPrice
     *
     * @return string
     */
    public function getProductsPrice()
    {
        return $this->productsPrice;
    }

    /**
     * Set productsEkPrice
     *
     * @param string $productsEkPrice
     *
     * @return Products
     */
    public function setProductsEkPrice($productsEkPrice)
    {
        $this->productsEkPrice = $productsEkPrice;

        return $this;
    }

    /**
     * Get productsEkPrice
     *
     * @return string
     */
    public function getProductsEkPrice()
    {
        return $this->productsEkPrice;
    }

    /**
     * Set productsBasePriceId
     *
     * @param integer $productsBasePriceId
     *
     * @return Products
     */
    public function setProductsBasePriceId($productsBasePriceId)
    {
        $this->productsBasePriceId = $productsBasePriceId;

        return $this;
    }

    /**
     * Get productsBasePriceId
     *
     * @return integer
     */
    public function getProductsBasePriceId()
    {
        return $this->productsBasePriceId;
    }

    /**
     * Set productsPriceRatio
     *
     * @param float $productsPriceRatio
     *
     * @return Products
     */
    public function setProductsPriceRatio($productsPriceRatio)
    {
        $this->productsPriceRatio = $productsPriceRatio;

        return $this;
    }

    /**
     * Get productsPriceRatio
     *
     * @return float
     */
    public function getProductsPriceRatio()
    {
        return $this->productsPriceRatio;
    }

    /**
     * Set productsPriceDisplay
     *
     * @param boolean $productsPriceDisplay
     *
     * @return Products
     */
    public function setProductsPriceDisplay($productsPriceDisplay)
    {
        $this->productsPriceDisplay = $productsPriceDisplay;

        return $this;
    }

    /**
     * Get productsPriceDisplay
     *
     * @return boolean
     */
    public function getProductsPriceDisplay()
    {
        return $this->productsPriceDisplay;
    }

    /**
     * Set productsDateAdded
     *
     * @param \DateTime $productsDateAdded
     *
     * @return Products
     */
    public function setProductsDateAdded($productsDateAdded)
    {
        $this->productsDateAdded = $productsDateAdded;

        return $this;
    }

    /**
     * Get productsDateAdded
     *
     * @return \DateTime
     */
    public function getProductsDateAdded()
    {
        return $this->productsDateAdded;
    }

    /**
     * Set productsLastModified
     *
     * @param \DateTime $productsLastModified
     *
     * @return Products
     */
    public function setProductsLastModified($productsLastModified)
    {
        $this->productsLastModified = $productsLastModified;

        return $this;
    }

    /**
     * Get productsLastModified
     *
     * @return \DateTime
     */
    public function getProductsLastModified()
    {
        return $this->productsLastModified;
    }

    /**
     * Set productsDateAvailable
     *
     * @param \DateTime $productsDateAvailable
     *
     * @return Products
     */
    public function setProductsDateAvailable($productsDateAvailable)
    {
        $this->productsDateAvailable = $productsDateAvailable;

        return $this;
    }

    /**
     * Get productsDateAvailable
     *
     * @return \DateTime
     */
    public function getProductsDateAvailable()
    {
        return $this->productsDateAvailable;
    }

    /**
     * Set productsWeight
     *
     * @param string $productsWeight
     *
     * @return Products
     */
    public function setProductsWeight($productsWeight)
    {
        $this->productsWeight = $productsWeight;

        return $this;
    }

    /**
     * Get productsWeight
     *
     * @return string
     */
    public function getProductsWeight()
    {
        return $this->productsWeight;
    }

    /**
     * Set productsShipping
     *
     * @param string $productsShipping
     *
     * @return Products
     */
    public function setProductsShipping($productsShipping)
    {
        $this->productsShipping = $productsShipping;

        return $this;
    }

    /**
     * Get productsShipping
     *
     * @return string
     */
    public function getProductsShipping()
    {
        return $this->productsShipping;
    }

    /**
     * Set productsShippingGroup
     *
     * @param string $productsShippingGroup
     *
     * @return Products
     */
    public function setProductsShippingGroup($productsShippingGroup)
    {
        $this->productsShippingGroup = $productsShippingGroup;

        return $this;
    }

    /**
     * Get productsShippingGroup
     *
     * @return string
     */
    public function getProductsShippingGroup()
    {
        return $this->productsShippingGroup;
    }

    /**
     * Set productsShippingPvm
     *
     * @param string $productsShippingPvm
     *
     * @return Products
     */
    public function setProductsShippingPvm($productsShippingPvm)
    {
        $this->productsShippingPvm = $productsShippingPvm;

        return $this;
    }

    /**
     * Get productsShippingPvm
     *
     * @return string
     */
    public function getProductsShippingPvm()
    {
        return $this->productsShippingPvm;
    }

    /**
     * Set productsStatus
     *
     * @param boolean $productsStatus
     *
     * @return Products
     */
    public function setProductsStatus($productsStatus)
    {
        $this->productsStatus = $productsStatus;

        return $this;
    }

    /**
     * Get productsStatus
     *
     * @return boolean
     */
    public function getProductsStatus()
    {
        return $this->productsStatus;
    }

    /**
     * Set productsTaxClassId
     *
     * @param integer $productsTaxClassId
     *
     * @return Products
     */
    public function setProductsTaxClassId($productsTaxClassId)
    {
        $this->productsTaxClassId = $productsTaxClassId;

        return $this;
    }

    /**
     * Get productsTaxClassId
     *
     * @return integer
     */
    public function getProductsTaxClassId()
    {
        return $this->productsTaxClassId;
    }

    /**
     * Set manufacturersId
     *
     * @param integer $manufacturersId
     *
     * @return Products
     */
    public function setManufacturersId($manufacturersId)
    {
        $this->manufacturersId = $manufacturersId;

        return $this;
    }

    /**
     * Get manufacturersId
     *
     * @return integer
     */
    public function getManufacturersId()
    {
        return $this->manufacturersId;
    }

    /**
     * Set productsOrdered
     *
     * @param integer $productsOrdered
     *
     * @return Products
     */
    public function setProductsOrdered($productsOrdered)
    {
        $this->productsOrdered = $productsOrdered;

        return $this;
    }

    /**
     * Get productsOrdered
     *
     * @return integer
     */
    public function getProductsOrdered()
    {
        return $this->productsOrdered;
    }

    /**
     * Set productsPmarketing
     *
     * @param boolean $productsPmarketing
     *
     * @return Products
     */
    public function setProductsPmarketing($productsPmarketing)
    {
        $this->productsPmarketing = $productsPmarketing;

        return $this;
    }

    /**
     * Get productsPmarketing
     *
     * @return boolean
     */
    public function getProductsPmarketing()
    {
        return $this->productsPmarketing;
    }

    /**
     * Set productsEantype
     *
     * @param boolean $productsEantype
     *
     * @return Products
     */
    public function setProductsEantype($productsEantype)
    {
        $this->productsEantype = $productsEantype;

        return $this;
    }

    /**
     * Get productsEantype
     *
     * @return boolean
     */
    public function getProductsEantype()
    {
        return $this->productsEantype;
    }

    /**
     * Set productsEan
     *
     * @param string $productsEan
     *
     * @return Products
     */
    public function setProductsEan($productsEan)
    {
        $this->productsEan = $productsEan;

        return $this;
    }

    /**
     * Get productsEan
     *
     * @return string
     */
    public function getProductsEan()
    {
        return $this->productsEan;
    }

    /**
     * Set productsModelOwn
     *
     * @param string $productsModelOwn
     *
     * @return Products
     */
    public function setProductsModelOwn($productsModelOwn)
    {
        $this->productsModelOwn = $productsModelOwn;

        return $this;
    }

    /**
     * Get productsModelOwn
     *
     * @return string
     */
    public function getProductsModelOwn()
    {
        return $this->productsModelOwn;
    }

    /**
     * Set productsAsin
     *
     * @param string $productsAsin
     *
     * @return Products
     */
    public function setProductsAsin($productsAsin)
    {
        $this->productsAsin = $productsAsin;

        return $this;
    }

    /**
     * Get productsAsin
     *
     * @return string
     */
    public function getProductsAsin()
    {
        return $this->productsAsin;
    }

    /**
     * Set productsUpc
     *
     * @param string $productsUpc
     *
     * @return Products
     */
    public function setProductsUpc($productsUpc)
    {
        $this->productsUpc = $productsUpc;

        return $this;
    }

    /**
     * Get productsUpc
     *
     * @return string
     */
    public function getProductsUpc()
    {
        return $this->productsUpc;
    }

    /**
     * Set productsIsbn
     *
     * @param string $productsIsbn
     *
     * @return Products
     */
    public function setProductsIsbn($productsIsbn)
    {
        $this->productsIsbn = $productsIsbn;

        return $this;
    }

    /**
     * Get productsIsbn
     *
     * @return string
     */
    public function getProductsIsbn()
    {
        return $this->productsIsbn;
    }

    /**
     * Set productsAmazoncond
     *
     * @param boolean $productsAmazoncond
     *
     * @return Products
     */
    public function setProductsAmazoncond($productsAmazoncond)
    {
        $this->productsAmazoncond = $productsAmazoncond;

        return $this;
    }

    /**
     * Get productsAmazoncond
     *
     * @return boolean
     */
    public function getProductsAmazoncond()
    {
        return $this->productsAmazoncond;
    }

    /**
     * Set productsAmazoncat
     *
     * @param string $productsAmazoncat
     *
     * @return Products
     */
    public function setProductsAmazoncat($productsAmazoncat)
    {
        $this->productsAmazoncat = $productsAmazoncat;

        return $this;
    }

    /**
     * Get productsAmazoncat
     *
     * @return string
     */
    public function getProductsAmazoncat()
    {
        return $this->productsAmazoncat;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Products
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set auktionwebId
     *
     * @param string $auktionwebId
     *
     * @return Products
     */
    public function setAuktionwebId($auktionwebId)
    {
        $this->auktionwebId = $auktionwebId;

        return $this;
    }

    /**
     * Get auktionwebId
     *
     * @return string
     */
    public function getAuktionwebId()
    {
        return $this->auktionwebId;
    }

    /**
     * Set afterbuyId
     *
     * @param string $afterbuyId
     *
     * @return Products
     */
    public function setAfterbuyId($afterbuyId)
    {
        $this->afterbuyId = $afterbuyId;

        return $this;
    }

    /**
     * Get afterbuyId
     *
     * @return string
     */
    public function getAfterbuyId()
    {
        return $this->afterbuyId;
    }

    /**
     * Set ebayId
     *
     * @param string $ebayId
     *
     * @return Products
     */
    public function setEbayId($ebayId)
    {
        $this->ebayId = $ebayId;

        return $this;
    }

    /**
     * Get ebayId
     *
     * @return string
     */
    public function getEbayId()
    {
        return $this->ebayId;
    }

    /**
     * Set importStatus
     *
     * @param boolean $importStatus
     *
     * @return Products
     */
    public function setImportStatus($importStatus)
    {
        $this->importStatus = $importStatus;

        return $this;
    }

    /**
     * Get importStatus
     *
     * @return boolean
     */
    public function getImportStatus()
    {
        return $this->importStatus;
    }

    /**
     * Set productsUvp
     *
     * @param string $productsUvp
     *
     * @return Products
     */
    public function setProductsUvp($productsUvp)
    {
        $this->productsUvp = $productsUvp;

        return $this;
    }

    /**
     * Get productsUvp
     *
     * @return string
     */
    public function getProductsUvp()
    {
        return $this->productsUvp;
    }

    /**
     * Set productsAvailability
     *
     * @param integer $productsAvailability
     *
     * @return Products
     */
    public function setProductsAvailability($productsAvailability)
    {
        $this->productsAvailability = $productsAvailability;

        return $this;
    }

    /**
     * Get productsAvailability
     *
     * @return integer
     */
    public function getProductsAvailability()
    {
        return $this->productsAvailability;
    }

    /**
     * Set productsDeliveryTime
     *
     * @param string $productsDeliveryTime
     *
     * @return Products
     */
    public function setProductsDeliveryTime($productsDeliveryTime)
    {
        $this->productsDeliveryTime = $productsDeliveryTime;

        return $this;
    }

    /**
     * Get productsDeliveryTime
     *
     * @return string
     */
    public function getProductsDeliveryTime()
    {
        return $this->productsDeliveryTime;
    }

    /**
     * Set productsUserNum1
     *
     * @param integer $productsUserNum1
     *
     * @return Products
     */
    public function setProductsUserNum1($productsUserNum1)
    {
        $this->productsUserNum1 = $productsUserNum1;

        return $this;
    }

    /**
     * Get productsUserNum1
     *
     * @return integer
     */
    public function getProductsUserNum1()
    {
        return $this->productsUserNum1;
    }

    /**
     * Set productsUserNum2
     *
     * @param string $productsUserNum2
     *
     * @return Products
     */
    public function setProductsUserNum2($productsUserNum2)
    {
        $this->productsUserNum2 = $productsUserNum2;

        return $this;
    }

    /**
     * Get productsUserNum2
     *
     * @return string
     */
    public function getProductsUserNum2()
    {
        return $this->productsUserNum2;
    }

    /**
     * Set productsUser1
     *
     * @param string $productsUser1
     *
     * @return Products
     */
    public function setProductsUser1($productsUser1)
    {
        $this->productsUser1 = $productsUser1;

        return $this;
    }

    /**
     * Get productsUser1
     *
     * @return string
     */
    public function getProductsUser1()
    {
        return $this->productsUser1;
    }

    /**
     * Set productsUser2
     *
     * @param string $productsUser2
     *
     * @return Products
     */
    public function setProductsUser2($productsUser2)
    {
        $this->productsUser2 = $productsUser2;

        return $this;
    }

    /**
     * Get productsUser2
     *
     * @return string
     */
    public function getProductsUser2()
    {
        return $this->productsUser2;
    }

    /**
     * Set productsUser3
     *
     * @param string $productsUser3
     *
     * @return Products
     */
    public function setProductsUser3($productsUser3)
    {
        $this->productsUser3 = $productsUser3;

        return $this;
    }

    /**
     * Get productsUser3
     *
     * @return string
     */
    public function getProductsUser3()
    {
        return $this->productsUser3;
    }

    /**
     * Set productsUser4
     *
     * @param string $productsUser4
     *
     * @return Products
     */
    public function setProductsUser4($productsUser4)
    {
        $this->productsUser4 = $productsUser4;

        return $this;
    }

    /**
     * Get productsUser4
     *
     * @return string
     */
    public function getProductsUser4()
    {
        return $this->productsUser4;
    }

    /**
     * Set productsVariants
     *
     * @param string $productsVariants
     *
     * @return Products
     */
    public function setProductsVariants($productsVariants)
    {
        $this->productsVariants = $productsVariants;

        return $this;
    }

    /**
     * Get productsVariants
     *
     * @return string
     */
    public function getProductsVariants()
    {
        return $this->productsVariants;
    }

    /**
     * Set productsPercentage
     *
     * @param boolean $productsPercentage
     *
     * @return Products
     */
    public function setProductsPercentage($productsPercentage)
    {
        $this->productsPercentage = $productsPercentage;

        return $this;
    }

    /**
     * Get productsPercentage
     *
     * @return boolean
     */
    public function getProductsPercentage()
    {
        return $this->productsPercentage;
    }

    /**
     * Set productsCondition
     *
     * @param boolean $productsCondition
     *
     * @return Products
     */
    public function setProductsCondition($productsCondition)
    {
        $this->productsCondition = $productsCondition;

        return $this;
    }

    /**
     * Get productsCondition
     *
     * @return boolean
     */
    public function getProductsCondition()
    {
        return $this->productsCondition;
    }

    /**
     * Set googlecat
     *
     * @param integer $googlecat
     *
     * @return Products
     */
    public function setGooglecat($googlecat)
    {
        $this->googlecat = $googlecat;

        return $this;
    }

    /**
     * Get googlecat
     *
     * @return integer
     */
    public function getGooglecat()
    {
        return $this->googlecat;
    }

    /**
     * Set productsDeliveryRestrictions
     *
     * @param string $productsDeliveryRestrictions
     *
     * @return Products
     */
    public function setProductsDeliveryRestrictions($productsDeliveryRestrictions)
    {
        $this->productsDeliveryRestrictions = $productsDeliveryRestrictions;

        return $this;
    }

    /**
     * Get productsDeliveryRestrictions
     *
     * @return string
     */
    public function getProductsDeliveryRestrictions()
    {
        return $this->productsDeliveryRestrictions;
    }
}
