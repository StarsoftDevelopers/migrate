<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * MatchingMoneybookers
 *
 * @ORM\Table(name="matching_moneybookers")
 * @ORM\Entity
 */
class MatchingMoneybookers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="temporderID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $temporderid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="orderID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $orderid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="transactionID", type="string", length=32, nullable=false)
     */
    private $transactionid = '';



    /**
     * Set temporderid
     *
     * @param integer $temporderid
     *
     * @return MatchingMoneybookers
     */
    public function setTemporderid($temporderid)
    {
        $this->temporderid = $temporderid;

        return $this;
    }

    /**
     * Get temporderid
     *
     * @return integer
     */
    public function getTemporderid()
    {
        return $this->temporderid;
    }

    /**
     * Set orderid
     *
     * @param integer $orderid
     *
     * @return MatchingMoneybookers
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return integer
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

    /**
     * Set transactionid
     *
     * @param string $transactionid
     *
     * @return MatchingMoneybookers
     */
    public function setTransactionid($transactionid)
    {
        $this->transactionid = $transactionid;

        return $this;
    }

    /**
     * Get transactionid
     *
     * @return string
     */
    public function getTransactionid()
    {
        return $this->transactionid;
    }
}
