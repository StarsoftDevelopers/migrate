<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * VisitorStatsTrace
 *
 * @ORM\Table(name="visitor_stats_trace")
 * @ORM\Entity
 */
class VisitorStatsTrace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="trace_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $traceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="visitors_id", type="integer", nullable=false)
     */
    private $visitorsId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=false)
     */
    private $uri = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';



    /**
     * Get traceId
     *
     * @return integer
     */
    public function getTraceId()
    {
        return $this->traceId;
    }

    /**
     * Set visitorsId
     *
     * @param integer $visitorsId
     *
     * @return VisitorStatsTrace
     */
    public function setVisitorsId($visitorsId)
    {
        $this->visitorsId = $visitorsId;

        return $this;
    }

    /**
     * Get visitorsId
     *
     * @return integer
     */
    public function getVisitorsId()
    {
        return $this->visitorsId;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return VisitorStatsTrace
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return VisitorStatsTrace
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
