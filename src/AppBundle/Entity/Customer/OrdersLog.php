<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrdersLog
 *
 * @ORM\Table(name="orders_log")
 * @ORM\Entity
 */
class OrdersLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="orders_log_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ordersLogId;

    /**
     * @var integer
     *
     * @ORM\Column(name="temporderID", type="integer", nullable=false)
     */
    private $temporderid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="transactionID", type="integer", nullable=false)
     */
    private $transactionid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="orderID", type="integer", nullable=false)
     */
    private $orderid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=2, nullable=false)
     */
    private $status = '';

    /**
     * @var string
     *
     * @ORM\Column(name="bestellung", type="string", length=1, nullable=false)
     */
    private $bestellung = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="date", nullable=false)
     */
    private $datum = '0000-00-00';



    /**
     * Get ordersLogId
     *
     * @return integer
     */
    public function getOrdersLogId()
    {
        return $this->ordersLogId;
    }

    /**
     * Set temporderid
     *
     * @param integer $temporderid
     *
     * @return OrdersLog
     */
    public function setTemporderid($temporderid)
    {
        $this->temporderid = $temporderid;

        return $this;
    }

    /**
     * Get temporderid
     *
     * @return integer
     */
    public function getTemporderid()
    {
        return $this->temporderid;
    }

    /**
     * Set transactionid
     *
     * @param integer $transactionid
     *
     * @return OrdersLog
     */
    public function setTransactionid($transactionid)
    {
        $this->transactionid = $transactionid;

        return $this;
    }

    /**
     * Get transactionid
     *
     * @return integer
     */
    public function getTransactionid()
    {
        return $this->transactionid;
    }

    /**
     * Set orderid
     *
     * @param integer $orderid
     *
     * @return OrdersLog
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return integer
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return OrdersLog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set bestellung
     *
     * @param string $bestellung
     *
     * @return OrdersLog
     */
    public function setBestellung($bestellung)
    {
        $this->bestellung = $bestellung;

        return $this;
    }

    /**
     * Get bestellung
     *
     * @return string
     */
    public function getBestellung()
    {
        return $this->bestellung;
    }

    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return OrdersLog
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }
}
