<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsMaster
 *
 * @ORM\Table(name="products_master")
 * @ORM\Entity
 */
class ProductsMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="string", length=128, nullable=false)
     */
    private $desc = '';



    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return ProductsMaster
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }
}
