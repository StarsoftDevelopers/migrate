<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShippingMethodsCustomergroups
 *
 * @ORM\Table(name="shipping_methods_customergroups")
 * @ORM\Entity
 */
class ShippingMethodsCustomergroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_group_id", type="integer", nullable=true)
     */
    private $customersGroupId;

    /**
     * @var integer
     *
     * @ORM\Column(name="shipping_method_id", type="integer", nullable=true)
     */
    private $shippingMethodId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customersGroupId
     *
     * @param integer $customersGroupId
     *
     * @return ShippingMethodsCustomergroups
     */
    public function setCustomersGroupId($customersGroupId)
    {
        $this->customersGroupId = $customersGroupId;

        return $this;
    }

    /**
     * Get customersGroupId
     *
     * @return integer
     */
    public function getCustomersGroupId()
    {
        return $this->customersGroupId;
    }

    /**
     * Set shippingMethodId
     *
     * @param integer $shippingMethodId
     *
     * @return ShippingMethodsCustomergroups
     */
    public function setShippingMethodId($shippingMethodId)
    {
        $this->shippingMethodId = $shippingMethodId;

        return $this;
    }

    /**
     * Get shippingMethodId
     *
     * @return integer
     */
    public function getShippingMethodId()
    {
        return $this->shippingMethodId;
    }
}
