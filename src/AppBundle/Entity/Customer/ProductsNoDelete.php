<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsNoDelete
 *
 * @ORM\Table(name="products_no_delete", indexes={@ORM\Index(name="products_model", columns={"products_model"}), @ORM\Index(name="products_id", columns={"products_id"})})
 * @ORM\Entity
 */
class ProductsNoDelete
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="products_model", type="string", length=32, nullable=false)
     */
    private $productsModel = '';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsNoDelete
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set productsModel
     *
     * @param string $productsModel
     *
     * @return ProductsNoDelete
     */
    public function setProductsModel($productsModel)
    {
        $this->productsModel = $productsModel;

        return $this;
    }

    /**
     * Get productsModel
     *
     * @return string
     */
    public function getProductsModel()
    {
        return $this->productsModel;
    }
}
