<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EkomiKunden
 *
 * @ORM\Table(name="ekomi_kunden", indexes={@ORM\Index(name="email", columns={"email"})})
 * @ORM\Entity
 */
class EkomiKunden
{
    /**
     * @var integer
     *
     * @ORM\Column(name="eintrag_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eintragId;

    /**
     * @var string
     *
     * @ORM\Column(name="kunden_id", type="string", length=50, nullable=false)
     */
    private $kundenId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=40, nullable=false)
     */
    private $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="vorname", type="string", length=40, nullable=false)
     */
    private $vorname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="nachname", type="string", length=40, nullable=false)
     */
    private $nachname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="eintrag_zeit", type="string", length=40, nullable=false)
     */
    private $eintragZeit = '';



    /**
     * Get eintragId
     *
     * @return integer
     */
    public function getEintragId()
    {
        return $this->eintragId;
    }

    /**
     * Set kundenId
     *
     * @param string $kundenId
     *
     * @return EkomiKunden
     */
    public function setKundenId($kundenId)
    {
        $this->kundenId = $kundenId;

        return $this;
    }

    /**
     * Get kundenId
     *
     * @return string
     */
    public function getKundenId()
    {
        return $this->kundenId;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return EkomiKunden
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set vorname
     *
     * @param string $vorname
     *
     * @return EkomiKunden
     */
    public function setVorname($vorname)
    {
        $this->vorname = $vorname;

        return $this;
    }

    /**
     * Get vorname
     *
     * @return string
     */
    public function getVorname()
    {
        return $this->vorname;
    }

    /**
     * Set nachname
     *
     * @param string $nachname
     *
     * @return EkomiKunden
     */
    public function setNachname($nachname)
    {
        $this->nachname = $nachname;

        return $this;
    }

    /**
     * Get nachname
     *
     * @return string
     */
    public function getNachname()
    {
        return $this->nachname;
    }

    /**
     * Set eintragZeit
     *
     * @param string $eintragZeit
     *
     * @return EkomiKunden
     */
    public function setEintragZeit($eintragZeit)
    {
        $this->eintragZeit = $eintragZeit;

        return $this;
    }

    /**
     * Get eintragZeit
     *
     * @return string
     */
    public function getEintragZeit()
    {
        return $this->eintragZeit;
    }
}
