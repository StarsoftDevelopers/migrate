<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartnerAnbieter
 *
 * @ORM\Table(name="partner_anbieter")
 * @ORM\Entity
 */
class PartnerAnbieter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Typ", type="boolean", nullable=false)
     */
    private $typ = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=128, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NameIntern", type="string", length=128, nullable=false)
     */
    private $nameintern = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="Status", type="boolean", nullable=false)
     */
    private $status = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="Partner_id", type="string", length=30, nullable=true)
     */
    private $partnerId;

    /**
     * @var float
     *
     * @ORM\Column(name="Provision", type="float", precision=10, scale=0, nullable=false)
     */
    private $provision = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="Standard", type="boolean", nullable=true)
     */
    private $standard;

    /**
     * @var string
     *
     * @ORM\Column(name="Payroll_Type", type="string", nullable=true)
     */
    private $payrollType;

    /**
     * @var string
     *
     * @ORM\Column(name="Login", type="string", length=50, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=50, nullable=true)
     */
    private $password;

    /**
     * @var float
     *
     * @ORM\Column(name="KlickPreis", type="float", precision=10, scale=0, nullable=true)
     */
    private $klickpreis;

    /**
     * @var integer
     *
     * @ORM\Column(name="PermStatus", type="integer", nullable=true)
     */
    private $permstatus;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typ
     *
     * @param boolean $typ
     *
     * @return PartnerAnbieter
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;

        return $this;
    }

    /**
     * Get typ
     *
     * @return boolean
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PartnerAnbieter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameintern
     *
     * @param string $nameintern
     *
     * @return PartnerAnbieter
     */
    public function setNameintern($nameintern)
    {
        $this->nameintern = $nameintern;

        return $this;
    }

    /**
     * Get nameintern
     *
     * @return string
     */
    public function getNameintern()
    {
        return $this->nameintern;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return PartnerAnbieter
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set partnerId
     *
     * @param string $partnerId
     *
     * @return PartnerAnbieter
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;

        return $this;
    }

    /**
     * Get partnerId
     *
     * @return string
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * Set provision
     *
     * @param float $provision
     *
     * @return PartnerAnbieter
     */
    public function setProvision($provision)
    {
        $this->provision = $provision;

        return $this;
    }

    /**
     * Get provision
     *
     * @return float
     */
    public function getProvision()
    {
        return $this->provision;
    }

    /**
     * Set standard
     *
     * @param boolean $standard
     *
     * @return PartnerAnbieter
     */
    public function setStandard($standard)
    {
        $this->standard = $standard;

        return $this;
    }

    /**
     * Get standard
     *
     * @return boolean
     */
    public function getStandard()
    {
        return $this->standard;
    }

    /**
     * Set payrollType
     *
     * @param string $payrollType
     *
     * @return PartnerAnbieter
     */
    public function setPayrollType($payrollType)
    {
        $this->payrollType = $payrollType;

        return $this;
    }

    /**
     * Get payrollType
     *
     * @return string
     */
    public function getPayrollType()
    {
        return $this->payrollType;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return PartnerAnbieter
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return PartnerAnbieter
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set klickpreis
     *
     * @param float $klickpreis
     *
     * @return PartnerAnbieter
     */
    public function setKlickpreis($klickpreis)
    {
        $this->klickpreis = $klickpreis;

        return $this;
    }

    /**
     * Get klickpreis
     *
     * @return float
     */
    public function getKlickpreis()
    {
        return $this->klickpreis;
    }

    /**
     * Set permstatus
     *
     * @param integer $permstatus
     *
     * @return PartnerAnbieter
     */
    public function setPermstatus($permstatus)
    {
        $this->permstatus = $permstatus;

        return $this;
    }

    /**
     * Get permstatus
     *
     * @return integer
     */
    public function getPermstatus()
    {
        return $this->permstatus;
    }
}
