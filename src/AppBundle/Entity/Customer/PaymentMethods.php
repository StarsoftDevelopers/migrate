<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentMethods
 *
 * @ORM\Table(name="payment_methods")
 * @ORM\Entity
 */
class PaymentMethods
{
    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentId;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", length=32, nullable=false)
     */
    private $paymentMethod = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_active", type="boolean", nullable=false)
     */
    private $paymentActive = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="max_value", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $maxValue;



    /**
     * Get paymentId
     *
     * @return integer
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     *
     * @return PaymentMethods
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set paymentActive
     *
     * @param boolean $paymentActive
     *
     * @return PaymentMethods
     */
    public function setPaymentActive($paymentActive)
    {
        $this->paymentActive = $paymentActive;

        return $this;
    }

    /**
     * Get paymentActive
     *
     * @return boolean
     */
    public function getPaymentActive()
    {
        return $this->paymentActive;
    }

    /**
     * Set maxValue
     *
     * @param string $maxValue
     *
     * @return PaymentMethods
     */
    public function setMaxValue($maxValue)
    {
        $this->maxValue = $maxValue;

        return $this;
    }

    /**
     * Get maxValue
     *
     * @return string
     */
    public function getMaxValue()
    {
        return $this->maxValue;
    }
}
