<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * QentaQpayTransaction
 *
 * @ORM\Table(name="qenta_qpay_transaction")
 * @ORM\Entity
 */
class QentaQpayTransaction
{
    /**
     * @var string
     *
     * @ORM\Column(name="TRID", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $trid = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="PAYSYS", type="string", length=50, nullable=false)
     */
    private $paysys = '';

    /**
     * @var string
     *
     * @ORM\Column(name="BRAND", type="string", length=100, nullable=false)
     */
    private $brand = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="QENTA_ORDERNUMBER", type="integer", nullable=false)
     */
    private $qentaOrdernumber = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="QENTA_ORDERDESCRIPTION", type="string", length=255, nullable=false)
     */
    private $qentaOrderdescription = '';

    /**
     * @var string
     *
     * @ORM\Column(name="QENTA_STATE", type="string", length=20, nullable=false)
     */
    private $qentaState = '';

    /**
     * @var string
     *
     * @ORM\Column(name="QENTA_MESSAGE", type="string", length=255, nullable=false)
     */
    private $qentaMessage = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDERID", type="integer", nullable=false)
     */
    private $orderid = '0';



    /**
     * Get trid
     *
     * @return string
     */
    public function getTrid()
    {
        return $this->trid;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return QentaQpayTransaction
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set paysys
     *
     * @param string $paysys
     *
     * @return QentaQpayTransaction
     */
    public function setPaysys($paysys)
    {
        $this->paysys = $paysys;

        return $this;
    }

    /**
     * Get paysys
     *
     * @return string
     */
    public function getPaysys()
    {
        return $this->paysys;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return QentaQpayTransaction
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set qentaOrdernumber
     *
     * @param integer $qentaOrdernumber
     *
     * @return QentaQpayTransaction
     */
    public function setQentaOrdernumber($qentaOrdernumber)
    {
        $this->qentaOrdernumber = $qentaOrdernumber;

        return $this;
    }

    /**
     * Get qentaOrdernumber
     *
     * @return integer
     */
    public function getQentaOrdernumber()
    {
        return $this->qentaOrdernumber;
    }

    /**
     * Set qentaOrderdescription
     *
     * @param string $qentaOrderdescription
     *
     * @return QentaQpayTransaction
     */
    public function setQentaOrderdescription($qentaOrderdescription)
    {
        $this->qentaOrderdescription = $qentaOrderdescription;

        return $this;
    }

    /**
     * Get qentaOrderdescription
     *
     * @return string
     */
    public function getQentaOrderdescription()
    {
        return $this->qentaOrderdescription;
    }

    /**
     * Set qentaState
     *
     * @param string $qentaState
     *
     * @return QentaQpayTransaction
     */
    public function setQentaState($qentaState)
    {
        $this->qentaState = $qentaState;

        return $this;
    }

    /**
     * Get qentaState
     *
     * @return string
     */
    public function getQentaState()
    {
        return $this->qentaState;
    }

    /**
     * Set qentaMessage
     *
     * @param string $qentaMessage
     *
     * @return QentaQpayTransaction
     */
    public function setQentaMessage($qentaMessage)
    {
        $this->qentaMessage = $qentaMessage;

        return $this;
    }

    /**
     * Get qentaMessage
     *
     * @return string
     */
    public function getQentaMessage()
    {
        return $this->qentaMessage;
    }

    /**
     * Set orderid
     *
     * @param integer $orderid
     *
     * @return QentaQpayTransaction
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return integer
     */
    public function getOrderid()
    {
        return $this->orderid;
    }
}
