<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarketingExportLog
 *
 * @ORM\Table(name="marketing_export_log")
 * @ORM\Entity
 */
class MarketingExportLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="export_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $exportId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="export_date", type="datetime", nullable=false)
     */
    private $exportDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     */
    private $affiliateId = '0';



    /**
     * Get exportId
     *
     * @return integer
     */
    public function getExportId()
    {
        return $this->exportId;
    }

    /**
     * Set exportDate
     *
     * @param \DateTime $exportDate
     *
     * @return MarketingExportLog
     */
    public function setExportDate($exportDate)
    {
        $this->exportDate = $exportDate;

        return $this;
    }

    /**
     * Get exportDate
     *
     * @return \DateTime
     */
    public function getExportDate()
    {
        return $this->exportDate;
    }

    /**
     * Set affiliateId
     *
     * @param integer $affiliateId
     *
     * @return MarketingExportLog
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;

        return $this;
    }

    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }
}
