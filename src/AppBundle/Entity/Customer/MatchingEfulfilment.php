<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * MatchingEfulfilment
 *
 * @ORM\Table(name="matching_efulfilment")
 * @ORM\Entity
 */
class MatchingEfulfilment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="OwnerID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $ownerid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="OuterID", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $outerid = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="Type", type="boolean", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $type = '0';



    /**
     * Set ownerid
     *
     * @param integer $ownerid
     *
     * @return MatchingEfulfilment
     */
    public function setOwnerid($ownerid)
    {
        $this->ownerid = $ownerid;

        return $this;
    }

    /**
     * Get ownerid
     *
     * @return integer
     */
    public function getOwnerid()
    {
        return $this->ownerid;
    }

    /**
     * Set outerid
     *
     * @param string $outerid
     *
     * @return MatchingEfulfilment
     */
    public function setOuterid($outerid)
    {
        $this->outerid = $outerid;

        return $this;
    }

    /**
     * Get outerid
     *
     * @return string
     */
    public function getOuterid()
    {
        return $this->outerid;
    }

    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return MatchingEfulfilment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }
}
