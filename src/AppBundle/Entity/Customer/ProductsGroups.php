<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsGroups
 *
 * @ORM\Table(name="products_groups")
 * @ORM\Entity
 */
class ProductsGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_group_id", type="integer", nullable=false)
     */
    private $customersGroupId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_group_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $customersGroupPrice = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="products_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $productsPrice = '0.00';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customersGroupId
     *
     * @param integer $customersGroupId
     *
     * @return ProductsGroups
     */
    public function setCustomersGroupId($customersGroupId)
    {
        $this->customersGroupId = $customersGroupId;

        return $this;
    }

    /**
     * Get customersGroupId
     *
     * @return integer
     */
    public function getCustomersGroupId()
    {
        return $this->customersGroupId;
    }

    /**
     * Set customersGroupPrice
     *
     * @param string $customersGroupPrice
     *
     * @return ProductsGroups
     */
    public function setCustomersGroupPrice($customersGroupPrice)
    {
        $this->customersGroupPrice = $customersGroupPrice;

        return $this;
    }

    /**
     * Get customersGroupPrice
     *
     * @return string
     */
    public function getCustomersGroupPrice()
    {
        return $this->customersGroupPrice;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsGroups
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set productsPrice
     *
     * @param string $productsPrice
     *
     * @return ProductsGroups
     */
    public function setProductsPrice($productsPrice)
    {
        $this->productsPrice = $productsPrice;

        return $this;
    }

    /**
     * Get productsPrice
     *
     * @return string
     */
    public function getProductsPrice()
    {
        return $this->productsPrice;
    }
}
