<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CsvFormatColumns
 *
 * @ORM\Table(name="csv_format_columns", indexes={@ORM\Index(name="csv_format_id", columns={"csv_format_id"})})
 * @ORM\Entity
 */
class CsvFormatColumns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="csv_column_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $csvColumnId;

    /**
     * @var integer
     *
     * @ORM\Column(name="csv_format_id", type="integer", nullable=false)
     */
    private $csvFormatId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="columns_sort_order", type="integer", nullable=false)
     */
    private $columnsSortOrder = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="columns_name", type="string", length=32, nullable=false)
     */
    private $columnsName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="columns_identifier", type="string", length=64, nullable=false)
     */
    private $columnsIdentifier = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="columns_fieldtype", type="boolean", nullable=false)
     */
    private $columnsFieldtype = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="columns_fieldlength", type="integer", nullable=false)
     */
    private $columnsFieldlength = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="columns_prefix", type="string", length=128, nullable=false)
     */
    private $columnsPrefix = '';

    /**
     * @var string
     *
     * @ORM\Column(name="columns_suffix", type="string", length=128, nullable=false)
     */
    private $columnsSuffix = '';

    /**
     * @var string
     *
     * @ORM\Column(name="columns_db_column", type="string", length=64, nullable=false)
     */
    private $columnsDbColumn = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="columns_convert_function", type="integer", nullable=false)
     */
    private $columnsConvertFunction = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="columns_required", type="boolean", nullable=true)
     */
    private $columnsRequired = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="columns_description", type="text", length=65535, nullable=true)
     */
    private $columnsDescription;



    /**
     * Get csvColumnId
     *
     * @return integer
     */
    public function getCsvColumnId()
    {
        return $this->csvColumnId;
    }

    /**
     * Set csvFormatId
     *
     * @param integer $csvFormatId
     *
     * @return CsvFormatColumns
     */
    public function setCsvFormatId($csvFormatId)
    {
        $this->csvFormatId = $csvFormatId;

        return $this;
    }

    /**
     * Get csvFormatId
     *
     * @return integer
     */
    public function getCsvFormatId()
    {
        return $this->csvFormatId;
    }

    /**
     * Set columnsSortOrder
     *
     * @param integer $columnsSortOrder
     *
     * @return CsvFormatColumns
     */
    public function setColumnsSortOrder($columnsSortOrder)
    {
        $this->columnsSortOrder = $columnsSortOrder;

        return $this;
    }

    /**
     * Get columnsSortOrder
     *
     * @return integer
     */
    public function getColumnsSortOrder()
    {
        return $this->columnsSortOrder;
    }

    /**
     * Set columnsName
     *
     * @param string $columnsName
     *
     * @return CsvFormatColumns
     */
    public function setColumnsName($columnsName)
    {
        $this->columnsName = $columnsName;

        return $this;
    }

    /**
     * Get columnsName
     *
     * @return string
     */
    public function getColumnsName()
    {
        return $this->columnsName;
    }

    /**
     * Set columnsIdentifier
     *
     * @param string $columnsIdentifier
     *
     * @return CsvFormatColumns
     */
    public function setColumnsIdentifier($columnsIdentifier)
    {
        $this->columnsIdentifier = $columnsIdentifier;

        return $this;
    }

    /**
     * Get columnsIdentifier
     *
     * @return string
     */
    public function getColumnsIdentifier()
    {
        return $this->columnsIdentifier;
    }

    /**
     * Set columnsFieldtype
     *
     * @param boolean $columnsFieldtype
     *
     * @return CsvFormatColumns
     */
    public function setColumnsFieldtype($columnsFieldtype)
    {
        $this->columnsFieldtype = $columnsFieldtype;

        return $this;
    }

    /**
     * Get columnsFieldtype
     *
     * @return boolean
     */
    public function getColumnsFieldtype()
    {
        return $this->columnsFieldtype;
    }

    /**
     * Set columnsFieldlength
     *
     * @param integer $columnsFieldlength
     *
     * @return CsvFormatColumns
     */
    public function setColumnsFieldlength($columnsFieldlength)
    {
        $this->columnsFieldlength = $columnsFieldlength;

        return $this;
    }

    /**
     * Get columnsFieldlength
     *
     * @return integer
     */
    public function getColumnsFieldlength()
    {
        return $this->columnsFieldlength;
    }

    /**
     * Set columnsPrefix
     *
     * @param string $columnsPrefix
     *
     * @return CsvFormatColumns
     */
    public function setColumnsPrefix($columnsPrefix)
    {
        $this->columnsPrefix = $columnsPrefix;

        return $this;
    }

    /**
     * Get columnsPrefix
     *
     * @return string
     */
    public function getColumnsPrefix()
    {
        return $this->columnsPrefix;
    }

    /**
     * Set columnsSuffix
     *
     * @param string $columnsSuffix
     *
     * @return CsvFormatColumns
     */
    public function setColumnsSuffix($columnsSuffix)
    {
        $this->columnsSuffix = $columnsSuffix;

        return $this;
    }

    /**
     * Get columnsSuffix
     *
     * @return string
     */
    public function getColumnsSuffix()
    {
        return $this->columnsSuffix;
    }

    /**
     * Set columnsDbColumn
     *
     * @param string $columnsDbColumn
     *
     * @return CsvFormatColumns
     */
    public function setColumnsDbColumn($columnsDbColumn)
    {
        $this->columnsDbColumn = $columnsDbColumn;

        return $this;
    }

    /**
     * Get columnsDbColumn
     *
     * @return string
     */
    public function getColumnsDbColumn()
    {
        return $this->columnsDbColumn;
    }

    /**
     * Set columnsConvertFunction
     *
     * @param integer $columnsConvertFunction
     *
     * @return CsvFormatColumns
     */
    public function setColumnsConvertFunction($columnsConvertFunction)
    {
        $this->columnsConvertFunction = $columnsConvertFunction;

        return $this;
    }

    /**
     * Get columnsConvertFunction
     *
     * @return integer
     */
    public function getColumnsConvertFunction()
    {
        return $this->columnsConvertFunction;
    }

    /**
     * Set columnsRequired
     *
     * @param boolean $columnsRequired
     *
     * @return CsvFormatColumns
     */
    public function setColumnsRequired($columnsRequired)
    {
        $this->columnsRequired = $columnsRequired;

        return $this;
    }

    /**
     * Get columnsRequired
     *
     * @return boolean
     */
    public function getColumnsRequired()
    {
        return $this->columnsRequired;
    }

    /**
     * Set columnsDescription
     *
     * @param string $columnsDescription
     *
     * @return CsvFormatColumns
     */
    public function setColumnsDescription($columnsDescription)
    {
        $this->columnsDescription = $columnsDescription;

        return $this;
    }

    /**
     * Get columnsDescription
     *
     * @return string
     */
    public function getColumnsDescription()
    {
        return $this->columnsDescription;
    }
}
