<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesMvariation
 *
 * @ORM\Table(name="eazysales_mvariation")
 * @ORM\Entity
 */
class EazysalesMvariation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="kEigenschaft", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $keigenschaft = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_options_id", type="integer", nullable=true)
     */
    private $productsOptionsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="kArtikel", type="integer", nullable=true)
     */
    private $kartikel;



    /**
     * Get keigenschaft
     *
     * @return integer
     */
    public function getKeigenschaft()
    {
        return $this->keigenschaft;
    }

    /**
     * Set productsOptionsId
     *
     * @param integer $productsOptionsId
     *
     * @return EazysalesMvariation
     */
    public function setProductsOptionsId($productsOptionsId)
    {
        $this->productsOptionsId = $productsOptionsId;

        return $this;
    }

    /**
     * Get productsOptionsId
     *
     * @return integer
     */
    public function getProductsOptionsId()
    {
        return $this->productsOptionsId;
    }

    /**
     * Set kartikel
     *
     * @param integer $kartikel
     *
     * @return EazysalesMvariation
     */
    public function setKartikel($kartikel)
    {
        $this->kartikel = $kartikel;

        return $this;
    }

    /**
     * Get kartikel
     *
     * @return integer
     */
    public function getKartikel()
    {
        return $this->kartikel;
    }
}
