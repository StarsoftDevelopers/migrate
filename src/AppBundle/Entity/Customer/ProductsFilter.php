<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsFilter
 *
 * @ORM\Table(name="products_filter")
 * @ORM\Entity
 */
class ProductsFilter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_filter_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsFilterId;

    /**
     * @var integer
     *
     * @ORM\Column(name="options_id", type="integer", nullable=false)
     */
    private $optionsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="options_values_id", type="integer", nullable=false)
     */
    private $optionsValuesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId;



    /**
     * Get productsFilterId
     *
     * @return integer
     */
    public function getProductsFilterId()
    {
        return $this->productsFilterId;
    }

    /**
     * Set optionsId
     *
     * @param integer $optionsId
     *
     * @return ProductsFilter
     */
    public function setOptionsId($optionsId)
    {
        $this->optionsId = $optionsId;

        return $this;
    }

    /**
     * Get optionsId
     *
     * @return integer
     */
    public function getOptionsId()
    {
        return $this->optionsId;
    }

    /**
     * Set optionsValuesId
     *
     * @param integer $optionsValuesId
     *
     * @return ProductsFilter
     */
    public function setOptionsValuesId($optionsValuesId)
    {
        $this->optionsValuesId = $optionsValuesId;

        return $this;
    }

    /**
     * Get optionsValuesId
     *
     * @return integer
     */
    public function getOptionsValuesId()
    {
        return $this->optionsValuesId;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsFilter
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }
}
