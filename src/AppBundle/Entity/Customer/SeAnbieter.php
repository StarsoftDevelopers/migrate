<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * SeAnbieter
 *
 * @ORM\Table(name="se_anbieter")
 * @ORM\Entity
 */
class SeAnbieter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=128, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NameIntern", type="string", length=100, nullable=true)
     */
    private $nameintern;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Status", type="boolean", nullable=false)
     */
    private $status = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="Budget", type="float", precision=10, scale=0, nullable=false)
     */
    private $budget = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="BudgetMonat", type="float", precision=10, scale=0, nullable=false)
     */
    private $budgetmonat = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="KlicksMonat", type="integer", nullable=false)
     */
    private $klicksmonat = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="KlickPreis", type="float", precision=10, scale=0, nullable=false)
     */
    private $klickpreis = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MonatAktiv", type="string", length=6, nullable=true)
     */
    private $monataktiv;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cflag", type="boolean", nullable=true)
     */
    private $cflag;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SeAnbieter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameintern
     *
     * @param string $nameintern
     *
     * @return SeAnbieter
     */
    public function setNameintern($nameintern)
    {
        $this->nameintern = $nameintern;

        return $this;
    }

    /**
     * Get nameintern
     *
     * @return string
     */
    public function getNameintern()
    {
        return $this->nameintern;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return SeAnbieter
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set budget
     *
     * @param float $budget
     *
     * @return SeAnbieter
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return float
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set budgetmonat
     *
     * @param float $budgetmonat
     *
     * @return SeAnbieter
     */
    public function setBudgetmonat($budgetmonat)
    {
        $this->budgetmonat = $budgetmonat;

        return $this;
    }

    /**
     * Get budgetmonat
     *
     * @return float
     */
    public function getBudgetmonat()
    {
        return $this->budgetmonat;
    }

    /**
     * Set klicksmonat
     *
     * @param integer $klicksmonat
     *
     * @return SeAnbieter
     */
    public function setKlicksmonat($klicksmonat)
    {
        $this->klicksmonat = $klicksmonat;

        return $this;
    }

    /**
     * Get klicksmonat
     *
     * @return integer
     */
    public function getKlicksmonat()
    {
        return $this->klicksmonat;
    }

    /**
     * Set klickpreis
     *
     * @param float $klickpreis
     *
     * @return SeAnbieter
     */
    public function setKlickpreis($klickpreis)
    {
        $this->klickpreis = $klickpreis;

        return $this;
    }

    /**
     * Get klickpreis
     *
     * @return float
     */
    public function getKlickpreis()
    {
        return $this->klickpreis;
    }

    /**
     * Set monataktiv
     *
     * @param string $monataktiv
     *
     * @return SeAnbieter
     */
    public function setMonataktiv($monataktiv)
    {
        $this->monataktiv = $monataktiv;

        return $this;
    }

    /**
     * Get monataktiv
     *
     * @return string
     */
    public function getMonataktiv()
    {
        return $this->monataktiv;
    }

    /**
     * Set cflag
     *
     * @param boolean $cflag
     *
     * @return SeAnbieter
     */
    public function setCflag($cflag)
    {
        $this->cflag = $cflag;

        return $this;
    }

    /**
     * Get cflag
     *
     * @return boolean
     */
    public function getCflag()
    {
        return $this->cflag;
    }
}
