<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * VisitorsTrace
 *
 * @ORM\Table(name="visitors_trace")
 * @ORM\Entity
 */
class VisitorsTrace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="trace_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $traceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_id", type="integer", nullable=false)
     */
    private $customersId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="browser_ip", type="string", length=15, nullable=false)
     */
    private $browserIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=false)
     */
    private $uri = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';



    /**
     * Get traceId
     *
     * @return integer
     */
    public function getTraceId()
    {
        return $this->traceId;
    }

    /**
     * Set customersId
     *
     * @param integer $customersId
     *
     * @return VisitorsTrace
     */
    public function setCustomersId($customersId)
    {
        $this->customersId = $customersId;

        return $this;
    }

    /**
     * Get customersId
     *
     * @return integer
     */
    public function getCustomersId()
    {
        return $this->customersId;
    }

    /**
     * Set browserIp
     *
     * @param string $browserIp
     *
     * @return VisitorsTrace
     */
    public function setBrowserIp($browserIp)
    {
        $this->browserIp = $browserIp;

        return $this;
    }

    /**
     * Get browserIp
     *
     * @return string
     */
    public function getBrowserIp()
    {
        return $this->browserIp;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return VisitorsTrace
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return VisitorsTrace
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
