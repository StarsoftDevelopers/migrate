<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * AfterbuyPaymentMethod
 *
 * @ORM\Table(name="afterbuy_payment_method")
 * @ORM\Entity
 */
class AfterbuyPaymentMethod
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="afterbuy_id", type="string", length=15, nullable=false)
     */
    private $afterbuyId;

    /**
     * @var string
     *
     * @ORM\Column(name="afterbuy_payment", type="string", length=50, nullable=false)
     */
    private $afterbuyPayment = '';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set afterbuyId
     *
     * @param string $afterbuyId
     *
     * @return AfterbuyPaymentMethod
     */
    public function setAfterbuyId($afterbuyId)
    {
        $this->afterbuyId = $afterbuyId;

        return $this;
    }

    /**
     * Get afterbuyId
     *
     * @return string
     */
    public function getAfterbuyId()
    {
        return $this->afterbuyId;
    }

    /**
     * Set afterbuyPayment
     *
     * @param string $afterbuyPayment
     *
     * @return AfterbuyPaymentMethod
     */
    public function setAfterbuyPayment($afterbuyPayment)
    {
        $this->afterbuyPayment = $afterbuyPayment;

        return $this;
    }

    /**
     * Get afterbuyPayment
     *
     * @return string
     */
    public function getAfterbuyPayment()
    {
        return $this->afterbuyPayment;
    }
}
