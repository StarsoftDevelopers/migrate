<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * VisitorStats
 *
 * @ORM\Table(name="visitor_stats", indexes={@ORM\Index(name="date", columns={"date"})})
 * @ORM\Entity
 */
class VisitorStats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="visitors_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $visitorsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_clickthroughs_id", type="integer", nullable=true)
     */
    private $affiliateClickthroughsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="online_time", type="integer", nullable=false)
     */
    private $onlineTime = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="clicks", type="integer", nullable=false)
     */
    private $clicks = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="browser_ip", type="integer", nullable=false)
     */
    private $browserIp = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_id", type="integer", nullable=true)
     */
    private $customersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     */
    private $languagesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="entry_page", type="string", length=255, nullable=false)
     */
    private $entryPage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="referer", type="string", length=255, nullable=true)
     */
    private $referer;

    /**
     * @var integer
     *
     * @ORM\Column(name="se_id", type="integer", nullable=true)
     */
    private $seId;

    /**
     * @var string
     *
     * @ORM\Column(name="se_query", type="string", length=32, nullable=true)
     */
    private $seQuery;



    /**
     * Get visitorsId
     *
     * @return integer
     */
    public function getVisitorsId()
    {
        return $this->visitorsId;
    }

    /**
     * Set affiliateClickthroughsId
     *
     * @param integer $affiliateClickthroughsId
     *
     * @return VisitorStats
     */
    public function setAffiliateClickthroughsId($affiliateClickthroughsId)
    {
        $this->affiliateClickthroughsId = $affiliateClickthroughsId;

        return $this;
    }

    /**
     * Get affiliateClickthroughsId
     *
     * @return integer
     */
    public function getAffiliateClickthroughsId()
    {
        return $this->affiliateClickthroughsId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return VisitorStats
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set onlineTime
     *
     * @param integer $onlineTime
     *
     * @return VisitorStats
     */
    public function setOnlineTime($onlineTime)
    {
        $this->onlineTime = $onlineTime;

        return $this;
    }

    /**
     * Get onlineTime
     *
     * @return integer
     */
    public function getOnlineTime()
    {
        return $this->onlineTime;
    }

    /**
     * Set clicks
     *
     * @param integer $clicks
     *
     * @return VisitorStats
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;

        return $this;
    }

    /**
     * Get clicks
     *
     * @return integer
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * Set browserIp
     *
     * @param integer $browserIp
     *
     * @return VisitorStats
     */
    public function setBrowserIp($browserIp)
    {
        $this->browserIp = $browserIp;

        return $this;
    }

    /**
     * Get browserIp
     *
     * @return integer
     */
    public function getBrowserIp()
    {
        return $this->browserIp;
    }

    /**
     * Set customersId
     *
     * @param integer $customersId
     *
     * @return VisitorStats
     */
    public function setCustomersId($customersId)
    {
        $this->customersId = $customersId;

        return $this;
    }

    /**
     * Get customersId
     *
     * @return integer
     */
    public function getCustomersId()
    {
        return $this->customersId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return VisitorStats
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set entryPage
     *
     * @param string $entryPage
     *
     * @return VisitorStats
     */
    public function setEntryPage($entryPage)
    {
        $this->entryPage = $entryPage;

        return $this;
    }

    /**
     * Get entryPage
     *
     * @return string
     */
    public function getEntryPage()
    {
        return $this->entryPage;
    }

    /**
     * Set referer
     *
     * @param string $referer
     *
     * @return VisitorStats
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set seId
     *
     * @param integer $seId
     *
     * @return VisitorStats
     */
    public function setSeId($seId)
    {
        $this->seId = $seId;

        return $this;
    }

    /**
     * Get seId
     *
     * @return integer
     */
    public function getSeId()
    {
        return $this->seId;
    }

    /**
     * Set seQuery
     *
     * @param string $seQuery
     *
     * @return VisitorStats
     */
    public function setSeQuery($seQuery)
    {
        $this->seQuery = $seQuery;

        return $this;
    }

    /**
     * Get seQuery
     *
     * @return string
     */
    public function getSeQuery()
    {
        return $this->seQuery;
    }
}
