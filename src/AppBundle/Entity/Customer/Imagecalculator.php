<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imagecalculator
 *
 * @ORM\Table(name="imagecalculator")
 * @ORM\Entity
 */
class Imagecalculator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="table", type="string", length=40, nullable=false)
     */
    private $table;

    /**
     * @var string
     *
     * @ORM\Column(name="pk_field", type="string", length=30, nullable=false)
     */
    private $pkField;

    /**
     * @var string
     *
     * @ORM\Column(name="pk_value", type="string", length=20, nullable=false)
     */
    private $pkValue;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=40, nullable=false)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=30, nullable=false)
     */
    private $filename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created = 'CURRENT_TIMESTAMP';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set table
     *
     * @param string $table
     *
     * @return Imagecalculator
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Get table
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set pkField
     *
     * @param string $pkField
     *
     * @return Imagecalculator
     */
    public function setPkField($pkField)
    {
        $this->pkField = $pkField;

        return $this;
    }

    /**
     * Get pkField
     *
     * @return string
     */
    public function getPkField()
    {
        return $this->pkField;
    }

    /**
     * Set pkValue
     *
     * @param string $pkValue
     *
     * @return Imagecalculator
     */
    public function setPkValue($pkValue)
    {
        $this->pkValue = $pkValue;

        return $this;
    }

    /**
     * Get pkValue
     *
     * @return string
     */
    public function getPkValue()
    {
        return $this->pkValue;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Imagecalculator
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set field
     *
     * @param string $field
     *
     * @return Imagecalculator
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Imagecalculator
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Imagecalculator
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Imagecalculator
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}
