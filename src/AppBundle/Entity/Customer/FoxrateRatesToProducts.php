<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * FoxrateRatesToProducts
 *
 * @ORM\Table(name="foxrate_rates_to_products", uniqueConstraints={@ORM\UniqueConstraint(name="products_id", columns={"products_id"})})
 * @ORM\Entity
 */
class FoxrateRatesToProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId;

    /**
     * @var string
     *
     * @ORM\Column(name="rate_info", type="text", length=65535, nullable=false)
     */
    private $rateInfo;

    /**
     * @var integer
     *
     * @ORM\Column(name="time", type="integer", nullable=false)
     */
    private $time;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return FoxrateRatesToProducts
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set rateInfo
     *
     * @param string $rateInfo
     *
     * @return FoxrateRatesToProducts
     */
    public function setRateInfo($rateInfo)
    {
        $this->rateInfo = $rateInfo;

        return $this;
    }

    /**
     * Get rateInfo
     *
     * @return string
     */
    public function getRateInfo()
    {
        return $this->rateInfo;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return FoxrateRatesToProducts
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }
}
