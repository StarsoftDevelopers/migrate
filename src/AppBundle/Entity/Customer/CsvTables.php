<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CsvTables
 *
 * @ORM\Table(name="csv_tables")
 * @ORM\Entity
 */
class CsvTables
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer", nullable=false)
     */
    private $accountId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="import_table", type="string", length=32, nullable=false)
     */
    private $importTable = '';

    /**
     * @var string
     *
     * @ORM\Column(name="import_row", type="string", length=32, nullable=false)
     */
    private $importRow = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="import_position", type="boolean", nullable=false)
     */
    private $importPosition = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountId
     *
     * @param integer $accountId
     *
     * @return CsvTables
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Get accountId
     *
     * @return integer
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Set importTable
     *
     * @param string $importTable
     *
     * @return CsvTables
     */
    public function setImportTable($importTable)
    {
        $this->importTable = $importTable;

        return $this;
    }

    /**
     * Get importTable
     *
     * @return string
     */
    public function getImportTable()
    {
        return $this->importTable;
    }

    /**
     * Set importRow
     *
     * @param string $importRow
     *
     * @return CsvTables
     */
    public function setImportRow($importRow)
    {
        $this->importRow = $importRow;

        return $this;
    }

    /**
     * Get importRow
     *
     * @return string
     */
    public function getImportRow()
    {
        return $this->importRow;
    }

    /**
     * Set importPosition
     *
     * @param boolean $importPosition
     *
     * @return CsvTables
     */
    public function setImportPosition($importPosition)
    {
        $this->importPosition = $importPosition;

        return $this;
    }

    /**
     * Get importPosition
     *
     * @return boolean
     */
    public function getImportPosition()
    {
        return $this->importPosition;
    }
}
