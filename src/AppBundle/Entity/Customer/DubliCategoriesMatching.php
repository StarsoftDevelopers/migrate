<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * DubliCategoriesMatching
 *
 * @ORM\Table(name="dubli_categories_matching")
 * @ORM\Entity
 */
class DubliCategoriesMatching
{
    /**
     * @var integer
     *
     * @ORM\Column(name="OwnerID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ownerid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DubliID", type="integer", nullable=false)
     */
    private $dubliid = '0';



    /**
     * Get ownerid
     *
     * @return integer
     */
    public function getOwnerid()
    {
        return $this->ownerid;
    }

    /**
     * Set dubliid
     *
     * @param integer $dubliid
     *
     * @return DubliCategoriesMatching
     */
    public function setDubliid($dubliid)
    {
        $this->dubliid = $dubliid;

        return $this;
    }

    /**
     * Get dubliid
     *
     * @return integer
     */
    public function getDubliid()
    {
        return $this->dubliid;
    }
}
