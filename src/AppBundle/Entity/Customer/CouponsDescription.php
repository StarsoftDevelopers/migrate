<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CouponsDescription
 *
 * @ORM\Table(name="coupons_description", indexes={@ORM\Index(name="coupon_id", columns={"coupon_id"})})
 * @ORM\Entity
 */
class CouponsDescription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_id", type="integer", nullable=false)
     */
    private $couponId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     */
    private $languageId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="coupon_name", type="string", length=32, nullable=false)
     */
    private $couponName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="coupon_description", type="text", length=65535, nullable=true)
     */
    private $couponDescription;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set couponId
     *
     * @param integer $couponId
     *
     * @return CouponsDescription
     */
    public function setCouponId($couponId)
    {
        $this->couponId = $couponId;

        return $this;
    }

    /**
     * Get couponId
     *
     * @return integer
     */
    public function getCouponId()
    {
        return $this->couponId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return CouponsDescription
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set couponName
     *
     * @param string $couponName
     *
     * @return CouponsDescription
     */
    public function setCouponName($couponName)
    {
        $this->couponName = $couponName;

        return $this;
    }

    /**
     * Get couponName
     *
     * @return string
     */
    public function getCouponName()
    {
        return $this->couponName;
    }

    /**
     * Set couponDescription
     *
     * @param string $couponDescription
     *
     * @return CouponsDescription
     */
    public function setCouponDescription($couponDescription)
    {
        $this->couponDescription = $couponDescription;

        return $this;
    }

    /**
     * Get couponDescription
     *
     * @return string
     */
    public function getCouponDescription()
    {
        return $this->couponDescription;
    }
}
