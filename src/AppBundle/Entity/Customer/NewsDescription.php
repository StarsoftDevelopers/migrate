<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsDescription
 *
 * @ORM\Table(name="news_description")
 * @ORM\Entity
 */
class NewsDescription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="news_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $newsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="news_title", type="text", length=65535, nullable=false)
     */
    private $newsTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="news_teaser", type="text", length=65535, nullable=false)
     */
    private $newsTeaser;

    /**
     * @var string
     *
     * @ORM\Column(name="news_article", type="text", length=65535, nullable=false)
     */
    private $newsArticle;

    /**
     * @var string
     *
     * @ORM\Column(name="news_cloudtags", type="text", length=65535, nullable=false)
     */
    private $newsCloudtags;

    /**
     * @var string
     *
     * @ORM\Column(name="news_metadescription", type="text", length=65535, nullable=false)
     */
    private $newsMetadescription;



    /**
     * Set newsId
     *
     * @param integer $newsId
     *
     * @return NewsDescription
     */
    public function setNewsId($newsId)
    {
        $this->newsId = $newsId;

        return $this;
    }

    /**
     * Get newsId
     *
     * @return integer
     */
    public function getNewsId()
    {
        return $this->newsId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return NewsDescription
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set newsTitle
     *
     * @param string $newsTitle
     *
     * @return NewsDescription
     */
    public function setNewsTitle($newsTitle)
    {
        $this->newsTitle = $newsTitle;

        return $this;
    }

    /**
     * Get newsTitle
     *
     * @return string
     */
    public function getNewsTitle()
    {
        return $this->newsTitle;
    }

    /**
     * Set newsTeaser
     *
     * @param string $newsTeaser
     *
     * @return NewsDescription
     */
    public function setNewsTeaser($newsTeaser)
    {
        $this->newsTeaser = $newsTeaser;

        return $this;
    }

    /**
     * Get newsTeaser
     *
     * @return string
     */
    public function getNewsTeaser()
    {
        return $this->newsTeaser;
    }

    /**
     * Set newsArticle
     *
     * @param string $newsArticle
     *
     * @return NewsDescription
     */
    public function setNewsArticle($newsArticle)
    {
        $this->newsArticle = $newsArticle;

        return $this;
    }

    /**
     * Get newsArticle
     *
     * @return string
     */
    public function getNewsArticle()
    {
        return $this->newsArticle;
    }

    /**
     * Set newsCloudtags
     *
     * @param string $newsCloudtags
     *
     * @return NewsDescription
     */
    public function setNewsCloudtags($newsCloudtags)
    {
        $this->newsCloudtags = $newsCloudtags;

        return $this;
    }

    /**
     * Get newsCloudtags
     *
     * @return string
     */
    public function getNewsCloudtags()
    {
        return $this->newsCloudtags;
    }

    /**
     * Set newsMetadescription
     *
     * @param string $newsMetadescription
     *
     * @return NewsDescription
     */
    public function setNewsMetadescription($newsMetadescription)
    {
        $this->newsMetadescription = $newsMetadescription;

        return $this;
    }

    /**
     * Get newsMetadescription
     *
     * @return string
     */
    public function getNewsMetadescription()
    {
        return $this->newsMetadescription;
    }
}
