<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsXsell
 *
 * @ORM\Table(name="products_xsell", uniqueConstraints={@ORM\UniqueConstraint(name="products_id", columns={"products_id", "xsell_id"})})
 * @ORM\Entity
 */
class ProductsXsell
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="xsell_id", type="integer", nullable=false)
     */
    private $xsellId = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '1';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsXsell
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set xsellId
     *
     * @param integer $xsellId
     *
     * @return ProductsXsell
     */
    public function setXsellId($xsellId)
    {
        $this->xsellId = $xsellId;

        return $this;
    }

    /**
     * Get xsellId
     *
     * @return integer
     */
    public function getXsellId()
    {
        return $this->xsellId;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return ProductsXsell
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}
