<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Information
 *
 * @ORM\Table(name="information")
 * @ORM\Entity
 */
class Information
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="information_id", type="boolean", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $informationId;

    /**
     * @var string
     *
     * @ORM\Column(name="visible", type="string", nullable=false)
     */
    private $visible = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="fb_visible", type="boolean", nullable=true)
     */
    private $fbVisible = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="permanent", type="string", nullable=false)
     */
    private $permanent = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="v_order", type="boolean", nullable=false)
     */
    private $vOrder = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_block", type="boolean", nullable=false)
     */
    private $isBlock = '0';



    /**
     * Get informationId
     *
     * @return boolean
     */
    public function getInformationId()
    {
        return $this->informationId;
    }

    /**
     * Set visible
     *
     * @param string $visible
     *
     * @return Information
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return string
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set fbVisible
     *
     * @param boolean $fbVisible
     *
     * @return Information
     */
    public function setFbVisible($fbVisible)
    {
        $this->fbVisible = $fbVisible;

        return $this;
    }

    /**
     * Get fbVisible
     *
     * @return boolean
     */
    public function getFbVisible()
    {
        return $this->fbVisible;
    }

    /**
     * Set permanent
     *
     * @param string $permanent
     *
     * @return Information
     */
    public function setPermanent($permanent)
    {
        $this->permanent = $permanent;

        return $this;
    }

    /**
     * Get permanent
     *
     * @return string
     */
    public function getPermanent()
    {
        return $this->permanent;
    }

    /**
     * Set vOrder
     *
     * @param boolean $vOrder
     *
     * @return Information
     */
    public function setVOrder($vOrder)
    {
        $this->vOrder = $vOrder;

        return $this;
    }

    /**
     * Get vOrder
     *
     * @return boolean
     */
    public function getVOrder()
    {
        return $this->vOrder;
    }

    /**
     * Set isBlock
     *
     * @param boolean $isBlock
     *
     * @return Information
     */
    public function setIsBlock($isBlock)
    {
        $this->isBlock = $isBlock;

        return $this;
    }

    /**
     * Get isBlock
     *
     * @return boolean
     */
    public function getIsBlock()
    {
        return $this->isBlock;
    }
}
