<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsToMasterproducts
 *
 * @ORM\Table(name="products_to_masterproducts", indexes={@ORM\Index(name="products_id", columns={"products_id"}), @ORM\Index(name="products_master", columns={"products_master"})})
 * @ORM\Entity
 */
class ProductsToMasterproducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="master_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $masterId;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_master", type="integer", nullable=false)
     */
    private $productsMaster = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="string", length=128, nullable=false)
     */
    private $desc = '';



    /**
     * Get masterId
     *
     * @return integer
     */
    public function getMasterId()
    {
        return $this->masterId;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsToMasterproducts
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set productsMaster
     *
     * @param integer $productsMaster
     *
     * @return ProductsToMasterproducts
     */
    public function setProductsMaster($productsMaster)
    {
        $this->productsMaster = $productsMaster;

        return $this;
    }

    /**
     * Get productsMaster
     *
     * @return integer
     */
    public function getProductsMaster()
    {
        return $this->productsMaster;
    }

    /**
     * Set desc
     *
     * @param string $desc
     *
     * @return ProductsToMasterproducts
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }
}
