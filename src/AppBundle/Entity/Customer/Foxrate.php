<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Foxrate
 *
 * @ORM\Table(name="foxrate")
 * @ORM\Entity
 */
class Foxrate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="modus", type="string", length=8, nullable=false)
     */
    private $modus;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="datetime", nullable=false)
     */
    private $datum = 'CURRENT_TIMESTAMP';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modus
     *
     * @param string $modus
     *
     * @return Foxrate
     */
    public function setModus($modus)
    {
        $this->modus = $modus;

        return $this;
    }

    /**
     * Get modus
     *
     * @return string
     */
    public function getModus()
    {
        return $this->modus;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Foxrate
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return Foxrate
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }
}
