<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesAdminsession
 *
 * @ORM\Table(name="eazysales_adminsession")
 * @ORM\Entity
 */
class EazysalesAdminsession
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cSessionId", type="string", length=255, nullable=true)
     */
    private $csessionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="nSessionExpires", type="integer", nullable=true)
     */
    private $nsessionexpires;

    /**
     * @var string
     *
     * @ORM\Column(name="cSessionData", type="text", length=65535, nullable=true)
     */
    private $csessiondata;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set csessionid
     *
     * @param string $csessionid
     *
     * @return EazysalesAdminsession
     */
    public function setCsessionid($csessionid)
    {
        $this->csessionid = $csessionid;

        return $this;
    }

    /**
     * Get csessionid
     *
     * @return string
     */
    public function getCsessionid()
    {
        return $this->csessionid;
    }

    /**
     * Set nsessionexpires
     *
     * @param integer $nsessionexpires
     *
     * @return EazysalesAdminsession
     */
    public function setNsessionexpires($nsessionexpires)
    {
        $this->nsessionexpires = $nsessionexpires;

        return $this;
    }

    /**
     * Get nsessionexpires
     *
     * @return integer
     */
    public function getNsessionexpires()
    {
        return $this->nsessionexpires;
    }

    /**
     * Set csessiondata
     *
     * @param string $csessiondata
     *
     * @return EazysalesAdminsession
     */
    public function setCsessiondata($csessiondata)
    {
        $this->csessiondata = $csessiondata;

        return $this;
    }

    /**
     * Get csessiondata
     *
     * @return string
     */
    public function getCsessiondata()
    {
        return $this->csessiondata;
    }
}
