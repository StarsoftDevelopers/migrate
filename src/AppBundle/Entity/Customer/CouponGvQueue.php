<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CouponGvQueue
 *
 * @ORM\Table(name="coupon_gv_queue", indexes={@ORM\Index(name="uid", columns={"unique_id", "customer_id", "order_id"})})
 * @ORM\Entity
 */
class CouponGvQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="unique_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uniqueId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $amount = '0.0000';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="ipaddr", type="string", length=32, nullable=false)
     */
    private $ipaddr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="release_flag", type="string", length=1, nullable=false)
     */
    private $releaseFlag = 'N';



    /**
     * Get uniqueId
     *
     * @return integer
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return CouponGvQueue
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     *
     * @return CouponGvQueue
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return CouponGvQueue
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return CouponGvQueue
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set ipaddr
     *
     * @param string $ipaddr
     *
     * @return CouponGvQueue
     */
    public function setIpaddr($ipaddr)
    {
        $this->ipaddr = $ipaddr;

        return $this;
    }

    /**
     * Get ipaddr
     *
     * @return string
     */
    public function getIpaddr()
    {
        return $this->ipaddr;
    }

    /**
     * Set releaseFlag
     *
     * @param string $releaseFlag
     *
     * @return CouponGvQueue
     */
    public function setReleaseFlag($releaseFlag)
    {
        $this->releaseFlag = $releaseFlag;

        return $this;
    }

    /**
     * Get releaseFlag
     *
     * @return string
     */
    public function getReleaseFlag()
    {
        return $this->releaseFlag;
    }
}
