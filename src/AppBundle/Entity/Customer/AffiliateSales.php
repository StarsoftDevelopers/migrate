<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * AffiliateSales
 *
 * @ORM\Table(name="affiliate_sales")
 * @ORM\Entity
 */
class AffiliateSales
{
    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_orders_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $affiliateOrdersId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $affiliateId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affiliate_date", type="datetime", nullable=false)
     */
    private $affiliateDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_browser", type="string", length=100, nullable=false)
     */
    private $affiliateBrowser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_ipaddress", type="string", length=20, nullable=false)
     */
    private $affiliateIpaddress = '';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_value", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $affiliateValue = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_payment", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $affiliatePayment = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_clickthroughs_id", type="integer", nullable=false)
     */
    private $affiliateClickthroughsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_billing_status", type="integer", nullable=false)
     */
    private $affiliateBillingStatus = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affiliate_payment_date", type="datetime", nullable=false)
     */
    private $affiliatePaymentDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_payment_id", type="integer", nullable=false)
     */
    private $affiliatePaymentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_percent", type="decimal", precision=4, scale=2, nullable=false)
     */
    private $affiliatePercent = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_salesman", type="integer", nullable=false)
     */
    private $affiliateSalesman = '0';



    /**
     * Set affiliateOrdersId
     *
     * @param integer $affiliateOrdersId
     *
     * @return AffiliateSales
     */
    public function setAffiliateOrdersId($affiliateOrdersId)
    {
        $this->affiliateOrdersId = $affiliateOrdersId;

        return $this;
    }

    /**
     * Get affiliateOrdersId
     *
     * @return integer
     */
    public function getAffiliateOrdersId()
    {
        return $this->affiliateOrdersId;
    }

    /**
     * Set affiliateId
     *
     * @param integer $affiliateId
     *
     * @return AffiliateSales
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;

        return $this;
    }

    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set affiliateDate
     *
     * @param \DateTime $affiliateDate
     *
     * @return AffiliateSales
     */
    public function setAffiliateDate($affiliateDate)
    {
        $this->affiliateDate = $affiliateDate;

        return $this;
    }

    /**
     * Get affiliateDate
     *
     * @return \DateTime
     */
    public function getAffiliateDate()
    {
        return $this->affiliateDate;
    }

    /**
     * Set affiliateBrowser
     *
     * @param string $affiliateBrowser
     *
     * @return AffiliateSales
     */
    public function setAffiliateBrowser($affiliateBrowser)
    {
        $this->affiliateBrowser = $affiliateBrowser;

        return $this;
    }

    /**
     * Get affiliateBrowser
     *
     * @return string
     */
    public function getAffiliateBrowser()
    {
        return $this->affiliateBrowser;
    }

    /**
     * Set affiliateIpaddress
     *
     * @param string $affiliateIpaddress
     *
     * @return AffiliateSales
     */
    public function setAffiliateIpaddress($affiliateIpaddress)
    {
        $this->affiliateIpaddress = $affiliateIpaddress;

        return $this;
    }

    /**
     * Get affiliateIpaddress
     *
     * @return string
     */
    public function getAffiliateIpaddress()
    {
        return $this->affiliateIpaddress;
    }

    /**
     * Set affiliateValue
     *
     * @param string $affiliateValue
     *
     * @return AffiliateSales
     */
    public function setAffiliateValue($affiliateValue)
    {
        $this->affiliateValue = $affiliateValue;

        return $this;
    }

    /**
     * Get affiliateValue
     *
     * @return string
     */
    public function getAffiliateValue()
    {
        return $this->affiliateValue;
    }

    /**
     * Set affiliatePayment
     *
     * @param string $affiliatePayment
     *
     * @return AffiliateSales
     */
    public function setAffiliatePayment($affiliatePayment)
    {
        $this->affiliatePayment = $affiliatePayment;

        return $this;
    }

    /**
     * Get affiliatePayment
     *
     * @return string
     */
    public function getAffiliatePayment()
    {
        return $this->affiliatePayment;
    }

    /**
     * Set affiliateClickthroughsId
     *
     * @param integer $affiliateClickthroughsId
     *
     * @return AffiliateSales
     */
    public function setAffiliateClickthroughsId($affiliateClickthroughsId)
    {
        $this->affiliateClickthroughsId = $affiliateClickthroughsId;

        return $this;
    }

    /**
     * Get affiliateClickthroughsId
     *
     * @return integer
     */
    public function getAffiliateClickthroughsId()
    {
        return $this->affiliateClickthroughsId;
    }

    /**
     * Set affiliateBillingStatus
     *
     * @param integer $affiliateBillingStatus
     *
     * @return AffiliateSales
     */
    public function setAffiliateBillingStatus($affiliateBillingStatus)
    {
        $this->affiliateBillingStatus = $affiliateBillingStatus;

        return $this;
    }

    /**
     * Get affiliateBillingStatus
     *
     * @return integer
     */
    public function getAffiliateBillingStatus()
    {
        return $this->affiliateBillingStatus;
    }

    /**
     * Set affiliatePaymentDate
     *
     * @param \DateTime $affiliatePaymentDate
     *
     * @return AffiliateSales
     */
    public function setAffiliatePaymentDate($affiliatePaymentDate)
    {
        $this->affiliatePaymentDate = $affiliatePaymentDate;

        return $this;
    }

    /**
     * Get affiliatePaymentDate
     *
     * @return \DateTime
     */
    public function getAffiliatePaymentDate()
    {
        return $this->affiliatePaymentDate;
    }

    /**
     * Set affiliatePaymentId
     *
     * @param integer $affiliatePaymentId
     *
     * @return AffiliateSales
     */
    public function setAffiliatePaymentId($affiliatePaymentId)
    {
        $this->affiliatePaymentId = $affiliatePaymentId;

        return $this;
    }

    /**
     * Get affiliatePaymentId
     *
     * @return integer
     */
    public function getAffiliatePaymentId()
    {
        return $this->affiliatePaymentId;
    }

    /**
     * Set affiliatePercent
     *
     * @param string $affiliatePercent
     *
     * @return AffiliateSales
     */
    public function setAffiliatePercent($affiliatePercent)
    {
        $this->affiliatePercent = $affiliatePercent;

        return $this;
    }

    /**
     * Get affiliatePercent
     *
     * @return string
     */
    public function getAffiliatePercent()
    {
        return $this->affiliatePercent;
    }

    /**
     * Set affiliateSalesman
     *
     * @param integer $affiliateSalesman
     *
     * @return AffiliateSales
     */
    public function setAffiliateSalesman($affiliateSalesman)
    {
        $this->affiliateSalesman = $affiliateSalesman;

        return $this;
    }

    /**
     * Get affiliateSalesman
     *
     * @return integer
     */
    public function getAffiliateSalesman()
    {
        return $this->affiliateSalesman;
    }
}
