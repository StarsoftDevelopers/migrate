<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customers
 *
 * @ORM\Table(name="customers")
 * @ORM\Entity
 */
class Customers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="customers_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $customersId;

    /**
     * @var string
     *
     * @ORM\Column(name="customers_gender", type="string", length=1, nullable=false)
     */
    private $customersGender = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_firstname", type="string", length=255, nullable=false)
     */
    private $customersFirstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_lastname", type="string", length=255, nullable=false)
     */
    private $customersLastname = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="customers_dob", type="datetime", nullable=false)
     */
    private $customersDob = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_email_address", type="string", length=96, nullable=false)
     */
    private $customersEmailAddress = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_default_address_id", type="integer", nullable=false)
     */
    private $customersDefaultAddressId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_telephone", type="string", length=32, nullable=false)
     */
    private $customersTelephone = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_fax", type="string", length=32, nullable=true)
     */
    private $customersFax;

    /**
     * @var string
     *
     * @ORM\Column(name="customers_password", type="string", length=40, nullable=false)
     */
    private $customersPassword = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_newsletter", type="string", length=1, nullable=true)
     */
    private $customersNewsletter;

    /**
     * @var string
     *
     * @ORM\Column(name="customers_data_security", type="string", length=1, nullable=true)
     */
    private $customersDataSecurity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="customers_invoice", type="boolean", nullable=false)
     */
    private $customersInvoice = '2';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=true)
     */
    private $affiliateId;

    /**
     * @var string
     *
     * @ORM\Column(name="referer", type="string", length=255, nullable=true)
     */
    private $referer;

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_group_id", type="integer", nullable=false)
     */
    private $customersGroupId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="member_level", type="integer", nullable=false)
     */
    private $memberLevel = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="entry_piva", type="string", length=16, nullable=false)
     */
    private $entryPiva = '';

    /**
     * @var string
     *
     * @ORM\Column(name="entry_cf", type="string", length=16, nullable=false)
     */
    private $entryCf = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_commercialcode", type="string", length=250, nullable=false)
     */
    private $customersCommercialcode = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="risk_check_data", type="datetime", nullable=false)
     */
    private $riskCheckData = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="risk_check_score", type="integer", nullable=false)
     */
    private $riskCheckScore = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="risk_check_ampel", type="string", length=10, nullable=false)
     */
    private $riskCheckAmpel = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="risk_check_counter", type="integer", nullable=false)
     */
    private $riskCheckCounter = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="customers_debit", type="boolean", nullable=false)
     */
    private $customersDebit = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="blacklist", type="integer", nullable=false)
     */
    private $blacklist = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_shopping_points", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $customersShoppingPoints = '0.00';



    /**
     * Get customersId
     *
     * @return integer
     */
    public function getCustomersId()
    {
        return $this->customersId;
    }

    /**
     * Set customersGender
     *
     * @param string $customersGender
     *
     * @return Customers
     */
    public function setCustomersGender($customersGender)
    {
        $this->customersGender = $customersGender;

        return $this;
    }

    /**
     * Get customersGender
     *
     * @return string
     */
    public function getCustomersGender()
    {
        return $this->customersGender;
    }

    /**
     * Set customersFirstname
     *
     * @param string $customersFirstname
     *
     * @return Customers
     */
    public function setCustomersFirstname($customersFirstname)
    {
        $this->customersFirstname = $customersFirstname;

        return $this;
    }

    /**
     * Get customersFirstname
     *
     * @return string
     */
    public function getCustomersFirstname()
    {
        return $this->customersFirstname;
    }

    /**
     * Set customersLastname
     *
     * @param string $customersLastname
     *
     * @return Customers
     */
    public function setCustomersLastname($customersLastname)
    {
        $this->customersLastname = $customersLastname;

        return $this;
    }

    /**
     * Get customersLastname
     *
     * @return string
     */
    public function getCustomersLastname()
    {
        return $this->customersLastname;
    }

    /**
     * Set customersDob
     *
     * @param \DateTime $customersDob
     *
     * @return Customers
     */
    public function setCustomersDob($customersDob)
    {
        $this->customersDob = $customersDob;

        return $this;
    }

    /**
     * Get customersDob
     *
     * @return \DateTime
     */
    public function getCustomersDob()
    {
        return $this->customersDob;
    }

    /**
     * Set customersEmailAddress
     *
     * @param string $customersEmailAddress
     *
     * @return Customers
     */
    public function setCustomersEmailAddress($customersEmailAddress)
    {
        $this->customersEmailAddress = $customersEmailAddress;

        return $this;
    }

    /**
     * Get customersEmailAddress
     *
     * @return string
     */
    public function getCustomersEmailAddress()
    {
        return $this->customersEmailAddress;
    }

    /**
     * Set customersDefaultAddressId
     *
     * @param integer $customersDefaultAddressId
     *
     * @return Customers
     */
    public function setCustomersDefaultAddressId($customersDefaultAddressId)
    {
        $this->customersDefaultAddressId = $customersDefaultAddressId;

        return $this;
    }

    /**
     * Get customersDefaultAddressId
     *
     * @return integer
     */
    public function getCustomersDefaultAddressId()
    {
        return $this->customersDefaultAddressId;
    }

    /**
     * Set customersTelephone
     *
     * @param string $customersTelephone
     *
     * @return Customers
     */
    public function setCustomersTelephone($customersTelephone)
    {
        $this->customersTelephone = $customersTelephone;

        return $this;
    }

    /**
     * Get customersTelephone
     *
     * @return string
     */
    public function getCustomersTelephone()
    {
        return $this->customersTelephone;
    }

    /**
     * Set customersFax
     *
     * @param string $customersFax
     *
     * @return Customers
     */
    public function setCustomersFax($customersFax)
    {
        $this->customersFax = $customersFax;

        return $this;
    }

    /**
     * Get customersFax
     *
     * @return string
     */
    public function getCustomersFax()
    {
        return $this->customersFax;
    }

    /**
     * Set customersPassword
     *
     * @param string $customersPassword
     *
     * @return Customers
     */
    public function setCustomersPassword($customersPassword)
    {
        $this->customersPassword = $customersPassword;

        return $this;
    }

    /**
     * Get customersPassword
     *
     * @return string
     */
    public function getCustomersPassword()
    {
        return $this->customersPassword;
    }

    /**
     * Set customersNewsletter
     *
     * @param string $customersNewsletter
     *
     * @return Customers
     */
    public function setCustomersNewsletter($customersNewsletter)
    {
        $this->customersNewsletter = $customersNewsletter;

        return $this;
    }

    /**
     * Get customersNewsletter
     *
     * @return string
     */
    public function getCustomersNewsletter()
    {
        return $this->customersNewsletter;
    }

    /**
     * Set customersDataSecurity
     *
     * @param string $customersDataSecurity
     *
     * @return Customers
     */
    public function setCustomersDataSecurity($customersDataSecurity)
    {
        $this->customersDataSecurity = $customersDataSecurity;

        return $this;
    }

    /**
     * Get customersDataSecurity
     *
     * @return string
     */
    public function getCustomersDataSecurity()
    {
        return $this->customersDataSecurity;
    }

    /**
     * Set customersInvoice
     *
     * @param boolean $customersInvoice
     *
     * @return Customers
     */
    public function setCustomersInvoice($customersInvoice)
    {
        $this->customersInvoice = $customersInvoice;

        return $this;
    }

    /**
     * Get customersInvoice
     *
     * @return boolean
     */
    public function getCustomersInvoice()
    {
        return $this->customersInvoice;
    }

    /**
     * Set affiliateId
     *
     * @param integer $affiliateId
     *
     * @return Customers
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;

        return $this;
    }

    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set referer
     *
     * @param string $referer
     *
     * @return Customers
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set customersGroupId
     *
     * @param integer $customersGroupId
     *
     * @return Customers
     */
    public function setCustomersGroupId($customersGroupId)
    {
        $this->customersGroupId = $customersGroupId;

        return $this;
    }

    /**
     * Get customersGroupId
     *
     * @return integer
     */
    public function getCustomersGroupId()
    {
        return $this->customersGroupId;
    }

    /**
     * Set memberLevel
     *
     * @param integer $memberLevel
     *
     * @return Customers
     */
    public function setMemberLevel($memberLevel)
    {
        $this->memberLevel = $memberLevel;

        return $this;
    }

    /**
     * Get memberLevel
     *
     * @return integer
     */
    public function getMemberLevel()
    {
        return $this->memberLevel;
    }

    /**
     * Set entryPiva
     *
     * @param string $entryPiva
     *
     * @return Customers
     */
    public function setEntryPiva($entryPiva)
    {
        $this->entryPiva = $entryPiva;

        return $this;
    }

    /**
     * Get entryPiva
     *
     * @return string
     */
    public function getEntryPiva()
    {
        return $this->entryPiva;
    }

    /**
     * Set entryCf
     *
     * @param string $entryCf
     *
     * @return Customers
     */
    public function setEntryCf($entryCf)
    {
        $this->entryCf = $entryCf;

        return $this;
    }

    /**
     * Get entryCf
     *
     * @return string
     */
    public function getEntryCf()
    {
        return $this->entryCf;
    }

    /**
     * Set customersCommercialcode
     *
     * @param string $customersCommercialcode
     *
     * @return Customers
     */
    public function setCustomersCommercialcode($customersCommercialcode)
    {
        $this->customersCommercialcode = $customersCommercialcode;

        return $this;
    }

    /**
     * Get customersCommercialcode
     *
     * @return string
     */
    public function getCustomersCommercialcode()
    {
        return $this->customersCommercialcode;
    }

    /**
     * Set riskCheckData
     *
     * @param \DateTime $riskCheckData
     *
     * @return Customers
     */
    public function setRiskCheckData($riskCheckData)
    {
        $this->riskCheckData = $riskCheckData;

        return $this;
    }

    /**
     * Get riskCheckData
     *
     * @return \DateTime
     */
    public function getRiskCheckData()
    {
        return $this->riskCheckData;
    }

    /**
     * Set riskCheckScore
     *
     * @param integer $riskCheckScore
     *
     * @return Customers
     */
    public function setRiskCheckScore($riskCheckScore)
    {
        $this->riskCheckScore = $riskCheckScore;

        return $this;
    }

    /**
     * Get riskCheckScore
     *
     * @return integer
     */
    public function getRiskCheckScore()
    {
        return $this->riskCheckScore;
    }

    /**
     * Set riskCheckAmpel
     *
     * @param string $riskCheckAmpel
     *
     * @return Customers
     */
    public function setRiskCheckAmpel($riskCheckAmpel)
    {
        $this->riskCheckAmpel = $riskCheckAmpel;

        return $this;
    }

    /**
     * Get riskCheckAmpel
     *
     * @return string
     */
    public function getRiskCheckAmpel()
    {
        return $this->riskCheckAmpel;
    }

    /**
     * Set riskCheckCounter
     *
     * @param integer $riskCheckCounter
     *
     * @return Customers
     */
    public function setRiskCheckCounter($riskCheckCounter)
    {
        $this->riskCheckCounter = $riskCheckCounter;

        return $this;
    }

    /**
     * Get riskCheckCounter
     *
     * @return integer
     */
    public function getRiskCheckCounter()
    {
        return $this->riskCheckCounter;
    }

    /**
     * Set customersDebit
     *
     * @param boolean $customersDebit
     *
     * @return Customers
     */
    public function setCustomersDebit($customersDebit)
    {
        $this->customersDebit = $customersDebit;

        return $this;
    }

    /**
     * Get customersDebit
     *
     * @return boolean
     */
    public function getCustomersDebit()
    {
        return $this->customersDebit;
    }

    /**
     * Set blacklist
     *
     * @param integer $blacklist
     *
     * @return Customers
     */
    public function setBlacklist($blacklist)
    {
        $this->blacklist = $blacklist;

        return $this;
    }

    /**
     * Get blacklist
     *
     * @return integer
     */
    public function getBlacklist()
    {
        return $this->blacklist;
    }

    /**
     * Set customersShoppingPoints
     *
     * @param string $customersShoppingPoints
     *
     * @return Customers
     */
    public function setCustomersShoppingPoints($customersShoppingPoints)
    {
        $this->customersShoppingPoints = $customersShoppingPoints;

        return $this;
    }

    /**
     * Get customersShoppingPoints
     *
     * @return string
     */
    public function getCustomersShoppingPoints()
    {
        return $this->customersShoppingPoints;
    }
}
