<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coupons
 *
 * @ORM\Table(name="coupons")
 * @ORM\Entity
 */
class Coupons
{
    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $couponId;

    /**
     * @var string
     *
     * @ORM\Column(name="coupon_type", type="string", length=1, nullable=false)
     */
    private $couponType = 'F';

    /**
     * @var string
     *
     * @ORM\Column(name="coupon_code", type="string", length=32, nullable=false)
     */
    private $couponCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="coupon_amount", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $couponAmount = '0.0000';

    /**
     * @var string
     *
     * @ORM\Column(name="coupon_minimum_order", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $couponMinimumOrder = '0.0000';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="coupon_start_date", type="datetime", nullable=false)
     */
    private $couponStartDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="coupon_expire_date", type="datetime", nullable=false)
     */
    private $couponExpireDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="uses_per_coupon", type="integer", nullable=false)
     */
    private $usesPerCoupon = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="uses_per_user", type="integer", nullable=false)
     */
    private $usesPerUser = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="restrict_to_products", type="string", length=255, nullable=true)
     */
    private $restrictToProducts;

    /**
     * @var string
     *
     * @ORM\Column(name="restrict_to_categories", type="string", length=255, nullable=true)
     */
    private $restrictToCategories;

    /**
     * @var string
     *
     * @ORM\Column(name="restrict_to_customers", type="text", length=65535, nullable=true)
     */
    private $restrictToCustomers;

    /**
     * @var string
     *
     * @ORM\Column(name="coupon_active", type="string", length=1, nullable=false)
     */
    private $couponActive = 'Y';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=false)
     */
    private $dateModified = '0000-00-00 00:00:00';



    /**
     * Get couponId
     *
     * @return integer
     */
    public function getCouponId()
    {
        return $this->couponId;
    }

    /**
     * Set couponType
     *
     * @param string $couponType
     *
     * @return Coupons
     */
    public function setCouponType($couponType)
    {
        $this->couponType = $couponType;

        return $this;
    }

    /**
     * Get couponType
     *
     * @return string
     */
    public function getCouponType()
    {
        return $this->couponType;
    }

    /**
     * Set couponCode
     *
     * @param string $couponCode
     *
     * @return Coupons
     */
    public function setCouponCode($couponCode)
    {
        $this->couponCode = $couponCode;

        return $this;
    }

    /**
     * Get couponCode
     *
     * @return string
     */
    public function getCouponCode()
    {
        return $this->couponCode;
    }

    /**
     * Set couponAmount
     *
     * @param string $couponAmount
     *
     * @return Coupons
     */
    public function setCouponAmount($couponAmount)
    {
        $this->couponAmount = $couponAmount;

        return $this;
    }

    /**
     * Get couponAmount
     *
     * @return string
     */
    public function getCouponAmount()
    {
        return $this->couponAmount;
    }

    /**
     * Set couponMinimumOrder
     *
     * @param string $couponMinimumOrder
     *
     * @return Coupons
     */
    public function setCouponMinimumOrder($couponMinimumOrder)
    {
        $this->couponMinimumOrder = $couponMinimumOrder;

        return $this;
    }

    /**
     * Get couponMinimumOrder
     *
     * @return string
     */
    public function getCouponMinimumOrder()
    {
        return $this->couponMinimumOrder;
    }

    /**
     * Set couponStartDate
     *
     * @param \DateTime $couponStartDate
     *
     * @return Coupons
     */
    public function setCouponStartDate($couponStartDate)
    {
        $this->couponStartDate = $couponStartDate;

        return $this;
    }

    /**
     * Get couponStartDate
     *
     * @return \DateTime
     */
    public function getCouponStartDate()
    {
        return $this->couponStartDate;
    }

    /**
     * Set couponExpireDate
     *
     * @param \DateTime $couponExpireDate
     *
     * @return Coupons
     */
    public function setCouponExpireDate($couponExpireDate)
    {
        $this->couponExpireDate = $couponExpireDate;

        return $this;
    }

    /**
     * Get couponExpireDate
     *
     * @return \DateTime
     */
    public function getCouponExpireDate()
    {
        return $this->couponExpireDate;
    }

    /**
     * Set usesPerCoupon
     *
     * @param integer $usesPerCoupon
     *
     * @return Coupons
     */
    public function setUsesPerCoupon($usesPerCoupon)
    {
        $this->usesPerCoupon = $usesPerCoupon;

        return $this;
    }

    /**
     * Get usesPerCoupon
     *
     * @return integer
     */
    public function getUsesPerCoupon()
    {
        return $this->usesPerCoupon;
    }

    /**
     * Set usesPerUser
     *
     * @param integer $usesPerUser
     *
     * @return Coupons
     */
    public function setUsesPerUser($usesPerUser)
    {
        $this->usesPerUser = $usesPerUser;

        return $this;
    }

    /**
     * Get usesPerUser
     *
     * @return integer
     */
    public function getUsesPerUser()
    {
        return $this->usesPerUser;
    }

    /**
     * Set restrictToProducts
     *
     * @param string $restrictToProducts
     *
     * @return Coupons
     */
    public function setRestrictToProducts($restrictToProducts)
    {
        $this->restrictToProducts = $restrictToProducts;

        return $this;
    }

    /**
     * Get restrictToProducts
     *
     * @return string
     */
    public function getRestrictToProducts()
    {
        return $this->restrictToProducts;
    }

    /**
     * Set restrictToCategories
     *
     * @param string $restrictToCategories
     *
     * @return Coupons
     */
    public function setRestrictToCategories($restrictToCategories)
    {
        $this->restrictToCategories = $restrictToCategories;

        return $this;
    }

    /**
     * Get restrictToCategories
     *
     * @return string
     */
    public function getRestrictToCategories()
    {
        return $this->restrictToCategories;
    }

    /**
     * Set restrictToCustomers
     *
     * @param string $restrictToCustomers
     *
     * @return Coupons
     */
    public function setRestrictToCustomers($restrictToCustomers)
    {
        $this->restrictToCustomers = $restrictToCustomers;

        return $this;
    }

    /**
     * Get restrictToCustomers
     *
     * @return string
     */
    public function getRestrictToCustomers()
    {
        return $this->restrictToCustomers;
    }

    /**
     * Set couponActive
     *
     * @param string $couponActive
     *
     * @return Coupons
     */
    public function setCouponActive($couponActive)
    {
        $this->couponActive = $couponActive;

        return $this;
    }

    /**
     * Get couponActive
     *
     * @return string
     */
    public function getCouponActive()
    {
        return $this->couponActive;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Coupons
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return Coupons
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }
}
