<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShippingMethods
 *
 * @ORM\Table(name="shipping_methods")
 * @ORM\Entity
 */
class ShippingMethods
{
    /**
     * @var integer
     *
     * @ORM\Column(name="shipping_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $shippingId;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_method", type="string", length=32, nullable=false)
     */
    private $shippingMethod = '';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_name", type="string", length=64, nullable=false)
     */
    private $shippingName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="shipping_sort", type="integer", nullable=false)
     */
    private $shippingSort = '999';

    /**
     * @var boolean
     *
     * @ORM\Column(name="shipping_active", type="boolean", nullable=false)
     */
    private $shippingActive = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="weight_from", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $weightFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="weight_to", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $weightTo;

    /**
     * @var string
     *
     * @ORM\Column(name="max_value", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $maxValue;



    /**
     * Get shippingId
     *
     * @return integer
     */
    public function getShippingId()
    {
        return $this->shippingId;
    }

    /**
     * Set shippingMethod
     *
     * @param string $shippingMethod
     *
     * @return ShippingMethods
     */
    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    /**
     * Get shippingMethod
     *
     * @return string
     */
    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }

    /**
     * Set shippingName
     *
     * @param string $shippingName
     *
     * @return ShippingMethods
     */
    public function setShippingName($shippingName)
    {
        $this->shippingName = $shippingName;

        return $this;
    }

    /**
     * Get shippingName
     *
     * @return string
     */
    public function getShippingName()
    {
        return $this->shippingName;
    }

    /**
     * Set shippingSort
     *
     * @param integer $shippingSort
     *
     * @return ShippingMethods
     */
    public function setShippingSort($shippingSort)
    {
        $this->shippingSort = $shippingSort;

        return $this;
    }

    /**
     * Get shippingSort
     *
     * @return integer
     */
    public function getShippingSort()
    {
        return $this->shippingSort;
    }

    /**
     * Set shippingActive
     *
     * @param boolean $shippingActive
     *
     * @return ShippingMethods
     */
    public function setShippingActive($shippingActive)
    {
        $this->shippingActive = $shippingActive;

        return $this;
    }

    /**
     * Get shippingActive
     *
     * @return boolean
     */
    public function getShippingActive()
    {
        return $this->shippingActive;
    }

    /**
     * Set weightFrom
     *
     * @param string $weightFrom
     *
     * @return ShippingMethods
     */
    public function setWeightFrom($weightFrom)
    {
        $this->weightFrom = $weightFrom;

        return $this;
    }

    /**
     * Get weightFrom
     *
     * @return string
     */
    public function getWeightFrom()
    {
        return $this->weightFrom;
    }

    /**
     * Set weightTo
     *
     * @param string $weightTo
     *
     * @return ShippingMethods
     */
    public function setWeightTo($weightTo)
    {
        $this->weightTo = $weightTo;

        return $this;
    }

    /**
     * Get weightTo
     *
     * @return string
     */
    public function getWeightTo()
    {
        return $this->weightTo;
    }

    /**
     * Set maxValue
     *
     * @param string $maxValue
     *
     * @return ShippingMethods
     */
    public function setMaxValue($maxValue)
    {
        $this->maxValue = $maxValue;

        return $this;
    }

    /**
     * Get maxValue
     *
     * @return string
     */
    public function getMaxValue()
    {
        return $this->maxValue;
    }
}
