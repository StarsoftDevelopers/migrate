<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categories
 *
 * @ORM\Table(name="categories", indexes={@ORM\Index(name="idx_categories_parent_id", columns={"parent_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Customer\Repository\CategoryRepository")
 */
class Categories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categories_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoriesId;

    /**
     * @var string
     *
     * @ORM\Column(name="categories_image", type="string", length=255, nullable=true)
     */
    private $categoriesImage;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime", nullable=true)
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_modified", type="datetime", nullable=true)
     */
    private $lastModified;

    /**
     * @var boolean
     *
     * @ORM\Column(name="categories_status", type="boolean", nullable=false)
     */
    private $categoriesStatus = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="ebaycat", type="text", length=65535, nullable=false)
     */
    private $ebaycat;

    /**
     * @var string
     *
     * @ORM\Column(name="yategocat", type="string", length=255, nullable=true)
     */
    private $yategocat;

    /**
     * @var integer
     *
     * @ORM\Column(name="googlecat", type="smallint", nullable=true)
     */
    private $googlecat;

    /**
     * @var string
     *
     * @ORM\Column(name="hoodcat", type="string", length=32, nullable=true)
     */
    private $hoodcat;

    /**
     * @var string
     *
     * @ORM\Column(name="amazoncat", type="string", length=32, nullable=true)
     */
    private $amazoncat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cpmarketing", type="boolean", nullable=false)
     */
    private $cpmarketing = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiplier", type="boolean", nullable=false)
     */
    private $multiplier = '0';



    /**
     * Get categoriesId
     *
     * @return integer
     */
    public function getCategoriesId()
    {
        return $this->categoriesId;
    }

    /**
     * Set categoriesImage
     *
     * @param string $categoriesImage
     *
     * @return Categories
     */
    public function setCategoriesImage($categoriesImage)
    {
        $this->categoriesImage = $categoriesImage;

        return $this;
    }

    /**
     * Get categoriesImage
     *
     * @return string
     */
    public function getCategoriesImage()
    {
        return $this->categoriesImage;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return Categories
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Categories
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Categories
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     *
     * @return Categories
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set categoriesStatus
     *
     * @param boolean $categoriesStatus
     *
     * @return Categories
     */
    public function setCategoriesStatus($categoriesStatus)
    {
        $this->categoriesStatus = $categoriesStatus;

        return $this;
    }

    /**
     * Get categoriesStatus
     *
     * @return boolean
     */
    public function getCategoriesStatus()
    {
        return $this->categoriesStatus;
    }

    /**
     * Set ebaycat
     *
     * @param string $ebaycat
     *
     * @return Categories
     */
    public function setEbaycat($ebaycat)
    {
        $this->ebaycat = $ebaycat;

        return $this;
    }

    /**
     * Get ebaycat
     *
     * @return string
     */
    public function getEbaycat()
    {
        return $this->ebaycat;
    }

    /**
     * Set yategocat
     *
     * @param string $yategocat
     *
     * @return Categories
     */
    public function setYategocat($yategocat)
    {
        $this->yategocat = $yategocat;

        return $this;
    }

    /**
     * Get yategocat
     *
     * @return string
     */
    public function getYategocat()
    {
        return $this->yategocat;
    }

    /**
     * Set googlecat
     *
     * @param integer $googlecat
     *
     * @return Categories
     */
    public function setGooglecat($googlecat)
    {
        $this->googlecat = $googlecat;

        return $this;
    }

    /**
     * Get googlecat
     *
     * @return integer
     */
    public function getGooglecat()
    {
        return $this->googlecat;
    }

    /**
     * Set hoodcat
     *
     * @param string $hoodcat
     *
     * @return Categories
     */
    public function setHoodcat($hoodcat)
    {
        $this->hoodcat = $hoodcat;

        return $this;
    }

    /**
     * Get hoodcat
     *
     * @return string
     */
    public function getHoodcat()
    {
        return $this->hoodcat;
    }

    /**
     * Set amazoncat
     *
     * @param string $amazoncat
     *
     * @return Categories
     */
    public function setAmazoncat($amazoncat)
    {
        $this->amazoncat = $amazoncat;

        return $this;
    }

    /**
     * Get amazoncat
     *
     * @return string
     */
    public function getAmazoncat()
    {
        return $this->amazoncat;
    }

    /**
     * Set cpmarketing
     *
     * @param boolean $cpmarketing
     *
     * @return Categories
     */
    public function setCpmarketing($cpmarketing)
    {
        $this->cpmarketing = $cpmarketing;

        return $this;
    }

    /**
     * Get cpmarketing
     *
     * @return boolean
     */
    public function getCpmarketing()
    {
        return $this->cpmarketing;
    }

    /**
     * Set multiplier
     *
     * @param boolean $multiplier
     *
     * @return Categories
     */
    public function setMultiplier($multiplier)
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    /**
     * Get multiplier
     *
     * @return boolean
     */
    public function getMultiplier()
    {
        return $this->multiplier;
    }
}
