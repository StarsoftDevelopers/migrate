<?php

namespace AppBundle\Entity\Customer\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class CategoryRepository
 * @package AppBundle\Entity\Customer\Repository
 */
class CategoryRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getCategoriesData()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c.categoriesId as id,
                c.parentId,
                cd.categoriesName as name,
                c.categoriesStatus as status,
                c.categoriesImage as image,
                p.categoriesName as parent,
                cd.categoriesHtml,
                cd.categoriesHeadTitleTag,
                cd.categoriesHeadDescTag,
                cd.categoriesHeadKeywordsTag')
            ->leftJoin('AppBundle\Entity\Customer\CategoriesDescription', 'cd', Join::WITH, 'cd.categoriesId = c.categoriesId')
            ->leftJoin('AppBundle\Entity\Customer\CategoriesDescription', 'p', Join::WITH, 'p.categoriesId = c.parentId')
            ->where('cd.languageId = :id')
            ->andWhere('p.languageId = :id OR c.parentId = 0')
            ->orderBy('c.categoriesId', 'ASC')
            ->setParameter(':id', 2)
//            ->setParameter(':id', 3)//for user 20150
           /* ->setFirstResult(0)
            ->setMaxResults(10)*/
            ->getQuery();

        return $qb->getResult();
    }
}
