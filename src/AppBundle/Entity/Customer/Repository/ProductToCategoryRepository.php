<?php

namespace AppBundle\Entity\Customer\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class CategoryRepository
 * @package AppBundle\Entity\Customer\Repository
 */
class ProductToCategoryRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getCategoriesData()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('DISTINCT c.categoriesId as id')
            ->getQuery();

        return $qb->getResult();
    }
}
