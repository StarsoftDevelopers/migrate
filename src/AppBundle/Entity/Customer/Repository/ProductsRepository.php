<?php

namespace AppBundle\Entity\Customer\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class ProductsRepository extends EntityRepository
{
    public function getProductsData()
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p.productsId as id,
                p.productsStatus as active,
                pd.productsName as name,
                cd.categoriesName as categories
                '
            )
            ->leftJoin('AppBundle\Entity\Customer\ProductsDescription', 'pd', Join::WITH, 'pd.productsId = p.productsId')
            ->leftJoin('AppBundle\Entity\Customer\ProductsToCategories', 'ptc', Join::WITH, 'ptc.productsId = p.productsId')
            ->leftJoin('AppBundle\Entity\Customer\CategoriesDescription', 'cd', Join::WITH, 'cd.categoriesId = ptc.categoriesId')
            ->where('pd.languageId = :id')
            ->where('cd.languageId = :id')
//            ->groupBy('categories')
            ->setParameter(':id', 2)
            ->getQuery();

        return $qb->getResult();
    }
}
