<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsToBestprice
 *
 * @ORM\Table(name="products_to_bestprice")
 * @ORM\Entity
 */
class ProductsToBestprice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $productsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="qty_from", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $qtyFrom = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="qty_to", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $qtyTo = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $price = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="calculate", type="boolean", nullable=false)
     */
    private $calculate = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="tara", type="boolean", nullable=false)
     */
    private $tara = '0';



    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsToBestprice
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set qtyFrom
     *
     * @param integer $qtyFrom
     *
     * @return ProductsToBestprice
     */
    public function setQtyFrom($qtyFrom)
    {
        $this->qtyFrom = $qtyFrom;

        return $this;
    }

    /**
     * Get qtyFrom
     *
     * @return integer
     */
    public function getQtyFrom()
    {
        return $this->qtyFrom;
    }

    /**
     * Set qtyTo
     *
     * @param integer $qtyTo
     *
     * @return ProductsToBestprice
     */
    public function setQtyTo($qtyTo)
    {
        $this->qtyTo = $qtyTo;

        return $this;
    }

    /**
     * Get qtyTo
     *
     * @return integer
     */
    public function getQtyTo()
    {
        return $this->qtyTo;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ProductsToBestprice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set calculate
     *
     * @param boolean $calculate
     *
     * @return ProductsToBestprice
     */
    public function setCalculate($calculate)
    {
        $this->calculate = $calculate;

        return $this;
    }

    /**
     * Get calculate
     *
     * @return boolean
     */
    public function getCalculate()
    {
        return $this->calculate;
    }

    /**
     * Set tara
     *
     * @param boolean $tara
     *
     * @return ProductsToBestprice
     */
    public function setTara($tara)
    {
        $this->tara = $tara;

        return $this;
    }

    /**
     * Get tara
     *
     * @return boolean
     */
    public function getTara()
    {
        return $this->tara;
    }
}
