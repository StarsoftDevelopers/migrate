<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsConditions
 *
 * @ORM\Table(name="products_conditions")
 * @ORM\Entity
 */
class ProductsConditions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="condition_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $conditionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId;

    /**
     * @var string
     *
     * @ORM\Column(name="condition_title", type="string", length=20, nullable=false)
     */
    private $conditionTitle;



    /**
     * Set conditionId
     *
     * @param integer $conditionId
     *
     * @return ProductsConditions
     */
    public function setConditionId($conditionId)
    {
        $this->conditionId = $conditionId;

        return $this;
    }

    /**
     * Get conditionId
     *
     * @return integer
     */
    public function getConditionId()
    {
        return $this->conditionId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return ProductsConditions
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set conditionTitle
     *
     * @param string $conditionTitle
     *
     * @return ProductsConditions
     */
    public function setConditionTitle($conditionTitle)
    {
        $this->conditionTitle = $conditionTitle;

        return $this;
    }

    /**
     * Get conditionTitle
     *
     * @return string
     */
    public function getConditionTitle()
    {
        return $this->conditionTitle;
    }
}
