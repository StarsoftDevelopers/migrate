<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CouponEmailTrack
 *
 * @ORM\Table(name="coupon_email_track")
 * @ORM\Entity
 */
class CouponEmailTrack
{
    /**
     * @var integer
     *
     * @ORM\Column(name="unique_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uniqueId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_id", type="integer", nullable=false)
     */
    private $couponId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id_sent", type="integer", nullable=false)
     */
    private $customerIdSent = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="sent_firstname", type="string", length=32, nullable=true)
     */
    private $sentFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="sent_lastname", type="string", length=32, nullable=true)
     */
    private $sentLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="emailed_to", type="string", length=32, nullable=true)
     */
    private $emailedTo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sent", type="datetime", nullable=false)
     */
    private $dateSent = '0000-00-00 00:00:00';



    /**
     * Get uniqueId
     *
     * @return integer
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set couponId
     *
     * @param integer $couponId
     *
     * @return CouponEmailTrack
     */
    public function setCouponId($couponId)
    {
        $this->couponId = $couponId;

        return $this;
    }

    /**
     * Get couponId
     *
     * @return integer
     */
    public function getCouponId()
    {
        return $this->couponId;
    }

    /**
     * Set customerIdSent
     *
     * @param integer $customerIdSent
     *
     * @return CouponEmailTrack
     */
    public function setCustomerIdSent($customerIdSent)
    {
        $this->customerIdSent = $customerIdSent;

        return $this;
    }

    /**
     * Get customerIdSent
     *
     * @return integer
     */
    public function getCustomerIdSent()
    {
        return $this->customerIdSent;
    }

    /**
     * Set sentFirstname
     *
     * @param string $sentFirstname
     *
     * @return CouponEmailTrack
     */
    public function setSentFirstname($sentFirstname)
    {
        $this->sentFirstname = $sentFirstname;

        return $this;
    }

    /**
     * Get sentFirstname
     *
     * @return string
     */
    public function getSentFirstname()
    {
        return $this->sentFirstname;
    }

    /**
     * Set sentLastname
     *
     * @param string $sentLastname
     *
     * @return CouponEmailTrack
     */
    public function setSentLastname($sentLastname)
    {
        $this->sentLastname = $sentLastname;

        return $this;
    }

    /**
     * Get sentLastname
     *
     * @return string
     */
    public function getSentLastname()
    {
        return $this->sentLastname;
    }

    /**
     * Set emailedTo
     *
     * @param string $emailedTo
     *
     * @return CouponEmailTrack
     */
    public function setEmailedTo($emailedTo)
    {
        $this->emailedTo = $emailedTo;

        return $this;
    }

    /**
     * Get emailedTo
     *
     * @return string
     */
    public function getEmailedTo()
    {
        return $this->emailedTo;
    }

    /**
     * Set dateSent
     *
     * @param \DateTime $dateSent
     *
     * @return CouponEmailTrack
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;

        return $this;
    }

    /**
     * Get dateSent
     *
     * @return \DateTime
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }
}
