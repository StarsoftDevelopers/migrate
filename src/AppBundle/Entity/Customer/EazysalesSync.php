<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesSync
 *
 * @ORM\Table(name="eazysales_sync")
 * @ORM\Entity
 */
class EazysalesSync
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cName", type="string", length=255, nullable=true)
     */
    private $cname;

    /**
     * @var string
     *
     * @ORM\Column(name="cPass", type="string", length=255, nullable=true)
     */
    private $cpass;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cname
     *
     * @param string $cname
     *
     * @return EazysalesSync
     */
    public function setCname($cname)
    {
        $this->cname = $cname;

        return $this;
    }

    /**
     * Get cname
     *
     * @return string
     */
    public function getCname()
    {
        return $this->cname;
    }

    /**
     * Set cpass
     *
     * @param string $cpass
     *
     * @return EazysalesSync
     */
    public function setCpass($cpass)
    {
        $this->cpass = $cpass;

        return $this;
    }

    /**
     * Get cpass
     *
     * @return string
     */
    public function getCpass()
    {
        return $this->cpass;
    }
}
