<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShippingMethodsZones
 *
 * @ORM\Table(name="shipping_methods_zones")
 * @ORM\Entity
 */
class ShippingMethodsZones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="association_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $associationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="country_id", type="integer", nullable=false)
     */
    private $countryId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="zone_id", type="integer", nullable=true)
     */
    private $zoneId;

    /**
     * @var integer
     *
     * @ORM\Column(name="shipping_id", type="integer", nullable=true)
     */
    private $shippingId;



    /**
     * Get associationId
     *
     * @return integer
     */
    public function getAssociationId()
    {
        return $this->associationId;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     *
     * @return ShippingMethodsZones
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return integer
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set zoneId
     *
     * @param integer $zoneId
     *
     * @return ShippingMethodsZones
     */
    public function setZoneId($zoneId)
    {
        $this->zoneId = $zoneId;

        return $this;
    }

    /**
     * Get zoneId
     *
     * @return integer
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * Set shippingId
     *
     * @param integer $shippingId
     *
     * @return ShippingMethodsZones
     */
    public function setShippingId($shippingId)
    {
        $this->shippingId = $shippingId;

        return $this;
    }

    /**
     * Get shippingId
     *
     * @return integer
     */
    public function getShippingId()
    {
        return $this->shippingId;
    }
}
