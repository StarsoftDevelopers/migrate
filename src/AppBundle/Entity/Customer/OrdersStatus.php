<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrdersStatus
 *
 * @ORM\Table(name="orders_status", indexes={@ORM\Index(name="idx_orders_status_name", columns={"orders_status_name"})})
 * @ORM\Entity
 */
class OrdersStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="orders_status_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $ordersStatusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="orders_status_name", type="string", length=64, nullable=false)
     */
    private $ordersStatusName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="standard_text", type="text", length=65535, nullable=false)
     */
    private $standardText;

    /**
     * @var boolean
     *
     * @ORM\Column(name="standard_kunde", type="boolean", nullable=false)
     */
    private $standardKunde = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="standard_kommentar", type="boolean", nullable=false)
     */
    private $standardKommentar = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="background_color", type="string", length=6, nullable=false)
     */
    private $backgroundColor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="flag", type="boolean", nullable=true)
     */
    private $flag;



    /**
     * Set ordersStatusId
     *
     * @param integer $ordersStatusId
     *
     * @return OrdersStatus
     */
    public function setOrdersStatusId($ordersStatusId)
    {
        $this->ordersStatusId = $ordersStatusId;

        return $this;
    }

    /**
     * Get ordersStatusId
     *
     * @return integer
     */
    public function getOrdersStatusId()
    {
        return $this->ordersStatusId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return OrdersStatus
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set ordersStatusName
     *
     * @param string $ordersStatusName
     *
     * @return OrdersStatus
     */
    public function setOrdersStatusName($ordersStatusName)
    {
        $this->ordersStatusName = $ordersStatusName;

        return $this;
    }

    /**
     * Get ordersStatusName
     *
     * @return string
     */
    public function getOrdersStatusName()
    {
        return $this->ordersStatusName;
    }

    /**
     * Set standardText
     *
     * @param string $standardText
     *
     * @return OrdersStatus
     */
    public function setStandardText($standardText)
    {
        $this->standardText = $standardText;

        return $this;
    }

    /**
     * Get standardText
     *
     * @return string
     */
    public function getStandardText()
    {
        return $this->standardText;
    }

    /**
     * Set standardKunde
     *
     * @param boolean $standardKunde
     *
     * @return OrdersStatus
     */
    public function setStandardKunde($standardKunde)
    {
        $this->standardKunde = $standardKunde;

        return $this;
    }

    /**
     * Get standardKunde
     *
     * @return boolean
     */
    public function getStandardKunde()
    {
        return $this->standardKunde;
    }

    /**
     * Set standardKommentar
     *
     * @param boolean $standardKommentar
     *
     * @return OrdersStatus
     */
    public function setStandardKommentar($standardKommentar)
    {
        $this->standardKommentar = $standardKommentar;

        return $this;
    }

    /**
     * Get standardKommentar
     *
     * @return boolean
     */
    public function getStandardKommentar()
    {
        return $this->standardKommentar;
    }

    /**
     * Set backgroundColor
     *
     * @param string $backgroundColor
     *
     * @return OrdersStatus
     */
    public function setBackgroundColor($backgroundColor)
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    /**
     * Get backgroundColor
     *
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    /**
     * Set flag
     *
     * @param boolean $flag
     *
     * @return OrdersStatus
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return boolean
     */
    public function getFlag()
    {
        return $this->flag;
    }
}
