<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Visitors
 *
 * @ORM\Table(name="visitors")
 * @ORM\Entity
 */
class Visitors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="browser_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $browserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_id", type="integer", nullable=false)
     */
    private $customersId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="online", type="datetime", nullable=false)
     */
    private $online = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="counter", type="integer", nullable=false)
     */
    private $counter = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="browser_ip", type="string", length=15, nullable=false)
     */
    private $browserIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="browser_language", type="string", length=32, nullable=false)
     */
    private $browserLanguage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=16, nullable=false)
     */
    private $language = '';

    /**
     * @var string
     *
     * @ORM\Column(name="referer", type="string", length=255, nullable=false)
     */
    private $referer = '';

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=false)
     */
    private $uri = '';



    /**
     * Get browserId
     *
     * @return integer
     */
    public function getBrowserId()
    {
        return $this->browserId;
    }

    /**
     * Set customersId
     *
     * @param integer $customersId
     *
     * @return Visitors
     */
    public function setCustomersId($customersId)
    {
        $this->customersId = $customersId;

        return $this;
    }

    /**
     * Get customersId
     *
     * @return integer
     */
    public function getCustomersId()
    {
        return $this->customersId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Visitors
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set online
     *
     * @param \DateTime $online
     *
     * @return Visitors
     */
    public function setOnline($online)
    {
        $this->online = $online;

        return $this;
    }

    /**
     * Get online
     *
     * @return \DateTime
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     *
     * @return Visitors
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Set browserIp
     *
     * @param string $browserIp
     *
     * @return Visitors
     */
    public function setBrowserIp($browserIp)
    {
        $this->browserIp = $browserIp;

        return $this;
    }

    /**
     * Get browserIp
     *
     * @return string
     */
    public function getBrowserIp()
    {
        return $this->browserIp;
    }

    /**
     * Set browserLanguage
     *
     * @param string $browserLanguage
     *
     * @return Visitors
     */
    public function setBrowserLanguage($browserLanguage)
    {
        $this->browserLanguage = $browserLanguage;

        return $this;
    }

    /**
     * Get browserLanguage
     *
     * @return string
     */
    public function getBrowserLanguage()
    {
        return $this->browserLanguage;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Visitors
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set referer
     *
     * @param string $referer
     *
     * @return Visitors
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return Visitors
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }
}
