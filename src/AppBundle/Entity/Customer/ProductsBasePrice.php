<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsBasePrice
 *
 * @ORM\Table(name="products_base_price")
 * @ORM\Entity
 */
class ProductsBasePrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="base_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $baseId;



    /**
     * Get baseId
     *
     * @return integer
     */
    public function getBaseId()
    {
        return $this->baseId;
    }
}
