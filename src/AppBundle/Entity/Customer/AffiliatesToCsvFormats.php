<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * AffiliatesToCsvFormats
 *
 * @ORM\Table(name="affiliates_to_csv_formats", indexes={@ORM\Index(name="csv_format_id", columns={"csv_format_id"})})
 * @ORM\Entity
 */
class AffiliatesToCsvFormats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $affiliateId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="csv_format_id", type="integer", nullable=false)
     */
    private $csvFormatId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="userdefined", type="boolean", nullable=false)
     */
    private $userdefined = '0';



    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set csvFormatId
     *
     * @param integer $csvFormatId
     *
     * @return AffiliatesToCsvFormats
     */
    public function setCsvFormatId($csvFormatId)
    {
        $this->csvFormatId = $csvFormatId;

        return $this;
    }

    /**
     * Get csvFormatId
     *
     * @return integer
     */
    public function getCsvFormatId()
    {
        return $this->csvFormatId;
    }

    /**
     * Set userdefined
     *
     * @param boolean $userdefined
     *
     * @return AffiliatesToCsvFormats
     */
    public function setUserdefined($userdefined)
    {
        $this->userdefined = $userdefined;

        return $this;
    }

    /**
     * Get userdefined
     *
     * @return boolean
     */
    public function getUserdefined()
    {
        return $this->userdefined;
    }
}
