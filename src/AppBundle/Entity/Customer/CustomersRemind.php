<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersRemind
 *
 * @ORM\Table(name="customers_remind")
 * @ORM\Entity
 */
class CustomersRemind
{
    /**
     * @var integer
     *
     * @ORM\Column(name="remind_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $remindId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customers_id", type="integer", nullable=false)
     */
    private $customersId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_firstname", type="string", length=32, nullable=false)
     */
    private $customersFirstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_lastname", type="string", length=32, nullable=false)
     */
    private $customersLastname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_email_address", type="string", length=96, nullable=false)
     */
    private $customersEmailAddress = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="products_name", type="string", length=64, nullable=false)
     */
    private $productsName = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="remind_date_added", type="datetime", nullable=true)
     */
    private $remindDateAdded = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="remind_date_mail_send", type="datetime", nullable=true)
     */
    private $remindDateMailSend = '0000-00-00 00:00:00';



    /**
     * Get remindId
     *
     * @return integer
     */
    public function getRemindId()
    {
        return $this->remindId;
    }

    /**
     * Set customersId
     *
     * @param integer $customersId
     *
     * @return CustomersRemind
     */
    public function setCustomersId($customersId)
    {
        $this->customersId = $customersId;

        return $this;
    }

    /**
     * Get customersId
     *
     * @return integer
     */
    public function getCustomersId()
    {
        return $this->customersId;
    }

    /**
     * Set customersFirstname
     *
     * @param string $customersFirstname
     *
     * @return CustomersRemind
     */
    public function setCustomersFirstname($customersFirstname)
    {
        $this->customersFirstname = $customersFirstname;

        return $this;
    }

    /**
     * Get customersFirstname
     *
     * @return string
     */
    public function getCustomersFirstname()
    {
        return $this->customersFirstname;
    }

    /**
     * Set customersLastname
     *
     * @param string $customersLastname
     *
     * @return CustomersRemind
     */
    public function setCustomersLastname($customersLastname)
    {
        $this->customersLastname = $customersLastname;

        return $this;
    }

    /**
     * Get customersLastname
     *
     * @return string
     */
    public function getCustomersLastname()
    {
        return $this->customersLastname;
    }

    /**
     * Set customersEmailAddress
     *
     * @param string $customersEmailAddress
     *
     * @return CustomersRemind
     */
    public function setCustomersEmailAddress($customersEmailAddress)
    {
        $this->customersEmailAddress = $customersEmailAddress;

        return $this;
    }

    /**
     * Get customersEmailAddress
     *
     * @return string
     */
    public function getCustomersEmailAddress()
    {
        return $this->customersEmailAddress;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return CustomersRemind
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set productsName
     *
     * @param string $productsName
     *
     * @return CustomersRemind
     */
    public function setProductsName($productsName)
    {
        $this->productsName = $productsName;

        return $this;
    }

    /**
     * Get productsName
     *
     * @return string
     */
    public function getProductsName()
    {
        return $this->productsName;
    }

    /**
     * Set remindDateAdded
     *
     * @param \DateTime $remindDateAdded
     *
     * @return CustomersRemind
     */
    public function setRemindDateAdded($remindDateAdded)
    {
        $this->remindDateAdded = $remindDateAdded;

        return $this;
    }

    /**
     * Get remindDateAdded
     *
     * @return \DateTime
     */
    public function getRemindDateAdded()
    {
        return $this->remindDateAdded;
    }

    /**
     * Set remindDateMailSend
     *
     * @param \DateTime $remindDateMailSend
     *
     * @return CustomersRemind
     */
    public function setRemindDateMailSend($remindDateMailSend)
    {
        $this->remindDateMailSend = $remindDateMailSend;

        return $this;
    }

    /**
     * Get remindDateMailSend
     *
     * @return \DateTime
     */
    public function getRemindDateMailSend()
    {
        return $this->remindDateMailSend;
    }
}
