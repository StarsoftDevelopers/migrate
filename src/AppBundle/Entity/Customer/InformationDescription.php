<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * InformationDescription
 *
 * @ORM\Table(name="information_description")
 * @ORM\Entity
 */
class InformationDescription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="information_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $informationId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languagesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="info_title", type="string", length=255, nullable=false)
     */
    private $infoTitle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="text", length=65535, nullable=false)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", length=65535, nullable=false)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keys", type="text", length=65535, nullable=false)
     */
    private $metaKeys;



    /**
     * Set informationId
     *
     * @param integer $informationId
     *
     * @return InformationDescription
     */
    public function setInformationId($informationId)
    {
        $this->informationId = $informationId;

        return $this;
    }

    /**
     * Get informationId
     *
     * @return integer
     */
    public function getInformationId()
    {
        return $this->informationId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return InformationDescription
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set infoTitle
     *
     * @param string $infoTitle
     *
     * @return InformationDescription
     */
    public function setInfoTitle($infoTitle)
    {
        $this->infoTitle = $infoTitle;

        return $this;
    }

    /**
     * Get infoTitle
     *
     * @return string
     */
    public function getInfoTitle()
    {
        return $this->infoTitle;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InformationDescription
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     *
     * @return InformationDescription
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return InformationDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeys
     *
     * @param string $metaKeys
     *
     * @return InformationDescription
     */
    public function setMetaKeys($metaKeys)
    {
        $this->metaKeys = $metaKeys;

        return $this;
    }

    /**
     * Get metaKeys
     *
     * @return string
     */
    public function getMetaKeys()
    {
        return $this->metaKeys;
    }
}
