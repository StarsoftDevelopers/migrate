<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesSentorders
 *
 * @ORM\Table(name="eazysales_sentorders")
 * @ORM\Entity
 */
class EazysalesSentorders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="orders_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ordersId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dGesendet", type="datetime", nullable=true)
     */
    private $dgesendet;



    /**
     * Get ordersId
     *
     * @return integer
     */
    public function getOrdersId()
    {
        return $this->ordersId;
    }

    /**
     * Set dgesendet
     *
     * @param \DateTime $dgesendet
     *
     * @return EazysalesSentorders
     */
    public function setDgesendet($dgesendet)
    {
        $this->dgesendet = $dgesendet;

        return $this;
    }

    /**
     * Get dgesendet
     *
     * @return \DateTime
     */
    public function getDgesendet()
    {
        return $this->dgesendet;
    }
}
