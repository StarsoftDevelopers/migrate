<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesDescription
 *
 * @ORM\Table(name="categories_description", indexes={@ORM\Index(name="idx_categories_name", columns={"categories_name"})})
 * @ORM\Entity
 */
class CategoriesDescription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categories_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $categoriesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="categories_name", type="string", length=120, nullable=false)
     */
    private $categoriesName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="categories_html", type="text", length=65535, nullable=false)
     */
    private $categoriesHtml;

    /**
     * @var string
     *
     * @ORM\Column(name="categories_html_footer", type="text", length=65535, nullable=false)
     */
    private $categoriesHtmlFooter;

    /**
     * @var string
     *
     * @ORM\Column(name="categories_head_title_tag", type="string", length=255, nullable=false)
     */
    private $categoriesHeadTitleTag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="categories_head_desc_tag", type="string", length=255, nullable=false)
     */
    private $categoriesHeadDescTag = '';

    /**
     * @var string
     *
     * @ORM\Column(name="categories_head_keywords_tag", type="string", length=255, nullable=false)
     */
    private $categoriesHeadKeywordsTag = '';



    /**
     * Set categoriesId
     *
     * @param integer $categoriesId
     *
     * @return CategoriesDescription
     */
    public function setCategoriesId($categoriesId)
    {
        $this->categoriesId = $categoriesId;

        return $this;
    }

    /**
     * Get categoriesId
     *
     * @return integer
     */
    public function getCategoriesId()
    {
        return $this->categoriesId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return CategoriesDescription
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set categoriesName
     *
     * @param string $categoriesName
     *
     * @return CategoriesDescription
     */
    public function setCategoriesName($categoriesName)
    {
        $this->categoriesName = $categoriesName;

        return $this;
    }

    /**
     * Get categoriesName
     *
     * @return string
     */
    public function getCategoriesName()
    {
        return $this->categoriesName;
    }

    /**
     * Set categoriesHtml
     *
     * @param string $categoriesHtml
     *
     * @return CategoriesDescription
     */
    public function setCategoriesHtml($categoriesHtml)
    {
        $this->categoriesHtml = $categoriesHtml;

        return $this;
    }

    /**
     * Get categoriesHtml
     *
     * @return string
     */
    public function getCategoriesHtml()
    {
        return $this->categoriesHtml;
    }

    /**
     * Set categoriesHtmlFooter
     *
     * @param string $categoriesHtmlFooter
     *
     * @return CategoriesDescription
     */
    public function setCategoriesHtmlFooter($categoriesHtmlFooter)
    {
        $this->categoriesHtmlFooter = $categoriesHtmlFooter;

        return $this;
    }

    /**
     * Get categoriesHtmlFooter
     *
     * @return string
     */
    public function getCategoriesHtmlFooter()
    {
        return $this->categoriesHtmlFooter;
    }

    /**
     * Set categoriesHeadTitleTag
     *
     * @param string $categoriesHeadTitleTag
     *
     * @return CategoriesDescription
     */
    public function setCategoriesHeadTitleTag($categoriesHeadTitleTag)
    {
        $this->categoriesHeadTitleTag = $categoriesHeadTitleTag;

        return $this;
    }

    /**
     * Get categoriesHeadTitleTag
     *
     * @return string
     */
    public function getCategoriesHeadTitleTag()
    {
        return $this->categoriesHeadTitleTag;
    }

    /**
     * Set categoriesHeadDescTag
     *
     * @param string $categoriesHeadDescTag
     *
     * @return CategoriesDescription
     */
    public function setCategoriesHeadDescTag($categoriesHeadDescTag)
    {
        $this->categoriesHeadDescTag = $categoriesHeadDescTag;

        return $this;
    }

    /**
     * Get categoriesHeadDescTag
     *
     * @return string
     */
    public function getCategoriesHeadDescTag()
    {
        return $this->categoriesHeadDescTag;
    }

    /**
     * Set categoriesHeadKeywordsTag
     *
     * @param string $categoriesHeadKeywordsTag
     *
     * @return CategoriesDescription
     */
    public function setCategoriesHeadKeywordsTag($categoriesHeadKeywordsTag)
    {
        $this->categoriesHeadKeywordsTag = $categoriesHeadKeywordsTag;

        return $this;
    }

    /**
     * Get categoriesHeadKeywordsTag
     *
     * @return string
     */
    public function getCategoriesHeadKeywordsTag()
    {
        return $this->categoriesHeadKeywordsTag;
    }
}
