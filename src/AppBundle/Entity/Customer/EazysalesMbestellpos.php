<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesMbestellpos
 *
 * @ORM\Table(name="eazysales_mbestellpos")
 * @ORM\Entity
 */
class EazysalesMbestellpos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="kBestellPos", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $kbestellpos;

    /**
     * @var integer
     *
     * @ORM\Column(name="orders_products_id", type="integer", nullable=true)
     */
    private $ordersProductsId;



    /**
     * Get kbestellpos
     *
     * @return integer
     */
    public function getKbestellpos()
    {
        return $this->kbestellpos;
    }

    /**
     * Set ordersProductsId
     *
     * @param integer $ordersProductsId
     *
     * @return EazysalesMbestellpos
     */
    public function setOrdersProductsId($ordersProductsId)
    {
        $this->ordersProductsId = $ordersProductsId;

        return $this;
    }

    /**
     * Get ordersProductsId
     *
     * @return integer
     */
    public function getOrdersProductsId()
    {
        return $this->ordersProductsId;
    }
}
