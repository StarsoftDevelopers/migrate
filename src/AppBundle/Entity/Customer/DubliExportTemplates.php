<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * DubliExportTemplates
 *
 * @ORM\Table(name="dubli_export_templates")
 * @ORM\Entity
 */
class DubliExportTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ProductID", type="integer", nullable=false)
     */
    private $productid = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="AuctionName", type="string", length=255, nullable=false)
     */
    private $auctionname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="AuctionDesc", type="string", length=255, nullable=false)
     */
    private $auctiondesc = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="AuctionTypeID", type="integer", nullable=false)
     */
    private $auctiontypeid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="AuctionModeID", type="integer", nullable=false)
     */
    private $auctionmodeid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CategoryID", type="integer", nullable=false)
     */
    private $categoryid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CategoryID2", type="integer", nullable=false)
     */
    private $categoryid2 = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="AuctionSubtitle", type="string", length=255, nullable=false)
     */
    private $auctionsubtitle = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="AuctionDuration", type="boolean", nullable=false)
     */
    private $auctionduration = '7';

    /**
     * @var integer
     *
     * @ORM\Column(name="ShippingMethodID", type="integer", nullable=false)
     */
    private $shippingmethodid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="Shipping", type="integer", nullable=false)
     */
    private $shipping = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ConditionID", type="integer", nullable=false)
     */
    private $conditionid = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PaymentTypeID", type="integer", nullable=false)
     */
    private $paymenttypeid = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     *
     * @return DubliExportTemplates
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set auctionname
     *
     * @param string $auctionname
     *
     * @return DubliExportTemplates
     */
    public function setAuctionname($auctionname)
    {
        $this->auctionname = $auctionname;

        return $this;
    }

    /**
     * Get auctionname
     *
     * @return string
     */
    public function getAuctionname()
    {
        return $this->auctionname;
    }

    /**
     * Set auctiondesc
     *
     * @param string $auctiondesc
     *
     * @return DubliExportTemplates
     */
    public function setAuctiondesc($auctiondesc)
    {
        $this->auctiondesc = $auctiondesc;

        return $this;
    }

    /**
     * Get auctiondesc
     *
     * @return string
     */
    public function getAuctiondesc()
    {
        return $this->auctiondesc;
    }

    /**
     * Set auctiontypeid
     *
     * @param integer $auctiontypeid
     *
     * @return DubliExportTemplates
     */
    public function setAuctiontypeid($auctiontypeid)
    {
        $this->auctiontypeid = $auctiontypeid;

        return $this;
    }

    /**
     * Get auctiontypeid
     *
     * @return integer
     */
    public function getAuctiontypeid()
    {
        return $this->auctiontypeid;
    }

    /**
     * Set auctionmodeid
     *
     * @param integer $auctionmodeid
     *
     * @return DubliExportTemplates
     */
    public function setAuctionmodeid($auctionmodeid)
    {
        $this->auctionmodeid = $auctionmodeid;

        return $this;
    }

    /**
     * Get auctionmodeid
     *
     * @return integer
     */
    public function getAuctionmodeid()
    {
        return $this->auctionmodeid;
    }

    /**
     * Set categoryid
     *
     * @param integer $categoryid
     *
     * @return DubliExportTemplates
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return integer
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set categoryid2
     *
     * @param integer $categoryid2
     *
     * @return DubliExportTemplates
     */
    public function setCategoryid2($categoryid2)
    {
        $this->categoryid2 = $categoryid2;

        return $this;
    }

    /**
     * Get categoryid2
     *
     * @return integer
     */
    public function getCategoryid2()
    {
        return $this->categoryid2;
    }

    /**
     * Set auctionsubtitle
     *
     * @param string $auctionsubtitle
     *
     * @return DubliExportTemplates
     */
    public function setAuctionsubtitle($auctionsubtitle)
    {
        $this->auctionsubtitle = $auctionsubtitle;

        return $this;
    }

    /**
     * Get auctionsubtitle
     *
     * @return string
     */
    public function getAuctionsubtitle()
    {
        return $this->auctionsubtitle;
    }

    /**
     * Set auctionduration
     *
     * @param boolean $auctionduration
     *
     * @return DubliExportTemplates
     */
    public function setAuctionduration($auctionduration)
    {
        $this->auctionduration = $auctionduration;

        return $this;
    }

    /**
     * Get auctionduration
     *
     * @return boolean
     */
    public function getAuctionduration()
    {
        return $this->auctionduration;
    }

    /**
     * Set shippingmethodid
     *
     * @param integer $shippingmethodid
     *
     * @return DubliExportTemplates
     */
    public function setShippingmethodid($shippingmethodid)
    {
        $this->shippingmethodid = $shippingmethodid;

        return $this;
    }

    /**
     * Get shippingmethodid
     *
     * @return integer
     */
    public function getShippingmethodid()
    {
        return $this->shippingmethodid;
    }

    /**
     * Set shipping
     *
     * @param integer $shipping
     *
     * @return DubliExportTemplates
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping
     *
     * @return integer
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set conditionid
     *
     * @param integer $conditionid
     *
     * @return DubliExportTemplates
     */
    public function setConditionid($conditionid)
    {
        $this->conditionid = $conditionid;

        return $this;
    }

    /**
     * Get conditionid
     *
     * @return integer
     */
    public function getConditionid()
    {
        return $this->conditionid;
    }

    /**
     * Set paymenttypeid
     *
     * @param integer $paymenttypeid
     *
     * @return DubliExportTemplates
     */
    public function setPaymenttypeid($paymenttypeid)
    {
        $this->paymenttypeid = $paymenttypeid;

        return $this;
    }

    /**
     * Get paymenttypeid
     *
     * @return integer
     */
    public function getPaymenttypeid()
    {
        return $this->paymenttypeid;
    }
}
