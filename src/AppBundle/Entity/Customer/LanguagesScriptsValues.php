<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * LanguagesScriptsValues
 *
 * @ORM\Table(name="languages_scripts_values")
 * @ORM\Entity
 */
class LanguagesScriptsValues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="values_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $valuesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languagesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="variable", type="text", length=65535, nullable=true)
     */
    private $variable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changed", type="boolean", nullable=false)
     */
    private $changed = '0';



    /**
     * Set valuesId
     *
     * @param integer $valuesId
     *
     * @return LanguagesScriptsValues
     */
    public function setValuesId($valuesId)
    {
        $this->valuesId = $valuesId;

        return $this;
    }

    /**
     * Get valuesId
     *
     * @return integer
     */
    public function getValuesId()
    {
        return $this->valuesId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return LanguagesScriptsValues
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set variable
     *
     * @param string $variable
     *
     * @return LanguagesScriptsValues
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * Get variable
     *
     * @return string
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * Set changed
     *
     * @param boolean $changed
     *
     * @return LanguagesScriptsValues
     */
    public function setChanged($changed)
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * Get changed
     *
     * @return boolean
     */
    public function getChanged()
    {
        return $this->changed;
    }
}
