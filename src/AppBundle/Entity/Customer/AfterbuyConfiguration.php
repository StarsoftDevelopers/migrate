<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * AfterbuyConfiguration
 *
 * @ORM\Table(name="afterbuy_configuration")
 * @ORM\Entity
 */
class AfterbuyConfiguration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=32, nullable=false)
     */
    private $value = '';

    /**
     * @var string
     *
     * @ORM\Column(name="value_name", type="string", length=32, nullable=false)
     */
    private $valueName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="value_table", type="string", length=32, nullable=false)
     */
    private $valueTable = 'products';

    /**
     * @var string
     *
     * @ORM\Column(name="aw_value_1", type="string", length=64, nullable=false)
     */
    private $awValue1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="aw_value_2", type="string", length=64, nullable=false)
     */
    private $awValue2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="value_desc", type="text", length=65535, nullable=false)
     */
    private $valueDesc;

    /**
     * @var boolean
     *
     * @ORM\Column(name="value_array", type="boolean", nullable=false)
     */
    private $valueArray = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="value_sort", type="boolean", nullable=false)
     */
    private $valueSort = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="values_used", type="boolean", nullable=false)
     */
    private $valuesUsed = '1';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return AfterbuyConfiguration
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set valueName
     *
     * @param string $valueName
     *
     * @return AfterbuyConfiguration
     */
    public function setValueName($valueName)
    {
        $this->valueName = $valueName;

        return $this;
    }

    /**
     * Get valueName
     *
     * @return string
     */
    public function getValueName()
    {
        return $this->valueName;
    }

    /**
     * Set valueTable
     *
     * @param string $valueTable
     *
     * @return AfterbuyConfiguration
     */
    public function setValueTable($valueTable)
    {
        $this->valueTable = $valueTable;

        return $this;
    }

    /**
     * Get valueTable
     *
     * @return string
     */
    public function getValueTable()
    {
        return $this->valueTable;
    }

    /**
     * Set awValue1
     *
     * @param string $awValue1
     *
     * @return AfterbuyConfiguration
     */
    public function setAwValue1($awValue1)
    {
        $this->awValue1 = $awValue1;

        return $this;
    }

    /**
     * Get awValue1
     *
     * @return string
     */
    public function getAwValue1()
    {
        return $this->awValue1;
    }

    /**
     * Set awValue2
     *
     * @param string $awValue2
     *
     * @return AfterbuyConfiguration
     */
    public function setAwValue2($awValue2)
    {
        $this->awValue2 = $awValue2;

        return $this;
    }

    /**
     * Get awValue2
     *
     * @return string
     */
    public function getAwValue2()
    {
        return $this->awValue2;
    }

    /**
     * Set valueDesc
     *
     * @param string $valueDesc
     *
     * @return AfterbuyConfiguration
     */
    public function setValueDesc($valueDesc)
    {
        $this->valueDesc = $valueDesc;

        return $this;
    }

    /**
     * Get valueDesc
     *
     * @return string
     */
    public function getValueDesc()
    {
        return $this->valueDesc;
    }

    /**
     * Set valueArray
     *
     * @param boolean $valueArray
     *
     * @return AfterbuyConfiguration
     */
    public function setValueArray($valueArray)
    {
        $this->valueArray = $valueArray;

        return $this;
    }

    /**
     * Get valueArray
     *
     * @return boolean
     */
    public function getValueArray()
    {
        return $this->valueArray;
    }

    /**
     * Set valueSort
     *
     * @param boolean $valueSort
     *
     * @return AfterbuyConfiguration
     */
    public function setValueSort($valueSort)
    {
        $this->valueSort = $valueSort;

        return $this;
    }

    /**
     * Get valueSort
     *
     * @return boolean
     */
    public function getValueSort()
    {
        return $this->valueSort;
    }

    /**
     * Set valuesUsed
     *
     * @param boolean $valuesUsed
     *
     * @return AfterbuyConfiguration
     */
    public function setValuesUsed($valuesUsed)
    {
        $this->valuesUsed = $valuesUsed;

        return $this;
    }

    /**
     * Get valuesUsed
     *
     * @return boolean
     */
    public function getValuesUsed()
    {
        return $this->valuesUsed;
    }
}
