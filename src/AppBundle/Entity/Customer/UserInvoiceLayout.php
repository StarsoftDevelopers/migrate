<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserInvoiceLayout
 *
 * @ORM\Table(name="user_invoice_layout", indexes={@ORM\Index(name="invoice_layout_id_idx", columns={"invoice_layout_id"})})
 * @ORM\Entity
 */
class UserInvoiceLayout
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_invoice_layout_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userInvoiceLayoutId;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_layout_id", type="integer", nullable=false)
     */
    private $invoiceLayoutId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="bfields", type="text", length=65535, nullable=true)
     */
    private $bfields;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disabled", type="boolean", nullable=true)
     */
    private $disabled = '0';



    /**
     * Get userInvoiceLayoutId
     *
     * @return integer
     */
    public function getUserInvoiceLayoutId()
    {
        return $this->userInvoiceLayoutId;
    }

    /**
     * Set invoiceLayoutId
     *
     * @param integer $invoiceLayoutId
     *
     * @return UserInvoiceLayout
     */
    public function setInvoiceLayoutId($invoiceLayoutId)
    {
        $this->invoiceLayoutId = $invoiceLayoutId;

        return $this;
    }

    /**
     * Get invoiceLayoutId
     *
     * @return integer
     */
    public function getInvoiceLayoutId()
    {
        return $this->invoiceLayoutId;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return UserInvoiceLayout
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set bfields
     *
     * @param string $bfields
     *
     * @return UserInvoiceLayout
     */
    public function setBfields($bfields)
    {
        $this->bfields = $bfields;

        return $this;
    }

    /**
     * Get bfields
     *
     * @return string
     */
    public function getBfields()
    {
        return $this->bfields;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     *
     * @return UserInvoiceLayout
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }
}
