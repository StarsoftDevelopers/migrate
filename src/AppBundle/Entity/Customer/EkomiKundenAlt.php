<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EkomiKundenAlt
 *
 * @ORM\Table(name="ekomi_kunden_alt", indexes={@ORM\Index(name="email", columns={"email"})})
 * @ORM\Entity
 */
class EkomiKundenAlt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="eintrag_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eintragId;

    /**
     * @var string
     *
     * @ORM\Column(name="kunden_id", type="string", length=50, nullable=false)
     */
    private $kundenId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=40, nullable=false)
     */
    private $email = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mail_datum", type="datetime", nullable=false)
     */
    private $mailDatum;



    /**
     * Get eintragId
     *
     * @return integer
     */
    public function getEintragId()
    {
        return $this->eintragId;
    }

    /**
     * Set kundenId
     *
     * @param string $kundenId
     *
     * @return EkomiKundenAlt
     */
    public function setKundenId($kundenId)
    {
        $this->kundenId = $kundenId;

        return $this;
    }

    /**
     * Get kundenId
     *
     * @return string
     */
    public function getKundenId()
    {
        return $this->kundenId;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return EkomiKundenAlt
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mailDatum
     *
     * @param \DateTime $mailDatum
     *
     * @return EkomiKundenAlt
     */
    public function setMailDatum($mailDatum)
    {
        $this->mailDatum = $mailDatum;

        return $this;
    }

    /**
     * Get mailDatum
     *
     * @return \DateTime
     */
    public function getMailDatum()
    {
        return $this->mailDatum;
    }
}
