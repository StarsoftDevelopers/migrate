<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * AffiliateClickthroughs
 *
 * @ORM\Table(name="affiliate_clickthroughs", indexes={@ORM\Index(name="refid", columns={"affiliate_id"})})
 * @ORM\Entity
 */
class AffiliateClickthroughs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_clickthrough_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $affiliateClickthroughId;

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     */
    private $affiliateId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="affiliate_clientdate", type="datetime", nullable=false)
     */
    private $affiliateClientdate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_clientbrowser", type="string", length=200, nullable=true)
     */
    private $affiliateClientbrowser = 'Could Not Find This Data';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_clientip", type="string", length=50, nullable=true)
     */
    private $affiliateClientip = 'Could Not Find This Data';

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_clientreferer", type="string", length=200, nullable=true)
     */
    private $affiliateClientreferer = 'none detected (maybe a direct link)';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_products_id", type="integer", nullable=true)
     */
    private $affiliateProductsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_banner_id", type="integer", nullable=false)
     */
    private $affiliateBannerId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="transmitted", type="boolean", nullable=true)
     */
    private $transmitted = '0';



    /**
     * Get affiliateClickthroughId
     *
     * @return integer
     */
    public function getAffiliateClickthroughId()
    {
        return $this->affiliateClickthroughId;
    }

    /**
     * Set affiliateId
     *
     * @param integer $affiliateId
     *
     * @return AffiliateClickthroughs
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;

        return $this;
    }

    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set affiliateClientdate
     *
     * @param \DateTime $affiliateClientdate
     *
     * @return AffiliateClickthroughs
     */
    public function setAffiliateClientdate($affiliateClientdate)
    {
        $this->affiliateClientdate = $affiliateClientdate;

        return $this;
    }

    /**
     * Get affiliateClientdate
     *
     * @return \DateTime
     */
    public function getAffiliateClientdate()
    {
        return $this->affiliateClientdate;
    }

    /**
     * Set affiliateClientbrowser
     *
     * @param string $affiliateClientbrowser
     *
     * @return AffiliateClickthroughs
     */
    public function setAffiliateClientbrowser($affiliateClientbrowser)
    {
        $this->affiliateClientbrowser = $affiliateClientbrowser;

        return $this;
    }

    /**
     * Get affiliateClientbrowser
     *
     * @return string
     */
    public function getAffiliateClientbrowser()
    {
        return $this->affiliateClientbrowser;
    }

    /**
     * Set affiliateClientip
     *
     * @param string $affiliateClientip
     *
     * @return AffiliateClickthroughs
     */
    public function setAffiliateClientip($affiliateClientip)
    {
        $this->affiliateClientip = $affiliateClientip;

        return $this;
    }

    /**
     * Get affiliateClientip
     *
     * @return string
     */
    public function getAffiliateClientip()
    {
        return $this->affiliateClientip;
    }

    /**
     * Set affiliateClientreferer
     *
     * @param string $affiliateClientreferer
     *
     * @return AffiliateClickthroughs
     */
    public function setAffiliateClientreferer($affiliateClientreferer)
    {
        $this->affiliateClientreferer = $affiliateClientreferer;

        return $this;
    }

    /**
     * Get affiliateClientreferer
     *
     * @return string
     */
    public function getAffiliateClientreferer()
    {
        return $this->affiliateClientreferer;
    }

    /**
     * Set affiliateProductsId
     *
     * @param integer $affiliateProductsId
     *
     * @return AffiliateClickthroughs
     */
    public function setAffiliateProductsId($affiliateProductsId)
    {
        $this->affiliateProductsId = $affiliateProductsId;

        return $this;
    }

    /**
     * Get affiliateProductsId
     *
     * @return integer
     */
    public function getAffiliateProductsId()
    {
        return $this->affiliateProductsId;
    }

    /**
     * Set affiliateBannerId
     *
     * @param integer $affiliateBannerId
     *
     * @return AffiliateClickthroughs
     */
    public function setAffiliateBannerId($affiliateBannerId)
    {
        $this->affiliateBannerId = $affiliateBannerId;

        return $this;
    }

    /**
     * Get affiliateBannerId
     *
     * @return integer
     */
    public function getAffiliateBannerId()
    {
        return $this->affiliateBannerId;
    }

    /**
     * Set transmitted
     *
     * @param boolean $transmitted
     *
     * @return AffiliateClickthroughs
     */
    public function setTransmitted($transmitted)
    {
        $this->transmitted = $transmitted;

        return $this;
    }

    /**
     * Get transmitted
     *
     * @return boolean
     */
    public function getTransmitted()
    {
        return $this->transmitted;
    }
}
