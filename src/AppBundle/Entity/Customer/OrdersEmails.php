<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrdersEmails
 *
 * @ORM\Table(name="orders_emails", indexes={@ORM\Index(name="orders_id", columns={"orders_id"})})
 * @ORM\Entity
 */
class OrdersEmails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="emails_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $emailsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="orders_id", type="integer", nullable=false)
     */
    private $ordersId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="emails_date", type="datetime", nullable=false)
     */
    private $emailsDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="emails_status", type="boolean", nullable=false)
     */
    private $emailsStatus = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="emails_sender", type="string", length=255, nullable=false)
     */
    private $emailsSender;

    /**
     * @var string
     *
     * @ORM\Column(name="emails_receiver", type="string", length=255, nullable=false)
     */
    private $emailsReceiver;

    /**
     * @var string
     *
     * @ORM\Column(name="emails_subject", type="string", length=255, nullable=true)
     */
    private $emailsSubject;



    /**
     * Get emailsId
     *
     * @return integer
     */
    public function getEmailsId()
    {
        return $this->emailsId;
    }

    /**
     * Set ordersId
     *
     * @param integer $ordersId
     *
     * @return OrdersEmails
     */
    public function setOrdersId($ordersId)
    {
        $this->ordersId = $ordersId;

        return $this;
    }

    /**
     * Get ordersId
     *
     * @return integer
     */
    public function getOrdersId()
    {
        return $this->ordersId;
    }

    /**
     * Set emailsDate
     *
     * @param \DateTime $emailsDate
     *
     * @return OrdersEmails
     */
    public function setEmailsDate($emailsDate)
    {
        $this->emailsDate = $emailsDate;

        return $this;
    }

    /**
     * Get emailsDate
     *
     * @return \DateTime
     */
    public function getEmailsDate()
    {
        return $this->emailsDate;
    }

    /**
     * Set emailsStatus
     *
     * @param boolean $emailsStatus
     *
     * @return OrdersEmails
     */
    public function setEmailsStatus($emailsStatus)
    {
        $this->emailsStatus = $emailsStatus;

        return $this;
    }

    /**
     * Get emailsStatus
     *
     * @return boolean
     */
    public function getEmailsStatus()
    {
        return $this->emailsStatus;
    }

    /**
     * Set emailsSender
     *
     * @param string $emailsSender
     *
     * @return OrdersEmails
     */
    public function setEmailsSender($emailsSender)
    {
        $this->emailsSender = $emailsSender;

        return $this;
    }

    /**
     * Get emailsSender
     *
     * @return string
     */
    public function getEmailsSender()
    {
        return $this->emailsSender;
    }

    /**
     * Set emailsReceiver
     *
     * @param string $emailsReceiver
     *
     * @return OrdersEmails
     */
    public function setEmailsReceiver($emailsReceiver)
    {
        $this->emailsReceiver = $emailsReceiver;

        return $this;
    }

    /**
     * Get emailsReceiver
     *
     * @return string
     */
    public function getEmailsReceiver()
    {
        return $this->emailsReceiver;
    }

    /**
     * Set emailsSubject
     *
     * @param string $emailsSubject
     *
     * @return OrdersEmails
     */
    public function setEmailsSubject($emailsSubject)
    {
        $this->emailsSubject = $emailsSubject;

        return $this;
    }

    /**
     * Get emailsSubject
     *
     * @return string
     */
    public function getEmailsSubject()
    {
        return $this->emailsSubject;
    }
}
