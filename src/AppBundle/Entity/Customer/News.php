<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity
 */
class News
{
    /**
     * @var integer
     *
     * @ORM\Column(name="news_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $newsId;

    /**
     * @var string
     *
     * @ORM\Column(name="src_type", type="text", length=65535, nullable=false)
     */
    private $srcType;

    /**
     * @var string
     *
     * @ORM\Column(name="news_products_ids", type="text", length=65535, nullable=false)
     */
    private $newsProductsIds;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="news_add", type="datetime", nullable=false)
     */
    private $newsAdd = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="news_author_id", type="bigint", nullable=false)
     */
    private $newsAuthorId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="news_active", type="boolean", nullable=false)
     */
    private $newsActive = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="news_picture", type="text", length=65535, nullable=true)
     */
    private $newsPicture;



    /**
     * Get newsId
     *
     * @return integer
     */
    public function getNewsId()
    {
        return $this->newsId;
    }

    /**
     * Set srcType
     *
     * @param string $srcType
     *
     * @return News
     */
    public function setSrcType($srcType)
    {
        $this->srcType = $srcType;

        return $this;
    }

    /**
     * Get srcType
     *
     * @return string
     */
    public function getSrcType()
    {
        return $this->srcType;
    }

    /**
     * Set newsProductsIds
     *
     * @param string $newsProductsIds
     *
     * @return News
     */
    public function setNewsProductsIds($newsProductsIds)
    {
        $this->newsProductsIds = $newsProductsIds;

        return $this;
    }

    /**
     * Get newsProductsIds
     *
     * @return string
     */
    public function getNewsProductsIds()
    {
        return $this->newsProductsIds;
    }

    /**
     * Set newsAdd
     *
     * @param \DateTime $newsAdd
     *
     * @return News
     */
    public function setNewsAdd($newsAdd)
    {
        $this->newsAdd = $newsAdd;

        return $this;
    }

    /**
     * Get newsAdd
     *
     * @return \DateTime
     */
    public function getNewsAdd()
    {
        return $this->newsAdd;
    }

    /**
     * Set newsAuthorId
     *
     * @param integer $newsAuthorId
     *
     * @return News
     */
    public function setNewsAuthorId($newsAuthorId)
    {
        $this->newsAuthorId = $newsAuthorId;

        return $this;
    }

    /**
     * Get newsAuthorId
     *
     * @return integer
     */
    public function getNewsAuthorId()
    {
        return $this->newsAuthorId;
    }

    /**
     * Set newsActive
     *
     * @param boolean $newsActive
     *
     * @return News
     */
    public function setNewsActive($newsActive)
    {
        $this->newsActive = $newsActive;

        return $this;
    }

    /**
     * Get newsActive
     *
     * @return boolean
     */
    public function getNewsActive()
    {
        return $this->newsActive;
    }

    /**
     * Set newsPicture
     *
     * @param string $newsPicture
     *
     * @return News
     */
    public function setNewsPicture($newsPicture)
    {
        $this->newsPicture = $newsPicture;

        return $this;
    }

    /**
     * Get newsPicture
     *
     * @return string
     */
    public function getNewsPicture()
    {
        return $this->newsPicture;
    }
}
