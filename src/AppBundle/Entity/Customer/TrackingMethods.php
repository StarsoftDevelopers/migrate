<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackingMethods
 *
 * @ORM\Table(name="tracking_methods")
 * @ORM\Entity
 */
class TrackingMethods
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tracking_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $trackingId;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_method", type="string", length=32, nullable=false)
     */
    private $trackingMethod = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="tracking_active", type="boolean", nullable=false)
     */
    private $trackingActive = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_data", type="text", length=65535, nullable=false)
     */
    private $trackingData;



    /**
     * Get trackingId
     *
     * @return integer
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * Set trackingMethod
     *
     * @param string $trackingMethod
     *
     * @return TrackingMethods
     */
    public function setTrackingMethod($trackingMethod)
    {
        $this->trackingMethod = $trackingMethod;

        return $this;
    }

    /**
     * Get trackingMethod
     *
     * @return string
     */
    public function getTrackingMethod()
    {
        return $this->trackingMethod;
    }

    /**
     * Set trackingActive
     *
     * @param boolean $trackingActive
     *
     * @return TrackingMethods
     */
    public function setTrackingActive($trackingActive)
    {
        $this->trackingActive = $trackingActive;

        return $this;
    }

    /**
     * Get trackingActive
     *
     * @return boolean
     */
    public function getTrackingActive()
    {
        return $this->trackingActive;
    }

    /**
     * Set trackingData
     *
     * @param string $trackingData
     *
     * @return TrackingMethods
     */
    public function setTrackingData($trackingData)
    {
        $this->trackingData = $trackingData;

        return $this;
    }

    /**
     * Get trackingData
     *
     * @return string
     */
    public function getTrackingData()
    {
        return $this->trackingData;
    }
}
