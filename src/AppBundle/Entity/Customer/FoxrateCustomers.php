<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * FoxrateCustomers
 *
 * @ORM\Table(name="foxrate_customers")
 * @ORM\Entity
 */
class FoxrateCustomers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fc_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $fcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fc_customers_id", type="integer", nullable=false)
     */
    private $fcCustomersId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fc_orders_id", type="integer", nullable=false)
     */
    private $fcOrdersId;

    /**
     * @var string
     *
     * @ORM\Column(name="fc_customers_email_address", type="string", length=96, nullable=false)
     */
    private $fcCustomersEmailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="fc_customers_firstname", type="string", length=128, nullable=false)
     */
    private $fcCustomersFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="fc_customers_lastname", type="string", length=128, nullable=false)
     */
    private $fcCustomersLastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fc_order_datum", type="datetime", nullable=false)
     */
    private $fcOrderDatum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fc_mail_send_datum", type="datetime", nullable=false)
     */
    private $fcMailSendDatum = 'CURRENT_TIMESTAMP';

    /**
     * @var boolean
     *
     * @ORM\Column(name="fc_mailshipping", type="boolean", nullable=false)
     */
    private $fcMailshipping = '0';



    /**
     * Get fcId
     *
     * @return integer
     */
    public function getFcId()
    {
        return $this->fcId;
    }

    /**
     * Set fcCustomersId
     *
     * @param integer $fcCustomersId
     *
     * @return FoxrateCustomers
     */
    public function setFcCustomersId($fcCustomersId)
    {
        $this->fcCustomersId = $fcCustomersId;

        return $this;
    }

    /**
     * Get fcCustomersId
     *
     * @return integer
     */
    public function getFcCustomersId()
    {
        return $this->fcCustomersId;
    }

    /**
     * Set fcOrdersId
     *
     * @param integer $fcOrdersId
     *
     * @return FoxrateCustomers
     */
    public function setFcOrdersId($fcOrdersId)
    {
        $this->fcOrdersId = $fcOrdersId;

        return $this;
    }

    /**
     * Get fcOrdersId
     *
     * @return integer
     */
    public function getFcOrdersId()
    {
        return $this->fcOrdersId;
    }

    /**
     * Set fcCustomersEmailAddress
     *
     * @param string $fcCustomersEmailAddress
     *
     * @return FoxrateCustomers
     */
    public function setFcCustomersEmailAddress($fcCustomersEmailAddress)
    {
        $this->fcCustomersEmailAddress = $fcCustomersEmailAddress;

        return $this;
    }

    /**
     * Get fcCustomersEmailAddress
     *
     * @return string
     */
    public function getFcCustomersEmailAddress()
    {
        return $this->fcCustomersEmailAddress;
    }

    /**
     * Set fcCustomersFirstname
     *
     * @param string $fcCustomersFirstname
     *
     * @return FoxrateCustomers
     */
    public function setFcCustomersFirstname($fcCustomersFirstname)
    {
        $this->fcCustomersFirstname = $fcCustomersFirstname;

        return $this;
    }

    /**
     * Get fcCustomersFirstname
     *
     * @return string
     */
    public function getFcCustomersFirstname()
    {
        return $this->fcCustomersFirstname;
    }

    /**
     * Set fcCustomersLastname
     *
     * @param string $fcCustomersLastname
     *
     * @return FoxrateCustomers
     */
    public function setFcCustomersLastname($fcCustomersLastname)
    {
        $this->fcCustomersLastname = $fcCustomersLastname;

        return $this;
    }

    /**
     * Get fcCustomersLastname
     *
     * @return string
     */
    public function getFcCustomersLastname()
    {
        return $this->fcCustomersLastname;
    }

    /**
     * Set fcOrderDatum
     *
     * @param \DateTime $fcOrderDatum
     *
     * @return FoxrateCustomers
     */
    public function setFcOrderDatum($fcOrderDatum)
    {
        $this->fcOrderDatum = $fcOrderDatum;

        return $this;
    }

    /**
     * Get fcOrderDatum
     *
     * @return \DateTime
     */
    public function getFcOrderDatum()
    {
        return $this->fcOrderDatum;
    }

    /**
     * Set fcMailSendDatum
     *
     * @param \DateTime $fcMailSendDatum
     *
     * @return FoxrateCustomers
     */
    public function setFcMailSendDatum($fcMailSendDatum)
    {
        $this->fcMailSendDatum = $fcMailSendDatum;

        return $this;
    }

    /**
     * Get fcMailSendDatum
     *
     * @return \DateTime
     */
    public function getFcMailSendDatum()
    {
        return $this->fcMailSendDatum;
    }

    /**
     * Set fcMailshipping
     *
     * @param boolean $fcMailshipping
     *
     * @return FoxrateCustomers
     */
    public function setFcMailshipping($fcMailshipping)
    {
        $this->fcMailshipping = $fcMailshipping;

        return $this;
    }

    /**
     * Get fcMailshipping
     *
     * @return boolean
     */
    public function getFcMailshipping()
    {
        return $this->fcMailshipping;
    }
}
