<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsAuthor
 *
 * @ORM\Table(name="news_author")
 * @ORM\Entity
 */
class NewsAuthor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="news_author_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $newsAuthorId;

    /**
     * @var string
     *
     * @ORM\Column(name="news_author_name", type="text", length=65535, nullable=false)
     */
    private $newsAuthorName;

    /**
     * @var string
     *
     * @ORM\Column(name="news_author_position", type="text", length=65535, nullable=false)
     */
    private $newsAuthorPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="news_author_signatur", type="text", length=65535, nullable=false)
     */
    private $newsAuthorSignatur;

    /**
     * @var string
     *
     * @ORM\Column(name="news_author_picture", type="text", length=65535, nullable=false)
     */
    private $newsAuthorPicture;



    /**
     * Get newsAuthorId
     *
     * @return integer
     */
    public function getNewsAuthorId()
    {
        return $this->newsAuthorId;
    }

    /**
     * Set newsAuthorName
     *
     * @param string $newsAuthorName
     *
     * @return NewsAuthor
     */
    public function setNewsAuthorName($newsAuthorName)
    {
        $this->newsAuthorName = $newsAuthorName;

        return $this;
    }

    /**
     * Get newsAuthorName
     *
     * @return string
     */
    public function getNewsAuthorName()
    {
        return $this->newsAuthorName;
    }

    /**
     * Set newsAuthorPosition
     *
     * @param string $newsAuthorPosition
     *
     * @return NewsAuthor
     */
    public function setNewsAuthorPosition($newsAuthorPosition)
    {
        $this->newsAuthorPosition = $newsAuthorPosition;

        return $this;
    }

    /**
     * Get newsAuthorPosition
     *
     * @return string
     */
    public function getNewsAuthorPosition()
    {
        return $this->newsAuthorPosition;
    }

    /**
     * Set newsAuthorSignatur
     *
     * @param string $newsAuthorSignatur
     *
     * @return NewsAuthor
     */
    public function setNewsAuthorSignatur($newsAuthorSignatur)
    {
        $this->newsAuthorSignatur = $newsAuthorSignatur;

        return $this;
    }

    /**
     * Get newsAuthorSignatur
     *
     * @return string
     */
    public function getNewsAuthorSignatur()
    {
        return $this->newsAuthorSignatur;
    }

    /**
     * Set newsAuthorPicture
     *
     * @param string $newsAuthorPicture
     *
     * @return NewsAuthor
     */
    public function setNewsAuthorPicture($newsAuthorPicture)
    {
        $this->newsAuthorPicture = $newsAuthorPicture;

        return $this;
    }

    /**
     * Get newsAuthorPicture
     *
     * @return string
     */
    public function getNewsAuthorPicture()
    {
        return $this->newsAuthorPicture;
    }
}
