<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsAttributes
 *
 * @ORM\Table(name="products_attributes")
 * @ORM\Entity
 */
class ProductsAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_attributes_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsAttributesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="options_id", type="integer", nullable=false)
     */
    private $optionsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="options_values_id", type="integer", nullable=false)
     */
    private $optionsValuesId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="options_values_price", type="decimal", precision=15, scale=4, nullable=false)
     */
    private $optionsValuesPrice = '0.0000';

    /**
     * @var string
     *
     * @ORM\Column(name="price_prefix", type="string", length=1, nullable=false)
     */
    private $pricePrefix = '';



    /**
     * Get productsAttributesId
     *
     * @return integer
     */
    public function getProductsAttributesId()
    {
        return $this->productsAttributesId;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsAttributes
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set optionsId
     *
     * @param integer $optionsId
     *
     * @return ProductsAttributes
     */
    public function setOptionsId($optionsId)
    {
        $this->optionsId = $optionsId;

        return $this;
    }

    /**
     * Get optionsId
     *
     * @return integer
     */
    public function getOptionsId()
    {
        return $this->optionsId;
    }

    /**
     * Set optionsValuesId
     *
     * @param integer $optionsValuesId
     *
     * @return ProductsAttributes
     */
    public function setOptionsValuesId($optionsValuesId)
    {
        $this->optionsValuesId = $optionsValuesId;

        return $this;
    }

    /**
     * Get optionsValuesId
     *
     * @return integer
     */
    public function getOptionsValuesId()
    {
        return $this->optionsValuesId;
    }

    /**
     * Set optionsValuesPrice
     *
     * @param string $optionsValuesPrice
     *
     * @return ProductsAttributes
     */
    public function setOptionsValuesPrice($optionsValuesPrice)
    {
        $this->optionsValuesPrice = $optionsValuesPrice;

        return $this;
    }

    /**
     * Get optionsValuesPrice
     *
     * @return string
     */
    public function getOptionsValuesPrice()
    {
        return $this->optionsValuesPrice;
    }

    /**
     * Set pricePrefix
     *
     * @param string $pricePrefix
     *
     * @return ProductsAttributes
     */
    public function setPricePrefix($pricePrefix)
    {
        $this->pricePrefix = $pricePrefix;

        return $this;
    }

    /**
     * Get pricePrefix
     *
     * @return string
     */
    public function getPricePrefix()
    {
        return $this->pricePrefix;
    }
}
