<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * MarketingExportErrors
 *
 * @ORM\Table(name="marketing_export_errors")
 * @ORM\Entity
 */
class MarketingExportErrors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="affiliate_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $affiliateId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="export_date_lastcheck", type="datetime", nullable=true)
     */
    private $exportDateLastcheck;

    /**
     * @var integer
     *
     * @ORM\Column(name="export_error_all_count", type="integer", nullable=false)
     */
    private $exportErrorAllCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="export_error_products_count", type="integer", nullable=false)
     */
    private $exportErrorProductsCount = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="export_error_head_json", type="text", length=65535, nullable=true)
     */
    private $exportErrorHeadJson;

    /**
     * @var string
     *
     * @ORM\Column(name="export_error_json", type="text", length=16777215, nullable=true)
     */
    private $exportErrorJson;

    /**
     * @var integer
     *
     * @ORM\Column(name="export_recommended_all_count", type="integer", nullable=false)
     */
    private $exportRecommendedAllCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="export_recommended_products_count", type="integer", nullable=false)
     */
    private $exportRecommendedProductsCount = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="export_recommended_head_json", type="text", length=65535, nullable=true)
     */
    private $exportRecommendedHeadJson;

    /**
     * @var string
     *
     * @ORM\Column(name="export_recommended_json", type="text", length=16777215, nullable=true)
     */
    private $exportRecommendedJson;



    /**
     * Get affiliateId
     *
     * @return integer
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * Set exportDateLastcheck
     *
     * @param \DateTime $exportDateLastcheck
     *
     * @return MarketingExportErrors
     */
    public function setExportDateLastcheck($exportDateLastcheck)
    {
        $this->exportDateLastcheck = $exportDateLastcheck;

        return $this;
    }

    /**
     * Get exportDateLastcheck
     *
     * @return \DateTime
     */
    public function getExportDateLastcheck()
    {
        return $this->exportDateLastcheck;
    }

    /**
     * Set exportErrorAllCount
     *
     * @param integer $exportErrorAllCount
     *
     * @return MarketingExportErrors
     */
    public function setExportErrorAllCount($exportErrorAllCount)
    {
        $this->exportErrorAllCount = $exportErrorAllCount;

        return $this;
    }

    /**
     * Get exportErrorAllCount
     *
     * @return integer
     */
    public function getExportErrorAllCount()
    {
        return $this->exportErrorAllCount;
    }

    /**
     * Set exportErrorProductsCount
     *
     * @param integer $exportErrorProductsCount
     *
     * @return MarketingExportErrors
     */
    public function setExportErrorProductsCount($exportErrorProductsCount)
    {
        $this->exportErrorProductsCount = $exportErrorProductsCount;

        return $this;
    }

    /**
     * Get exportErrorProductsCount
     *
     * @return integer
     */
    public function getExportErrorProductsCount()
    {
        return $this->exportErrorProductsCount;
    }

    /**
     * Set exportErrorHeadJson
     *
     * @param string $exportErrorHeadJson
     *
     * @return MarketingExportErrors
     */
    public function setExportErrorHeadJson($exportErrorHeadJson)
    {
        $this->exportErrorHeadJson = $exportErrorHeadJson;

        return $this;
    }

    /**
     * Get exportErrorHeadJson
     *
     * @return string
     */
    public function getExportErrorHeadJson()
    {
        return $this->exportErrorHeadJson;
    }

    /**
     * Set exportErrorJson
     *
     * @param string $exportErrorJson
     *
     * @return MarketingExportErrors
     */
    public function setExportErrorJson($exportErrorJson)
    {
        $this->exportErrorJson = $exportErrorJson;

        return $this;
    }

    /**
     * Get exportErrorJson
     *
     * @return string
     */
    public function getExportErrorJson()
    {
        return $this->exportErrorJson;
    }

    /**
     * Set exportRecommendedAllCount
     *
     * @param integer $exportRecommendedAllCount
     *
     * @return MarketingExportErrors
     */
    public function setExportRecommendedAllCount($exportRecommendedAllCount)
    {
        $this->exportRecommendedAllCount = $exportRecommendedAllCount;

        return $this;
    }

    /**
     * Get exportRecommendedAllCount
     *
     * @return integer
     */
    public function getExportRecommendedAllCount()
    {
        return $this->exportRecommendedAllCount;
    }

    /**
     * Set exportRecommendedProductsCount
     *
     * @param integer $exportRecommendedProductsCount
     *
     * @return MarketingExportErrors
     */
    public function setExportRecommendedProductsCount($exportRecommendedProductsCount)
    {
        $this->exportRecommendedProductsCount = $exportRecommendedProductsCount;

        return $this;
    }

    /**
     * Get exportRecommendedProductsCount
     *
     * @return integer
     */
    public function getExportRecommendedProductsCount()
    {
        return $this->exportRecommendedProductsCount;
    }

    /**
     * Set exportRecommendedHeadJson
     *
     * @param string $exportRecommendedHeadJson
     *
     * @return MarketingExportErrors
     */
    public function setExportRecommendedHeadJson($exportRecommendedHeadJson)
    {
        $this->exportRecommendedHeadJson = $exportRecommendedHeadJson;

        return $this;
    }

    /**
     * Get exportRecommendedHeadJson
     *
     * @return string
     */
    public function getExportRecommendedHeadJson()
    {
        return $this->exportRecommendedHeadJson;
    }

    /**
     * Set exportRecommendedJson
     *
     * @param string $exportRecommendedJson
     *
     * @return MarketingExportErrors
     */
    public function setExportRecommendedJson($exportRecommendedJson)
    {
        $this->exportRecommendedJson = $exportRecommendedJson;

        return $this;
    }

    /**
     * Get exportRecommendedJson
     *
     * @return string
     */
    public function getExportRecommendedJson()
    {
        return $this->exportRecommendedJson;
    }
}
