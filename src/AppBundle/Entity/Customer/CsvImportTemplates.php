<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CsvImportTemplates
 *
 * @ORM\Table(name="csv_import_templates", uniqueConstraints={@ORM\UniqueConstraint(name="headline_md5", columns={"headline_md5"})})
 * @ORM\Entity
 */
class CsvImportTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="headline_md5", type="string", length=32, nullable=false)
     */
    private $headlineMd5 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="correlation", type="text", length=65535, nullable=false)
     */
    private $correlation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime", nullable=false)
     */
    private $dateAdded = '0000-00-00 00:00:00';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set headlineMd5
     *
     * @param string $headlineMd5
     *
     * @return CsvImportTemplates
     */
    public function setHeadlineMd5($headlineMd5)
    {
        $this->headlineMd5 = $headlineMd5;

        return $this;
    }

    /**
     * Get headlineMd5
     *
     * @return string
     */
    public function getHeadlineMd5()
    {
        return $this->headlineMd5;
    }

    /**
     * Set correlation
     *
     * @param string $correlation
     *
     * @return CsvImportTemplates
     */
    public function setCorrelation($correlation)
    {
        $this->correlation = $correlation;

        return $this;
    }

    /**
     * Get correlation
     *
     * @return string
     */
    public function getCorrelation()
    {
        return $this->correlation;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return CsvImportTemplates
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }
}
