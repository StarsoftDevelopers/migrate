<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * LanguagesScripts
 *
 * @ORM\Table(name="languages_scripts", uniqueConstraints={@ORM\UniqueConstraint(name="admin", columns={"admin", "filename"})})
 * @ORM\Entity
 */
class LanguagesScripts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="file_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $fileId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="file_typ", type="boolean", nullable=false)
     */
    private $fileTyp = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=70, nullable=false)
     */
    private $filename = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="admin", type="boolean", nullable=false)
     */
    private $admin = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_link", type="boolean", nullable=false)
     */
    private $showLink = '0';



    /**
     * Get fileId
     *
     * @return integer
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Set fileTyp
     *
     * @param boolean $fileTyp
     *
     * @return LanguagesScripts
     */
    public function setFileTyp($fileTyp)
    {
        $this->fileTyp = $fileTyp;

        return $this;
    }

    /**
     * Get fileTyp
     *
     * @return boolean
     */
    public function getFileTyp()
    {
        return $this->fileTyp;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return LanguagesScripts
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return LanguagesScripts
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set admin
     *
     * @param boolean $admin
     *
     * @return LanguagesScripts
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return boolean
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set showLink
     *
     * @param boolean $showLink
     *
     * @return LanguagesScripts
     */
    public function setShowLink($showLink)
    {
        $this->showLink = $showLink;

        return $this;
    }

    /**
     * Get showLink
     *
     * @return boolean
     */
    public function getShowLink()
    {
        return $this->showLink;
    }
}
