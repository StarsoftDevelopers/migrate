<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersInquiry
 *
 * @ORM\Table(name="customers_inquiry", indexes={@ORM\Index(name="type", columns={"type"})})
 * @ORM\Entity
 */
class CustomersInquiry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inquiry_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inquiryId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=128, nullable=false)
     */
    private $mail = '';

    /**
     * @var string
     *
     * @ORM\Column(name="fon", type="string", length=64, nullable=false)
     */
    private $fon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mitbewerberpreis", type="string", length=16, nullable=false)
     */
    private $mitbewerberpreis = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mitbewerberurl", type="string", length=128, nullable=false)
     */
    private $mitbewerberurl = '';

    /**
     * @var string
     *
     * @ORM\Column(name="anfrage", type="text", length=65535, nullable=false)
     */
    private $anfrage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="date", nullable=false)
     */
    private $datum = '0000-00-00';

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '0';



    /**
     * Get inquiryId
     *
     * @return integer
     */
    public function getInquiryId()
    {
        return $this->inquiryId;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CustomersInquiry
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustomersInquiry
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return CustomersInquiry
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set fon
     *
     * @param string $fon
     *
     * @return CustomersInquiry
     */
    public function setFon($fon)
    {
        $this->fon = $fon;

        return $this;
    }

    /**
     * Get fon
     *
     * @return string
     */
    public function getFon()
    {
        return $this->fon;
    }

    /**
     * Set mitbewerberpreis
     *
     * @param string $mitbewerberpreis
     *
     * @return CustomersInquiry
     */
    public function setMitbewerberpreis($mitbewerberpreis)
    {
        $this->mitbewerberpreis = $mitbewerberpreis;

        return $this;
    }

    /**
     * Get mitbewerberpreis
     *
     * @return string
     */
    public function getMitbewerberpreis()
    {
        return $this->mitbewerberpreis;
    }

    /**
     * Set mitbewerberurl
     *
     * @param string $mitbewerberurl
     *
     * @return CustomersInquiry
     */
    public function setMitbewerberurl($mitbewerberurl)
    {
        $this->mitbewerberurl = $mitbewerberurl;

        return $this;
    }

    /**
     * Get mitbewerberurl
     *
     * @return string
     */
    public function getMitbewerberurl()
    {
        return $this->mitbewerberurl;
    }

    /**
     * Set anfrage
     *
     * @param string $anfrage
     *
     * @return CustomersInquiry
     */
    public function setAnfrage($anfrage)
    {
        $this->anfrage = $anfrage;

        return $this;
    }

    /**
     * Get anfrage
     *
     * @return string
     */
    public function getAnfrage()
    {
        return $this->anfrage;
    }

    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return CustomersInquiry
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return CustomersInquiry
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }
}
