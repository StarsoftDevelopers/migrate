<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * TempOrdersAdvInfo
 *
 * @ORM\Table(name="temp_orders_adv_info")
 * @ORM\Entity
 */
class TempOrdersAdvInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="orders_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ordersId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="orders_reference", type="string", length=32, nullable=false)
     */
    private $ordersReference = '';

    /**
     * @var string
     *
     * @ORM\Column(name="orders_language", type="string", length=2, nullable=false)
     */
    private $ordersLanguage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_country", type="string", length=3, nullable=false)
     */
    private $customersCountry = '';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_country", type="string", length=3, nullable=false)
     */
    private $billingCountry = '';

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_country", type="string", length=3, nullable=false)
     */
    private $deliveryCountry = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="customers_salutation", type="boolean", nullable=false)
     */
    private $customersSalutation = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_firstname", type="string", length=32, nullable=false)
     */
    private $customersFirstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customers_lastname", type="string", length=32, nullable=false)
     */
    private $customersLastname = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="billing_salutation", type="boolean", nullable=false)
     */
    private $billingSalutation = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_firstname", type="string", length=32, nullable=false)
     */
    private $billingFirstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_lastname", type="string", length=32, nullable=false)
     */
    private $billingLastname = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="delivery_salutation", type="boolean", nullable=false)
     */
    private $deliverySalutation = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_firstname", type="string", length=32, nullable=false)
     */
    private $deliveryFirstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_lastname", type="string", length=32, nullable=false)
     */
    private $deliveryLastname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_module", type="string", length=32, nullable=false)
     */
    private $shippingModule = '';

    /**
     * @var string
     *
     * @ORM\Column(name="payment_module", type="string", length=32, nullable=false)
     */
    private $paymentModule = '';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_tax", type="decimal", precision=15, scale=4, nullable=false)
     */
    private $shippingTax = '0.0000';



    /**
     * Get ordersId
     *
     * @return integer
     */
    public function getOrdersId()
    {
        return $this->ordersId;
    }

    /**
     * Set ordersReference
     *
     * @param string $ordersReference
     *
     * @return TempOrdersAdvInfo
     */
    public function setOrdersReference($ordersReference)
    {
        $this->ordersReference = $ordersReference;

        return $this;
    }

    /**
     * Get ordersReference
     *
     * @return string
     */
    public function getOrdersReference()
    {
        return $this->ordersReference;
    }

    /**
     * Set ordersLanguage
     *
     * @param string $ordersLanguage
     *
     * @return TempOrdersAdvInfo
     */
    public function setOrdersLanguage($ordersLanguage)
    {
        $this->ordersLanguage = $ordersLanguage;

        return $this;
    }

    /**
     * Get ordersLanguage
     *
     * @return string
     */
    public function getOrdersLanguage()
    {
        return $this->ordersLanguage;
    }

    /**
     * Set customersCountry
     *
     * @param string $customersCountry
     *
     * @return TempOrdersAdvInfo
     */
    public function setCustomersCountry($customersCountry)
    {
        $this->customersCountry = $customersCountry;

        return $this;
    }

    /**
     * Get customersCountry
     *
     * @return string
     */
    public function getCustomersCountry()
    {
        return $this->customersCountry;
    }

    /**
     * Set billingCountry
     *
     * @param string $billingCountry
     *
     * @return TempOrdersAdvInfo
     */
    public function setBillingCountry($billingCountry)
    {
        $this->billingCountry = $billingCountry;

        return $this;
    }

    /**
     * Get billingCountry
     *
     * @return string
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * Set deliveryCountry
     *
     * @param string $deliveryCountry
     *
     * @return TempOrdersAdvInfo
     */
    public function setDeliveryCountry($deliveryCountry)
    {
        $this->deliveryCountry = $deliveryCountry;

        return $this;
    }

    /**
     * Get deliveryCountry
     *
     * @return string
     */
    public function getDeliveryCountry()
    {
        return $this->deliveryCountry;
    }

    /**
     * Set customersSalutation
     *
     * @param boolean $customersSalutation
     *
     * @return TempOrdersAdvInfo
     */
    public function setCustomersSalutation($customersSalutation)
    {
        $this->customersSalutation = $customersSalutation;

        return $this;
    }

    /**
     * Get customersSalutation
     *
     * @return boolean
     */
    public function getCustomersSalutation()
    {
        return $this->customersSalutation;
    }

    /**
     * Set customersFirstname
     *
     * @param string $customersFirstname
     *
     * @return TempOrdersAdvInfo
     */
    public function setCustomersFirstname($customersFirstname)
    {
        $this->customersFirstname = $customersFirstname;

        return $this;
    }

    /**
     * Get customersFirstname
     *
     * @return string
     */
    public function getCustomersFirstname()
    {
        return $this->customersFirstname;
    }

    /**
     * Set customersLastname
     *
     * @param string $customersLastname
     *
     * @return TempOrdersAdvInfo
     */
    public function setCustomersLastname($customersLastname)
    {
        $this->customersLastname = $customersLastname;

        return $this;
    }

    /**
     * Get customersLastname
     *
     * @return string
     */
    public function getCustomersLastname()
    {
        return $this->customersLastname;
    }

    /**
     * Set billingSalutation
     *
     * @param boolean $billingSalutation
     *
     * @return TempOrdersAdvInfo
     */
    public function setBillingSalutation($billingSalutation)
    {
        $this->billingSalutation = $billingSalutation;

        return $this;
    }

    /**
     * Get billingSalutation
     *
     * @return boolean
     */
    public function getBillingSalutation()
    {
        return $this->billingSalutation;
    }

    /**
     * Set billingFirstname
     *
     * @param string $billingFirstname
     *
     * @return TempOrdersAdvInfo
     */
    public function setBillingFirstname($billingFirstname)
    {
        $this->billingFirstname = $billingFirstname;

        return $this;
    }

    /**
     * Get billingFirstname
     *
     * @return string
     */
    public function getBillingFirstname()
    {
        return $this->billingFirstname;
    }

    /**
     * Set billingLastname
     *
     * @param string $billingLastname
     *
     * @return TempOrdersAdvInfo
     */
    public function setBillingLastname($billingLastname)
    {
        $this->billingLastname = $billingLastname;

        return $this;
    }

    /**
     * Get billingLastname
     *
     * @return string
     */
    public function getBillingLastname()
    {
        return $this->billingLastname;
    }

    /**
     * Set deliverySalutation
     *
     * @param boolean $deliverySalutation
     *
     * @return TempOrdersAdvInfo
     */
    public function setDeliverySalutation($deliverySalutation)
    {
        $this->deliverySalutation = $deliverySalutation;

        return $this;
    }

    /**
     * Get deliverySalutation
     *
     * @return boolean
     */
    public function getDeliverySalutation()
    {
        return $this->deliverySalutation;
    }

    /**
     * Set deliveryFirstname
     *
     * @param string $deliveryFirstname
     *
     * @return TempOrdersAdvInfo
     */
    public function setDeliveryFirstname($deliveryFirstname)
    {
        $this->deliveryFirstname = $deliveryFirstname;

        return $this;
    }

    /**
     * Get deliveryFirstname
     *
     * @return string
     */
    public function getDeliveryFirstname()
    {
        return $this->deliveryFirstname;
    }

    /**
     * Set deliveryLastname
     *
     * @param string $deliveryLastname
     *
     * @return TempOrdersAdvInfo
     */
    public function setDeliveryLastname($deliveryLastname)
    {
        $this->deliveryLastname = $deliveryLastname;

        return $this;
    }

    /**
     * Get deliveryLastname
     *
     * @return string
     */
    public function getDeliveryLastname()
    {
        return $this->deliveryLastname;
    }

    /**
     * Set shippingModule
     *
     * @param string $shippingModule
     *
     * @return TempOrdersAdvInfo
     */
    public function setShippingModule($shippingModule)
    {
        $this->shippingModule = $shippingModule;

        return $this;
    }

    /**
     * Get shippingModule
     *
     * @return string
     */
    public function getShippingModule()
    {
        return $this->shippingModule;
    }

    /**
     * Set paymentModule
     *
     * @param string $paymentModule
     *
     * @return TempOrdersAdvInfo
     */
    public function setPaymentModule($paymentModule)
    {
        $this->paymentModule = $paymentModule;

        return $this;
    }

    /**
     * Get paymentModule
     *
     * @return string
     */
    public function getPaymentModule()
    {
        return $this->paymentModule;
    }

    /**
     * Set shippingTax
     *
     * @param string $shippingTax
     *
     * @return TempOrdersAdvInfo
     */
    public function setShippingTax($shippingTax)
    {
        $this->shippingTax = $shippingTax;

        return $this;
    }

    /**
     * Get shippingTax
     *
     * @return string
     */
    public function getShippingTax()
    {
        return $this->shippingTax;
    }
}
