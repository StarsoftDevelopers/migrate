<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesCaching
 *
 * @ORM\Table(name="categories_caching")
 * @ORM\Entity
 */
class CategoriesCaching
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=64, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lang_id", type="boolean", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $langId;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=254, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $domain;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=false)
     */
    private $time = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="json", type="text", length=16777215, nullable=true)
     */
    private $json;



    /**
     * Set id
     *
     * @param string $id
     *
     * @return CategoriesCaching
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set langId
     *
     * @param boolean $langId
     *
     * @return CategoriesCaching
     */
    public function setLangId($langId)
    {
        $this->langId = $langId;

        return $this;
    }

    /**
     * Get langId
     *
     * @return boolean
     */
    public function getLangId()
    {
        return $this->langId;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return CategoriesCaching
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return CategoriesCaching
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set json
     *
     * @param string $json
     *
     * @return CategoriesCaching
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Get json
     *
     * @return string
     */
    public function getJson()
    {
        return $this->json;
    }
}
