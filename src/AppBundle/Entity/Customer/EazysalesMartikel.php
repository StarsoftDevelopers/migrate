<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesMartikel
 *
 * @ORM\Table(name="eazysales_martikel")
 * @ORM\Entity
 */
class EazysalesMartikel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="kArtikel", type="integer", nullable=true)
     */
    private $kartikel;



    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set kartikel
     *
     * @param integer $kartikel
     *
     * @return EazysalesMartikel
     */
    public function setKartikel($kartikel)
    {
        $this->kartikel = $kartikel;

        return $this;
    }

    /**
     * Get kartikel
     *
     * @return integer
     */
    public function getKartikel()
    {
        return $this->kartikel;
    }
}
