<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesMvariationswert
 *
 * @ORM\Table(name="eazysales_mvariationswert")
 * @ORM\Entity
 */
class EazysalesMvariationswert
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_attributes_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsAttributesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="kEigenschaftsWert", type="integer", nullable=true)
     */
    private $keigenschaftswert;

    /**
     * @var integer
     *
     * @ORM\Column(name="kArtikel", type="integer", nullable=true)
     */
    private $kartikel;



    /**
     * Get productsAttributesId
     *
     * @return integer
     */
    public function getProductsAttributesId()
    {
        return $this->productsAttributesId;
    }

    /**
     * Set keigenschaftswert
     *
     * @param integer $keigenschaftswert
     *
     * @return EazysalesMvariationswert
     */
    public function setKeigenschaftswert($keigenschaftswert)
    {
        $this->keigenschaftswert = $keigenschaftswert;

        return $this;
    }

    /**
     * Get keigenschaftswert
     *
     * @return integer
     */
    public function getKeigenschaftswert()
    {
        return $this->keigenschaftswert;
    }

    /**
     * Set kartikel
     *
     * @param integer $kartikel
     *
     * @return EazysalesMvariationswert
     */
    public function setKartikel($kartikel)
    {
        $this->kartikel = $kartikel;

        return $this;
    }

    /**
     * Get kartikel
     *
     * @return integer
     */
    public function getKartikel()
    {
        return $this->kartikel;
    }
}
