<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CouponRedeemTrack
 *
 * @ORM\Table(name="coupon_redeem_track")
 * @ORM\Entity
 */
class CouponRedeemTrack
{
    /**
     * @var integer
     *
     * @ORM\Column(name="unique_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uniqueId;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_id", type="integer", nullable=false)
     */
    private $couponId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customerId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="redeem_date", type="datetime", nullable=false)
     */
    private $redeemDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="redeem_ip", type="string", length=32, nullable=false)
     */
    private $redeemIp = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId = '0';



    /**
     * Get uniqueId
     *
     * @return integer
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set couponId
     *
     * @param integer $couponId
     *
     * @return CouponRedeemTrack
     */
    public function setCouponId($couponId)
    {
        $this->couponId = $couponId;

        return $this;
    }

    /**
     * Get couponId
     *
     * @return integer
     */
    public function getCouponId()
    {
        return $this->couponId;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return CouponRedeemTrack
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set redeemDate
     *
     * @param \DateTime $redeemDate
     *
     * @return CouponRedeemTrack
     */
    public function setRedeemDate($redeemDate)
    {
        $this->redeemDate = $redeemDate;

        return $this;
    }

    /**
     * Get redeemDate
     *
     * @return \DateTime
     */
    public function getRedeemDate()
    {
        return $this->redeemDate;
    }

    /**
     * Set redeemIp
     *
     * @param string $redeemIp
     *
     * @return CouponRedeemTrack
     */
    public function setRedeemIp($redeemIp)
    {
        $this->redeemIp = $redeemIp;

        return $this;
    }

    /**
     * Get redeemIp
     *
     * @return string
     */
    public function getRedeemIp()
    {
        return $this->redeemIp;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     *
     * @return CouponRedeemTrack
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
