<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesMkategorie
 *
 * @ORM\Table(name="eazysales_mkategorie")
 * @ORM\Entity
 */
class EazysalesMkategorie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categories_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoriesId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="kKategorie", type="integer", nullable=true)
     */
    private $kkategorie;



    /**
     * Get categoriesId
     *
     * @return integer
     */
    public function getCategoriesId()
    {
        return $this->categoriesId;
    }

    /**
     * Set kkategorie
     *
     * @param integer $kkategorie
     *
     * @return EazysalesMkategorie
     */
    public function setKkategorie($kkategorie)
    {
        $this->kkategorie = $kkategorie;

        return $this;
    }

    /**
     * Get kkategorie
     *
     * @return integer
     */
    public function getKkategorie()
    {
        return $this->kkategorie;
    }
}
