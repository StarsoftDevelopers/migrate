<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * PreisvgAnbieter
 *
 * @ORM\Table(name="preisvg_anbieter", indexes={@ORM\Index(name="NameIntern", columns={"NameIntern"}), @ORM\Index(name="NameIntern_2", columns={"NameIntern"})})
 * @ORM\Entity
 */
class PreisvgAnbieter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Parent", type="string", length=128, nullable=true)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=128, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NameIntern", type="string", length=100, nullable=true)
     */
    private $nameintern;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Status", type="boolean", nullable=false)
     */
    private $status = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="PermStatus", type="boolean", nullable=false)
     */
    private $permstatus = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="BudgetMonat", type="float", precision=10, scale=0, nullable=false)
     */
    private $budgetmonat = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="KlickPreis", type="float", precision=10, scale=0, nullable=false)
     */
    private $klickpreis = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="Sort", type="integer", nullable=true)
     */
    private $sort;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cflag", type="boolean", nullable=true)
     */
    private $cflag;

    /**
     * @var string
     *
     * @ORM\Column(name="Login", type="string", length=100, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=100, nullable=true)
     */
    private $password;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Creating", type="boolean", nullable=false)
     */
    private $creating = '1';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent
     *
     * @param string $parent
     *
     * @return PreisvgAnbieter
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PreisvgAnbieter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameintern
     *
     * @param string $nameintern
     *
     * @return PreisvgAnbieter
     */
    public function setNameintern($nameintern)
    {
        $this->nameintern = $nameintern;

        return $this;
    }

    /**
     * Get nameintern
     *
     * @return string
     */
    public function getNameintern()
    {
        return $this->nameintern;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return PreisvgAnbieter
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set permstatus
     *
     * @param boolean $permstatus
     *
     * @return PreisvgAnbieter
     */
    public function setPermstatus($permstatus)
    {
        $this->permstatus = $permstatus;

        return $this;
    }

    /**
     * Get permstatus
     *
     * @return boolean
     */
    public function getPermstatus()
    {
        return $this->permstatus;
    }

    /**
     * Set budgetmonat
     *
     * @param float $budgetmonat
     *
     * @return PreisvgAnbieter
     */
    public function setBudgetmonat($budgetmonat)
    {
        $this->budgetmonat = $budgetmonat;

        return $this;
    }

    /**
     * Get budgetmonat
     *
     * @return float
     */
    public function getBudgetmonat()
    {
        return $this->budgetmonat;
    }

    /**
     * Set klickpreis
     *
     * @param float $klickpreis
     *
     * @return PreisvgAnbieter
     */
    public function setKlickpreis($klickpreis)
    {
        $this->klickpreis = $klickpreis;

        return $this;
    }

    /**
     * Get klickpreis
     *
     * @return float
     */
    public function getKlickpreis()
    {
        return $this->klickpreis;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return PreisvgAnbieter
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set cflag
     *
     * @param boolean $cflag
     *
     * @return PreisvgAnbieter
     */
    public function setCflag($cflag)
    {
        $this->cflag = $cflag;

        return $this;
    }

    /**
     * Get cflag
     *
     * @return boolean
     */
    public function getCflag()
    {
        return $this->cflag;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return PreisvgAnbieter
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return PreisvgAnbieter
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set creating
     *
     * @param boolean $creating
     *
     * @return PreisvgAnbieter
     */
    public function setCreating($creating)
    {
        $this->creating = $creating;

        return $this;
    }

    /**
     * Get creating
     *
     * @return boolean
     */
    public function getCreating()
    {
        return $this->creating;
    }
}
