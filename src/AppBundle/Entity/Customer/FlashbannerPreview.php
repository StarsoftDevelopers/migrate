<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * FlashbannerPreview
 *
 * @ORM\Table(name="flashbanner_preview")
 * @ORM\Entity
 */
class FlashbannerPreview
{
    /**
     * @var integer
     *
     * @ORM\Column(name="banner_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bannerId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="template_id", type="integer", nullable=false)
     */
    private $templateId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="products_src", type="string", length=50, nullable=false)
     */
    private $productsSrc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="src_ids", type="string", length=250, nullable=false)
     */
    private $srcIds = '';

    /**
     * @var string
     *
     * @ORM\Column(name="src_file", type="string", length=250, nullable=false)
     */
    private $srcFile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=10, nullable=false)
     */
    private $size = '';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=false)
     */
    private $type = '';



    /**
     * Get bannerId
     *
     * @return integer
     */
    public function getBannerId()
    {
        return $this->bannerId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FlashbannerPreview
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return FlashbannerPreview
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set templateId
     *
     * @param integer $templateId
     *
     * @return FlashbannerPreview
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;

        return $this;
    }

    /**
     * Get templateId
     *
     * @return integer
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * Set productsSrc
     *
     * @param string $productsSrc
     *
     * @return FlashbannerPreview
     */
    public function setProductsSrc($productsSrc)
    {
        $this->productsSrc = $productsSrc;

        return $this;
    }

    /**
     * Get productsSrc
     *
     * @return string
     */
    public function getProductsSrc()
    {
        return $this->productsSrc;
    }

    /**
     * Set srcIds
     *
     * @param string $srcIds
     *
     * @return FlashbannerPreview
     */
    public function setSrcIds($srcIds)
    {
        $this->srcIds = $srcIds;

        return $this;
    }

    /**
     * Get srcIds
     *
     * @return string
     */
    public function getSrcIds()
    {
        return $this->srcIds;
    }

    /**
     * Set srcFile
     *
     * @param string $srcFile
     *
     * @return FlashbannerPreview
     */
    public function setSrcFile($srcFile)
    {
        $this->srcFile = $srcFile;

        return $this;
    }

    /**
     * Get srcFile
     *
     * @return string
     */
    public function getSrcFile()
    {
        return $this->srcFile;
    }

    /**
     * Set size
     *
     * @param string $size
     *
     * @return FlashbannerPreview
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return FlashbannerPreview
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
