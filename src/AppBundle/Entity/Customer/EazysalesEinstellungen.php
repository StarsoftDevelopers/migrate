<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * EazysalesEinstellungen
 *
 * @ORM\Table(name="eazysales_einstellungen")
 * @ORM\Entity
 */
class EazysalesEinstellungen
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="currencies_id", type="smallint", nullable=true)
     */
    private $currenciesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="smallint", nullable=true)
     */
    private $languagesId;

    /**
     * @var string
     *
     * @ORM\Column(name="mappingEndkunde", type="string", length=255, nullable=true)
     */
    private $mappingendkunde;

    /**
     * @var string
     *
     * @ORM\Column(name="mappingHaendlerkunde", type="string", length=255, nullable=true)
     */
    private $mappinghaendlerkunde;

    /**
     * @var string
     *
     * @ORM\Column(name="shopURL", type="string", length=255, nullable=true)
     */
    private $shopurl;

    /**
     * @var integer
     *
     * @ORM\Column(name="tax_class_id", type="integer", nullable=true)
     */
    private $taxClassId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tax_zone_id", type="integer", nullable=true)
     */
    private $taxZoneId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tax_priority", type="integer", nullable=true)
     */
    private $taxPriority;

    /**
     * @var integer
     *
     * @ORM\Column(name="shipping_status_id", type="integer", nullable=true)
     */
    private $shippingStatusId;

    /**
     * @var float
     *
     * @ORM\Column(name="versandMwst", type="float", precision=10, scale=0, nullable=true)
     */
    private $versandmwst;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_listing_template", type="string", length=255, nullable=true)
     */
    private $catListingTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_category_template", type="string", length=255, nullable=true)
     */
    private $catCategoryTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_sorting", type="string", length=255, nullable=true)
     */
    private $catSorting;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_sorting2", type="string", length=255, nullable=true)
     */
    private $catSorting2;

    /**
     * @var string
     *
     * @ORM\Column(name="prod_product_template", type="string", length=255, nullable=true)
     */
    private $prodProductTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="prod_options_template", type="string", length=255, nullable=true)
     */
    private $prodOptionsTemplate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="StatusAbgeholt", type="boolean", nullable=false)
     */
    private $statusabgeholt = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="StatusVersendet", type="boolean", nullable=false)
     */
    private $statusversendet = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenciesId
     *
     * @param integer $currenciesId
     *
     * @return EazysalesEinstellungen
     */
    public function setCurrenciesId($currenciesId)
    {
        $this->currenciesId = $currenciesId;

        return $this;
    }

    /**
     * Get currenciesId
     *
     * @return integer
     */
    public function getCurrenciesId()
    {
        return $this->currenciesId;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return EazysalesEinstellungen
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }

    /**
     * Set mappingendkunde
     *
     * @param string $mappingendkunde
     *
     * @return EazysalesEinstellungen
     */
    public function setMappingendkunde($mappingendkunde)
    {
        $this->mappingendkunde = $mappingendkunde;

        return $this;
    }

    /**
     * Get mappingendkunde
     *
     * @return string
     */
    public function getMappingendkunde()
    {
        return $this->mappingendkunde;
    }

    /**
     * Set mappinghaendlerkunde
     *
     * @param string $mappinghaendlerkunde
     *
     * @return EazysalesEinstellungen
     */
    public function setMappinghaendlerkunde($mappinghaendlerkunde)
    {
        $this->mappinghaendlerkunde = $mappinghaendlerkunde;

        return $this;
    }

    /**
     * Get mappinghaendlerkunde
     *
     * @return string
     */
    public function getMappinghaendlerkunde()
    {
        return $this->mappinghaendlerkunde;
    }

    /**
     * Set shopurl
     *
     * @param string $shopurl
     *
     * @return EazysalesEinstellungen
     */
    public function setShopurl($shopurl)
    {
        $this->shopurl = $shopurl;

        return $this;
    }

    /**
     * Get shopurl
     *
     * @return string
     */
    public function getShopurl()
    {
        return $this->shopurl;
    }

    /**
     * Set taxClassId
     *
     * @param integer $taxClassId
     *
     * @return EazysalesEinstellungen
     */
    public function setTaxClassId($taxClassId)
    {
        $this->taxClassId = $taxClassId;

        return $this;
    }

    /**
     * Get taxClassId
     *
     * @return integer
     */
    public function getTaxClassId()
    {
        return $this->taxClassId;
    }

    /**
     * Set taxZoneId
     *
     * @param integer $taxZoneId
     *
     * @return EazysalesEinstellungen
     */
    public function setTaxZoneId($taxZoneId)
    {
        $this->taxZoneId = $taxZoneId;

        return $this;
    }

    /**
     * Get taxZoneId
     *
     * @return integer
     */
    public function getTaxZoneId()
    {
        return $this->taxZoneId;
    }

    /**
     * Set taxPriority
     *
     * @param integer $taxPriority
     *
     * @return EazysalesEinstellungen
     */
    public function setTaxPriority($taxPriority)
    {
        $this->taxPriority = $taxPriority;

        return $this;
    }

    /**
     * Get taxPriority
     *
     * @return integer
     */
    public function getTaxPriority()
    {
        return $this->taxPriority;
    }

    /**
     * Set shippingStatusId
     *
     * @param integer $shippingStatusId
     *
     * @return EazysalesEinstellungen
     */
    public function setShippingStatusId($shippingStatusId)
    {
        $this->shippingStatusId = $shippingStatusId;

        return $this;
    }

    /**
     * Get shippingStatusId
     *
     * @return integer
     */
    public function getShippingStatusId()
    {
        return $this->shippingStatusId;
    }

    /**
     * Set versandmwst
     *
     * @param float $versandmwst
     *
     * @return EazysalesEinstellungen
     */
    public function setVersandmwst($versandmwst)
    {
        $this->versandmwst = $versandmwst;

        return $this;
    }

    /**
     * Get versandmwst
     *
     * @return float
     */
    public function getVersandmwst()
    {
        return $this->versandmwst;
    }

    /**
     * Set catListingTemplate
     *
     * @param string $catListingTemplate
     *
     * @return EazysalesEinstellungen
     */
    public function setCatListingTemplate($catListingTemplate)
    {
        $this->catListingTemplate = $catListingTemplate;

        return $this;
    }

    /**
     * Get catListingTemplate
     *
     * @return string
     */
    public function getCatListingTemplate()
    {
        return $this->catListingTemplate;
    }

    /**
     * Set catCategoryTemplate
     *
     * @param string $catCategoryTemplate
     *
     * @return EazysalesEinstellungen
     */
    public function setCatCategoryTemplate($catCategoryTemplate)
    {
        $this->catCategoryTemplate = $catCategoryTemplate;

        return $this;
    }

    /**
     * Get catCategoryTemplate
     *
     * @return string
     */
    public function getCatCategoryTemplate()
    {
        return $this->catCategoryTemplate;
    }

    /**
     * Set catSorting
     *
     * @param string $catSorting
     *
     * @return EazysalesEinstellungen
     */
    public function setCatSorting($catSorting)
    {
        $this->catSorting = $catSorting;

        return $this;
    }

    /**
     * Get catSorting
     *
     * @return string
     */
    public function getCatSorting()
    {
        return $this->catSorting;
    }

    /**
     * Set catSorting2
     *
     * @param string $catSorting2
     *
     * @return EazysalesEinstellungen
     */
    public function setCatSorting2($catSorting2)
    {
        $this->catSorting2 = $catSorting2;

        return $this;
    }

    /**
     * Get catSorting2
     *
     * @return string
     */
    public function getCatSorting2()
    {
        return $this->catSorting2;
    }

    /**
     * Set prodProductTemplate
     *
     * @param string $prodProductTemplate
     *
     * @return EazysalesEinstellungen
     */
    public function setProdProductTemplate($prodProductTemplate)
    {
        $this->prodProductTemplate = $prodProductTemplate;

        return $this;
    }

    /**
     * Get prodProductTemplate
     *
     * @return string
     */
    public function getProdProductTemplate()
    {
        return $this->prodProductTemplate;
    }

    /**
     * Set prodOptionsTemplate
     *
     * @param string $prodOptionsTemplate
     *
     * @return EazysalesEinstellungen
     */
    public function setProdOptionsTemplate($prodOptionsTemplate)
    {
        $this->prodOptionsTemplate = $prodOptionsTemplate;

        return $this;
    }

    /**
     * Get prodOptionsTemplate
     *
     * @return string
     */
    public function getProdOptionsTemplate()
    {
        return $this->prodOptionsTemplate;
    }

    /**
     * Set statusabgeholt
     *
     * @param boolean $statusabgeholt
     *
     * @return EazysalesEinstellungen
     */
    public function setStatusabgeholt($statusabgeholt)
    {
        $this->statusabgeholt = $statusabgeholt;

        return $this;
    }

    /**
     * Get statusabgeholt
     *
     * @return boolean
     */
    public function getStatusabgeholt()
    {
        return $this->statusabgeholt;
    }

    /**
     * Set statusversendet
     *
     * @param boolean $statusversendet
     *
     * @return EazysalesEinstellungen
     */
    public function setStatusversendet($statusversendet)
    {
        $this->statusversendet = $statusversendet;

        return $this;
    }

    /**
     * Get statusversendet
     *
     * @return boolean
     */
    public function getStatusversendet()
    {
        return $this->statusversendet;
    }
}
