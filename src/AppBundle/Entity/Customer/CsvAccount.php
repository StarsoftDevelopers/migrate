<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * CsvAccount
 *
 * @ORM\Table(name="csv_account")
 * @ORM\Entity
 */
class CsvAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="account_name", type="string", length=64, nullable=false)
     */
    private $accountName = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="account_type", type="boolean", nullable=false)
     */
    private $accountType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="account_separator", type="string", length=12, nullable=false)
     */
    private $accountSeparator = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="account_preview", type="integer", nullable=false)
     */
    private $accountPreview = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="account_filename", type="string", length=64, nullable=false)
     */
    private $accountFilename = '';

    /**
     * @var string
     *
     * @ORM\Column(name="account_categories", type="string", length=12, nullable=false)
     */
    private $accountCategories = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_host", type="string", length=64, nullable=false)
     */
    private $ftpHost = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_user", type="string", length=32, nullable=false)
     */
    private $ftpUser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_password", type="string", length=32, nullable=false)
     */
    private $ftpPassword = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_import", type="datetime", nullable=false)
     */
    private $lastImport = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="ftp_bildpfad", type="string", length=255, nullable=true)
     */
    private $ftpBildpfad;

    /**
     * @var integer
     *
     * @ORM\Column(name="languages_id", type="integer", nullable=true)
     */
    private $languagesId;



    /**
     * Get accountId
     *
     * @return integer
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Set accountName
     *
     * @param string $accountName
     *
     * @return CsvAccount
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;

        return $this;
    }

    /**
     * Get accountName
     *
     * @return string
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * Set accountType
     *
     * @param boolean $accountType
     *
     * @return CsvAccount
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get accountType
     *
     * @return boolean
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * Set accountSeparator
     *
     * @param string $accountSeparator
     *
     * @return CsvAccount
     */
    public function setAccountSeparator($accountSeparator)
    {
        $this->accountSeparator = $accountSeparator;

        return $this;
    }

    /**
     * Get accountSeparator
     *
     * @return string
     */
    public function getAccountSeparator()
    {
        return $this->accountSeparator;
    }

    /**
     * Set accountPreview
     *
     * @param integer $accountPreview
     *
     * @return CsvAccount
     */
    public function setAccountPreview($accountPreview)
    {
        $this->accountPreview = $accountPreview;

        return $this;
    }

    /**
     * Get accountPreview
     *
     * @return integer
     */
    public function getAccountPreview()
    {
        return $this->accountPreview;
    }

    /**
     * Set accountFilename
     *
     * @param string $accountFilename
     *
     * @return CsvAccount
     */
    public function setAccountFilename($accountFilename)
    {
        $this->accountFilename = $accountFilename;

        return $this;
    }

    /**
     * Get accountFilename
     *
     * @return string
     */
    public function getAccountFilename()
    {
        return $this->accountFilename;
    }

    /**
     * Set accountCategories
     *
     * @param string $accountCategories
     *
     * @return CsvAccount
     */
    public function setAccountCategories($accountCategories)
    {
        $this->accountCategories = $accountCategories;

        return $this;
    }

    /**
     * Get accountCategories
     *
     * @return string
     */
    public function getAccountCategories()
    {
        return $this->accountCategories;
    }

    /**
     * Set ftpHost
     *
     * @param string $ftpHost
     *
     * @return CsvAccount
     */
    public function setFtpHost($ftpHost)
    {
        $this->ftpHost = $ftpHost;

        return $this;
    }

    /**
     * Get ftpHost
     *
     * @return string
     */
    public function getFtpHost()
    {
        return $this->ftpHost;
    }

    /**
     * Set ftpUser
     *
     * @param string $ftpUser
     *
     * @return CsvAccount
     */
    public function setFtpUser($ftpUser)
    {
        $this->ftpUser = $ftpUser;

        return $this;
    }

    /**
     * Get ftpUser
     *
     * @return string
     */
    public function getFtpUser()
    {
        return $this->ftpUser;
    }

    /**
     * Set ftpPassword
     *
     * @param string $ftpPassword
     *
     * @return CsvAccount
     */
    public function setFtpPassword($ftpPassword)
    {
        $this->ftpPassword = $ftpPassword;

        return $this;
    }

    /**
     * Get ftpPassword
     *
     * @return string
     */
    public function getFtpPassword()
    {
        return $this->ftpPassword;
    }

    /**
     * Set lastImport
     *
     * @param \DateTime $lastImport
     *
     * @return CsvAccount
     */
    public function setLastImport($lastImport)
    {
        $this->lastImport = $lastImport;

        return $this;
    }

    /**
     * Get lastImport
     *
     * @return \DateTime
     */
    public function getLastImport()
    {
        return $this->lastImport;
    }

    /**
     * Set ftpBildpfad
     *
     * @param string $ftpBildpfad
     *
     * @return CsvAccount
     */
    public function setFtpBildpfad($ftpBildpfad)
    {
        $this->ftpBildpfad = $ftpBildpfad;

        return $this;
    }

    /**
     * Get ftpBildpfad
     *
     * @return string
     */
    public function getFtpBildpfad()
    {
        return $this->ftpBildpfad;
    }

    /**
     * Set languagesId
     *
     * @param integer $languagesId
     *
     * @return CsvAccount
     */
    public function setLanguagesId($languagesId)
    {
        $this->languagesId = $languagesId;

        return $this;
    }

    /**
     * Get languagesId
     *
     * @return integer
     */
    public function getLanguagesId()
    {
        return $this->languagesId;
    }
}
