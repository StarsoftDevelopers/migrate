<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsBasePriceDescription
 *
 * @ORM\Table(name="products_base_price_description")
 * @ORM\Entity
 */
class ProductsBasePriceDescription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="base_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $baseId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="base_name", type="string", length=32, nullable=false)
     */
    private $baseName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="base_value", type="string", length=10, nullable=true)
     */
    private $baseValue;

    /**
     * @var string
     *
     * @ORM\Column(name="base_unit", type="string", length=10, nullable=true)
     */
    private $baseUnit;



    /**
     * Set baseId
     *
     * @param integer $baseId
     *
     * @return ProductsBasePriceDescription
     */
    public function setBaseId($baseId)
    {
        $this->baseId = $baseId;

        return $this;
    }

    /**
     * Get baseId
     *
     * @return integer
     */
    public function getBaseId()
    {
        return $this->baseId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return ProductsBasePriceDescription
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set baseName
     *
     * @param string $baseName
     *
     * @return ProductsBasePriceDescription
     */
    public function setBaseName($baseName)
    {
        $this->baseName = $baseName;

        return $this;
    }

    /**
     * Get baseName
     *
     * @return string
     */
    public function getBaseName()
    {
        return $this->baseName;
    }

    /**
     * Set baseValue
     *
     * @param string $baseValue
     *
     * @return ProductsBasePriceDescription
     */
    public function setBaseValue($baseValue)
    {
        $this->baseValue = $baseValue;

        return $this;
    }

    /**
     * Get baseValue
     *
     * @return string
     */
    public function getBaseValue()
    {
        return $this->baseValue;
    }

    /**
     * Set baseUnit
     *
     * @param string $baseUnit
     *
     * @return ProductsBasePriceDescription
     */
    public function setBaseUnit($baseUnit)
    {
        $this->baseUnit = $baseUnit;

        return $this;
    }

    /**
     * Get baseUnit
     *
     * @return string
     */
    public function getBaseUnit()
    {
        return $this->baseUnit;
    }
}
