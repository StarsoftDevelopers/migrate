<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Featured
 *
 * @ORM\Table(name="featured", indexes={@ORM\Index(name="products_id", columns={"products_id"})})
 * @ORM\Entity
 */
class Featured
{
    /**
     * @var integer
     *
     * @ORM\Column(name="featured_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $featuredId;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     */
    private $productsId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="featured_date_added", type="datetime", nullable=true)
     */
    private $featuredDateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="featured_last_modified", type="datetime", nullable=true)
     */
    private $featuredLastModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires_date", type="datetime", nullable=true)
     */
    private $expiresDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_status_change", type="datetime", nullable=true)
     */
    private $dateStatusChange;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status = '1';



    /**
     * Get featuredId
     *
     * @return integer
     */
    public function getFeaturedId()
    {
        return $this->featuredId;
    }

    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return Featured
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set featuredDateAdded
     *
     * @param \DateTime $featuredDateAdded
     *
     * @return Featured
     */
    public function setFeaturedDateAdded($featuredDateAdded)
    {
        $this->featuredDateAdded = $featuredDateAdded;

        return $this;
    }

    /**
     * Get featuredDateAdded
     *
     * @return \DateTime
     */
    public function getFeaturedDateAdded()
    {
        return $this->featuredDateAdded;
    }

    /**
     * Set featuredLastModified
     *
     * @param \DateTime $featuredLastModified
     *
     * @return Featured
     */
    public function setFeaturedLastModified($featuredLastModified)
    {
        $this->featuredLastModified = $featuredLastModified;

        return $this;
    }

    /**
     * Get featuredLastModified
     *
     * @return \DateTime
     */
    public function getFeaturedLastModified()
    {
        return $this->featuredLastModified;
    }

    /**
     * Set expiresDate
     *
     * @param \DateTime $expiresDate
     *
     * @return Featured
     */
    public function setExpiresDate($expiresDate)
    {
        $this->expiresDate = $expiresDate;

        return $this;
    }

    /**
     * Get expiresDate
     *
     * @return \DateTime
     */
    public function getExpiresDate()
    {
        return $this->expiresDate;
    }

    /**
     * Set dateStatusChange
     *
     * @param \DateTime $dateStatusChange
     *
     * @return Featured
     */
    public function setDateStatusChange($dateStatusChange)
    {
        $this->dateStatusChange = $dateStatusChange;

        return $this;
    }

    /**
     * Get dateStatusChange
     *
     * @return \DateTime
     */
    public function getDateStatusChange()
    {
        return $this->dateStatusChange;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Featured
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
