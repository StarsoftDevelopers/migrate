<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsDescription
 *
 * @ORM\Table(name="products_description", indexes={@ORM\Index(name="products_name", columns={"products_name"})})
 * @ORM\Entity
 */
class ProductsDescription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="products_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $productsId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageId = '2';

    /**
     * @var string
     *
     * @ORM\Column(name="products_name", type="string", length=64, nullable=false)
     */
    private $productsName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="products_description", type="text", length=65535, nullable=true)
     */
    private $productsDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="products_unit", type="string", length=128, nullable=true)
     */
    private $productsUnit;

    /**
     * @var string
     *
     * @ORM\Column(name="products_url", type="string", length=255, nullable=true)
     */
    private $productsUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="products_viewed", type="integer", nullable=true)
     */
    private $productsViewed = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="products_head_title_tag", type="string", length=250, nullable=true)
     */
    private $productsHeadTitleTag;

    /**
     * @var string
     *
     * @ORM\Column(name="products_head_desc_tag", type="text", nullable=true)
     */
    private $productsHeadDescTag;

    /**
     * @var string
     *
     * @ORM\Column(name="products_head_keywords_tag", type="text", nullable=true)
     */
    private $productsHeadKeywordsTag;



    /**
     * Set productsId
     *
     * @param integer $productsId
     *
     * @return ProductsDescription
     */
    public function setProductsId($productsId)
    {
        $this->productsId = $productsId;

        return $this;
    }

    /**
     * Get productsId
     *
     * @return integer
     */
    public function getProductsId()
    {
        return $this->productsId;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return ProductsDescription
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set productsName
     *
     * @param string $productsName
     *
     * @return ProductsDescription
     */
    public function setProductsName($productsName)
    {
        $this->productsName = $productsName;

        return $this;
    }

    /**
     * Get productsName
     *
     * @return string
     */
    public function getProductsName()
    {
        return $this->productsName;
    }

    /**
     * Set productsDescription
     *
     * @param string $productsDescription
     *
     * @return ProductsDescription
     */
    public function setProductsDescription($productsDescription)
    {
        $this->productsDescription = $productsDescription;

        return $this;
    }

    /**
     * Get productsDescription
     *
     * @return string
     */
    public function getProductsDescription()
    {
        return $this->productsDescription;
    }

    /**
     * Set productsUnit
     *
     * @param string $productsUnit
     *
     * @return ProductsDescription
     */
    public function setProductsUnit($productsUnit)
    {
        $this->productsUnit = $productsUnit;

        return $this;
    }

    /**
     * Get productsUnit
     *
     * @return string
     */
    public function getProductsUnit()
    {
        return $this->productsUnit;
    }

    /**
     * Set productsUrl
     *
     * @param string $productsUrl
     *
     * @return ProductsDescription
     */
    public function setProductsUrl($productsUrl)
    {
        $this->productsUrl = $productsUrl;

        return $this;
    }

    /**
     * Get productsUrl
     *
     * @return string
     */
    public function getProductsUrl()
    {
        return $this->productsUrl;
    }

    /**
     * Set productsViewed
     *
     * @param integer $productsViewed
     *
     * @return ProductsDescription
     */
    public function setProductsViewed($productsViewed)
    {
        $this->productsViewed = $productsViewed;

        return $this;
    }

    /**
     * Get productsViewed
     *
     * @return integer
     */
    public function getProductsViewed()
    {
        return $this->productsViewed;
    }

    /**
     * Set productsHeadTitleTag
     *
     * @param string $productsHeadTitleTag
     *
     * @return ProductsDescription
     */
    public function setProductsHeadTitleTag($productsHeadTitleTag)
    {
        $this->productsHeadTitleTag = $productsHeadTitleTag;

        return $this;
    }

    /**
     * Get productsHeadTitleTag
     *
     * @return string
     */
    public function getProductsHeadTitleTag()
    {
        return $this->productsHeadTitleTag;
    }

    /**
     * Set productsHeadDescTag
     *
     * @param string $productsHeadDescTag
     *
     * @return ProductsDescription
     */
    public function setProductsHeadDescTag($productsHeadDescTag)
    {
        $this->productsHeadDescTag = $productsHeadDescTag;

        return $this;
    }

    /**
     * Get productsHeadDescTag
     *
     * @return string
     */
    public function getProductsHeadDescTag()
    {
        return $this->productsHeadDescTag;
    }

    /**
     * Set productsHeadKeywordsTag
     *
     * @param string $productsHeadKeywordsTag
     *
     * @return ProductsDescription
     */
    public function setProductsHeadKeywordsTag($productsHeadKeywordsTag)
    {
        $this->productsHeadKeywordsTag = $productsHeadKeywordsTag;

        return $this;
    }

    /**
     * Get productsHeadKeywordsTag
     *
     * @return string
     */
    public function getProductsHeadKeywordsTag()
    {
        return $this->productsHeadKeywordsTag;
    }
}
