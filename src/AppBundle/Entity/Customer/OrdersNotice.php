<?php

namespace AppBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrdersNotice
 *
 * @ORM\Table(name="orders_notice", indexes={@ORM\Index(name="orders_id", columns={"orders_id"})})
 * @ORM\Entity
 */
class OrdersNotice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="orders_notice_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ordersNoticeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="orders_id", type="integer", nullable=false)
     */
    private $ordersId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="orders_notice_date_added", type="datetime", nullable=false)
     */
    private $ordersNoticeDateAdded;

    /**
     * @var string
     *
     * @ORM\Column(name="notice", type="text", length=65535, nullable=false)
     */
    private $notice;



    /**
     * Get ordersNoticeId
     *
     * @return integer
     */
    public function getOrdersNoticeId()
    {
        return $this->ordersNoticeId;
    }

    /**
     * Set ordersId
     *
     * @param integer $ordersId
     *
     * @return OrdersNotice
     */
    public function setOrdersId($ordersId)
    {
        $this->ordersId = $ordersId;

        return $this;
    }

    /**
     * Get ordersId
     *
     * @return integer
     */
    public function getOrdersId()
    {
        return $this->ordersId;
    }

    /**
     * Set ordersNoticeDateAdded
     *
     * @param \DateTime $ordersNoticeDateAdded
     *
     * @return OrdersNotice
     */
    public function setOrdersNoticeDateAdded($ordersNoticeDateAdded)
    {
        $this->ordersNoticeDateAdded = $ordersNoticeDateAdded;

        return $this;
    }

    /**
     * Get ordersNoticeDateAdded
     *
     * @return \DateTime
     */
    public function getOrdersNoticeDateAdded()
    {
        return $this->ordersNoticeDateAdded;
    }

    /**
     * Set notice
     *
     * @param string $notice
     *
     * @return OrdersNotice
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }
}
