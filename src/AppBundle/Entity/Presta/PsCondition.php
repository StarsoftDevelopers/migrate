<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCondition
 *
 * @ORM\Table(name="hd_condition")
 * @ORM\Entity
 */
class PsCondition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_condition", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCondition;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_ps_condition", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPsCondition;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="request", type="text", length=65535, nullable=true)
     */
    private $request;

    /**
     * @var string
     *
     * @ORM\Column(name="operator", type="string", length=32, nullable=true)
     */
    private $operator;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=64, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=64, nullable=true)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="calculation_type", type="string", nullable=true)
     */
    private $calculationType;

    /**
     * @var string
     *
     * @ORM\Column(name="calculation_detail", type="string", length=64, nullable=true)
     */
    private $calculationDetail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false)
     */
    private $dateUpd;



    /**
     * Set idCondition
     *
     * @param integer $idCondition
     *
     * @return PsCondition
     */
    public function setIdCondition($idCondition)
    {
        $this->idCondition = $idCondition;

        return $this;
    }

    /**
     * Get idCondition
     *
     * @return integer
     */
    public function getIdCondition()
    {
        return $this->idCondition;
    }

    /**
     * Set idPsCondition
     *
     * @param integer $idPsCondition
     *
     * @return PsCondition
     */
    public function setIdPsCondition($idPsCondition)
    {
        $this->idPsCondition = $idPsCondition;

        return $this;
    }

    /**
     * Get idPsCondition
     *
     * @return integer
     */
    public function getIdPsCondition()
    {
        return $this->idPsCondition;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PsCondition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set request
     *
     * @param string $request
     *
     * @return PsCondition
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return string
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set operator
     *
     * @param string $operator
     *
     * @return PsCondition
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * Get operator
     *
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return PsCondition
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return PsCondition
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set calculationType
     *
     * @param string $calculationType
     *
     * @return PsCondition
     */
    public function setCalculationType($calculationType)
    {
        $this->calculationType = $calculationType;

        return $this;
    }

    /**
     * Get calculationType
     *
     * @return string
     */
    public function getCalculationType()
    {
        return $this->calculationType;
    }

    /**
     * Set calculationDetail
     *
     * @param string $calculationDetail
     *
     * @return PsCondition
     */
    public function setCalculationDetail($calculationDetail)
    {
        $this->calculationDetail = $calculationDetail;

        return $this;
    }

    /**
     * Get calculationDetail
     *
     * @return string
     */
    public function getCalculationDetail()
    {
        return $this->calculationDetail;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return PsCondition
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsCondition
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsCondition
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }
}
