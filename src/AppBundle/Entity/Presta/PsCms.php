<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCms
 *
 * @ORM\Table(name="hd_cms")
 * @ORM\Entity
 */
class PsCms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cms", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCms;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cms_category", type="integer", nullable=false)
     */
    private $idCmsCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="indexation", type="boolean", nullable=false)
     */
    private $indexation = '1';



    /**
     * Get idCms
     *
     * @return integer
     */
    public function getIdCms()
    {
        return $this->idCms;
    }

    /**
     * Set idCmsCategory
     *
     * @param integer $idCmsCategory
     *
     * @return PsCms
     */
    public function setIdCmsCategory($idCmsCategory)
    {
        $this->idCmsCategory = $idCmsCategory;

        return $this;
    }

    /**
     * Get idCmsCategory
     *
     * @return integer
     */
    public function getIdCmsCategory()
    {
        return $this->idCmsCategory;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return PsCms
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsCms
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set indexation
     *
     * @param boolean $indexation
     *
     * @return PsCms
     */
    public function setIndexation($indexation)
    {
        $this->indexation = $indexation;

        return $this;
    }

    /**
     * Get indexation
     *
     * @return boolean
     */
    public function getIndexation()
    {
        return $this->indexation;
    }
}
