<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSupplyOrderState
 *
 * @ORM\Table(name="hd_supply_order_state")
 * @ORM\Entity
 */
class PsSupplyOrderState
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_supply_order_state", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSupplyOrderState;

    /**
     * @var boolean
     *
     * @ORM\Column(name="delivery_note", type="boolean", nullable=false)
     */
    private $deliveryNote = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="editable", type="boolean", nullable=false)
     */
    private $editable = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="receipt_state", type="boolean", nullable=false)
     */
    private $receiptState = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="pending_receipt", type="boolean", nullable=false)
     */
    private $pendingReceipt = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="enclosed", type="boolean", nullable=false)
     */
    private $enclosed = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=32, nullable=true)
     */
    private $color;



    /**
     * Get idSupplyOrderState
     *
     * @return integer
     */
    public function getIdSupplyOrderState()
    {
        return $this->idSupplyOrderState;
    }

    /**
     * Set deliveryNote
     *
     * @param boolean $deliveryNote
     *
     * @return PsSupplyOrderState
     */
    public function setDeliveryNote($deliveryNote)
    {
        $this->deliveryNote = $deliveryNote;

        return $this;
    }

    /**
     * Get deliveryNote
     *
     * @return boolean
     */
    public function getDeliveryNote()
    {
        return $this->deliveryNote;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     *
     * @return PsSupplyOrderState
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Get editable
     *
     * @return boolean
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set receiptState
     *
     * @param boolean $receiptState
     *
     * @return PsSupplyOrderState
     */
    public function setReceiptState($receiptState)
    {
        $this->receiptState = $receiptState;

        return $this;
    }

    /**
     * Get receiptState
     *
     * @return boolean
     */
    public function getReceiptState()
    {
        return $this->receiptState;
    }

    /**
     * Set pendingReceipt
     *
     * @param boolean $pendingReceipt
     *
     * @return PsSupplyOrderState
     */
    public function setPendingReceipt($pendingReceipt)
    {
        $this->pendingReceipt = $pendingReceipt;

        return $this;
    }

    /**
     * Get pendingReceipt
     *
     * @return boolean
     */
    public function getPendingReceipt()
    {
        return $this->pendingReceipt;
    }

    /**
     * Set enclosed
     *
     * @param boolean $enclosed
     *
     * @return PsSupplyOrderState
     */
    public function setEnclosed($enclosed)
    {
        $this->enclosed = $enclosed;

        return $this;
    }

    /**
     * Get enclosed
     *
     * @return boolean
     */
    public function getEnclosed()
    {
        return $this->enclosed;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return PsSupplyOrderState
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
