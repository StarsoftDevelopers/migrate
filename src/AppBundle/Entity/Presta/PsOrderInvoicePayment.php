<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderInvoicePayment
 *
 * @ORM\Table(name="hd_order_invoice_payment", indexes={@ORM\Index(name="order_payment", columns={"id_order_payment"}), @ORM\Index(name="id_order", columns={"id_order"})})
 * @ORM\Entity
 */
class PsOrderInvoicePayment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_invoice", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idOrderInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_payment", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idOrderPayment;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_order", type="integer", nullable=false)
     */
    private $idOrder;



    /**
     * Set idOrderInvoice
     *
     * @param integer $idOrderInvoice
     *
     * @return PsOrderInvoicePayment
     */
    public function setIdOrderInvoice($idOrderInvoice)
    {
        $this->idOrderInvoice = $idOrderInvoice;

        return $this;
    }

    /**
     * Get idOrderInvoice
     *
     * @return integer
     */
    public function getIdOrderInvoice()
    {
        return $this->idOrderInvoice;
    }

    /**
     * Set idOrderPayment
     *
     * @param integer $idOrderPayment
     *
     * @return PsOrderInvoicePayment
     */
    public function setIdOrderPayment($idOrderPayment)
    {
        $this->idOrderPayment = $idOrderPayment;

        return $this;
    }

    /**
     * Get idOrderPayment
     *
     * @return integer
     */
    public function getIdOrderPayment()
    {
        return $this->idOrderPayment;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     *
     * @return PsOrderInvoicePayment
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;

        return $this;
    }

    /**
     * Get idOrder
     *
     * @return integer
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }
}
