<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsContact
 *
 * @ORM\Table(name="hd_contact")
 * @ORM\Entity
 */
class PsContact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contact", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idContact;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="customer_service", type="boolean", nullable=false)
     */
    private $customerService = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="position", type="boolean", nullable=false)
     */
    private $position = '0';



    /**
     * Get idContact
     *
     * @return integer
     */
    public function getIdContact()
    {
        return $this->idContact;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return PsContact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set customerService
     *
     * @param boolean $customerService
     *
     * @return PsContact
     */
    public function setCustomerService($customerService)
    {
        $this->customerService = $customerService;

        return $this;
    }

    /**
     * Get customerService
     *
     * @return boolean
     */
    public function getCustomerService()
    {
        return $this->customerService;
    }

    /**
     * Set position
     *
     * @param boolean $position
     *
     * @return PsContact
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }
}
