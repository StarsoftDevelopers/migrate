<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsTabAdvice
 *
 * @ORM\Table(name="hd_tab_advice")
 * @ORM\Entity
 */
class PsTabAdvice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tab", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTab;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_advice", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAdvice;



    /**
     * Set idTab
     *
     * @param integer $idTab
     *
     * @return PsTabAdvice
     */
    public function setIdTab($idTab)
    {
        $this->idTab = $idTab;

        return $this;
    }

    /**
     * Get idTab
     *
     * @return integer
     */
    public function getIdTab()
    {
        return $this->idTab;
    }

    /**
     * Set idAdvice
     *
     * @param integer $idAdvice
     *
     * @return PsTabAdvice
     */
    public function setIdAdvice($idAdvice)
    {
        $this->idAdvice = $idAdvice;

        return $this;
    }

    /**
     * Get idAdvice
     *
     * @return integer
     */
    public function getIdAdvice()
    {
        return $this->idAdvice;
    }
}
