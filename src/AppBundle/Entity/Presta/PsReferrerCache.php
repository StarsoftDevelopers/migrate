<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsReferrerCache
 *
 * @ORM\Table(name="hd_referrer_cache")
 * @ORM\Entity
 */
class PsReferrerCache
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_connections_source", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idConnectionsSource;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_referrer", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idReferrer;



    /**
     * Set idConnectionsSource
     *
     * @param integer $idConnectionsSource
     *
     * @return PsReferrerCache
     */
    public function setIdConnectionsSource($idConnectionsSource)
    {
        $this->idConnectionsSource = $idConnectionsSource;

        return $this;
    }

    /**
     * Get idConnectionsSource
     *
     * @return integer
     */
    public function getIdConnectionsSource()
    {
        return $this->idConnectionsSource;
    }

    /**
     * Set idReferrer
     *
     * @param integer $idReferrer
     *
     * @return PsReferrerCache
     */
    public function setIdReferrer($idReferrer)
    {
        $this->idReferrer = $idReferrer;

        return $this;
    }

    /**
     * Get idReferrer
     *
     * @return integer
     */
    public function getIdReferrer()
    {
        return $this->idReferrer;
    }
}
