<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCmsRole
 *
 * @ORM\Table(name="hd_cms_role", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 * @ORM\Entity
 */
class PsCmsRole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cms_role", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCmsRole;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cms", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCms;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;



    /**
     * Set idCmsRole
     *
     * @param integer $idCmsRole
     *
     * @return PsCmsRole
     */
    public function setIdCmsRole($idCmsRole)
    {
        $this->idCmsRole = $idCmsRole;

        return $this;
    }

    /**
     * Get idCmsRole
     *
     * @return integer
     */
    public function getIdCmsRole()
    {
        return $this->idCmsRole;
    }

    /**
     * Set idCms
     *
     * @param integer $idCms
     *
     * @return PsCmsRole
     */
    public function setIdCms($idCms)
    {
        $this->idCms = $idCms;

        return $this;
    }

    /**
     * Get idCms
     *
     * @return integer
     */
    public function getIdCms()
    {
        return $this->idCms;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsCmsRole
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
