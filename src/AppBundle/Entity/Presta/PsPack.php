<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsPack
 *
 * @ORM\Table(name="hd_pack", indexes={@ORM\Index(name="product_item", columns={"id_product_item", "id_product_attribute_item"})})
 * @ORM\Entity
 */
class PsPack
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_pack", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductPack;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_item", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_attribute_item", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductAttributeItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity = '1';



    /**
     * Set idProductPack
     *
     * @param integer $idProductPack
     *
     * @return PsPack
     */
    public function setIdProductPack($idProductPack)
    {
        $this->idProductPack = $idProductPack;

        return $this;
    }

    /**
     * Get idProductPack
     *
     * @return integer
     */
    public function getIdProductPack()
    {
        return $this->idProductPack;
    }

    /**
     * Set idProductItem
     *
     * @param integer $idProductItem
     *
     * @return PsPack
     */
    public function setIdProductItem($idProductItem)
    {
        $this->idProductItem = $idProductItem;

        return $this;
    }

    /**
     * Get idProductItem
     *
     * @return integer
     */
    public function getIdProductItem()
    {
        return $this->idProductItem;
    }

    /**
     * Set idProductAttributeItem
     *
     * @param integer $idProductAttributeItem
     *
     * @return PsPack
     */
    public function setIdProductAttributeItem($idProductAttributeItem)
    {
        $this->idProductAttributeItem = $idProductAttributeItem;

        return $this;
    }

    /**
     * Get idProductAttributeItem
     *
     * @return integer
     */
    public function getIdProductAttributeItem()
    {
        return $this->idProductAttributeItem;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PsPack
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
