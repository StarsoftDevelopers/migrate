<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSearchEngine
 *
 * @ORM\Table(name="hd_search_engine")
 * @ORM\Entity
 */
class PsSearchEngine
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_search_engine", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSearchEngine;

    /**
     * @var string
     *
     * @ORM\Column(name="server", type="string", length=64, nullable=false)
     */
    private $server;

    /**
     * @var string
     *
     * @ORM\Column(name="getvar", type="string", length=16, nullable=false)
     */
    private $getvar;



    /**
     * Get idSearchEngine
     *
     * @return integer
     */
    public function getIdSearchEngine()
    {
        return $this->idSearchEngine;
    }

    /**
     * Set server
     *
     * @param string $server
     *
     * @return PsSearchEngine
     */
    public function setServer($server)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * Get server
     *
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Set getvar
     *
     * @param string $getvar
     *
     * @return PsSearchEngine
     */
    public function setGetvar($getvar)
    {
        $this->getvar = $getvar;

        return $this;
    }

    /**
     * Get getvar
     *
     * @return string
     */
    public function getGetvar()
    {
        return $this->getvar;
    }
}
