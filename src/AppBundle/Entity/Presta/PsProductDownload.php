<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductDownload
 *
 * @ORM\Table(name="hd_product_download", uniqueConstraints={@ORM\UniqueConstraint(name="id_product", columns={"id_product"})}, indexes={@ORM\Index(name="product_active", columns={"id_product", "active"})})
 * @ORM\Entity
 */
class PsProductDownload
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_download", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProductDownload;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     */
    private $idProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="display_filename", type="string", length=255, nullable=true)
     */
    private $displayFilename;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_expiration", type="datetime", nullable=true)
     */
    private $dateExpiration;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_days_accessible", type="integer", nullable=true)
     */
    private $nbDaysAccessible;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_downloadable", type="integer", nullable=true)
     */
    private $nbDownloadable = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_shareable", type="boolean", nullable=false)
     */
    private $isShareable = '0';



    /**
     * Get idProductDownload
     *
     * @return integer
     */
    public function getIdProductDownload()
    {
        return $this->idProductDownload;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsProductDownload
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set displayFilename
     *
     * @param string $displayFilename
     *
     * @return PsProductDownload
     */
    public function setDisplayFilename($displayFilename)
    {
        $this->displayFilename = $displayFilename;

        return $this;
    }

    /**
     * Get displayFilename
     *
     * @return string
     */
    public function getDisplayFilename()
    {
        return $this->displayFilename;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return PsProductDownload
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsProductDownload
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateExpiration
     *
     * @param \DateTime $dateExpiration
     *
     * @return PsProductDownload
     */
    public function setDateExpiration($dateExpiration)
    {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }

    /**
     * Get dateExpiration
     *
     * @return \DateTime
     */
    public function getDateExpiration()
    {
        return $this->dateExpiration;
    }

    /**
     * Set nbDaysAccessible
     *
     * @param integer $nbDaysAccessible
     *
     * @return PsProductDownload
     */
    public function setNbDaysAccessible($nbDaysAccessible)
    {
        $this->nbDaysAccessible = $nbDaysAccessible;

        return $this;
    }

    /**
     * Get nbDaysAccessible
     *
     * @return integer
     */
    public function getNbDaysAccessible()
    {
        return $this->nbDaysAccessible;
    }

    /**
     * Set nbDownloadable
     *
     * @param integer $nbDownloadable
     *
     * @return PsProductDownload
     */
    public function setNbDownloadable($nbDownloadable)
    {
        $this->nbDownloadable = $nbDownloadable;

        return $this;
    }

    /**
     * Get nbDownloadable
     *
     * @return integer
     */
    public function getNbDownloadable()
    {
        return $this->nbDownloadable;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsProductDownload
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set isShareable
     *
     * @param boolean $isShareable
     *
     * @return PsProductDownload
     */
    public function setIsShareable($isShareable)
    {
        $this->isShareable = $isShareable;

        return $this;
    }

    /**
     * Get isShareable
     *
     * @return boolean
     */
    public function getIsShareable()
    {
        return $this->isShareable;
    }
}
