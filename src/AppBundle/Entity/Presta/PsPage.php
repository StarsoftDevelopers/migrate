<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsPage
 *
 * @ORM\Table(name="hd_page", indexes={@ORM\Index(name="id_page_type", columns={"id_page_type"}), @ORM\Index(name="id_object", columns={"id_object"})})
 * @ORM\Entity
 */
class PsPage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_page", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_page_type", type="integer", nullable=false)
     */
    private $idPageType;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_object", type="integer", nullable=true)
     */
    private $idObject;



    /**
     * Get idPage
     *
     * @return integer
     */
    public function getIdPage()
    {
        return $this->idPage;
    }

    /**
     * Set idPageType
     *
     * @param integer $idPageType
     *
     * @return PsPage
     */
    public function setIdPageType($idPageType)
    {
        $this->idPageType = $idPageType;

        return $this;
    }

    /**
     * Get idPageType
     *
     * @return integer
     */
    public function getIdPageType()
    {
        return $this->idPageType;
    }

    /**
     * Set idObject
     *
     * @param integer $idObject
     *
     * @return PsPage
     */
    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;

        return $this;
    }

    /**
     * Get idObject
     *
     * @return integer
     */
    public function getIdObject()
    {
        return $this->idObject;
    }
}
