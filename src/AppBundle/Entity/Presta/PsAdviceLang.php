<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsAdviceLang
 *
 * @ORM\Table(name="hd_advice_lang")
 * @ORM\Entity
 */
class PsAdviceLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_advice", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAdvice;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text", length=65535, nullable=true)
     */
    private $html;



    /**
     * Set idAdvice
     *
     * @param integer $idAdvice
     *
     * @return PsAdviceLang
     */
    public function setIdAdvice($idAdvice)
    {
        $this->idAdvice = $idAdvice;

        return $this;
    }

    /**
     * Get idAdvice
     *
     * @return integer
     */
    public function getIdAdvice()
    {
        return $this->idAdvice;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsAdviceLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set html
     *
     * @param string $html
     *
     * @return PsAdviceLang
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html
     *
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }
}
