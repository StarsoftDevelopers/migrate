<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsHook
 *
 * @ORM\Table(name="hd_hook", uniqueConstraints={@ORM\UniqueConstraint(name="hook_name", columns={"name"})})
 * @ORM\Entity
 */
class PsHook
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_hook", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idHook;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="position", type="boolean", nullable=false)
     */
    private $position = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="live_edit", type="boolean", nullable=false)
     */
    private $liveEdit = '0';



    /**
     * Get idHook
     *
     * @return integer
     */
    public function getIdHook()
    {
        return $this->idHook;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsHook
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PsHook
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PsHook
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set position
     *
     * @param boolean $position
     *
     * @return PsHook
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set liveEdit
     *
     * @param boolean $liveEdit
     *
     * @return PsHook
     */
    public function setLiveEdit($liveEdit)
    {
        $this->liveEdit = $liveEdit;

        return $this;
    }

    /**
     * Get liveEdit
     *
     * @return boolean
     */
    public function getLiveEdit()
    {
        return $this->liveEdit;
    }
}
