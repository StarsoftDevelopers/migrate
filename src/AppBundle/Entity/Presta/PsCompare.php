<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCompare
 *
 * @ORM\Table(name="hd_compare")
 * @ORM\Entity
 */
class PsCompare
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_compare", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCompare;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=false)
     */
    private $idCustomer;



    /**
     * Get idCompare
     *
     * @return integer
     */
    public function getIdCompare()
    {
        return $this->idCompare;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     *
     * @return PsCompare
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }
}
