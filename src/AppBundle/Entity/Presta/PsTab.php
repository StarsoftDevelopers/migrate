<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsTab
 *
 * @ORM\Table(name="hd_tab", indexes={@ORM\Index(name="class_name", columns={"class_name"}), @ORM\Index(name="id_parent", columns={"id_parent"})})
 * @ORM\Entity
 */
class PsTab
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tab", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTab;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_parent", type="integer", nullable=false)
     */
    private $idParent;

    /**
     * @var string
     *
     * @ORM\Column(name="class_name", type="string", length=64, nullable=false)
     */
    private $className;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=64, nullable=true)
     */
    private $module;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_host_mode", type="boolean", nullable=false)
     */
    private $hideHostMode = '0';



    /**
     * Get idTab
     *
     * @return integer
     */
    public function getIdTab()
    {
        return $this->idTab;
    }

    /**
     * Set idParent
     *
     * @param integer $idParent
     *
     * @return PsTab
     */
    public function setIdParent($idParent)
    {
        $this->idParent = $idParent;

        return $this;
    }

    /**
     * Get idParent
     *
     * @return integer
     */
    public function getIdParent()
    {
        return $this->idParent;
    }

    /**
     * Set className
     *
     * @param string $className
     *
     * @return PsTab
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Get className
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Set module
     *
     * @param string $module
     *
     * @return PsTab
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return PsTab
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsTab
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set hideHostMode
     *
     * @param boolean $hideHostMode
     *
     * @return PsTab
     */
    public function setHideHostMode($hideHostMode)
    {
        $this->hideHostMode = $hideHostMode;

        return $this;
    }

    /**
     * Get hideHostMode
     *
     * @return boolean
     */
    public function getHideHostMode()
    {
        return $this->hideHostMode;
    }
}
