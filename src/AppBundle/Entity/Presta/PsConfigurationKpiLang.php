<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsConfigurationKpiLang
 *
 * @ORM\Table(name="hd_configuration_kpi_lang")
 * @ORM\Entity
 */
class PsConfigurationKpiLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_configuration_kpi", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idConfigurationKpi;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=true)
     */
    private $dateUpd;



    /**
     * Set idConfigurationKpi
     *
     * @param integer $idConfigurationKpi
     *
     * @return PsConfigurationKpiLang
     */
    public function setIdConfigurationKpi($idConfigurationKpi)
    {
        $this->idConfigurationKpi = $idConfigurationKpi;

        return $this;
    }

    /**
     * Get idConfigurationKpi
     *
     * @return integer
     */
    public function getIdConfigurationKpi()
    {
        return $this->idConfigurationKpi;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsConfigurationKpiLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return PsConfigurationKpiLang
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsConfigurationKpiLang
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }
}
