<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductCommentCriterionProduct
 *
 * @ORM\Table(name="hd_product_comment_criterion_product", indexes={@ORM\Index(name="id_product_comment_criterion", columns={"id_product_comment_criterion"})})
 * @ORM\Entity
 */
class PsProductCommentCriterionProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_comment_criterion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductCommentCriterion;



    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsProductCommentCriterionProduct
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set idProductCommentCriterion
     *
     * @param integer $idProductCommentCriterion
     *
     * @return PsProductCommentCriterionProduct
     */
    public function setIdProductCommentCriterion($idProductCommentCriterion)
    {
        $this->idProductCommentCriterion = $idProductCommentCriterion;

        return $this;
    }

    /**
     * Get idProductCommentCriterion
     *
     * @return integer
     */
    public function getIdProductCommentCriterion()
    {
        return $this->idProductCommentCriterion;
    }
}
