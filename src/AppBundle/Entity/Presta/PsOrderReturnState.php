<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderReturnState
 *
 * @ORM\Table(name="hd_order_return_state")
 * @ORM\Entity
 */
class PsOrderReturnState
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_return_state", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idOrderReturnState;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=32, nullable=true)
     */
    private $color;



    /**
     * Get idOrderReturnState
     *
     * @return integer
     */
    public function getIdOrderReturnState()
    {
        return $this->idOrderReturnState;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return PsOrderReturnState
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
