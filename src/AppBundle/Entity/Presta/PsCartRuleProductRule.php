<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCartRuleProductRule
 *
 * @ORM\Table(name="hd_cart_rule_product_rule")
 * @ORM\Entity
 */
class PsCartRuleProductRule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_rule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProductRule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_rule_group", type="integer", nullable=false)
     */
    private $idProductRuleGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;



    /**
     * Get idProductRule
     *
     * @return integer
     */
    public function getIdProductRule()
    {
        return $this->idProductRule;
    }

    /**
     * Set idProductRuleGroup
     *
     * @param integer $idProductRuleGroup
     *
     * @return PsCartRuleProductRule
     */
    public function setIdProductRuleGroup($idProductRuleGroup)
    {
        $this->idProductRuleGroup = $idProductRuleGroup;

        return $this;
    }

    /**
     * Get idProductRuleGroup
     *
     * @return integer
     */
    public function getIdProductRuleGroup()
    {
        return $this->idProductRuleGroup;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PsCartRuleProductRule
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
