<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsFeatureValue
 *
 * @ORM\Table(name="hd_feature_value", indexes={@ORM\Index(name="feature", columns={"id_feature"})})
 * @ORM\Entity
 */
class PsFeatureValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_feature_value", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFeatureValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_feature", type="integer", nullable=false)
     */
    private $idFeature;

    /**
     * @var boolean
     *
     * @ORM\Column(name="custom", type="boolean", nullable=true)
     */
    private $custom;



    /**
     * Get idFeatureValue
     *
     * @return integer
     */
    public function getIdFeatureValue()
    {
        return $this->idFeatureValue;
    }

    /**
     * Set idFeature
     *
     * @param integer $idFeature
     *
     * @return PsFeatureValue
     */
    public function setIdFeature($idFeature)
    {
        $this->idFeature = $idFeature;

        return $this;
    }

    /**
     * Get idFeature
     *
     * @return integer
     */
    public function getIdFeature()
    {
        return $this->idFeature;
    }

    /**
     * Set custom
     *
     * @param boolean $custom
     *
     * @return PsFeatureValue
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;

        return $this;
    }

    /**
     * Get custom
     *
     * @return boolean
     */
    public function getCustom()
    {
        return $this->custom;
    }
}
