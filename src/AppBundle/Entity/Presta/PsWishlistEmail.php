<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsWishlistEmail
 *
 * @ORM\Table(name="hd_wishlist_email")
 * @ORM\Entity
 */
class PsWishlistEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_wishlist", type="integer", nullable=false)
     */
    private $idWishlist;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWishlist
     *
     * @param integer $idWishlist
     *
     * @return PsWishlistEmail
     */
    public function setIdWishlist($idWishlist)
    {
        $this->idWishlist = $idWishlist;

        return $this;
    }

    /**
     * Get idWishlist
     *
     * @return integer
     */
    public function getIdWishlist()
    {
        return $this->idWishlist;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return PsWishlistEmail
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsWishlistEmail
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
}
