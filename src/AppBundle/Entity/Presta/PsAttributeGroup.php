<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsAttributeGroup
 *
 * @ORM\Table(name="hd_attribute_group")
 * @ORM\Entity
 */
class PsAttributeGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_attribute_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAttributeGroup;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_color_group", type="boolean", nullable=false)
     */
    private $isColorGroup = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="group_type", type="string", nullable=false)
     */
    private $groupType = 'select';

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position = '0';



    /**
     * Get idAttributeGroup
     *
     * @return integer
     */
    public function getIdAttributeGroup()
    {
        return $this->idAttributeGroup;
    }

    /**
     * Set isColorGroup
     *
     * @param boolean $isColorGroup
     *
     * @return PsAttributeGroup
     */
    public function setIsColorGroup($isColorGroup)
    {
        $this->isColorGroup = $isColorGroup;

        return $this;
    }

    /**
     * Get isColorGroup
     *
     * @return boolean
     */
    public function getIsColorGroup()
    {
        return $this->isColorGroup;
    }

    /**
     * Set groupType
     *
     * @param string $groupType
     *
     * @return PsAttributeGroup
     */
    public function setGroupType($groupType)
    {
        $this->groupType = $groupType;

        return $this;
    }

    /**
     * Get groupType
     *
     * @return string
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return PsAttributeGroup
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
}
