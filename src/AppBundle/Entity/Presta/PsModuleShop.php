<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsModuleShop
 *
 * @ORM\Table(name="hd_module_shop", indexes={@ORM\Index(name="id_shop", columns={"id_shop"})})
 * @ORM\Entity
 */
class PsModuleShop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_module", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idModule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idShop;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enable_device", type="boolean", nullable=false)
     */
    private $enableDevice = '7';



    /**
     * Set idModule
     *
     * @param integer $idModule
     *
     * @return PsModuleShop
     */
    public function setIdModule($idModule)
    {
        $this->idModule = $idModule;

        return $this;
    }

    /**
     * Get idModule
     *
     * @return integer
     */
    public function getIdModule()
    {
        return $this->idModule;
    }

    /**
     * Set idShop
     *
     * @param integer $idShop
     *
     * @return PsModuleShop
     */
    public function setIdShop($idShop)
    {
        $this->idShop = $idShop;

        return $this;
    }

    /**
     * Get idShop
     *
     * @return integer
     */
    public function getIdShop()
    {
        return $this->idShop;
    }

    /**
     * Set enableDevice
     *
     * @param boolean $enableDevice
     *
     * @return PsModuleShop
     */
    public function setEnableDevice($enableDevice)
    {
        $this->enableDevice = $enableDevice;

        return $this;
    }

    /**
     * Get enableDevice
     *
     * @return boolean
     */
    public function getEnableDevice()
    {
        return $this->enableDevice;
    }
}
