<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsModuleAccess
 *
 * @ORM\Table(name="hd_module_access")
 * @ORM\Entity
 */
class PsModuleAccess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_profile", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProfile;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_module", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idModule;

    /**
     * @var boolean
     *
     * @ORM\Column(name="view", type="boolean", nullable=false)
     */
    private $view = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="configure", type="boolean", nullable=false)
     */
    private $configure = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="uninstall", type="boolean", nullable=false)
     */
    private $uninstall = '0';



    /**
     * Set idProfile
     *
     * @param integer $idProfile
     *
     * @return PsModuleAccess
     */
    public function setIdProfile($idProfile)
    {
        $this->idProfile = $idProfile;

        return $this;
    }

    /**
     * Get idProfile
     *
     * @return integer
     */
    public function getIdProfile()
    {
        return $this->idProfile;
    }

    /**
     * Set idModule
     *
     * @param integer $idModule
     *
     * @return PsModuleAccess
     */
    public function setIdModule($idModule)
    {
        $this->idModule = $idModule;

        return $this;
    }

    /**
     * Get idModule
     *
     * @return integer
     */
    public function getIdModule()
    {
        return $this->idModule;
    }

    /**
     * Set view
     *
     * @param boolean $view
     *
     * @return PsModuleAccess
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return boolean
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set configure
     *
     * @param boolean $configure
     *
     * @return PsModuleAccess
     */
    public function setConfigure($configure)
    {
        $this->configure = $configure;

        return $this;
    }

    /**
     * Get configure
     *
     * @return boolean
     */
    public function getConfigure()
    {
        return $this->configure;
    }

    /**
     * Set uninstall
     *
     * @param boolean $uninstall
     *
     * @return PsModuleAccess
     */
    public function setUninstall($uninstall)
    {
        $this->uninstall = $uninstall;

        return $this;
    }

    /**
     * Get uninstall
     *
     * @return boolean
     */
    public function getUninstall()
    {
        return $this->uninstall;
    }
}
