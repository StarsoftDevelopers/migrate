<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCustomizationFieldLang
 *
 * @ORM\Table(name="hd_customization_field_lang")
 * @ORM\Entity
 */
class PsCustomizationFieldLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_customization_field", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCustomizationField;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idShop = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;



    /**
     * Set idCustomizationField
     *
     * @param integer $idCustomizationField
     *
     * @return PsCustomizationFieldLang
     */
    public function setIdCustomizationField($idCustomizationField)
    {
        $this->idCustomizationField = $idCustomizationField;

        return $this;
    }

    /**
     * Get idCustomizationField
     *
     * @return integer
     */
    public function getIdCustomizationField()
    {
        return $this->idCustomizationField;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsCustomizationFieldLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set idShop
     *
     * @param integer $idShop
     *
     * @return PsCustomizationFieldLang
     */
    public function setIdShop($idShop)
    {
        $this->idShop = $idShop;

        return $this;
    }

    /**
     * Get idShop
     *
     * @return integer
     */
    public function getIdShop()
    {
        return $this->idShop;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsCustomizationFieldLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
