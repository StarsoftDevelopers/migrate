<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsAdvice
 *
 * @ORM\Table(name="hd_advice")
 * @ORM\Entity
 */
class PsAdvice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_advice", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAdvice;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_ps_advice", type="integer", nullable=false)
     */
    private $idPsAdvice;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tab", type="integer", nullable=false)
     */
    private $idTab;

    /**
     * @var string
     *
     * @ORM\Column(name="ids_tab", type="text", length=65535, nullable=true)
     */
    private $idsTab;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide", type="boolean", nullable=false)
     */
    private $hide = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable=false)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="selector", type="string", length=255, nullable=true)
     */
    private $selector;

    /**
     * @var integer
     *
     * @ORM\Column(name="start_day", type="integer", nullable=false)
     */
    private $startDay = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="stop_day", type="integer", nullable=false)
     */
    private $stopDay = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight = '1';



    /**
     * Get idAdvice
     *
     * @return integer
     */
    public function getIdAdvice()
    {
        return $this->idAdvice;
    }

    /**
     * Set idPsAdvice
     *
     * @param integer $idPsAdvice
     *
     * @return PsAdvice
     */
    public function setIdPsAdvice($idPsAdvice)
    {
        $this->idPsAdvice = $idPsAdvice;

        return $this;
    }

    /**
     * Get idPsAdvice
     *
     * @return integer
     */
    public function getIdPsAdvice()
    {
        return $this->idPsAdvice;
    }

    /**
     * Set idTab
     *
     * @param integer $idTab
     *
     * @return PsAdvice
     */
    public function setIdTab($idTab)
    {
        $this->idTab = $idTab;

        return $this;
    }

    /**
     * Get idTab
     *
     * @return integer
     */
    public function getIdTab()
    {
        return $this->idTab;
    }

    /**
     * Set idsTab
     *
     * @param string $idsTab
     *
     * @return PsAdvice
     */
    public function setIdsTab($idsTab)
    {
        $this->idsTab = $idsTab;

        return $this;
    }

    /**
     * Get idsTab
     *
     * @return string
     */
    public function getIdsTab()
    {
        return $this->idsTab;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return PsAdvice
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set hide
     *
     * @param boolean $hide
     *
     * @return PsAdvice
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return boolean
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return PsAdvice
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set selector
     *
     * @param string $selector
     *
     * @return PsAdvice
     */
    public function setSelector($selector)
    {
        $this->selector = $selector;

        return $this;
    }

    /**
     * Get selector
     *
     * @return string
     */
    public function getSelector()
    {
        return $this->selector;
    }

    /**
     * Set startDay
     *
     * @param integer $startDay
     *
     * @return PsAdvice
     */
    public function setStartDay($startDay)
    {
        $this->startDay = $startDay;

        return $this;
    }

    /**
     * Get startDay
     *
     * @return integer
     */
    public function getStartDay()
    {
        return $this->startDay;
    }

    /**
     * Set stopDay
     *
     * @param integer $stopDay
     *
     * @return PsAdvice
     */
    public function setStopDay($stopDay)
    {
        $this->stopDay = $stopDay;

        return $this;
    }

    /**
     * Get stopDay
     *
     * @return integer
     */
    public function getStopDay()
    {
        return $this->stopDay;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return PsAdvice
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }
}
