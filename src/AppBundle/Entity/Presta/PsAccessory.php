<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsAccessory
 *
 * @ORM\Table(name="hd_ccessory", indexes={@ORM\Index(name="accessory_product", columns={"id_product_1", "id_product_2"})})
 * @ORM\Entity
 */
class PsAccessory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_1", type="integer", nullable=false)
     */
    private $idProduct1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_2", type="integer", nullable=false)
     */
    private $idProduct2;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProduct1
     *
     * @param integer $idProduct1
     *
     * @return PsAccessory
     */
    public function setIdProduct1($idProduct1)
    {
        $this->idProduct1 = $idProduct1;

        return $this;
    }

    /**
     * Get idProduct1
     *
     * @return integer
     */
    public function getIdProduct1()
    {
        return $this->idProduct1;
    }

    /**
     * Set idProduct2
     *
     * @param integer $idProduct2
     *
     * @return PsAccessory
     */
    public function setIdProduct2($idProduct2)
    {
        $this->idProduct2 = $idProduct2;

        return $this;
    }

    /**
     * Get idProduct2
     *
     * @return integer
     */
    public function getIdProduct2()
    {
        return $this->idProduct2;
    }
}
