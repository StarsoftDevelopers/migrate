<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductCarrier
 *
 * @ORM\Table(name="hd_product_carrier")
 * @ORM\Entity
 */
class PsProductCarrier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_carrier_reference", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCarrierReference;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idShop;



    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsProductCarrier
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set idCarrierReference
     *
     * @param integer $idCarrierReference
     *
     * @return PsProductCarrier
     */
    public function setIdCarrierReference($idCarrierReference)
    {
        $this->idCarrierReference = $idCarrierReference;

        return $this;
    }

    /**
     * Get idCarrierReference
     *
     * @return integer
     */
    public function getIdCarrierReference()
    {
        return $this->idCarrierReference;
    }

    /**
     * Set idShop
     *
     * @param integer $idShop
     *
     * @return PsProductCarrier
     */
    public function setIdShop($idShop)
    {
        $this->idShop = $idShop;

        return $this;
    }

    /**
     * Get idShop
     *
     * @return integer
     */
    public function getIdShop()
    {
        return $this->idShop;
    }
}
