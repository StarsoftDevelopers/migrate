<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsAccess
 *
 * @ORM\Table(name="hd_access")
 * @ORM\Entity
 */
class PsAccess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_profile", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProfile;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tab", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idTab;

    /**
     * @var integer
     *
     * @ORM\Column(name="view", type="integer", nullable=false)
     */
    private $view;

    /**
     * @var integer
     *
     * @ORM\Column(name="add", type="integer", nullable=false)
     */
    private $add;

    /**
     * @var integer
     *
     * @ORM\Column(name="edit", type="integer", nullable=false)
     */
    private $edit;

    /**
     * @var integer
     *
     * @ORM\Column(name="delete", type="integer", nullable=false)
     */
    private $delete;



    /**
     * Set idProfile
     *
     * @param integer $idProfile
     *
     * @return PsAccess
     */
    public function setIdProfile($idProfile)
    {
        $this->idProfile = $idProfile;

        return $this;
    }

    /**
     * Get idProfile
     *
     * @return integer
     */
    public function getIdProfile()
    {
        return $this->idProfile;
    }

    /**
     * Set idTab
     *
     * @param integer $idTab
     *
     * @return PsAccess
     */
    public function setIdTab($idTab)
    {
        $this->idTab = $idTab;

        return $this;
    }

    /**
     * Get idTab
     *
     * @return integer
     */
    public function getIdTab()
    {
        return $this->idTab;
    }

    /**
     * Set view
     *
     * @param integer $view
     *
     * @return PsAccess
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return integer
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set add
     *
     * @param integer $add
     *
     * @return PsAccess
     */
    public function setAdd($add)
    {
        $this->add = $add;

        return $this;
    }

    /**
     * Get add
     *
     * @return integer
     */
    public function getAdd()
    {
        return $this->add;
    }

    /**
     * Set edit
     *
     * @param integer $edit
     *
     * @return PsAccess
     */
    public function setEdit($edit)
    {
        $this->edit = $edit;

        return $this;
    }

    /**
     * Get edit
     *
     * @return integer
     */
    public function getEdit()
    {
        return $this->edit;
    }

    /**
     * Set delete
     *
     * @param integer $delete
     *
     * @return PsAccess
     */
    public function setDelete($delete)
    {
        $this->delete = $delete;

        return $this;
    }

    /**
     * Get delete
     *
     * @return integer
     */
    public function getDelete()
    {
        return $this->delete;
    }
}
