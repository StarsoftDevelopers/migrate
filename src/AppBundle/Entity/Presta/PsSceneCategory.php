<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSceneCategory
 *
 * @ORM\Table(name="hd_scene_category")
 * @ORM\Entity
 */
class PsSceneCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_scene", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idScene;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_category", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCategory;



    /**
     * Set idScene
     *
     * @param integer $idScene
     *
     * @return PsSceneCategory
     */
    public function setIdScene($idScene)
    {
        $this->idScene = $idScene;

        return $this;
    }

    /**
     * Get idScene
     *
     * @return integer
     */
    public function getIdScene()
    {
        return $this->idScene;
    }

    /**
     * Set idCategory
     *
     * @param integer $idCategory
     *
     * @return PsSceneCategory
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;

        return $this;
    }

    /**
     * Get idCategory
     *
     * @return integer
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }
}
