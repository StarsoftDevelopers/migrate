<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsModulesPerfs
 *
 * @ORM\Table(name="hd_modules_perfs", indexes={@ORM\Index(name="session", columns={"session"})})
 * @ORM\Entity
 */
class PsModulesPerfs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_modules_perfs", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idModulesPerfs;

    /**
     * @var integer
     *
     * @ORM\Column(name="session", type="integer", nullable=false)
     */
    private $session;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=62, nullable=false)
     */
    private $module;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=126, nullable=false)
     */
    private $method;

    /**
     * @var float
     *
     * @ORM\Column(name="time_start", type="float", precision=10, scale=0, nullable=false)
     */
    private $timeStart;

    /**
     * @var float
     *
     * @ORM\Column(name="time_end", type="float", precision=10, scale=0, nullable=false)
     */
    private $timeEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="memory_start", type="integer", nullable=false)
     */
    private $memoryStart;

    /**
     * @var integer
     *
     * @ORM\Column(name="memory_end", type="integer", nullable=false)
     */
    private $memoryEnd;



    /**
     * Get idModulesPerfs
     *
     * @return integer
     */
    public function getIdModulesPerfs()
    {
        return $this->idModulesPerfs;
    }

    /**
     * Set session
     *
     * @param integer $session
     *
     * @return PsModulesPerfs
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return integer
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set module
     *
     * @param string $module
     *
     * @return PsModulesPerfs
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set method
     *
     * @param string $method
     *
     * @return PsModulesPerfs
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set timeStart
     *
     * @param float $timeStart
     *
     * @return PsModulesPerfs
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return float
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd
     *
     * @param float $timeEnd
     *
     * @return PsModulesPerfs
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd
     *
     * @return float
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set memoryStart
     *
     * @param integer $memoryStart
     *
     * @return PsModulesPerfs
     */
    public function setMemoryStart($memoryStart)
    {
        $this->memoryStart = $memoryStart;

        return $this;
    }

    /**
     * Get memoryStart
     *
     * @return integer
     */
    public function getMemoryStart()
    {
        return $this->memoryStart;
    }

    /**
     * Set memoryEnd
     *
     * @param integer $memoryEnd
     *
     * @return PsModulesPerfs
     */
    public function setMemoryEnd($memoryEnd)
    {
        $this->memoryEnd = $memoryEnd;

        return $this;
    }

    /**
     * Get memoryEnd
     *
     * @return integer
     */
    public function getMemoryEnd()
    {
        return $this->memoryEnd;
    }
}
