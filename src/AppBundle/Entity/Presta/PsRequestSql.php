<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsRequestSql
 *
 * @ORM\Table(name="hd_request_sql")
 * @ORM\Entity
 */
class PsRequestSql
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_request_sql", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRequestSql;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sql", type="text", length=65535, nullable=false)
     */
    private $sql;



    /**
     * Get idRequestSql
     *
     * @return integer
     */
    public function getIdRequestSql()
    {
        return $this->idRequestSql;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsRequestSql
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sql
     *
     * @param string $sql
     *
     * @return PsRequestSql
     */
    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    /**
     * Get sql
     *
     * @return string
     */
    public function getSql()
    {
        return $this->sql;
    }
}
