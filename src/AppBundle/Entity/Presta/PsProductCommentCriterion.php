<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductCommentCriterion
 *
 * @ORM\Table(name="hd_product_comment_criterion")
 * @ORM\Entity
 */
class PsProductCommentCriterion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_comment_criterion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProductCommentCriterion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="id_product_comment_criterion_type", type="boolean", nullable=false)
     */
    private $idProductCommentCriterionType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;



    /**
     * Get idProductCommentCriterion
     *
     * @return integer
     */
    public function getIdProductCommentCriterion()
    {
        return $this->idProductCommentCriterion;
    }

    /**
     * Set idProductCommentCriterionType
     *
     * @param boolean $idProductCommentCriterionType
     *
     * @return PsProductCommentCriterion
     */
    public function setIdProductCommentCriterionType($idProductCommentCriterionType)
    {
        $this->idProductCommentCriterionType = $idProductCommentCriterionType;

        return $this;
    }

    /**
     * Get idProductCommentCriterionType
     *
     * @return boolean
     */
    public function getIdProductCommentCriterionType()
    {
        return $this->idProductCommentCriterionType;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsProductCommentCriterion
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
