<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsReferrer
 *
 * @ORM\Table(name="hd_referrer")
 * @ORM\Entity
 */
class PsReferrer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_referrer", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idReferrer;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="passwd", type="string", length=32, nullable=true)
     */
    private $passwd;

    /**
     * @var string
     *
     * @ORM\Column(name="http_referer_regexp", type="string", length=64, nullable=true)
     */
    private $httpRefererRegexp;

    /**
     * @var string
     *
     * @ORM\Column(name="http_referer_like", type="string", length=64, nullable=true)
     */
    private $httpRefererLike;

    /**
     * @var string
     *
     * @ORM\Column(name="request_uri_regexp", type="string", length=64, nullable=true)
     */
    private $requestUriRegexp;

    /**
     * @var string
     *
     * @ORM\Column(name="request_uri_like", type="string", length=64, nullable=true)
     */
    private $requestUriLike;

    /**
     * @var string
     *
     * @ORM\Column(name="http_referer_regexp_not", type="string", length=64, nullable=true)
     */
    private $httpRefererRegexpNot;

    /**
     * @var string
     *
     * @ORM\Column(name="http_referer_like_not", type="string", length=64, nullable=true)
     */
    private $httpRefererLikeNot;

    /**
     * @var string
     *
     * @ORM\Column(name="request_uri_regexp_not", type="string", length=64, nullable=true)
     */
    private $requestUriRegexpNot;

    /**
     * @var string
     *
     * @ORM\Column(name="request_uri_like_not", type="string", length=64, nullable=true)
     */
    private $requestUriLikeNot;

    /**
     * @var string
     *
     * @ORM\Column(name="base_fee", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $baseFee = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="percent_fee", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $percentFee = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="click_fee", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $clickFee = '0.00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;



    /**
     * Get idReferrer
     *
     * @return integer
     */
    public function getIdReferrer()
    {
        return $this->idReferrer;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsReferrer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set passwd
     *
     * @param string $passwd
     *
     * @return PsReferrer
     */
    public function setPasswd($passwd)
    {
        $this->passwd = $passwd;

        return $this;
    }

    /**
     * Get passwd
     *
     * @return string
     */
    public function getPasswd()
    {
        return $this->passwd;
    }

    /**
     * Set httpRefererRegexp
     *
     * @param string $httpRefererRegexp
     *
     * @return PsReferrer
     */
    public function setHttpRefererRegexp($httpRefererRegexp)
    {
        $this->httpRefererRegexp = $httpRefererRegexp;

        return $this;
    }

    /**
     * Get httpRefererRegexp
     *
     * @return string
     */
    public function getHttpRefererRegexp()
    {
        return $this->httpRefererRegexp;
    }

    /**
     * Set httpRefererLike
     *
     * @param string $httpRefererLike
     *
     * @return PsReferrer
     */
    public function setHttpRefererLike($httpRefererLike)
    {
        $this->httpRefererLike = $httpRefererLike;

        return $this;
    }

    /**
     * Get httpRefererLike
     *
     * @return string
     */
    public function getHttpRefererLike()
    {
        return $this->httpRefererLike;
    }

    /**
     * Set requestUriRegexp
     *
     * @param string $requestUriRegexp
     *
     * @return PsReferrer
     */
    public function setRequestUriRegexp($requestUriRegexp)
    {
        $this->requestUriRegexp = $requestUriRegexp;

        return $this;
    }

    /**
     * Get requestUriRegexp
     *
     * @return string
     */
    public function getRequestUriRegexp()
    {
        return $this->requestUriRegexp;
    }

    /**
     * Set requestUriLike
     *
     * @param string $requestUriLike
     *
     * @return PsReferrer
     */
    public function setRequestUriLike($requestUriLike)
    {
        $this->requestUriLike = $requestUriLike;

        return $this;
    }

    /**
     * Get requestUriLike
     *
     * @return string
     */
    public function getRequestUriLike()
    {
        return $this->requestUriLike;
    }

    /**
     * Set httpRefererRegexpNot
     *
     * @param string $httpRefererRegexpNot
     *
     * @return PsReferrer
     */
    public function setHttpRefererRegexpNot($httpRefererRegexpNot)
    {
        $this->httpRefererRegexpNot = $httpRefererRegexpNot;

        return $this;
    }

    /**
     * Get httpRefererRegexpNot
     *
     * @return string
     */
    public function getHttpRefererRegexpNot()
    {
        return $this->httpRefererRegexpNot;
    }

    /**
     * Set httpRefererLikeNot
     *
     * @param string $httpRefererLikeNot
     *
     * @return PsReferrer
     */
    public function setHttpRefererLikeNot($httpRefererLikeNot)
    {
        $this->httpRefererLikeNot = $httpRefererLikeNot;

        return $this;
    }

    /**
     * Get httpRefererLikeNot
     *
     * @return string
     */
    public function getHttpRefererLikeNot()
    {
        return $this->httpRefererLikeNot;
    }

    /**
     * Set requestUriRegexpNot
     *
     * @param string $requestUriRegexpNot
     *
     * @return PsReferrer
     */
    public function setRequestUriRegexpNot($requestUriRegexpNot)
    {
        $this->requestUriRegexpNot = $requestUriRegexpNot;

        return $this;
    }

    /**
     * Get requestUriRegexpNot
     *
     * @return string
     */
    public function getRequestUriRegexpNot()
    {
        return $this->requestUriRegexpNot;
    }

    /**
     * Set requestUriLikeNot
     *
     * @param string $requestUriLikeNot
     *
     * @return PsReferrer
     */
    public function setRequestUriLikeNot($requestUriLikeNot)
    {
        $this->requestUriLikeNot = $requestUriLikeNot;

        return $this;
    }

    /**
     * Get requestUriLikeNot
     *
     * @return string
     */
    public function getRequestUriLikeNot()
    {
        return $this->requestUriLikeNot;
    }

    /**
     * Set baseFee
     *
     * @param string $baseFee
     *
     * @return PsReferrer
     */
    public function setBaseFee($baseFee)
    {
        $this->baseFee = $baseFee;

        return $this;
    }

    /**
     * Get baseFee
     *
     * @return string
     */
    public function getBaseFee()
    {
        return $this->baseFee;
    }

    /**
     * Set percentFee
     *
     * @param string $percentFee
     *
     * @return PsReferrer
     */
    public function setPercentFee($percentFee)
    {
        $this->percentFee = $percentFee;

        return $this;
    }

    /**
     * Get percentFee
     *
     * @return string
     */
    public function getPercentFee()
    {
        return $this->percentFee;
    }

    /**
     * Set clickFee
     *
     * @param string $clickFee
     *
     * @return PsReferrer
     */
    public function setClickFee($clickFee)
    {
        $this->clickFee = $clickFee;

        return $this;
    }

    /**
     * Get clickFee
     *
     * @return string
     */
    public function getClickFee()
    {
        return $this->clickFee;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsReferrer
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
}
