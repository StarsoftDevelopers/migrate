<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSmartyLastFlush
 *
 * @ORM\Table(name="hd_smarty_last_flush")
 * @ORM\Entity
 */
class PsSmartyLastFlush
{
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $type = 'compile';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_flush", type="datetime", nullable=false)
     */
    private $lastFlush = '0000-00-00 00:00:00';



    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lastFlush
     *
     * @param \DateTime $lastFlush
     *
     * @return PsSmartyLastFlush
     */
    public function setLastFlush($lastFlush)
    {
        $this->lastFlush = $lastFlush;

        return $this;
    }

    /**
     * Get lastFlush
     *
     * @return \DateTime
     */
    public function getLastFlush()
    {
        return $this->lastFlush;
    }
}
