<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCurrency
 *
 * @ORM\Table(name="hd_currency")
 * @ORM\Entity
 */
class PsCurrency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_currency", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCurrency;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="iso_code", type="string", length=3, nullable=false)
     */
    private $isoCode = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="iso_code_num", type="string", length=3, nullable=false)
     */
    private $isoCodeNum = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="sign", type="string", length=8, nullable=false)
     */
    private $sign;

    /**
     * @var boolean
     *
     * @ORM\Column(name="blank", type="boolean", nullable=false)
     */
    private $blank = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="format", type="boolean", nullable=false)
     */
    private $format = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="decimals", type="boolean", nullable=false)
     */
    private $decimals = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="conversion_rate", type="decimal", precision=13, scale=6, nullable=false)
     */
    private $conversionRate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';



    /**
     * Get idCurrency
     *
     * @return integer
     */
    public function getIdCurrency()
    {
        return $this->idCurrency;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsCurrency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isoCode
     *
     * @param string $isoCode
     *
     * @return PsCurrency
     */
    public function setIsoCode($isoCode)
    {
        $this->isoCode = $isoCode;

        return $this;
    }

    /**
     * Get isoCode
     *
     * @return string
     */
    public function getIsoCode()
    {
        return $this->isoCode;
    }

    /**
     * Set isoCodeNum
     *
     * @param string $isoCodeNum
     *
     * @return PsCurrency
     */
    public function setIsoCodeNum($isoCodeNum)
    {
        $this->isoCodeNum = $isoCodeNum;

        return $this;
    }

    /**
     * Get isoCodeNum
     *
     * @return string
     */
    public function getIsoCodeNum()
    {
        return $this->isoCodeNum;
    }

    /**
     * Set sign
     *
     * @param string $sign
     *
     * @return PsCurrency
     */
    public function setSign($sign)
    {
        $this->sign = $sign;

        return $this;
    }

    /**
     * Get sign
     *
     * @return string
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * Set blank
     *
     * @param boolean $blank
     *
     * @return PsCurrency
     */
    public function setBlank($blank)
    {
        $this->blank = $blank;

        return $this;
    }

    /**
     * Get blank
     *
     * @return boolean
     */
    public function getBlank()
    {
        return $this->blank;
    }

    /**
     * Set format
     *
     * @param boolean $format
     *
     * @return PsCurrency
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return boolean
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set decimals
     *
     * @param boolean $decimals
     *
     * @return PsCurrency
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;

        return $this;
    }

    /**
     * Get decimals
     *
     * @return boolean
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * Set conversionRate
     *
     * @param string $conversionRate
     *
     * @return PsCurrency
     */
    public function setConversionRate($conversionRate)
    {
        $this->conversionRate = $conversionRate;

        return $this;
    }

    /**
     * Get conversionRate
     *
     * @return string
     */
    public function getConversionRate()
    {
        return $this->conversionRate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return PsCurrency
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsCurrency
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
