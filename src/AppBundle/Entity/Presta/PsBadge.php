<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsBadge
 *
 * @ORM\Table(name="hd_badge")
 * @ORM\Entity
 */
class PsBadge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_badge", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBadge;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_ps_badge", type="integer", nullable=false)
     */
    private $idPsBadge;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32, nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     */
    private $idGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_position", type="integer", nullable=false)
     */
    private $groupPosition;

    /**
     * @var integer
     *
     * @ORM\Column(name="scoring", type="integer", nullable=false)
     */
    private $scoring;

    /**
     * @var integer
     *
     * @ORM\Column(name="awb", type="integer", nullable=true)
     */
    private $awb = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated = '0';



    /**
     * Get idBadge
     *
     * @return integer
     */
    public function getIdBadge()
    {
        return $this->idBadge;
    }

    /**
     * Set idPsBadge
     *
     * @param integer $idPsBadge
     *
     * @return PsBadge
     */
    public function setIdPsBadge($idPsBadge)
    {
        $this->idPsBadge = $idPsBadge;

        return $this;
    }

    /**
     * Get idPsBadge
     *
     * @return integer
     */
    public function getIdPsBadge()
    {
        return $this->idPsBadge;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PsBadge
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set idGroup
     *
     * @param integer $idGroup
     *
     * @return PsBadge
     */
    public function setIdGroup($idGroup)
    {
        $this->idGroup = $idGroup;

        return $this;
    }

    /**
     * Get idGroup
     *
     * @return integer
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set groupPosition
     *
     * @param integer $groupPosition
     *
     * @return PsBadge
     */
    public function setGroupPosition($groupPosition)
    {
        $this->groupPosition = $groupPosition;

        return $this;
    }

    /**
     * Get groupPosition
     *
     * @return integer
     */
    public function getGroupPosition()
    {
        return $this->groupPosition;
    }

    /**
     * Set scoring
     *
     * @param integer $scoring
     *
     * @return PsBadge
     */
    public function setScoring($scoring)
    {
        $this->scoring = $scoring;

        return $this;
    }

    /**
     * Get scoring
     *
     * @return integer
     */
    public function getScoring()
    {
        return $this->scoring;
    }

    /**
     * Set awb
     *
     * @param integer $awb
     *
     * @return PsBadge
     */
    public function setAwb($awb)
    {
        $this->awb = $awb;

        return $this;
    }

    /**
     * Get awb
     *
     * @return integer
     */
    public function getAwb()
    {
        return $this->awb;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return PsBadge
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }
}
