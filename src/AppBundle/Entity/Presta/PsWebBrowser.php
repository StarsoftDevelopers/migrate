<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsWebBrowser
 *
 * @ORM\Table(name="hd_web_browser")
 * @ORM\Entity
 */
class PsWebBrowser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_web_browser", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idWebBrowser;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;



    /**
     * Get idWebBrowser
     *
     * @return integer
     */
    public function getIdWebBrowser()
    {
        return $this->idWebBrowser;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsWebBrowser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
