<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsRequiredField
 *
 * @ORM\Table(name="hd_required_field", indexes={@ORM\Index(name="object_name", columns={"object_name"})})
 * @ORM\Entity
 */
class PsRequiredField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_required_field", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRequiredField;

    /**
     * @var string
     *
     * @ORM\Column(name="object_name", type="string", length=32, nullable=false)
     */
    private $objectName;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=32, nullable=false)
     */
    private $fieldName;



    /**
     * Get idRequiredField
     *
     * @return integer
     */
    public function getIdRequiredField()
    {
        return $this->idRequiredField;
    }

    /**
     * Set objectName
     *
     * @param string $objectName
     *
     * @return PsRequiredField
     */
    public function setObjectName($objectName)
    {
        $this->objectName = $objectName;

        return $this;
    }

    /**
     * Get objectName
     *
     * @return string
     */
    public function getObjectName()
    {
        return $this->objectName;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return PsRequiredField
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }
}
