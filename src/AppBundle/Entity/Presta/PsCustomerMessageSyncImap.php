<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCustomerMessageSyncImap
 *
 * @ORM\Table(name="hd_customer_message_sync_imap", indexes={@ORM\Index(name="md5_header_index", columns={"md5_header"})})
 * @ORM\Entity
 */
class PsCustomerMessageSyncImap
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var binary
     *
     * @ORM\Column(name="md5_header", type="binary", nullable=false)
     */
    private $md5Header;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set md5Header
     *
     * @param binary $md5Header
     *
     * @return PsCustomerMessageSyncImap
     */
    public function setMd5Header($md5Header)
    {
        $this->md5Header = $md5Header;

        return $this;
    }

    /**
     * Get md5Header
     *
     * @return binary
     */
    public function getMd5Header()
    {
        return $this->md5Header;
    }
}
