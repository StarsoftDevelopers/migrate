<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderSlipDetailTax
 *
 * @ORM\Table(name="hd_order_slip_detail_tax", indexes={@ORM\Index(name="id_order_slip_detail", columns={"id_order_slip_detail"}), @ORM\Index(name="id_tax", columns={"id_tax"})})
 * @ORM\Entity
 */
class PsOrderSlipDetailTax
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_slip_detail", type="integer", nullable=false)
     */
    private $idOrderSlipDetail;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tax", type="integer", nullable=false)
     */
    private $idTax;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_amount", type="decimal", precision=16, scale=6, nullable=false)
     */
    private $unitAmount = '0.000000';

    /**
     * @var string
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=16, scale=6, nullable=false)
     */
    private $totalAmount = '0.000000';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idOrderSlipDetail
     *
     * @param integer $idOrderSlipDetail
     *
     * @return PsOrderSlipDetailTax
     */
    public function setIdOrderSlipDetail($idOrderSlipDetail)
    {
        $this->idOrderSlipDetail = $idOrderSlipDetail;

        return $this;
    }

    /**
     * Get idOrderSlipDetail
     *
     * @return integer
     */
    public function getIdOrderSlipDetail()
    {
        return $this->idOrderSlipDetail;
    }

    /**
     * Set idTax
     *
     * @param integer $idTax
     *
     * @return PsOrderSlipDetailTax
     */
    public function setIdTax($idTax)
    {
        $this->idTax = $idTax;

        return $this;
    }

    /**
     * Get idTax
     *
     * @return integer
     */
    public function getIdTax()
    {
        return $this->idTax;
    }

    /**
     * Set unitAmount
     *
     * @param string $unitAmount
     *
     * @return PsOrderSlipDetailTax
     */
    public function setUnitAmount($unitAmount)
    {
        $this->unitAmount = $unitAmount;

        return $this;
    }

    /**
     * Get unitAmount
     *
     * @return string
     */
    public function getUnitAmount()
    {
        return $this->unitAmount;
    }

    /**
     * Set totalAmount
     *
     * @param string $totalAmount
     *
     * @return PsOrderSlipDetailTax
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return string
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }
}
