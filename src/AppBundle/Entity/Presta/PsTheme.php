<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsTheme
 *
 * @ORM\Table(name="hd_theme")
 * @ORM\Entity
 */
class PsTheme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_theme", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTheme;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="directory", type="string", length=64, nullable=false)
     */
    private $directory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="responsive", type="boolean", nullable=false)
     */
    private $responsive = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_left_column", type="boolean", nullable=false)
     */
    private $defaultLeftColumn = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_right_column", type="boolean", nullable=false)
     */
    private $defaultRightColumn = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="product_per_page", type="integer", nullable=false)
     */
    private $productPerPage;



    /**
     * Get idTheme
     *
     * @return integer
     */
    public function getIdTheme()
    {
        return $this->idTheme;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsTheme
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set directory
     *
     * @param string $directory
     *
     * @return PsTheme
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * Get directory
     *
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * Set responsive
     *
     * @param boolean $responsive
     *
     * @return PsTheme
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;

        return $this;
    }

    /**
     * Get responsive
     *
     * @return boolean
     */
    public function getResponsive()
    {
        return $this->responsive;
    }

    /**
     * Set defaultLeftColumn
     *
     * @param boolean $defaultLeftColumn
     *
     * @return PsTheme
     */
    public function setDefaultLeftColumn($defaultLeftColumn)
    {
        $this->defaultLeftColumn = $defaultLeftColumn;

        return $this;
    }

    /**
     * Get defaultLeftColumn
     *
     * @return boolean
     */
    public function getDefaultLeftColumn()
    {
        return $this->defaultLeftColumn;
    }

    /**
     * Set defaultRightColumn
     *
     * @param boolean $defaultRightColumn
     *
     * @return PsTheme
     */
    public function setDefaultRightColumn($defaultRightColumn)
    {
        $this->defaultRightColumn = $defaultRightColumn;

        return $this;
    }

    /**
     * Get defaultRightColumn
     *
     * @return boolean
     */
    public function getDefaultRightColumn()
    {
        return $this->defaultRightColumn;
    }

    /**
     * Set productPerPage
     *
     * @param integer $productPerPage
     *
     * @return PsTheme
     */
    public function setProductPerPage($productPerPage)
    {
        $this->productPerPage = $productPerPage;

        return $this;
    }

    /**
     * Get productPerPage
     *
     * @return integer
     */
    public function getProductPerPage()
    {
        return $this->productPerPage;
    }
}
