<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductCommentReport
 *
 * @ORM\Table(name="hd_product_comment_report")
 * @ORM\Entity
 */
class PsProductCommentReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_comment", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCustomer;



    /**
     * Set idProductComment
     *
     * @param integer $idProductComment
     *
     * @return PsProductCommentReport
     */
    public function setIdProductComment($idProductComment)
    {
        $this->idProductComment = $idProductComment;

        return $this;
    }

    /**
     * Get idProductComment
     *
     * @return integer
     */
    public function getIdProductComment()
    {
        return $this->idProductComment;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     *
     * @return PsProductCommentReport
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }
}
