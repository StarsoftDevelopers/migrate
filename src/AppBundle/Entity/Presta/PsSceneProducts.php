<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSceneProducts
 *
 * @ORM\Table(name="hd_scene_products")
 * @ORM\Entity
 */
class PsSceneProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_scene", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idScene;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="x_axis", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $xAxis;

    /**
     * @var integer
     *
     * @ORM\Column(name="y_axis", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $yAxis;

    /**
     * @var integer
     *
     * @ORM\Column(name="zone_width", type="integer", nullable=false)
     */
    private $zoneWidth;

    /**
     * @var integer
     *
     * @ORM\Column(name="zone_height", type="integer", nullable=false)
     */
    private $zoneHeight;



    /**
     * Set idScene
     *
     * @param integer $idScene
     *
     * @return PsSceneProducts
     */
    public function setIdScene($idScene)
    {
        $this->idScene = $idScene;

        return $this;
    }

    /**
     * Get idScene
     *
     * @return integer
     */
    public function getIdScene()
    {
        return $this->idScene;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsSceneProducts
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set xAxis
     *
     * @param integer $xAxis
     *
     * @return PsSceneProducts
     */
    public function setXAxis($xAxis)
    {
        $this->xAxis = $xAxis;

        return $this;
    }

    /**
     * Get xAxis
     *
     * @return integer
     */
    public function getXAxis()
    {
        return $this->xAxis;
    }

    /**
     * Set yAxis
     *
     * @param integer $yAxis
     *
     * @return PsSceneProducts
     */
    public function setYAxis($yAxis)
    {
        $this->yAxis = $yAxis;

        return $this;
    }

    /**
     * Get yAxis
     *
     * @return integer
     */
    public function getYAxis()
    {
        return $this->yAxis;
    }

    /**
     * Set zoneWidth
     *
     * @param integer $zoneWidth
     *
     * @return PsSceneProducts
     */
    public function setZoneWidth($zoneWidth)
    {
        $this->zoneWidth = $zoneWidth;

        return $this;
    }

    /**
     * Get zoneWidth
     *
     * @return integer
     */
    public function getZoneWidth()
    {
        return $this->zoneWidth;
    }

    /**
     * Set zoneHeight
     *
     * @param integer $zoneHeight
     *
     * @return PsSceneProducts
     */
    public function setZoneHeight($zoneHeight)
    {
        $this->zoneHeight = $zoneHeight;

        return $this;
    }

    /**
     * Get zoneHeight
     *
     * @return integer
     */
    public function getZoneHeight()
    {
        return $this->zoneHeight;
    }
}
