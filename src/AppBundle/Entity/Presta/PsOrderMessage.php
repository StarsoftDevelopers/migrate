<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderMessage
 *
 * @ORM\Table(name="hd_order_message")
 * @ORM\Entity
 */
class PsOrderMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_message", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idOrderMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;



    /**
     * Get idOrderMessage
     *
     * @return integer
     */
    public function getIdOrderMessage()
    {
        return $this->idOrderMessage;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsOrderMessage
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
}
