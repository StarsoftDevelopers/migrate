<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsHookAlias
 *
 * @ORM\Table(name="hd_hook_alias", uniqueConstraints={@ORM\UniqueConstraint(name="alias", columns={"alias"})})
 * @ORM\Entity
 */
class PsHookAlias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_hook_alias", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idHookAlias;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=64, nullable=false)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;



    /**
     * Get idHookAlias
     *
     * @return integer
     */
    public function getIdHookAlias()
    {
        return $this->idHookAlias;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return PsHookAlias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsHookAlias
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
