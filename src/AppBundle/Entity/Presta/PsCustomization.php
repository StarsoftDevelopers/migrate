<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCustomization
 *
 * @ORM\Table(name="hd_customization", indexes={@ORM\Index(name="id_product_attribute", columns={"id_product_attribute"}), @ORM\Index(name="id_cart_product", columns={"id_cart", "id_product", "id_product_attribute"})})
 * @ORM\Entity
 */
class PsCustomization
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_customization", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCustomization;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cart", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCart;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_address_delivery", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAddressDelivery = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_attribute", type="integer", nullable=false)
     */
    private $idProductAttribute = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity_refunded", type="integer", nullable=false)
     */
    private $quantityRefunded = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity_returned", type="integer", nullable=false)
     */
    private $quantityReturned = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="in_cart", type="boolean", nullable=false)
     */
    private $inCart = '0';



    /**
     * Set idCustomization
     *
     * @param integer $idCustomization
     *
     * @return PsCustomization
     */
    public function setIdCustomization($idCustomization)
    {
        $this->idCustomization = $idCustomization;

        return $this;
    }

    /**
     * Get idCustomization
     *
     * @return integer
     */
    public function getIdCustomization()
    {
        return $this->idCustomization;
    }

    /**
     * Set idCart
     *
     * @param integer $idCart
     *
     * @return PsCustomization
     */
    public function setIdCart($idCart)
    {
        $this->idCart = $idCart;

        return $this;
    }

    /**
     * Get idCart
     *
     * @return integer
     */
    public function getIdCart()
    {
        return $this->idCart;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsCustomization
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set idAddressDelivery
     *
     * @param integer $idAddressDelivery
     *
     * @return PsCustomization
     */
    public function setIdAddressDelivery($idAddressDelivery)
    {
        $this->idAddressDelivery = $idAddressDelivery;

        return $this;
    }

    /**
     * Get idAddressDelivery
     *
     * @return integer
     */
    public function getIdAddressDelivery()
    {
        return $this->idAddressDelivery;
    }

    /**
     * Set idProductAttribute
     *
     * @param integer $idProductAttribute
     *
     * @return PsCustomization
     */
    public function setIdProductAttribute($idProductAttribute)
    {
        $this->idProductAttribute = $idProductAttribute;

        return $this;
    }

    /**
     * Get idProductAttribute
     *
     * @return integer
     */
    public function getIdProductAttribute()
    {
        return $this->idProductAttribute;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PsCustomization
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set quantityRefunded
     *
     * @param integer $quantityRefunded
     *
     * @return PsCustomization
     */
    public function setQuantityRefunded($quantityRefunded)
    {
        $this->quantityRefunded = $quantityRefunded;

        return $this;
    }

    /**
     * Get quantityRefunded
     *
     * @return integer
     */
    public function getQuantityRefunded()
    {
        return $this->quantityRefunded;
    }

    /**
     * Set quantityReturned
     *
     * @param integer $quantityReturned
     *
     * @return PsCustomization
     */
    public function setQuantityReturned($quantityReturned)
    {
        $this->quantityReturned = $quantityReturned;

        return $this;
    }

    /**
     * Get quantityReturned
     *
     * @return integer
     */
    public function getQuantityReturned()
    {
        return $this->quantityReturned;
    }

    /**
     * Set inCart
     *
     * @param boolean $inCart
     *
     * @return PsCustomization
     */
    public function setInCart($inCart)
    {
        $this->inCart = $inCart;

        return $this;
    }

    /**
     * Get inCart
     *
     * @return boolean
     */
    public function getInCart()
    {
        return $this->inCart;
    }
}
