<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsWishlistProduct
 *
 * @ORM\Table(name="hd_wishlist_product")
 * @ORM\Entity
 */
class PsWishlistProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_wishlist_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idWishlistProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_wishlist", type="integer", nullable=false)
     */
    private $idWishlist;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_attribute", type="integer", nullable=false)
     */
    private $idProductAttribute;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority;



    /**
     * Get idWishlistProduct
     *
     * @return integer
     */
    public function getIdWishlistProduct()
    {
        return $this->idWishlistProduct;
    }

    /**
     * Set idWishlist
     *
     * @param integer $idWishlist
     *
     * @return PsWishlistProduct
     */
    public function setIdWishlist($idWishlist)
    {
        $this->idWishlist = $idWishlist;

        return $this;
    }

    /**
     * Get idWishlist
     *
     * @return integer
     */
    public function getIdWishlist()
    {
        return $this->idWishlist;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsWishlistProduct
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set idProductAttribute
     *
     * @param integer $idProductAttribute
     *
     * @return PsWishlistProduct
     */
    public function setIdProductAttribute($idProductAttribute)
    {
        $this->idProductAttribute = $idProductAttribute;

        return $this;
    }

    /**
     * Get idProductAttribute
     *
     * @return integer
     */
    public function getIdProductAttribute()
    {
        return $this->idProductAttribute;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PsWishlistProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return PsWishlistProduct
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
