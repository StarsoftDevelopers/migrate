<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCronjobs
 *
 * @ORM\Table(name="hd_cronjobs", indexes={@ORM\Index(name="id_module", columns={"id_module"})})
 * @ORM\Entity
 */
class PsCronjobs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cronjob", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCronjob;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_module", type="integer", nullable=true)
     */
    private $idModule;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="task", type="text", length=65535, nullable=true)
     */
    private $task;

    /**
     * @var integer
     *
     * @ORM\Column(name="hour", type="integer", nullable=true)
     */
    private $hour = '-1';

    /**
     * @var integer
     *
     * @ORM\Column(name="day", type="integer", nullable=true)
     */
    private $day = '-1';

    /**
     * @var integer
     *
     * @ORM\Column(name="month", type="integer", nullable=true)
     */
    private $month = '-1';

    /**
     * @var integer
     *
     * @ORM\Column(name="day_of_week", type="integer", nullable=true)
     */
    private $dayOfWeek = '-1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="one_shot", type="boolean", nullable=false)
     */
    private $oneShot = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop", type="integer", nullable=true)
     */
    private $idShop = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop_group", type="integer", nullable=true)
     */
    private $idShopGroup = '0';



    /**
     * Get idCronjob
     *
     * @return integer
     */
    public function getIdCronjob()
    {
        return $this->idCronjob;
    }

    /**
     * Set idModule
     *
     * @param integer $idModule
     *
     * @return PsCronjobs
     */
    public function setIdModule($idModule)
    {
        $this->idModule = $idModule;

        return $this;
    }

    /**
     * Get idModule
     *
     * @return integer
     */
    public function getIdModule()
    {
        return $this->idModule;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PsCronjobs
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set task
     *
     * @param string $task
     *
     * @return PsCronjobs
     */
    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set hour
     *
     * @param integer $hour
     *
     * @return PsCronjobs
     */
    public function setHour($hour)
    {
        $this->hour = $hour;

        return $this;
    }

    /**
     * Get hour
     *
     * @return integer
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * Set day
     *
     * @param integer $day
     *
     * @return PsCronjobs
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return integer
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return PsCronjobs
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set dayOfWeek
     *
     * @param integer $dayOfWeek
     *
     * @return PsCronjobs
     */
    public function setDayOfWeek($dayOfWeek)
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    /**
     * Get dayOfWeek
     *
     * @return integer
     */
    public function getDayOfWeek()
    {
        return $this->dayOfWeek;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return PsCronjobs
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set oneShot
     *
     * @param boolean $oneShot
     *
     * @return PsCronjobs
     */
    public function setOneShot($oneShot)
    {
        $this->oneShot = $oneShot;

        return $this;
    }

    /**
     * Get oneShot
     *
     * @return boolean
     */
    public function getOneShot()
    {
        return $this->oneShot;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsCronjobs
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set idShop
     *
     * @param integer $idShop
     *
     * @return PsCronjobs
     */
    public function setIdShop($idShop)
    {
        $this->idShop = $idShop;

        return $this;
    }

    /**
     * Get idShop
     *
     * @return integer
     */
    public function getIdShop()
    {
        return $this->idShop;
    }

    /**
     * Set idShopGroup
     *
     * @param integer $idShopGroup
     *
     * @return PsCronjobs
     */
    public function setIdShopGroup($idShopGroup)
    {
        $this->idShopGroup = $idShopGroup;

        return $this;
    }

    /**
     * Get idShopGroup
     *
     * @return integer
     */
    public function getIdShopGroup()
    {
        return $this->idShopGroup;
    }
}
