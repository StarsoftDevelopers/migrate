<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsScene
 *
 * @ORM\Table(name="hd_scene")
 * @ORM\Entity
 */
class PsScene
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_scene", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idScene;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';



    /**
     * Get idScene
     *
     * @return integer
     */
    public function getIdScene()
    {
        return $this->idScene;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsScene
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
