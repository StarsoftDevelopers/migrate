<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSpecificPriceRuleConditionGroup
 *
 * @ORM\Table(name="hd_specific_price_rule_condition_group")
 * @ORM\Entity
 */
class PsSpecificPriceRuleConditionGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_specific_price_rule_condition_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idSpecificPriceRuleConditionGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_specific_price_rule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idSpecificPriceRule;



    /**
     * Set idSpecificPriceRuleConditionGroup
     *
     * @param integer $idSpecificPriceRuleConditionGroup
     *
     * @return PsSpecificPriceRuleConditionGroup
     */
    public function setIdSpecificPriceRuleConditionGroup($idSpecificPriceRuleConditionGroup)
    {
        $this->idSpecificPriceRuleConditionGroup = $idSpecificPriceRuleConditionGroup;

        return $this;
    }

    /**
     * Get idSpecificPriceRuleConditionGroup
     *
     * @return integer
     */
    public function getIdSpecificPriceRuleConditionGroup()
    {
        return $this->idSpecificPriceRuleConditionGroup;
    }

    /**
     * Set idSpecificPriceRule
     *
     * @param integer $idSpecificPriceRule
     *
     * @return PsSpecificPriceRuleConditionGroup
     */
    public function setIdSpecificPriceRule($idSpecificPriceRule)
    {
        $this->idSpecificPriceRule = $idSpecificPriceRule;

        return $this;
    }

    /**
     * Get idSpecificPriceRule
     *
     * @return integer
     */
    public function getIdSpecificPriceRule()
    {
        return $this->idSpecificPriceRule;
    }
}
