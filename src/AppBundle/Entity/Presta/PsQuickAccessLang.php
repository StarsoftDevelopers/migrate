<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsQuickAccessLang
 *
 * @ORM\Table(name="hd_quick_access_lang")
 * @ORM\Entity
 */
class PsQuickAccessLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_quick_access", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idQuickAccess;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     */
    private $name;



    /**
     * Set idQuickAccess
     *
     * @param integer $idQuickAccess
     *
     * @return PsQuickAccessLang
     */
    public function setIdQuickAccess($idQuickAccess)
    {
        $this->idQuickAccess = $idQuickAccess;

        return $this;
    }

    /**
     * Get idQuickAccess
     *
     * @return integer
     */
    public function getIdQuickAccess()
    {
        return $this->idQuickAccess;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsQuickAccessLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsQuickAccessLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
