<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderReturnStateLang
 *
 * @ORM\Table(name="hd_order_return_state_lang")
 * @ORM\Entity
 */
class PsOrderReturnStateLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_return_state", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idOrderReturnState;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;



    /**
     * Set idOrderReturnState
     *
     * @param integer $idOrderReturnState
     *
     * @return PsOrderReturnStateLang
     */
    public function setIdOrderReturnState($idOrderReturnState)
    {
        $this->idOrderReturnState = $idOrderReturnState;

        return $this;
    }

    /**
     * Get idOrderReturnState
     *
     * @return integer
     */
    public function getIdOrderReturnState()
    {
        return $this->idOrderReturnState;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsOrderReturnStateLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsOrderReturnStateLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
