<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductCommentCriterionCategory
 *
 * @ORM\Table(name="hd_product_comment_criterion_category", indexes={@ORM\Index(name="id_category", columns={"id_category"})})
 * @ORM\Entity
 */
class PsProductCommentCriterionCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_comment_criterion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductCommentCriterion;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_category", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCategory;



    /**
     * Set idProductCommentCriterion
     *
     * @param integer $idProductCommentCriterion
     *
     * @return PsProductCommentCriterionCategory
     */
    public function setIdProductCommentCriterion($idProductCommentCriterion)
    {
        $this->idProductCommentCriterion = $idProductCommentCriterion;

        return $this;
    }

    /**
     * Get idProductCommentCriterion
     *
     * @return integer
     */
    public function getIdProductCommentCriterion()
    {
        return $this->idProductCommentCriterion;
    }

    /**
     * Set idCategory
     *
     * @param integer $idCategory
     *
     * @return PsProductCommentCriterionCategory
     */
    public function setIdCategory($idCategory)
    {
        $this->idCategory = $idCategory;

        return $this;
    }

    /**
     * Get idCategory
     *
     * @return integer
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }
}
