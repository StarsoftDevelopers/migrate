<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCartRuleProductRuleValue
 *
 * @ORM\Table(name="hd_cart_rule_product_rule_value")
 * @ORM\Entity
 */
class PsCartRuleProductRuleValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_rule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductRule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idItem;



    /**
     * Set idProductRule
     *
     * @param integer $idProductRule
     *
     * @return PsCartRuleProductRuleValue
     */
    public function setIdProductRule($idProductRule)
    {
        $this->idProductRule = $idProductRule;

        return $this;
    }

    /**
     * Get idProductRule
     *
     * @return integer
     */
    public function getIdProductRule()
    {
        return $this->idProductRule;
    }

    /**
     * Set idItem
     *
     * @param integer $idItem
     *
     * @return PsCartRuleProductRuleValue
     */
    public function setIdItem($idItem)
    {
        $this->idItem = $idItem;

        return $this;
    }

    /**
     * Get idItem
     *
     * @return integer
     */
    public function getIdItem()
    {
        return $this->idItem;
    }
}
