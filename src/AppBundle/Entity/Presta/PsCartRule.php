<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCartRule
 *
 * @ORM\Table(name="hd_cart_rule", indexes={@ORM\Index(name="id_customer", columns={"id_customer", "active", "date_to"}), @ORM\Index(name="group_restriction", columns={"group_restriction", "active", "date_to"}), @ORM\Index(name="id_customer_2", columns={"id_customer", "active", "highlight", "date_to"}), @ORM\Index(name="group_restriction_2", columns={"group_restriction", "active", "highlight", "date_to"})})
 * @ORM\Entity
 */
class PsCartRule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cart_rule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCartRule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_customer", type="integer", nullable=false)
     */
    private $idCustomer = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_from", type="datetime", nullable=false)
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to", type="datetime", nullable=false)
     */
    private $dateTo;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity_per_user", type="integer", nullable=false)
     */
    private $quantityPerUser = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="partial_use", type="boolean", nullable=false)
     */
    private $partialUse = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=254, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="minimum_amount", type="decimal", precision=17, scale=2, nullable=false)
     */
    private $minimumAmount = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="minimum_amount_tax", type="boolean", nullable=false)
     */
    private $minimumAmountTax = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="minimum_amount_currency", type="integer", nullable=false)
     */
    private $minimumAmountCurrency = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="minimum_amount_shipping", type="boolean", nullable=false)
     */
    private $minimumAmountShipping = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="country_restriction", type="boolean", nullable=false)
     */
    private $countryRestriction = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="carrier_restriction", type="boolean", nullable=false)
     */
    private $carrierRestriction = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="group_restriction", type="boolean", nullable=false)
     */
    private $groupRestriction = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="cart_rule_restriction", type="boolean", nullable=false)
     */
    private $cartRuleRestriction = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="product_restriction", type="boolean", nullable=false)
     */
    private $productRestriction = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="shop_restriction", type="boolean", nullable=false)
     */
    private $shopRestriction = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="free_shipping", type="boolean", nullable=false)
     */
    private $freeShipping = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="reduction_percent", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $reductionPercent = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="reduction_amount", type="decimal", precision=17, scale=2, nullable=false)
     */
    private $reductionAmount = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="reduction_tax", type="boolean", nullable=false)
     */
    private $reductionTax = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="reduction_currency", type="integer", nullable=false)
     */
    private $reductionCurrency = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="reduction_product", type="integer", nullable=false)
     */
    private $reductionProduct = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="gift_product", type="integer", nullable=false)
     */
    private $giftProduct = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="gift_product_attribute", type="integer", nullable=false)
     */
    private $giftProductAttribute = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="highlight", type="boolean", nullable=false)
     */
    private $highlight = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false)
     */
    private $dateUpd;



    /**
     * Get idCartRule
     *
     * @return integer
     */
    public function getIdCartRule()
    {
        return $this->idCartRule;
    }

    /**
     * Set idCustomer
     *
     * @param integer $idCustomer
     *
     * @return PsCartRule
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     *
     * @return PsCartRule
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return PsCartRule
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PsCartRule
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PsCartRule
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set quantityPerUser
     *
     * @param integer $quantityPerUser
     *
     * @return PsCartRule
     */
    public function setQuantityPerUser($quantityPerUser)
    {
        $this->quantityPerUser = $quantityPerUser;

        return $this;
    }

    /**
     * Get quantityPerUser
     *
     * @return integer
     */
    public function getQuantityPerUser()
    {
        return $this->quantityPerUser;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return PsCartRule
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set partialUse
     *
     * @param boolean $partialUse
     *
     * @return PsCartRule
     */
    public function setPartialUse($partialUse)
    {
        $this->partialUse = $partialUse;

        return $this;
    }

    /**
     * Get partialUse
     *
     * @return boolean
     */
    public function getPartialUse()
    {
        return $this->partialUse;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return PsCartRule
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set minimumAmount
     *
     * @param string $minimumAmount
     *
     * @return PsCartRule
     */
    public function setMinimumAmount($minimumAmount)
    {
        $this->minimumAmount = $minimumAmount;

        return $this;
    }

    /**
     * Get minimumAmount
     *
     * @return string
     */
    public function getMinimumAmount()
    {
        return $this->minimumAmount;
    }

    /**
     * Set minimumAmountTax
     *
     * @param boolean $minimumAmountTax
     *
     * @return PsCartRule
     */
    public function setMinimumAmountTax($minimumAmountTax)
    {
        $this->minimumAmountTax = $minimumAmountTax;

        return $this;
    }

    /**
     * Get minimumAmountTax
     *
     * @return boolean
     */
    public function getMinimumAmountTax()
    {
        return $this->minimumAmountTax;
    }

    /**
     * Set minimumAmountCurrency
     *
     * @param integer $minimumAmountCurrency
     *
     * @return PsCartRule
     */
    public function setMinimumAmountCurrency($minimumAmountCurrency)
    {
        $this->minimumAmountCurrency = $minimumAmountCurrency;

        return $this;
    }

    /**
     * Get minimumAmountCurrency
     *
     * @return integer
     */
    public function getMinimumAmountCurrency()
    {
        return $this->minimumAmountCurrency;
    }

    /**
     * Set minimumAmountShipping
     *
     * @param boolean $minimumAmountShipping
     *
     * @return PsCartRule
     */
    public function setMinimumAmountShipping($minimumAmountShipping)
    {
        $this->minimumAmountShipping = $minimumAmountShipping;

        return $this;
    }

    /**
     * Get minimumAmountShipping
     *
     * @return boolean
     */
    public function getMinimumAmountShipping()
    {
        return $this->minimumAmountShipping;
    }

    /**
     * Set countryRestriction
     *
     * @param boolean $countryRestriction
     *
     * @return PsCartRule
     */
    public function setCountryRestriction($countryRestriction)
    {
        $this->countryRestriction = $countryRestriction;

        return $this;
    }

    /**
     * Get countryRestriction
     *
     * @return boolean
     */
    public function getCountryRestriction()
    {
        return $this->countryRestriction;
    }

    /**
     * Set carrierRestriction
     *
     * @param boolean $carrierRestriction
     *
     * @return PsCartRule
     */
    public function setCarrierRestriction($carrierRestriction)
    {
        $this->carrierRestriction = $carrierRestriction;

        return $this;
    }

    /**
     * Get carrierRestriction
     *
     * @return boolean
     */
    public function getCarrierRestriction()
    {
        return $this->carrierRestriction;
    }

    /**
     * Set groupRestriction
     *
     * @param boolean $groupRestriction
     *
     * @return PsCartRule
     */
    public function setGroupRestriction($groupRestriction)
    {
        $this->groupRestriction = $groupRestriction;

        return $this;
    }

    /**
     * Get groupRestriction
     *
     * @return boolean
     */
    public function getGroupRestriction()
    {
        return $this->groupRestriction;
    }

    /**
     * Set cartRuleRestriction
     *
     * @param boolean $cartRuleRestriction
     *
     * @return PsCartRule
     */
    public function setCartRuleRestriction($cartRuleRestriction)
    {
        $this->cartRuleRestriction = $cartRuleRestriction;

        return $this;
    }

    /**
     * Get cartRuleRestriction
     *
     * @return boolean
     */
    public function getCartRuleRestriction()
    {
        return $this->cartRuleRestriction;
    }

    /**
     * Set productRestriction
     *
     * @param boolean $productRestriction
     *
     * @return PsCartRule
     */
    public function setProductRestriction($productRestriction)
    {
        $this->productRestriction = $productRestriction;

        return $this;
    }

    /**
     * Get productRestriction
     *
     * @return boolean
     */
    public function getProductRestriction()
    {
        return $this->productRestriction;
    }

    /**
     * Set shopRestriction
     *
     * @param boolean $shopRestriction
     *
     * @return PsCartRule
     */
    public function setShopRestriction($shopRestriction)
    {
        $this->shopRestriction = $shopRestriction;

        return $this;
    }

    /**
     * Get shopRestriction
     *
     * @return boolean
     */
    public function getShopRestriction()
    {
        return $this->shopRestriction;
    }

    /**
     * Set freeShipping
     *
     * @param boolean $freeShipping
     *
     * @return PsCartRule
     */
    public function setFreeShipping($freeShipping)
    {
        $this->freeShipping = $freeShipping;

        return $this;
    }

    /**
     * Get freeShipping
     *
     * @return boolean
     */
    public function getFreeShipping()
    {
        return $this->freeShipping;
    }

    /**
     * Set reductionPercent
     *
     * @param string $reductionPercent
     *
     * @return PsCartRule
     */
    public function setReductionPercent($reductionPercent)
    {
        $this->reductionPercent = $reductionPercent;

        return $this;
    }

    /**
     * Get reductionPercent
     *
     * @return string
     */
    public function getReductionPercent()
    {
        return $this->reductionPercent;
    }

    /**
     * Set reductionAmount
     *
     * @param string $reductionAmount
     *
     * @return PsCartRule
     */
    public function setReductionAmount($reductionAmount)
    {
        $this->reductionAmount = $reductionAmount;

        return $this;
    }

    /**
     * Get reductionAmount
     *
     * @return string
     */
    public function getReductionAmount()
    {
        return $this->reductionAmount;
    }

    /**
     * Set reductionTax
     *
     * @param boolean $reductionTax
     *
     * @return PsCartRule
     */
    public function setReductionTax($reductionTax)
    {
        $this->reductionTax = $reductionTax;

        return $this;
    }

    /**
     * Get reductionTax
     *
     * @return boolean
     */
    public function getReductionTax()
    {
        return $this->reductionTax;
    }

    /**
     * Set reductionCurrency
     *
     * @param integer $reductionCurrency
     *
     * @return PsCartRule
     */
    public function setReductionCurrency($reductionCurrency)
    {
        $this->reductionCurrency = $reductionCurrency;

        return $this;
    }

    /**
     * Get reductionCurrency
     *
     * @return integer
     */
    public function getReductionCurrency()
    {
        return $this->reductionCurrency;
    }

    /**
     * Set reductionProduct
     *
     * @param integer $reductionProduct
     *
     * @return PsCartRule
     */
    public function setReductionProduct($reductionProduct)
    {
        $this->reductionProduct = $reductionProduct;

        return $this;
    }

    /**
     * Get reductionProduct
     *
     * @return integer
     */
    public function getReductionProduct()
    {
        return $this->reductionProduct;
    }

    /**
     * Set giftProduct
     *
     * @param integer $giftProduct
     *
     * @return PsCartRule
     */
    public function setGiftProduct($giftProduct)
    {
        $this->giftProduct = $giftProduct;

        return $this;
    }

    /**
     * Get giftProduct
     *
     * @return integer
     */
    public function getGiftProduct()
    {
        return $this->giftProduct;
    }

    /**
     * Set giftProductAttribute
     *
     * @param integer $giftProductAttribute
     *
     * @return PsCartRule
     */
    public function setGiftProductAttribute($giftProductAttribute)
    {
        $this->giftProductAttribute = $giftProductAttribute;

        return $this;
    }

    /**
     * Get giftProductAttribute
     *
     * @return integer
     */
    public function getGiftProductAttribute()
    {
        return $this->giftProductAttribute;
    }

    /**
     * Set highlight
     *
     * @param boolean $highlight
     *
     * @return PsCartRule
     */
    public function setHighlight($highlight)
    {
        $this->highlight = $highlight;

        return $this;
    }

    /**
     * Get highlight
     *
     * @return boolean
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsCartRule
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsCartRule
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsCartRule
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }
}
