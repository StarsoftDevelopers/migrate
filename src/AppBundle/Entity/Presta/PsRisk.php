<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsRisk
 *
 * @ORM\Table(name="hd_risk")
 * @ORM\Entity
 */
class PsRisk
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_risk", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRisk;

    /**
     * @var boolean
     *
     * @ORM\Column(name="percent", type="boolean", nullable=false)
     */
    private $percent;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=32, nullable=true)
     */
    private $color;



    /**
     * Get idRisk
     *
     * @return integer
     */
    public function getIdRisk()
    {
        return $this->idRisk;
    }

    /**
     * Set percent
     *
     * @param boolean $percent
     *
     * @return PsRisk
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return boolean
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return PsRisk
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
