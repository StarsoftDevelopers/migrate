<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsShopGroup
 *
 * @ORM\Table(name="hd_shop_group", indexes={@ORM\Index(name="deleted", columns={"deleted", "name"})})
 * @ORM\Entity
 */
class PsShopGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idShopGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="share_customer", type="boolean", nullable=false)
     */
    private $shareCustomer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="share_order", type="boolean", nullable=false)
     */
    private $shareOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="share_stock", type="boolean", nullable=false)
     */
    private $shareStock;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';



    /**
     * Get idShopGroup
     *
     * @return integer
     */
    public function getIdShopGroup()
    {
        return $this->idShopGroup;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsShopGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shareCustomer
     *
     * @param boolean $shareCustomer
     *
     * @return PsShopGroup
     */
    public function setShareCustomer($shareCustomer)
    {
        $this->shareCustomer = $shareCustomer;

        return $this;
    }

    /**
     * Get shareCustomer
     *
     * @return boolean
     */
    public function getShareCustomer()
    {
        return $this->shareCustomer;
    }

    /**
     * Set shareOrder
     *
     * @param boolean $shareOrder
     *
     * @return PsShopGroup
     */
    public function setShareOrder($shareOrder)
    {
        $this->shareOrder = $shareOrder;

        return $this;
    }

    /**
     * Get shareOrder
     *
     * @return boolean
     */
    public function getShareOrder()
    {
        return $this->shareOrder;
    }

    /**
     * Set shareStock
     *
     * @param boolean $shareStock
     *
     * @return PsShopGroup
     */
    public function setShareStock($shareStock)
    {
        $this->shareStock = $shareStock;

        return $this;
    }

    /**
     * Get shareStock
     *
     * @return boolean
     */
    public function getShareStock()
    {
        return $this->shareStock;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsShopGroup
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return PsShopGroup
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
