<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsMeta
 *
 * @ORM\Table(name="hd_meta", uniqueConstraints={@ORM\UniqueConstraint(name="page", columns={"page"})})
 * @ORM\Entity
 */
class PsMeta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_meta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMeta;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=64, nullable=false)
     */
    private $page;

    /**
     * @var boolean
     *
     * @ORM\Column(name="configurable", type="boolean", nullable=false)
     */
    private $configurable = '1';



    /**
     * Get idMeta
     *
     * @return integer
     */
    public function getIdMeta()
    {
        return $this->idMeta;
    }

    /**
     * Set page
     *
     * @param string $page
     *
     * @return PsMeta
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set configurable
     *
     * @param boolean $configurable
     *
     * @return PsMeta
     */
    public function setConfigurable($configurable)
    {
        $this->configurable = $configurable;

        return $this;
    }

    /**
     * Get configurable
     *
     * @return boolean
     */
    public function getConfigurable()
    {
        return $this->configurable;
    }
}
