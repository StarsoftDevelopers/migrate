<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderCartRule
 *
 * @ORM\Table(name="hd_order_cart_rule", indexes={@ORM\Index(name="id_order", columns={"id_order"}), @ORM\Index(name="id_cart_rule", columns={"id_cart_rule"})})
 * @ORM\Entity
 */
class PsOrderCartRule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_cart_rule", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idOrderCartRule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_order", type="integer", nullable=false)
     */
    private $idOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cart_rule", type="integer", nullable=false)
     */
    private $idCartRule;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_invoice", type="integer", nullable=true)
     */
    private $idOrderInvoice = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=254, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", precision=17, scale=2, nullable=false)
     */
    private $value = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="value_tax_excl", type="decimal", precision=17, scale=2, nullable=false)
     */
    private $valueTaxExcl = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="free_shipping", type="boolean", nullable=false)
     */
    private $freeShipping = '0';



    /**
     * Get idOrderCartRule
     *
     * @return integer
     */
    public function getIdOrderCartRule()
    {
        return $this->idOrderCartRule;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     *
     * @return PsOrderCartRule
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;

        return $this;
    }

    /**
     * Get idOrder
     *
     * @return integer
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }

    /**
     * Set idCartRule
     *
     * @param integer $idCartRule
     *
     * @return PsOrderCartRule
     */
    public function setIdCartRule($idCartRule)
    {
        $this->idCartRule = $idCartRule;

        return $this;
    }

    /**
     * Get idCartRule
     *
     * @return integer
     */
    public function getIdCartRule()
    {
        return $this->idCartRule;
    }

    /**
     * Set idOrderInvoice
     *
     * @param integer $idOrderInvoice
     *
     * @return PsOrderCartRule
     */
    public function setIdOrderInvoice($idOrderInvoice)
    {
        $this->idOrderInvoice = $idOrderInvoice;

        return $this;
    }

    /**
     * Get idOrderInvoice
     *
     * @return integer
     */
    public function getIdOrderInvoice()
    {
        return $this->idOrderInvoice;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsOrderCartRule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return PsOrderCartRule
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set valueTaxExcl
     *
     * @param string $valueTaxExcl
     *
     * @return PsOrderCartRule
     */
    public function setValueTaxExcl($valueTaxExcl)
    {
        $this->valueTaxExcl = $valueTaxExcl;

        return $this;
    }

    /**
     * Get valueTaxExcl
     *
     * @return string
     */
    public function getValueTaxExcl()
    {
        return $this->valueTaxExcl;
    }

    /**
     * Set freeShipping
     *
     * @param boolean $freeShipping
     *
     * @return PsOrderCartRule
     */
    public function setFreeShipping($freeShipping)
    {
        $this->freeShipping = $freeShipping;

        return $this;
    }

    /**
     * Get freeShipping
     *
     * @return boolean
     */
    public function getFreeShipping()
    {
        return $this->freeShipping;
    }
}
