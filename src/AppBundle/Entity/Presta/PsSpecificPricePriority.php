<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSpecificPricePriority
 *
 * @ORM\Table(name="hd_specific_price_priority", uniqueConstraints={@ORM\UniqueConstraint(name="id_product", columns={"id_product"})})
 * @ORM\Entity
 */
class PsSpecificPricePriority
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_specific_price_priority", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idSpecificPricePriority;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="priority", type="string", length=80, nullable=false)
     */
    private $priority;



    /**
     * Set idSpecificPricePriority
     *
     * @param integer $idSpecificPricePriority
     *
     * @return PsSpecificPricePriority
     */
    public function setIdSpecificPricePriority($idSpecificPricePriority)
    {
        $this->idSpecificPricePriority = $idSpecificPricePriority;

        return $this;
    }

    /**
     * Get idSpecificPricePriority
     *
     * @return integer
     */
    public function getIdSpecificPricePriority()
    {
        return $this->idSpecificPricePriority;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsSpecificPricePriority
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set priority
     *
     * @param string $priority
     *
     * @return PsSpecificPricePriority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return string
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
