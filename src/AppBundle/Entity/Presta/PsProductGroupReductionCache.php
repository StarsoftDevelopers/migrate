<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductGroupReductionCache
 *
 * @ORM\Table(name="hd_product_group_reduction_cache")
 * @ORM\Entity
 */
class PsProductGroupReductionCache
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="reduction", type="decimal", precision=4, scale=3, nullable=false)
     */
    private $reduction;



    /**
     * Set idProduct
     *
     * @param integer $idProduct
     *
     * @return PsProductGroupReductionCache
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set idGroup
     *
     * @param integer $idGroup
     *
     * @return PsProductGroupReductionCache
     */
    public function setIdGroup($idGroup)
    {
        $this->idGroup = $idGroup;

        return $this;
    }

    /**
     * Get idGroup
     *
     * @return integer
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set reduction
     *
     * @param string $reduction
     *
     * @return PsProductGroupReductionCache
     */
    public function setReduction($reduction)
    {
        $this->reduction = $reduction;

        return $this;
    }

    /**
     * Get reduction
     *
     * @return string
     */
    public function getReduction()
    {
        return $this->reduction;
    }
}
