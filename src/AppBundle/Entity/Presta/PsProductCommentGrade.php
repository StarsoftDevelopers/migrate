<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductCommentGrade
 *
 * @ORM\Table(name="hd_product_comment_grade", indexes={@ORM\Index(name="id_product_comment_criterion", columns={"id_product_comment_criterion"})})
 * @ORM\Entity
 */
class PsProductCommentGrade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_comment", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_comment_criterion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductCommentCriterion;

    /**
     * @var integer
     *
     * @ORM\Column(name="grade", type="integer", nullable=false)
     */
    private $grade;



    /**
     * Set idProductComment
     *
     * @param integer $idProductComment
     *
     * @return PsProductCommentGrade
     */
    public function setIdProductComment($idProductComment)
    {
        $this->idProductComment = $idProductComment;

        return $this;
    }

    /**
     * Get idProductComment
     *
     * @return integer
     */
    public function getIdProductComment()
    {
        return $this->idProductComment;
    }

    /**
     * Set idProductCommentCriterion
     *
     * @param integer $idProductCommentCriterion
     *
     * @return PsProductCommentGrade
     */
    public function setIdProductCommentCriterion($idProductCommentCriterion)
    {
        $this->idProductCommentCriterion = $idProductCommentCriterion;

        return $this;
    }

    /**
     * Get idProductCommentCriterion
     *
     * @return integer
     */
    public function getIdProductCommentCriterion()
    {
        return $this->idProductCommentCriterion;
    }

    /**
     * Set grade
     *
     * @param integer $grade
     *
     * @return PsProductCommentGrade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer
     */
    public function getGrade()
    {
        return $this->grade;
    }
}
