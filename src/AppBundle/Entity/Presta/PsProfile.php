<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProfile
 *
 * @ORM\Table(name="hd_profile")
 * @ORM\Entity
 */
class PsProfile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_profile", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProfile;



    /**
     * Get idProfile
     *
     * @return integer
     */
    public function getIdProfile()
    {
        return $this->idProfile;
    }
}
