<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsGroup
 *
 * @ORM\Table(name="hd_group")
 * @ORM\Entity
 */
class PsGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="reduction", type="decimal", precision=17, scale=2, nullable=false)
     */
    private $reduction = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="price_display_method", type="boolean", nullable=false)
     */
    private $priceDisplayMethod = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_prices", type="boolean", nullable=false)
     */
    private $showPrices = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false)
     */
    private $dateUpd;



    /**
     * Get idGroup
     *
     * @return integer
     */
    public function getIdGroup()
    {
        return $this->idGroup;
    }

    /**
     * Set reduction
     *
     * @param string $reduction
     *
     * @return PsGroup
     */
    public function setReduction($reduction)
    {
        $this->reduction = $reduction;

        return $this;
    }

    /**
     * Get reduction
     *
     * @return string
     */
    public function getReduction()
    {
        return $this->reduction;
    }

    /**
     * Set priceDisplayMethod
     *
     * @param boolean $priceDisplayMethod
     *
     * @return PsGroup
     */
    public function setPriceDisplayMethod($priceDisplayMethod)
    {
        $this->priceDisplayMethod = $priceDisplayMethod;

        return $this;
    }

    /**
     * Get priceDisplayMethod
     *
     * @return boolean
     */
    public function getPriceDisplayMethod()
    {
        return $this->priceDisplayMethod;
    }

    /**
     * Set showPrices
     *
     * @param boolean $showPrices
     *
     * @return PsGroup
     */
    public function setShowPrices($showPrices)
    {
        $this->showPrices = $showPrices;

        return $this;
    }

    /**
     * Get showPrices
     *
     * @return boolean
     */
    public function getShowPrices()
    {
        return $this->showPrices;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsGroup
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsGroup
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }
}
