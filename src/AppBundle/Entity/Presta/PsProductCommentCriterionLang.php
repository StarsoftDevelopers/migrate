<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsProductCommentCriterionLang
 *
 * @ORM\Table(name="hd_product_comment_criterion_lang")
 * @ORM\Entity
 */
class PsProductCommentCriterionLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product_comment_criterion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProductCommentCriterion;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;



    /**
     * Set idProductCommentCriterion
     *
     * @param integer $idProductCommentCriterion
     *
     * @return PsProductCommentCriterionLang
     */
    public function setIdProductCommentCriterion($idProductCommentCriterion)
    {
        $this->idProductCommentCriterion = $idProductCommentCriterion;

        return $this;
    }

    /**
     * Get idProductCommentCriterion
     *
     * @return integer
     */
    public function getIdProductCommentCriterion()
    {
        return $this->idProductCommentCriterion;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsProductCommentCriterionLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsProductCommentCriterionLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
