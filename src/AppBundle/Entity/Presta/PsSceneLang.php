<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSceneLang
 *
 * @ORM\Table(name="hd_scene_lang")
 * @ORM\Entity
 */
class PsSceneLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_scene", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idScene;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;



    /**
     * Set idScene
     *
     * @param integer $idScene
     *
     * @return PsSceneLang
     */
    public function setIdScene($idScene)
    {
        $this->idScene = $idScene;

        return $this;
    }

    /**
     * Get idScene
     *
     * @return integer
     */
    public function getIdScene()
    {
        return $this->idScene;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsSceneLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsSceneLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
