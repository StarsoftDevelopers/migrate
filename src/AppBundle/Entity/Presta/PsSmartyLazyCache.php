<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsSmartyLazyCache
 *
 * @ORM\Table(name="hd_smarty_lazy_cache")
 * @ORM\Entity
 */
class PsSmartyLazyCache
{
    /**
     * @var string
     *
     * @ORM\Column(name="template_hash", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $templateHash = '';

    /**
     * @var string
     *
     * @ORM\Column(name="cache_id", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $cacheId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="compile_id", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $compileId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="filepath", type="string", length=255, nullable=false)
     */
    private $filepath = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=false)
     */
    private $lastUpdate = '0000-00-00 00:00:00';



    /**
     * Set templateHash
     *
     * @param string $templateHash
     *
     * @return PsSmartyLazyCache
     */
    public function setTemplateHash($templateHash)
    {
        $this->templateHash = $templateHash;

        return $this;
    }

    /**
     * Get templateHash
     *
     * @return string
     */
    public function getTemplateHash()
    {
        return $this->templateHash;
    }

    /**
     * Set cacheId
     *
     * @param string $cacheId
     *
     * @return PsSmartyLazyCache
     */
    public function setCacheId($cacheId)
    {
        $this->cacheId = $cacheId;

        return $this;
    }

    /**
     * Get cacheId
     *
     * @return string
     */
    public function getCacheId()
    {
        return $this->cacheId;
    }

    /**
     * Set compileId
     *
     * @param string $compileId
     *
     * @return PsSmartyLazyCache
     */
    public function setCompileId($compileId)
    {
        $this->compileId = $compileId;

        return $this;
    }

    /**
     * Get compileId
     *
     * @return string
     */
    public function getCompileId()
    {
        return $this->compileId;
    }

    /**
     * Set filepath
     *
     * @param string $filepath
     *
     * @return PsSmartyLazyCache
     */
    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;

        return $this;
    }

    /**
     * Get filepath
     *
     * @return string
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return PsSmartyLazyCache
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }
}
