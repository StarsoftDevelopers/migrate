<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOperatingSystem
 *
 * @ORM\Table(name="hd_operating_system")
 * @ORM\Entity
 */
class PsOperatingSystem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_operating_system", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idOperatingSystem;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;



    /**
     * Get idOperatingSystem
     *
     * @return integer
     */
    public function getIdOperatingSystem()
    {
        return $this->idOperatingSystem;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsOperatingSystem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
