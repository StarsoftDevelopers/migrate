<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsThemeMeta
 *
 * @ORM\Table(name="hd_theme_meta", uniqueConstraints={@ORM\UniqueConstraint(name="id_theme_2", columns={"id_theme", "id_meta"})}, indexes={@ORM\Index(name="id_theme", columns={"id_theme"}), @ORM\Index(name="id_meta", columns={"id_meta"})})
 * @ORM\Entity
 */
class PsThemeMeta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_theme_meta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idThemeMeta;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_theme", type="integer", nullable=false)
     */
    private $idTheme;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_meta", type="integer", nullable=false)
     */
    private $idMeta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="left_column", type="boolean", nullable=false)
     */
    private $leftColumn = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="right_column", type="boolean", nullable=false)
     */
    private $rightColumn = '1';



    /**
     * Get idThemeMeta
     *
     * @return integer
     */
    public function getIdThemeMeta()
    {
        return $this->idThemeMeta;
    }

    /**
     * Set idTheme
     *
     * @param integer $idTheme
     *
     * @return PsThemeMeta
     */
    public function setIdTheme($idTheme)
    {
        $this->idTheme = $idTheme;

        return $this;
    }

    /**
     * Get idTheme
     *
     * @return integer
     */
    public function getIdTheme()
    {
        return $this->idTheme;
    }

    /**
     * Set idMeta
     *
     * @param integer $idMeta
     *
     * @return PsThemeMeta
     */
    public function setIdMeta($idMeta)
    {
        $this->idMeta = $idMeta;

        return $this;
    }

    /**
     * Get idMeta
     *
     * @return integer
     */
    public function getIdMeta()
    {
        return $this->idMeta;
    }

    /**
     * Set leftColumn
     *
     * @param boolean $leftColumn
     *
     * @return PsThemeMeta
     */
    public function setLeftColumn($leftColumn)
    {
        $this->leftColumn = $leftColumn;

        return $this;
    }

    /**
     * Get leftColumn
     *
     * @return boolean
     */
    public function getLeftColumn()
    {
        return $this->leftColumn;
    }

    /**
     * Set rightColumn
     *
     * @param boolean $rightColumn
     *
     * @return PsThemeMeta
     */
    public function setRightColumn($rightColumn)
    {
        $this->rightColumn = $rightColumn;

        return $this;
    }

    /**
     * Get rightColumn
     *
     * @return boolean
     */
    public function getRightColumn()
    {
        return $this->rightColumn;
    }
}
