<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderReturnDetail
 *
 * @ORM\Table(name="hd_order_return_detail")
 * @ORM\Entity
 */
class PsOrderReturnDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_return", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idOrderReturn;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_detail", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idOrderDetail;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_customization", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCustomization = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="product_quantity", type="integer", nullable=false)
     */
    private $productQuantity = '0';



    /**
     * Set idOrderReturn
     *
     * @param integer $idOrderReturn
     *
     * @return PsOrderReturnDetail
     */
    public function setIdOrderReturn($idOrderReturn)
    {
        $this->idOrderReturn = $idOrderReturn;

        return $this;
    }

    /**
     * Get idOrderReturn
     *
     * @return integer
     */
    public function getIdOrderReturn()
    {
        return $this->idOrderReturn;
    }

    /**
     * Set idOrderDetail
     *
     * @param integer $idOrderDetail
     *
     * @return PsOrderReturnDetail
     */
    public function setIdOrderDetail($idOrderDetail)
    {
        $this->idOrderDetail = $idOrderDetail;

        return $this;
    }

    /**
     * Get idOrderDetail
     *
     * @return integer
     */
    public function getIdOrderDetail()
    {
        return $this->idOrderDetail;
    }

    /**
     * Set idCustomization
     *
     * @param integer $idCustomization
     *
     * @return PsOrderReturnDetail
     */
    public function setIdCustomization($idCustomization)
    {
        $this->idCustomization = $idCustomization;

        return $this;
    }

    /**
     * Get idCustomization
     *
     * @return integer
     */
    public function getIdCustomization()
    {
        return $this->idCustomization;
    }

    /**
     * Set productQuantity
     *
     * @param integer $productQuantity
     *
     * @return PsOrderReturnDetail
     */
    public function setProductQuantity($productQuantity)
    {
        $this->productQuantity = $productQuantity;

        return $this;
    }

    /**
     * Get productQuantity
     *
     * @return integer
     */
    public function getProductQuantity()
    {
        return $this->productQuantity;
    }
}
