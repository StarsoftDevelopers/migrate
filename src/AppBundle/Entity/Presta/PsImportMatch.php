<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsImportMatch
 *
 * @ORM\Table(name="hd_import_match")
 * @ORM\Entity
 */
class PsImportMatch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_import_match", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idImportMatch;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="match", type="text", length=65535, nullable=false)
     */
    private $match;

    /**
     * @var integer
     *
     * @ORM\Column(name="skip", type="integer", nullable=false)
     */
    private $skip;



    /**
     * Get idImportMatch
     *
     * @return integer
     */
    public function getIdImportMatch()
    {
        return $this->idImportMatch;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsImportMatch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set match
     *
     * @param string $match
     *
     * @return PsImportMatch
     */
    public function setMatch($match)
    {
        $this->match = $match;

        return $this;
    }

    /**
     * Get match
     *
     * @return string
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * Set skip
     *
     * @param integer $skip
     *
     * @return PsImportMatch
     */
    public function setSkip($skip)
    {
        $this->skip = $skip;

        return $this;
    }

    /**
     * Get skip
     *
     * @return integer
     */
    public function getSkip()
    {
        return $this->skip;
    }
}
