<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsPageType
 *
 * @ORM\Table(name="hd_page_type", indexes={@ORM\Index(name="name", columns={"name"})})
 * @ORM\Entity
 */
class PsPageType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_page_type", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPageType;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;



    /**
     * Get idPageType
     *
     * @return integer
     */
    public function getIdPageType()
    {
        return $this->idPageType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsPageType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
