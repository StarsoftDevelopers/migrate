<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsOrderMessageLang
 *
 * @ORM\Table(name="hd_order_message_lang")
 * @ORM\Entity
 */
class PsOrderMessageLang
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_message", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idOrderMessage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idLang;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", length=65535, nullable=false)
     */
    private $message;



    /**
     * Set idOrderMessage
     *
     * @param integer $idOrderMessage
     *
     * @return PsOrderMessageLang
     */
    public function setIdOrderMessage($idOrderMessage)
    {
        $this->idOrderMessage = $idOrderMessage;

        return $this;
    }

    /**
     * Get idOrderMessage
     *
     * @return integer
     */
    public function getIdOrderMessage()
    {
        return $this->idOrderMessage;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsOrderMessageLang
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsOrderMessageLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return PsOrderMessageLang
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
