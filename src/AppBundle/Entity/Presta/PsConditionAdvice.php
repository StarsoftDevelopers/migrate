<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsConditionAdvice
 *
 * @ORM\Table(name="hd_condition_advice")
 * @ORM\Entity
 */
class PsConditionAdvice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_condition", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCondition;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_advice", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAdvice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display", type="boolean", nullable=false)
     */
    private $display = '0';



    /**
     * Set idCondition
     *
     * @param integer $idCondition
     *
     * @return PsConditionAdvice
     */
    public function setIdCondition($idCondition)
    {
        $this->idCondition = $idCondition;

        return $this;
    }

    /**
     * Get idCondition
     *
     * @return integer
     */
    public function getIdCondition()
    {
        return $this->idCondition;
    }

    /**
     * Set idAdvice
     *
     * @param integer $idAdvice
     *
     * @return PsConditionAdvice
     */
    public function setIdAdvice($idAdvice)
    {
        $this->idAdvice = $idAdvice;

        return $this;
    }

    /**
     * Get idAdvice
     *
     * @return integer
     */
    public function getIdAdvice()
    {
        return $this->idAdvice;
    }

    /**
     * Set display
     *
     * @param boolean $display
     *
     * @return PsConditionAdvice
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display
     *
     * @return boolean
     */
    public function getDisplay()
    {
        return $this->display;
    }
}
