<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsWishlistProductCart
 *
 * @ORM\Table(name="hd_wishlist_product_cart")
 * @ORM\Entity
 */
class PsWishlistProductCart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_wishlist_product", type="integer", nullable=false)
     */
    private $idWishlistProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cart", type="integer", nullable=false)
     */
    private $idCart;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idWishlistProduct
     *
     * @param integer $idWishlistProduct
     *
     * @return PsWishlistProductCart
     */
    public function setIdWishlistProduct($idWishlistProduct)
    {
        $this->idWishlistProduct = $idWishlistProduct;

        return $this;
    }

    /**
     * Get idWishlistProduct
     *
     * @return integer
     */
    public function getIdWishlistProduct()
    {
        return $this->idWishlistProduct;
    }

    /**
     * Set idCart
     *
     * @param integer $idCart
     *
     * @return PsWishlistProductCart
     */
    public function setIdCart($idCart)
    {
        $this->idCart = $idCart;

        return $this;
    }

    /**
     * Get idCart
     *
     * @return integer
     */
    public function getIdCart()
    {
        return $this->idCart;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PsWishlistProductCart
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsWishlistProductCart
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
}
