<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsTimezone
 *
 * @ORM\Table(name="hd_timezone")
 * @ORM\Entity
 */
class PsTimezone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_timezone", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTimezone;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     */
    private $name;



    /**
     * Get idTimezone
     *
     * @return integer
     */
    public function getIdTimezone()
    {
        return $this->idTimezone;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsTimezone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
