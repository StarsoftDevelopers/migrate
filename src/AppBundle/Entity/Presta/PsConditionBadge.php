<?php

namespace AppBundle\Entity\Presta;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsConditionBadge
 *
 * @ORM\Table(name="hd_condition_badge")
 * @ORM\Entity
 */
class PsConditionBadge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_condition", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCondition;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_badge", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idBadge;



    /**
     * Set idCondition
     *
     * @param integer $idCondition
     *
     * @return PsConditionBadge
     */
    public function setIdCondition($idCondition)
    {
        $this->idCondition = $idCondition;

        return $this;
    }

    /**
     * Get idCondition
     *
     * @return integer
     */
    public function getIdCondition()
    {
        return $this->idCondition;
    }

    /**
     * Set idBadge
     *
     * @param integer $idBadge
     *
     * @return PsConditionBadge
     */
    public function setIdBadge($idBadge)
    {
        $this->idBadge = $idBadge;

        return $this;
    }

    /**
     * Get idBadge
     *
     * @return integer
     */
    public function getIdBadge()
    {
        return $this->idBadge;
    }
}
