<?php

namespace AppBundle\Service;

use AppBundle\Entity\Customer\Categories;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Ddeboer\DataImport\Writer\CsvWriter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class Migrate
 * @package AppBundle\Service
 */
class Migrate
{
    /** @var  EntityManager $defaultEntityManager */
    protected $defaultEntityManager;

    /** @var  EntityManager $customerEntityManager */
    protected $customerEntityManager;

    /** @var  Connection */
    protected $connection;

    /** @var  string $rootDir */
    protected $rootDir;

    /** @var  Categories $categoriesList */
    protected $categoriesList;
    protected $categoriesMap;

    private $dbPrefix = 'hd_';

    /** @var  string $localImageDir */
    private $localImageDir;

    /**
     * Migrate constructor.
     * @param EntityManager $defaultEntityManager
     * @param EntityManager $customerEntityManager
     * @param Connection    $connection
     * @param string        $rootDir
     * @param string        $localImageDir
     */
    public function __construct(
        EntityManager $defaultEntityManager,
        EntityManager $customerEntityManager,
        Connection $connection,
        $rootDir,
        $localImageDir
    ) {
        $this->defaultEntityManager = $defaultEntityManager;
        $this->customerEntityManager = $customerEntityManager;
        $this->connection = $connection;
        $this->rootDir = $rootDir;
        $this->localImageDir = $localImageDir;
    }

    /**
     *
     * @param string $username
     * @return Response
     */
    public function getCategoriesCsv($username)
    {
        $oldCategoriesList = $this->customerEntityManager->getRepository("AppBundle:Customer\Categories")->getCategoriesData();
//        $activeCategoriesList = array_column($this->customerEntityManager->getRepository("AppBundle:Customer\ProductsToCategories")->getCategoriesData(), 'id');

        $response = new Response();
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$username.'_cat_export.csv');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');

        $csvWriter = new CsvWriter();
        $csvWriter->writeItem(['ID', 'Active', 'Name', 'Parent category', 'Root category', 'Description',
            'Meta title', 'Meta keywords', 'Meta description', 'URL rewritten', 'Image URL', 'ID / Name of shop',
        ]);

        foreach ($oldCategoriesList as $oldCategory) {
            if ($this->clearCategoryName($oldCategory['name'])) {
                if ($oldCategory['image']) {
                    if (!filter_var($oldCategory['image'], FILTER_VALIDATE_URL) === false) {
                        $categoryImageLink = $oldCategory['image'];
                    } else {
                        $categoryImageLink = trim("http://".$username.".hotdigital.org/images/".$oldCategory['image']);
                    }
                } else {
                    $categoryImageLink = '';
                }
                if ($oldCategory['id'] && $oldCategory['id'] == 1) {
                    $categoryId = (int) $oldCategory['id'] + 2;
                } else {
                    $categoryId = $oldCategory['id'];
                }
                $csvWriter->writeItem(
                    [
                        'ID' => $categoryId + 1,
                        'Active' => $oldCategory['status'] != '' ? $oldCategory['status'] : 0,
                        'Name' => $this->clearCategoryName($oldCategory['name']),
                        'Parent category' => $oldCategory['parent'] ? $this->clearCategoryName($oldCategory['parent']) : 2,
                        'Root category' => 0,
                        'Description' => $this->convertTo($oldCategory['categoriesHtml']),
                        'Meta title' => /*strip_tags(substr($oldCategory['categoriesHeadTitleTag'], 0, 128))*/'',
                        'Meta keywords' => /*strip_tags(substr($oldCategory['categoriesHeadKeywordsTag'], 0, 128))*/'',
                        'Meta description' => /*strip_tags(substr($oldCategory['categoriesHeadDescTag'], 0, 128))*/'',
                        'URL rewritten' => $oldCategory['name'],
                        'Image URL' => $categoryImageLink,
                        'ID / Name of shop' => 1,
                    ]
                );
            }
        }
        rewind($csvWriter->getStream());
        $response->setContent(stream_get_contents($csvWriter->getStream()));
        $csvWriter->finish();

        return $response;
    }

    /**
     *
     * @param string $username
     * @param array  $oldProductsList
     * @return Response
     */
    public function getProductsCsv($username, $oldProductsList = [])
    {
        if (!$oldProductsList) {
            $oldProductsList = $this->getProductsList();
        }
        $this->setCategories();

        $response = new Response();
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$username.'_prod_export.csv');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');

        $csvWriter = new CsvWriter();
        $csvWriter->writeItem(['ID', 'Active', 'Name', 'Categories(x,y,z...)', 'Price tax excluded or Price tax included',
            'Tax rules ID', 'Wholesale price',
            'On sale (0/1)', 'Discount amount', 'Discount percent', 'Discount from (yyyy-mm-dd)', 'Discount to (yyyy-mm-dd)',
            'Reference #', 'Supplier reference #', 'Supplier', 'Manufacturer', 'EAN13', 'UPC', 'Ecotax',
            'Width', 'Height', 'Depth', 'Weight', 'Quantity', 'Minimal quantity', 'Visibility', 'Additional shipping cost',
            'Unity', 'Unit price', 'Short description', 'Description', 'Tags (x,y,z...)', 'Meta title', 'Meta keywords',
            'Meta description', 'URL rewritten',
            'Text when in stock', 'Text when backorder allowed', 'Available for order (0 = No, 1 = Yes)',
            'Product available date', 'Product creation date',
            'Show price (0 = No, 1 = Yes)', 'Image URLs (x,y,z...)', 'Delete existing images (0 = No, 1 = Yes)',
            'Feature(Name:Value:Position)', 'Available online only (0 = No, 1 = Yes)',
            'Condition', 'Customizable (0 = No, 1 = Yes)', 'Uploadable files (0 = No, 1 = Yes)', 'Text fields (0 = No, 1 = Yes)',
            'Out of stock',
        ]);

        foreach ($oldProductsList as $oldProduct) {
            $urlRewritten = $this->getProductUrlRewrite($oldProduct['URL_rewritten']);
            $csvWriter->writeItem(
                [
                    'ID' => $oldProduct['products_id_0'],
                    'Active' => $oldProduct['active'] != '' ? $oldProduct['active'] : false,
                    'Name' => substr($this->clearCategoryName($this->convertTo($oldProduct['name'])), 0, 128),
                    'Categories(x|y|z...)' => $this->getCategoryPath($this->clearCategoryName($oldProduct['categoriesIds'])),
                    'Price tax excluded or Price tax included' => $oldProduct['Price_tax_excluded_or_Price_tax_included'],
                    'Tax rules ID' => $oldProduct['tax_rules_id_new'],
                    'Wholesale price' => $oldProduct['Wholesale_price'],
                    'On sale (0/1)' => (bool) $oldProduct['on_sale'],
                    'Discount amount' => $oldProduct['Discount_amount'],
                    'Discount percent' => $oldProduct['Discount_percent'],
                    'Discount from (yyyy-mm-dd)' => $oldProduct['Discount_from'],
                    'Discount to (yyyy-mm-dd)' => $oldProduct['Discount_to'],
                    'Reference #' => $oldProduct['products_id_0'],
                    'Supplier reference #' => '',
                    'Supplier' => $oldProduct['Supplier'],
                    'Manufacturer' => $oldProduct['Manufacturer'],
                    'EAN13' => $this->clearEanValue($oldProduct['EAN13'], 13),
                    'UPC' => $this->clearEanValue($oldProduct['UPC'], 12),
                    'Ecotax' => $oldProduct['Ecotax'],
                    'Width' => $oldProduct['Width'],
                    'Height' => $oldProduct['Height'],
                    'Depth' => $oldProduct['Depth'],
                    'Weight' => $oldProduct['Weight'],
                    'Quantity' => $oldProduct['Quantity'],
                    'Minimal quantity' => 1,
                    'Visibility' => 'both',
                    'Additional shipping cost' => $this->getValueWithOutTax($oldProduct['Additional_shipping_cost'], $oldProduct['tax_value_percent'], 1),
                    'Unity' => $oldProduct['Unit_for_the_unit_price'] && $oldProduct['Unit_for_the_unit_price'] > 0 ? ' 1m' :'',
                    'Unit price' => $oldProduct['Unit_for_the_unit_price'] && $oldProduct['Unit_for_the_unit_price'] > 0 ? ((1 / $oldProduct['Unit_for_the_unit_price']) * $oldProduct['Unit_price']) : '',
                    'Short description' => 'Voraussichtliche Versandkosten '.$oldProduct['Additional_shipping_cost'].' €',
                    'Description' => $this->convertTo($oldProduct['Description']),
                    'Tags (x,y,z...)' => $this->convertTo($oldProduct['Tags']),
                    'Meta title' => $this->convertTo($oldProduct['Meta_title']),
                    'Meta keywords' => $this->convertTo($oldProduct['Meta_keywords']),
                    'Meta description' => $this->convertTo($oldProduct['Meta_description']),
                    'URL rewritten' => $urlRewritten,
                    'Text when in stock' => $oldProduct['Text_when_in_stock'],
                    'Text when backorder allowed' => $oldProduct['Text_when_backorder_allowed'],
                    'Available for order (0 = No, 1 = Yes)' => /*$oldProduct['Available_for_order']*/1,
                    'Product available date' => $oldProduct['Product_availability_date'],
                    'Product creation date' => $oldProduct['Product_creation_date'],
                    'Show price (0 = No, 1 = Yes)' => (bool) $oldProduct['Show_price'],
                    'Image URLs (x,y,z...)' => $this->getAllImagesForProduct($oldProduct, $username),
                    'Delete existing images (0 = No, 1 = Yes)' => (bool) $oldProduct['Delete_existing_images'],
                    'Feature(Name:Value:Position)' => $oldProduct['Feature'],
                    'Available online only (0 = No, 1 = Yes)' => (bool) $oldProduct['Available_online_only'],
                    'Condition' => in_array($oldProduct['Condition_1'], ['new', 'used', 'refurbished'])?$oldProduct['Condition_1']:'new',
                    'Customizable (0 = No, 1 = Yes)' => (bool) $oldProduct['Customizable'],
                    'Uploadable files (0 = No, 1 = Yes)' => (bool) $oldProduct['Uploadable_files'],
                    'Text fields (0 = No, 1 = Yes)' => (bool) $oldProduct['Text_fields'],
                    'Out of stock' => $oldProduct['Action_when_out_of_stock'],
                ]
            );
        }
        rewind($csvWriter->getStream());
        $response->setContent(stream_get_contents($csvWriter->getStream()));
        $csvWriter->finish();

        return $response;
    }


    /**
     * @return array
     */
    public function getProductsList()
    {
        $langIds = $this->getLangIdFromPresta();
        $de = $langIds['de'];
        $select = ("
            SELECT p.*, p.products_id AS products_id_0,
             p.products_status AS active,
             pd.products_name AS name,
             GROUP_CONCAT(DISTINCT cd.categories_name SEPARATOR '|') AS categories,
             GROUP_CONCAT(DISTINCT cd.categories_id SEPARATOR ',') AS categoriesIds,
             p.products_price as Price_tax_excluded_or_Price_tax_included,
             p.products_tax_class_id as tax_rule_id,
             p.products_ek_price AS Wholesale_price,
             0 AS on_sale,
             0 AS Discount_amount,
             0 AS Discount_percent,
             NULL as Discount_from,
             NULL AS Discount_to,
             p.products_model_own as Reference,
             p.products_model as Reference1,
             1 AS Supplier_reference,
             1 AS Supplier,
             pm.manufacturers_name as Manufacturer,
             p.products_ean AS EAN13,
             p.products_upc as UPC,
             0 as Ecotax,
             0 AS Width,
             0 AS Height,
             0 AS Depth,
             p.products_weight AS Weight,
             p.products_quantity AS Quantity,
             p.products_quantity_min AS Minimal_quantity,
             p.products_availability AS Visibility,
             p.products_shipping AS Additional_shipping_cost,
             p.products_price_ratio AS Unit_for_the_unit_price,
             p.products_price AS Unit_price,
             pd.products_head_title_tag AS Short_description,
             pd.products_description AS Description,
             pd.products_head_keywords_tag AS Tags,
             NULL AS Meta_title,
             pd.products_head_keywords_tag AS Meta_keywords,
             NULL AS Meta_description,
             pd.products_name AS URL_rewritten,
             NULL AS Text_when_in_stock,
             NULL AS Text_when_backorder_allowed,
             p.products_availability AS Available_for_order,
             NULL AS Product_availability_date,
             p.products_date_added AS Product_creation_date,
             p.products_price_display AS Show_price,
             p.products_image AS Image_URLs,
             0 AS Delete_existing_images,
             NULL AS Feature,
             0 as Available_online_only,
             pcon.condition_title as Condition_1,
             0 AS Customizable,
             0 AS Uploadable_files,
             0 AS Text_fields,
             0 AS Action_when_out_of_stock,
             ptc.categories_id AS product_category,
             pd_en.products_name AS name_en,
             pd_en.products_head_title_tag AS products_head_title_tag_en,
             pd_en.products_description AS products_description_en,
             pd_en.products_head_keywords_tag AS products_head_keywords_tag_en,
             pd_en.products_head_keywords_tag AS products_head_keywords_tag_en,
             pd_en.products_name AS URL_rewritten_en,
             pd_en.language_id AS lang_id_en,
             CASE WHEN tr.tax_rate LIKE '%19%' THEN 1 WHEN tr.tax_rate LIKE '%7%' THEN 2 ELSE 0 END AS tax_rules_id_new,
             tr.tax_rate as tax_value_percent
            FROM products AS p
             LEFT JOIN products_description AS pd ON (pd.products_id = p.products_id AND pd.language_id = 2)
             LEFT JOIN products_description AS pd_en ON (pd_en.products_id = p.products_id AND pd_en.language_id = 3)
             LEFT JOIN products_to_categories AS ptc ON (ptc.products_id = p.products_id)
             LEFT JOIN categories_description AS cd ON (cd.categories_id = ptc.categories_id AND cd.language_id = 2)
             LEFT JOIN manufacturers as pm ON p.manufacturers_id = pm.manufacturers_id
             LEFT JOIN products_conditions AS pcon ON p.products_condition = pcon.condition_id
             LEFT JOIN tax_class as tc ON p.products_tax_class_id = tc.tax_class_id
             LEFT JOIN tax_rates AS tr ON tc.tax_class_id = tr.tax_class_id
             WHERE
              /*(p.products_id = '1252'
              OR p.products_id = '1605'
              OR p.products_id = '1820'
              OR p.products_id = '2299'
              OR p.products_id = '2300'
              OR p.products_id = '2301'
              OR p.products_id = '2352'
              OR p.products_id = '2353'
              OR p.products_id = '2364'
              OR p.products_id = '2365'
              OR p.products_id = '2366'
              OR p.products_id = '2367'
              OR p.products_id = '2368'
              OR p.products_id = '2369'
              OR p.products_id = '2370'
              OR p.products_id = '2371'
              OR p.products_id = '2372'
              )
              AND*/
             pd.language_id = $de
             GROUP BY p.products_id
             LIMIT 1000 OFFSET 0
        ;");

        return $this->customerEntityManager->getConnection()->executeQuery($select)->fetchAll();
    }

    /**
     * @param string $username
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getManufacturersCsv($username)
    {
        $langIds = $this->getLangIdFromPresta();
        $de = $langIds['de'];
        $sql = ("
            SELECT m.*, mi.*
            FROM manufacturers AS m
            LEFT JOIN manufacturers_info AS mi ON m.manufacturers_id = mi.manufacturers_id
            WHERE mi.languages_id = $de
        ");

        $manufacturersList = $this->customerEntityManager->getConnection()->executeQuery($sql)->fetchAll();

        $response = new Response();
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$username.'_manufacturer_export.csv');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');

        $csvWriter = new CsvWriter();
        $csvWriter->writeItem([
            'ID', 'Active (0/1)', 'Name', 'Description', 'Short description', 'Meta title', 'Meta keywords',
            'Meta description', 'Image URL', 'ID / Name of group shop',
        ]);

        foreach ($manufacturersList as $manufacturer) {
            if (isset($manufacturer['manufacturers_name'])) {
                $csvWriter->writeItem([
                    'ID' => $manufacturer['manufacturers_id'],
                    'Active (0/1)' => 1,
                    'Name' => $this->convertTo($manufacturer['manufacturers_name']),
                    'Description' => $this->convertTo($manufacturer['manufacturers_name']),
                    'Short description' => '',
                    'Meta title' => '',
                    'Meta keywords' => '',
                    'Meta description' => '',
                    'Image URL' => $this->getImageUrl($manufacturer['manufacturers_image'], $username),
                    'ID / Name of group shop' => 1,
                ]);
            }
        }

        rewind($csvWriter->getStream());
        $response->setContent(stream_get_contents($csvWriter->getStream()));
        $csvWriter->finish();

        return $response;

    }

    /**
     * @param string $username
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getCombinationsCsv($username)
    {
        $langDe = (int) $this->getLangIdFromPresta()['de'];
        $sql = ("
            SELECT
            pmo.*,
            p.products_id AS productId,
            p.products_ean AS EAN13,
            p.products_upc AS UPC,
            0 AS Wholesale_price,
            0 as Impact_on_price,
            0 AS Ecotax,
            p.products_quantity AS Quantity,
            p.products_quantity_min AS Minimal_quantity,
            0 AS Impact_on_weight,
            0 AS Default_1,
            po.*,
            pov.*
            FROM products_master_options AS pmo
            LEFT JOIN products AS p ON pmo.products_id = p.products_id
            LEFT JOIN products_options AS po ON pmo.products_options_id = po.products_options_id
            LEFT JOIN products_options_values AS pov ON pmo.products_options_values_id = pov.products_options_values_id
            WHERE po.language_id = $langDe
        ");

        $combinationsList = $this->customerEntityManager->getConnection()->executeQuery($sql)->fetchAll();

        $response = new Response();
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$username.'_combination_export.csv');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');

        $csvWriter = new CsvWriter();
        $csvWriter->writeItem([
            'Product ID', 'Product Reference', 'Attribute (Name:Type:Position)', 'Value (Value:Position)',
            'Supplier reference', 'Reference', 'EAN13', 'UPC', 'Wholesale price', 'Impact on price', 'Ecotax',
            'Quantity', 'Minimal quantity',
            'Impact on weight', 'Default (0 = No, 1 = Yes)', 'Combination availability date',
            'Choose among product images by position', 'Image URLs', 'Delete existing images',
            'ID / Name of shop', 'Advanced Stock Management', 'Depends on stock', 'Warehouse',
        ]);

        foreach ($combinationsList as $combination) {
            $csvWriter->writeItem([
                'Product ID' => $combination['products_id'],
                'Product Reference' => $combination['productId'],
                'Attribute (Name:Type:Position)' =>
                    $combination['products_options_name'].':'.strtolower($combination['products_options_name']).':0',
                'Value (Value:Position)' => $combination['products_options_values_name'].':0',
                'Supplier reference' => '',
                'Reference' => '',
                'EAN13' => strlen($combination['EAN13']) < 14 ? $combination['EAN13'] : '',
                'UPC' => $combination['UPC'],
                'Wholesale price' => '',
                'Impact on price' => '',
                'Ecotax' => 0,
                'Quantity' => $combination['Quantity'],
                'Minimal quantity' => 1,
                'Impact on weight' => 0,
                'Default (0 = No, 1 = Yes)' => 0,
                'Combination availability date' => '01.01.2016',
                'Choose among product images by position' => '',
                'Image URLs' => '',
                'Delete existing images' => 0,
                'ID / Name of shop' => '',
                'Advanced Stock Management' => '',
                'Depends on stock' => '',
                'Warehouse' => '',
            ]);
        }

        rewind($csvWriter->getStream());
        $response->setContent(stream_get_contents($csvWriter->getStream()));
        $csvWriter->finish();

        return $response;

    }

    /**
     * @param string $username
     * @return Response
     */
    public function getCustomersCsv($username)
    {
        $sql = ("SELECT c.* FROM customers AS c");
        $customersList = $this->customerEntityManager->getConnection()->executeQuery($sql)->fetchAll();

        $response = new Response();
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$username.'_users_export.csv');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');

        $csvWriter = new CsvWriter();
        $csvWriter->writeItem([
            'ID', 'Active (0/1)', 'Titles ID', 'Email', 'Password', 'Birthday', 'Last Name', 'First Name',
            'Newsletter', 'Opt-in', 'Groups', 'Default group ID',
        ]);

        foreach ($customersList as $customer) {
            if ($customer['customers_gender'] == 'm') {
                $gender = 1;
            } elseif ($customer['customers_gender'] == 'f') {
                $gender = 2;
            } else {
                $gender = 0;
            }
            $csvWriter->writeItem([
                'ID' => $customer['customers_id'],
                'Active (0/1)' => 1,
                'Titles ID' => $gender,
                'Email' => $customer['customers_email_address'],
                'Password' => 'newpass',
                'Birthday' => '0000-00-00',
                'Last Name' => $customer['customers_lastname'],
                'First Name' => $customer['customers_firstname'],
                'Newsletter' => $customer['customers_newsletter'] ? $customer['customers_newsletter'] : 0,
                'Opt-in' => 1,
                'Groups' => 'Customer',
                'Default group ID' => 'Default group ID',
            ]);
        }

        rewind($csvWriter->getStream());
        $response->setContent(stream_get_contents($csvWriter->getStream()));
        $csvWriter->finish();

        return $response;
    }

    /**
     * @param string $username
     * @return Response
     */
    public function getAddressCsv($username)
    {
        $sql = ("
          SELECT
          c.*,
          ab.*,
          cn.countries_name countryName
          FROM customers AS c
          LEFT JOIN address_book AS ab ON c.customers_id = ab.customers_id
          LEFT JOIN countries AS cn ON (ab.entry_country_id = cn.countries_id)
        ");
        $addressList = $this->customerEntityManager->getConnection()->executeQuery($sql)->fetchAll();

        $response = new Response();
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$username.'_address_export.csv');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');

        $csvWriter = new CsvWriter();
        $csvWriter->writeItem([
            'ID', 'Alias', 'Active (0/1)', 'Customer email', 'Customer ID', 'Manufacturer', 'Supplier', 'Company',
            'Last Name', 'First Name', 'Address 1', 'Address 2', 'Zip/postal code',
            'City', 'Country', 'State', 'Other', 'Phone', 'Mobile Phone', 'VAT number', 'DNI/NIF/NIE',
        ]);

        foreach ($addressList as $address) {
            $csvWriter->writeItem([
                'ID' => $address['address_book_id'],
                'Alias' => $address['entry_firstname'].'.'.$address['entry_lastname'].'/'.$address['entry_city'],
                'Active (0/1)' => 1,
                'Customer email' => $address['customers_email_address'],
                'Customer ID' => $address['customers_id'],
                'Manufacturer' => '',
                'Supplier' => '',
                'Company' => $address['entry_company'],
                'Last Name' => $address['entry_firstname'],
                'First Name' => $address['entry_lastname'],
                'Address 1' => $address['entry_street_address'],
                'Address 2' => '',
                'Zip/postal code' => $address['entry_postcode'],
                'City' => $address['entry_city'],
                'Country' => $address['countryName'],
                'State' => $address['entry_state'],
                'Other' => '',
                'Phone' => '',
                'Mobile Phone' => $this->formatPhone($address['customers_telephone']),
                'VAT number' => '',
                'DNI/NIF/NIE' => '',
            ]);
        }

        rewind($csvWriter->getStream());
        $response->setContent(stream_get_contents($csvWriter->getStream()));
        $csvWriter->finish();

        return $response;
    }

    /**
     * @param string $username
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getCombinationCsv($username)
    {
        $langDe = (int) $this->getLangIdFromPresta()['de'];

        $response = new Response();
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$username.'_combination_export.csv');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');

        $csvWriter = new CsvWriter();
        $csvWriter->writeItem([
            'Product ID', 'Product Reference', 'Attribute (Name:Type:Position)', 'Value (Value:Position)',
            'Supplier reference', 'Reference', 'EAN13', 'UPC', 'Wholesale price', 'Impact on price', 'Ecotax',
            'Quantity', 'Minimal quantity',
            'Impact on weight', 'Default (0 = No, 1 = Yes)', 'Combination availability date',
            'Choose among product images by position', 'Image URLs', 'Delete existing images',
            'ID / Name of shop', 'Advanced Stock Management', 'Depends on stock', 'Warehouse',
        ]);

        $resource = $this->getProductsResources();
        while ($product = $resource->fetch()) {
            $productVariants = $this->getProductVariants($product['products_id'], $langDe);
            foreach ($productVariants as $combination) {
                $csvWriter->writeItem([
                    'Product ID' => $product['products_id'],
                    'Product Reference' => $product['products_id'],
                    'Attribute (Name:Type:Position)' => implode('|', array_column($combination, 'products_options_name')),
                    'Value (Value:Position)' => implode('|', array_column($combination, 'products_options_values_name')),
                    'Supplier reference' => '',
                    'Reference' => '',
                    'EAN13' => strlen($product['EAN13']) < 14 ? $product['EAN13'] : '',
                    'UPC' => $product['UPC'],
                    'Wholesale price' => '',
                    'Impact on price' => array_sum(array_column($combination, 'options_values_price')),
                    'Ecotax' => 0,
                    'Quantity' => $product['Quantity'],
                    'Minimal quantity' => 1,
                    'Impact on weight' => 0,
                    'Default (0 = No, 1 = Yes)' => 0,
                    'Combination availability date' => '01.01.2016',
                    'Choose among product images by position' => '',
                    'Image URLs' => '',
                    'Delete existing images' => 0,
                    'ID / Name of shop' => '',
                    'Advanced Stock Management' => '',
                    'Depends on stock' => '',
                    'Warehouse' => '',
                ]);
            }
        }

        rewind($csvWriter->getStream());
        $response->setContent(stream_get_contents($csvWriter->getStream()));
        $csvWriter->finish();

        return $response;

    }

    /**
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getProductsResources()
    {
        $query = "SELECT p.products_id,
                    0 AS Wholesale_price,
                    0 as Impact_on_price,
                    0 AS Ecotax,
                    p.products_quantity AS Quantity,
                    p.products_quantity_min AS Minimal_quantity,
                    p.products_ean AS EAN13,
                    p.products_upc AS UPC

                  FROM products p";

        return $this->customerEntityManager->getConnection()->executeQuery($query);
    }

    /**
     * @param integer $productId
     * @param integer $langId
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getProductVariants($productId, $langId)
    {
        $productVariants = array();

        $query = "SELECT DISTINCT popt.*, popt.*, psp.*
                   FROM products_options popt
                   LEFT JOIN products_attributes patrib ON (patrib.options_id = popt.products_options_id )
                   LEFT JOIN products_options_sort psp  ON (psp.products_id = $productId and
                                                            psp.options_id = popt.products_options_id )
                   WHERE patrib.products_id = $productId AND
                           popt.language_id = $langId
                   ORDER BY psp.sort_order, popt.products_options_name";
        $query = "SELECT pa.options_id, po.products_options_name, pov.products_options_values_id,
                    pov.products_options_values_name, pa.options_values_price, pa.price_prefix

                FROM products_attributes AS pa
                LEFT JOIN products_options AS po ON (po.products_options_id = pa.options_id
                                                            AND po.language_id = $langId)
                LEFT JOIN products_options_values AS pov ON (
                                                            pov.products_options_values_id = pa.options_values_id
                                                            AND pov.language_id = $langId)
                LEFT JOIN products_options_sort psp  ON (psp.products_id = $productId and
                                                            psp.options_id = po.products_options_id )
                WHERE pa.products_id = $productId
                ORDER BY psp.sort_order, pa.options_values_price, pov.products_options_values_name";

        $resource = $this->customerEntityManager->getConnection()->executeQuery($query);
        while ($options = $resource->fetch()) {
            $productVariants[$options['options_id']][] = $options;
        }

        if (count($productVariants)) {
            $productVariants = $this->arrayCombinations($productVariants);
        }

        return $productVariants;
    }

    /**
     * @param array $array
     * @param bool  $firstTime
     * @return array|mixed
     */
    public function arrayCombinations($array, $firstTime = true)
    {
        $response = array();
        if (count($array) <= 1) {
            if ($firstTime && count($array)) {
                $key = key($array);
                foreach (current($array) as $value) {
                    $response[] = array($key => $value);
                }

                return $response;
            } else {
                return current($array);
            }
        }

        $keys = array_keys($array);
        $currentKey = array_shift($keys);
        $currentArrays = array_shift($array);
        $nextArrays = $this->arrayCombinations($array, false);
        foreach ($currentArrays as $currentArray) {
            foreach ($nextArrays as $nextArray) {
                if ($firstTime) {
                    (count($keys) == 1) && $nextArray = array($nextArray);
                    $response[] = array($currentKey => $currentArray) + array_combine($keys, $nextArray);
                } else {
                    !is_array(current($nextArray)) && $nextArray = array($nextArray);
                    array_unshift($nextArray, $currentArray);
                    $response[] = $nextArray;
                }
            }
        }

        return $response;
    }

    /**
     * @param string $customer
     */
    public function changeDb($customer)
    {
        if ($this->connection->isConnected()) {
            $this->connection->close();
        }

        $params = $this->connection->getParams();
        $params['dbname'] = 'db_'.$customer;

        $this->connection->__construct(
            $params,
            $this->connection->getDriver(),
            $this->connection->getConfiguration(),
            $this->connection->getEventManager()
        );

        try {
            $this->connection->connect();
            $this->connection->getParams();
        } catch (\Exception $e) {
            throw new BadRequestHttpException(sprintf("Couldn't connect to database: %s.", $e->getMessage()));
        }
    }

    /**
     * @param string $username
     * @return Response
     */
    public function synchronizeProductsData($username)
    {
        $productsList = $this->getProductsList();
        $array = [];
        foreach ($productsList as $product) {
            $sql = ("SELECT * FROM ".$this->dbPrefix."product where reference = ".$product['products_id_0']);
            $tmp = $this->defaultEntityManager->getConnection()->executeQuery($sql)->fetchAll();
            if (!$tmp) {
                $array[] = $product;
            }
        }

        return $this->getProductsCsv($username, $array);
    }

    private function getLangIdFromPresta()
    {
        $sql = ("SELECT id_lang as id, iso_code FROM ".$this->dbPrefix."lang where iso_code = 'en' or iso_code = 'de'");

        $array = $this->defaultEntityManager->getConnection()->executeQuery($sql)->fetchAll();

        $l = [];
        foreach ($array as $lang) {
            $l[$lang['iso_code']] = $lang['id'];
        }

        return $l;
    }

    private function setSeoFromPresta()
    {
        $sql = ("SELECT name FROM ".$this->dbPrefix."product_lang");

        $array = $this->defaultEntityManager->getConnection()->executeQuery($sql)->fetchAll();

        foreach ($array as $item) {
            $name = $item['name'];

            $update = ("INSERT INTO ".$this->dbPrefix."product_lang(meta_title) VALUES '$name'");

            $arr = $this->defaultEntityManager->getConnection()->executeQuery($update)->fetchAll();
        }
    }

    /**
     * @param $imageUrl
     * @param $username
     * @return string
     */
    private function getImageUrl($imageUrl, $username)
    {
        if (!filter_var($imageUrl, FILTER_VALIDATE_URL) === false) {
            $url = urldecode(trim($imageUrl));
            $parcedUrl = parse_url($url);

            $parcedUrl['path'] = str_replace('==', '', $parcedUrl['path']);
            $parcedUrl['path'] = str_replace('=', '', $parcedUrl['path']);
            $parcedUrl['path'] = str_replace(',', '', $parcedUrl['path']);

            $imageUrl = $parcedUrl['scheme'].'://'.$parcedUrl['host'].$parcedUrl['path'];
        } else {
            $imageUrl = "http://".$username.".hotdigital.org/images/".$imageUrl;
        }

        return $imageUrl;
    }

    private function convertTo($value)
    {
        if (strpos($value, 'script>') !== false) {
            $value = preg_replace('/<script[\s\S]+?script>/', '', $value);
        }
        if (strpos($value, 'iframe>') !== false) {
            $value = preg_replace('/<iframe[\s\S]+?iframe>/', '', $value);
        }
        $isUTF8 = @iconv_strlen($value, 'UTF-8');
        if (!$isUTF8) {
            $isISO8859I15 = @iconv_strlen($value, 'ISO-8859-15');
            $isISO8859I2 = @iconv_strlen($value, 'ISO-8859-2');
            $isISO = @iconv_strlen($value, 'ISO-8859-1');
            if ($isISO8859I15 !== false) {
                $value = iconv('ISO-8859-15', 'UTF-8', $value);
            } elseif ($isISO8859I2 !== false) {
                $value = iconv('ISO-8859-2', 'UTF-8', $value);
            } else {
                if ($isISO !== false) {
                    $value = iconv('ISO-8859-1', 'UTF-8', $value);
                }
            }
        }
        if (!mb_check_encoding($value, 'UTF-8')) {
            $value = mb_convert_encoding($value, 'UTF-8');
        }

        return $value;
    }

    private function formatPhone($number)
    {
        $number = str_replace('/', '', $number);
        $number = str_replace(' ', '', $number);

        return $number;
    }

    private function clearCategoryName($value)
    {
//        $value = str_replace('+', ' ', $value);
//        $value = str_replace('=', ' ', $value);
//        $value = str_replace('#', ' ', $value);
//        $value = str_replace('-', ' ', $value);
//        $value = str_replace('.', ' ', $value);
        $value = str_replace('?', ' ', $value);
        $value = str_replace('°', ' ', $value);
        $value = str_replace('®', ' ', $value);
        $value = str_replace('"', ' ', $value);
        $value = str_replace('*', ' ', $value);
        $value = str_replace(';', ' ', $value);
        $value = str_replace('Ø', ' ', $value);

        return $value;
    }

    private function clearEanValue($value, $length)
    {
        if (strlen($value) > $length) {
            $value = substr($value, 0, $length);
        }
        if (!is_numeric($value)) {
            return '';
        }

        return $value;
    }

    private function getParentCategoryId($parentId)
    {
        if ($parentId == 1 || $parentId == 0 || is_null($parentId)) {
            $parentId = 2;
        }

        return $parentId;
    }

    private function getCategoryUrlRewrite($url)
    {
        $url = str_replace(' ', '-', strtolower($url));
        $url = str_replace('&', '-', $url);
        $url = str_replace('+', '-', $url);

        return $url;
    }

    private function getProductUrlRewrite($value)
    {
        $value = str_replace(',', '', $value);
        $value = str_replace('.', '', $value);
        $value = str_replace('/', '', $value);
        $value = str_replace(' ', '-', strtolower($value));
        $value = substr($value, 0, 125);

        return $value;
    }

    private function getCategoryPath($ids)
    {
        $categoryTree = array();

        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $categoryPath = '';
            if (isset($this->categoriesMap[$id])) {
                $categoryTree[] = $this->categoriesMap[$id];
            } elseif ($category = $this->getCategory(intval($id))) {
                $depth = 21; // Limit maximum 20 parent category of an product

                while ($category && $depth--) {
                    if ($categoryPath) {
                        $categoryPath = $this->clearCategoryName($category['name']).'|'.$categoryPath;
                    } else {
                        $categoryPath = $this->clearCategoryName($category['name']);
                    }
                    $category = $this->getCategory(intval($category['parentId']));
                }
                $categoryTree[] = $this->categoriesMap[$id] = $categoryPath;
            }
        }

        return implode('##', $categoryTree);
    }

    private function setCategories()
    {
        $this->categoriesList = $this->customerEntityManager->getRepository("AppBundle:Customer\Categories")->getCategoriesData();
    }

    /**
     * @return array
     */
    private function getCategories()
    {
        return $this->categoriesList;
    }

    private function getCategory($id)
    {
        $category = array_filter($this->getCategories(), function ($element) use ($id) {
            return $element['id'] === $id;
        });

        return current($category);
    }

    private function getAllImagesForProduct($oldProduct, $username)
    {
        $urlsImage = [];
        if (isset($oldProduct['products_image']) && $oldProduct['products_image']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image'], $username);
        }
       /* if (isset($oldProduct['products_image_origin']) && $oldProduct['products_image_origin']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_origin'], $username);
        }*/
       /* if (isset($oldProduct['products_image_med']) && $oldProduct['products_image_med']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_med'], $username);
        }*/
        if (isset($oldProduct['products_image_lrg']) && $oldProduct['products_image_lrg']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_lrg'], $username);
        }
       /* if (isset($oldProduct['products_image_sm_1']) && $oldProduct['products_image_sm_1']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_sm_1'], $username);
        }
        if (isset($oldProduct['products_image_xl_1']) && $oldProduct['products_image_xl_1']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_xl_1'], $username);
        }
       /* if (isset($oldProduct['products_image_sm_2']) && $oldProduct['products_image_sm_2']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_sm_2'], $username);
        }*/
        if (isset($oldProduct['products_image_xl_2']) && $oldProduct['products_image_xl_2']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_xl_2'], $username);
        }
        /*if (isset($oldProduct['products_image_sm_3']) && $oldProduct['products_image_sm_3']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_sm_3'], $username);
        }*/
        if (isset($oldProduct['products_image_xl_3']) && $oldProduct['products_image_xl_3']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_xl_3'], $username);
        }
        /*if (isset($oldProduct['products_image_sm_4']) && $oldProduct['products_image_sm_4']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_sm_4'], $username);
        }*/
        if (isset($oldProduct['products_image_xl_4']) && $oldProduct['products_image_xl_4']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_xl_4'], $username);
        }
        /*if (isset($oldProduct['products_image_sm_5']) && $oldProduct['products_image_sm_5']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_sm_5'], $username);
        }*/
        if (isset($oldProduct['products_image_xl_5']) && $oldProduct['products_image_xl_5']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_xl_5'], $username);
        }
        /*if (isset($oldProduct['products_image_sm_6']) && $oldProduct['products_image_sm_6']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_sm_6'], $username);
        }*/
        if (isset($oldProduct['products_image_xl_6']) && $oldProduct['products_image_xl_6']) {
            $urlsImage[] = $this->getImageUrl($oldProduct['products_image_xl_6'], $username);
        }

        if (!$urlsImage) {
            echo '';
        }

        return implode('##', $urlsImage);
    }

    private function getValueWithOutTax($productPrice, $taxValuePercent, $excludeTax)
    {
        if (!$excludeTax) {
            return $productPrice;
        }
        /*switch (intval($taxValuePercent)) {
            case 19:
                $return = $productPrice - ($productPrice * 16 / 100);
                break;
            case 7:
                $return = $productPrice - ($productPrice * 6.53 / 100);
                break;
            default:
                $return = $productPrice;
        }*/

        return $productPrice - ($productPrice * 16 / 100);
    }
}
